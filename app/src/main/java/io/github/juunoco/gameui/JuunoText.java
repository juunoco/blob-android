package io.github.juunoco.gameui;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import io.github.juunoco.blobmobile.Globals;

/**
 * Created by Donna on 23/09/2017.
 */

public class JuunoText{

    private float x, y;
    private String message;
    private Paint paint;
    private int drawAlpha;
    private boolean bFadeOut;

    public void draw(Canvas c){
        c.drawText(message, x, y, paint);
    }

    public static JuunoText create(float x, float y, String m, int size){
        return new JuunoText(x, y, m, size);
    }

    public void update(){
        if(bFadeOut){
           if(drawAlpha > 0 + 50) drawAlpha -= 50;
           else drawAlpha = 0;
        }
        paint.setAlpha(drawAlpha);
    }

    public void initFadeOut(){
        bFadeOut = true;
    }

    public void reset(){
        drawAlpha = 255;
        bFadeOut = false;
    }

    public void setMessage(String m){
        message = m;
    }

    private JuunoText(float x, float y, String m, int textSize) {
        this.x = x * Globals.CELL_WIDTH;
        this.y = (y * Globals.CELL_WIDTH);
        message = m;
        paint = new Paint();
        paint.setTypeface(Globals.getOrbitronFont());
        paint.setColor(Color.DKGRAY);
//        paint.setTextAlign(Paint.Align.LEFT);
        paint.setTextSize(textSize);
        this.drawAlpha = 255;
        bFadeOut = false;
    }
}

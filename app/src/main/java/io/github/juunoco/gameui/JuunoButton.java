package io.github.juunoco.gameui;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.RectF;
import android.util.Log;
import android.view.MotionEvent;

import io.github.juunoco.blobmobile.ColourScheme;

/**
 * Created by Donna on 23/09/2017.
 */

public class JuunoButton extends GridObject{

    boolean enabled;
    private boolean bFadeOut;
    private int drawAlpha;

    public static JuunoButton create(float startX, float startY, float w, float h, String name){
        return new JuunoButton(startX,startY,w,h,name);
    }

    /**
     *
     * constructor taking in grid position
     * @param startX
     * @param startY
     * @param w
     * @param h
     * @param m
     */
    private JuunoButton(float startX, float startY, float w, float h, String m) {
        super(startX, startY, w, h, m);
        setEnabled(true);
        bFadeOut = false;
        drawAlpha = 255;
    }

    @Override
    public void draw(Canvas c){
        paint.setColor(ColourScheme.getColour(0));
        RectF rect = new RectF();
        rect.set(super.getX(),super.getY(),super.getWidth(),super.getHeight());
        c.drawRoundRect(rect,20,20,paint);
        c.drawText(message,
                (super.getX()+ super.getWidth())/2,
                (super.getY()+ super.getHeight())/2 + 20,textPaint);
    }

    public void draw(Bitmap b,Canvas c){
        //paint.setColor(ColourScheme.getColour(0));
        RectF rect = new RectF();
        rect.set(super.getX(),super.getY(),super.getWidth(),super.getHeight());
        paint.setAlpha(drawAlpha);
        c.drawBitmap(b,null,rect,paint); //paint can be null

//        if(bFadeOut) { //not causing fade out as expected, may be fixed by not using bitmap and using programmatic buttons?
//            //draw background color rectangle to cover the button with inverse alpha
//            paint.setAlpha(drawAlpha);
//            paint.setColor(Color.WHITE);
//            c.drawRect(rect, paint);
//        }
    }

    public void update(){
        if(bFadeOut){
//           if(drawAlpha < 255 - 50) drawAlpha+=50;
//           else drawAlpha = 255;
            if(drawAlpha > 0 + 50) drawAlpha -= 50;
            else drawAlpha = 0;
        }
    }

    public void initFadeout(){
        bFadeOut = true;
    }

    public void reset(){
        bFadeOut = false;
        drawAlpha = 255;
    }

    public boolean isClicked(MotionEvent e){
        if(e.getAction() == MotionEvent.ACTION_UP){
            if(e.getX() > getX() && e.getX() < getWidth() &&
                    e.getY() > getY() && e.getY() < getHeight()){
                Log.w("CLICKED",message);
                return true;
            }
        }
        Log.w("", "RETURNING CLICK COORDINATES NOT MET");
        return false;
    }

    public boolean isEnabled(){
        return enabled;
    }

    public void setEnabled(boolean value){
        enabled = value;
    }
    public void setFaded(boolean b){
        if(b) {
            drawAlpha = 10;
        }
        else {
            drawAlpha = 255;
        }
    }
}

package io.github.juunoco.gameui;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import io.github.juunoco.blobmobile.Globals;

/**
 * Created by Donna on 4/09/2017.
 */

public class GridObject {

    private float x, y, width, height;
    protected int gridX, gridY, gridWidth, gridHeight;
    protected String message;
    protected Paint paint,textPaint;
    //private final int TEXT_SIZE = 75; //from 50


    public static GridObject create(float x, float y, float w, float h, String m){
        return new GridObject(x,y,w,h,m);
    }

    public void draw(Canvas c){
        paint.setColor(Color.LTGRAY);
        c.drawRect(x,y,width,height,paint);
        c.drawText(message,(x+ width)/2,(y+ height)/2,textPaint);
    }

    public float getX(){ return x; }

    public float getY(){
        return y;
    }

    public float getWidth(){ return width; }

    public float getHeight(){
        return height;
    }

    public int getTextSize() {
        return Globals.BLOB_TEXT_SIZE; //TEXT_SIZE;
    }

    public int getTextCenter() {

        return Globals.BLOB_TEXT_SIZE / 2 + 10; //TEXT_SIZE /2 + 10;
    } //10 is offset to make it center

    public float getGridWidth(){
        return gridWidth;
    }
    public float getGridHeight(){
        return gridHeight;
    }
    public int getGridX(){ return gridX; }
    public int getGridY(){ return gridY; }

    public GridObject(float startX, float startY, float w, float h){
        this.x = Globals.CELL_WIDTH*startX;
        this.y = Globals.CELL_WIDTH*startY;
        this.gridX = (int)startX;
        this.gridY = (int)startY;
        this.gridWidth = (int)w;
        this.gridHeight = (int)h; //Math.max((int)h, 1);
        if(gridHeight == 0){
            gridHeight = 1;
        }
        width = x+(Globals.CELL_WIDTH*w);
        height = y+(Globals.CELL_WIDTH*h);
        paint = new Paint();
        textPaint = new Paint();
        textPaint.setTypeface(Globals.getOrbitronFont());
        textPaint.setColor(Color.DKGRAY);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTextSize(Globals.BLOB_TEXT_SIZE);
    }

    public GridObject(float startX, float startY, float w, float h, String m){
        this(startX,startY,w,h);
        message = m;
    }


}

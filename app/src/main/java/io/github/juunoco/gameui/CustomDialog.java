package io.github.juunoco.gameui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import io.github.juunoco.blobmobile.Globals;
import io.github.juunoco.blobmobile.R;

/**
 * Created by Jun on 10/03/2018.
 */

public class CustomDialog {//<T> {

    //private T v;

    private final static String APP_TITLE = "Blob";
    private final static String APP_PNAME = "io.github.juunoco.blobmobile";

    public String mTitle = "";
    public ArrayList<String> aMessages = new ArrayList<>(); //variable number of messages
    public ArrayList<Button> aButtons = new ArrayList<>();
    private final Activity mActivity;
    private final Dialog mDialog;
    private final Context mContext;
    private Window window;

//    private void setView(T view) throws IllegalAccessException, InstantiationException {
//        this.v = view;
//    }

    private CustomDialog(@NonNull final Activity activity, Context context){
        this.mContext = context;
        this.mDialog = new Dialog(new ContextThemeWrapper(mContext, R.style.CustomDialog));
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //remove empty space of title
        this.mActivity = activity;
    }

    /**
     * static class that buttons created from external files will reference to start a scale animation
     * deprecated due to simply changing the background color being much better :)
     */
    public static void scaleButton(Button tButton, Context context){
        final Animation btnAnimation = AnimationUtils.loadAnimation(context, R.anim.bttn_scale_up);
        tButton.startAnimation(btnAnimation);
    }

    public static boolean didMoveOut(View v, Rect rect, MotionEvent e){
        if(!rect.contains(v.getLeft() + (int) e.getX(), v.getTop() + (int) e.getY())){
            //user moved out of bounds
            return true;
        }
        return false;
    }


    public void runThread(){
        new Thread(){
            public void run(){
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(mTitle != null) {
                            mDialog.setTitle(mTitle);
                        }
                        LinearLayout ll = new LinearLayout(mContext);
                        ll.setOrientation(LinearLayout.VERTICAL);

                        for(String tMessage : aMessages){
                            TextView tv = new TextView(mContext);
                            tv.setText(tMessage);
                            tv.setTextColor(Color.WHITE);
                            tv.setWidth((int)(Globals.SCREEN_WIDTH / 1.5f));
                            tv.setPadding(32, 24, 32, 20);
                            ll.addView(tv);
                        }

                        for(Button tButton : aButtons){
                            //buttons have already been defined and created, just implement them into the UI here
                            ll.addView(tButton);
                        }

                        mDialog.setContentView(ll);
                        mDialog.show();
                    }
                });
            }
        }.run();
    }

    public final Dialog getDialog(){
        return this.mDialog;
    }

    public static class Builder {

        private String mTitle = "";
        private ArrayList<String> aMessages = new ArrayList<>();
        private ArrayList<Button> aButtons = new ArrayList<>();
        private float mX, mY;
        private Window mWindow;

        public Builder() {
        }

        private <T> T set( T newValue) { //<T extends Object>
            return newValue;
        }

        public CustomDialog build(final Activity activity, Context context){
            CustomDialog cd = new CustomDialog(activity, context);
            cd.mTitle = set(mTitle);
            cd.aMessages = set(aMessages);
            cd.aButtons = set(aButtons);
            if(mWindow != null){
                cd.window = mWindow;
            }
            cd.runThread();
            return cd;
        }

        public Builder setTitle(String title){
            mTitle = title;
            //by returning this, it's much easier to read when actually creating this.
            return this;
        }

        public Builder setMessage(String... messages){
            for(String tMessage : messages){
                aMessages.add(tMessage);
            }
            return this;
        }

        public Builder setButtons(Button... buttons){
            for(Button tButton : buttons){
                aButtons.add(tButton);
            }
            return this;
        }

        public Builder setWindow(Window window){
            mWindow = window;
            return this;
        }
    }
}

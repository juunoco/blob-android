package io.github.juunoco.blobmobile;

import android.annotation.SuppressLint;
import android.app.*;
import android.content.Context;
import android.graphics.Canvas;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.android.billingclient.api.SkuDetails;

/**
 * Created by Jun on 26/02/2018.
 * horizontal scrolling purchase popup that shows the current product,
 * and links directly to gogole play and launches in app billing requests
 *
 * probably will use a dialog from Android class rather than drawing over SurfaceView again.
 */

public class PurchasePopup {

    Activity activity;
    View popupView;
    PopupWindow popupWindow;
    LinearLayout productGallery;

    PurchasePopup(Activity a, int pageNo, boolean bIsHint){
        this.activity = a;
        //center on premium1 or 2 based on the pageNo displayed

        initPopup(bIsHint);

        for(SkuDetails item : IAB.skuInventoryList){
            //create horizontal scroll item programmatically?
            if(item.getSku() == PurchasesManager.SKU_PREMIUM_PACK_1){
                //put first bitmap here
            }
            else if(item.getSku() == PurchasesManager.SKU_AMATEUR){
                //put second bitmap here
            }
//            else if(item.getSku() == PurchasesManager.SKU_LAZY){
//                //put third bitmap here
//            }
//            else if(item.getSku() == PurchasesManager.SKU_CHEATER){
//                //put fourth bitmap here
//            }
        }

        //initXLButton();

    }

    /**
     * initializes a popup
     * @param isHint to display hint purchases or not.
     */
    private void initPopup(boolean isHint){
        LayoutInflater layoutInflater =
                (LayoutInflater) this.activity.getBaseContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.popupView = layoutInflater.inflate(R.layout.hint_purchase_popup, null);
        //this.productGallery = (LinearLayout) this.popupView.findViewById(R.id.);

        this.popupWindow = new PopupWindow(
                popupView,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                true
        );

        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);

        //can set background here
        //popupWindow.setBackgroundDrawable();

        //add images to LinearLayout
        setupImageClicks();

        View parent = ((LevelActivity)this.activity).getLevelView();
        popupWindow.showAtLocation(parent, Gravity.CENTER, 0, 0);
    }

    @SuppressLint("ClickableViewAccessibility")
    void setupImageClicks(){

        ImageView hint3 = (ImageView) this.activity.findViewById(R.id.hint3);
        hint3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    //scale Up
                }
                else if(event.getAction() == MotionEvent.ACTION_UP){
                    //purchase hint 3
                }
                return false;
            }
        });

        ImageView hint5 = (ImageView) this.activity.findViewById(R.id.hint5);
        hint5.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    //scale up
                }
                else if(event.getAction() == MotionEvent.ACTION_UP){
                    //purchase hint 5
                }
                return false;
            }
        });


        ImageView hint10 = (ImageView) this.activity.findViewById(R.id.hint10);
        hint10.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    //TODO scale up
                }
                else if(event.getAction() == MotionEvent.ACTION_UP){
                    //purchase hint10
                }
                return false;
            }
        });
    }

//    private void initXLButton(){
//        ImageButton cancelButton = (ImageButton) this.popupView.findViewById(R.id.button_cancel);
//        cancelButton.setOnClickListener(new View.OnClickListener(){
//
//            @Override
//            public void onClick(View v) {
//                close();
//            }
//        });
//    }

    //close this
//    void close(){
//        this.popupWindow.dismiss();
//    }

    void draw(Canvas c){

    }

}

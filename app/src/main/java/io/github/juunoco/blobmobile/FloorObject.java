package io.github.juunoco.blobmobile;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

/**
 * Created by Donna on 3/11/2017.
 */

public class FloorObject {

    private static final FloorObject INSTANCE = new FloorObject();
    private float x, y, w, h;

    RectF getBounds(){
        RectF bounds = new RectF();
        bounds.set(x + 10,y + 10, w + 10,h + 10);
        return bounds;
    }

    float getTop(){
        return y;
    }

    void draw(Canvas c){
        Paint p = new Paint();
        p.setColor(Color.GREEN);
        c.drawRect(x,y,w,h,p);
    }

    static FloorObject getFloorObject(){
        return INSTANCE;
    }

    private void init(){
        this.x = 0;
        this.y = Globals.AD_HEIGHT + (Globals.CELL_WIDTH*9);
        this.w = Globals.SCREEN_WIDTH;
        this.h = y + Globals.CELL_WIDTH;
    }

    private FloorObject(){
        init();
    }
}

package io.github.juunoco.blobmobile.junk_classes;

/**
 *
 * For encrypting public key received by Google during In app Billing
 * Created by Jun on 23/02/2018.
 */

public class XOREncryption {

    public static String encryptDecrypt(String input){

        char[] key = {'K', 'C', 'Q' };
        StringBuilder output = new StringBuilder();

        for (int i = 0; i < input.length(); i++ ){
            output.append((char) (input.charAt(i) ^ key[i % key.length]));
        }
        return output.toString();
    }

}

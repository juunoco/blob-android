package io.github.juunoco.blobmobile.junk_classes;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.opengl.GLSurfaceView;

import io.github.juunoco.blobmobile.junk_classes.OpenGlSurfaceViewRenderer;

/**
 * Created by Jun on 21/03/2018.
 *
 * This si small, so can also just be an inner class from the Activity that uses it
 */

public class OpenGlView extends GLSurfaceView {

    private final OpenGlSurfaceViewRenderer mRenderer;

    public OpenGlView(Context context) {
        super(context);

        //check if the system supports OpenGl ES 2.0.
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final ConfigurationInfo configurationInfo = activityManager.getDeviceConfigurationInfo();
        final boolean supportsEs2 = configurationInfo.reqGlEsVersion >= 0x20000;

        if(supportsEs2)
        {
            // Create an OpenGL ES 2.0 Context
            setEGLContextClientVersion(2);

            mRenderer = new OpenGlSurfaceViewRenderer();
            // Set the Renderer for drawing on the GLSurfaceView
            setRenderer(mRenderer);
        }
        else{
            mRenderer = null;
            return;
        }




    }
}

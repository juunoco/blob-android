package io.github.juunoco.blobmobile;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

import io.github.juunoco.blobmobile.ui.TweenManager;

/**
 * Created by Donna on 4/09/2017.
 */

public class Goal extends Blob {

    private Position position;
    boolean isCompleted;
    DIRECTION alphaDirection;
    private int goalAlpha = 0, connectionAlpha = 0;
    private final int GOAL_ALPHA_MIDWAY = 150;
    private float EXTRUDE_AMT = Globals.CELL_WIDTH / 2;
    private RectF connectionRect;
    private int targetColor = 0; //when the goal slice is 11, choose the colour of the connected blob, set externally via Level.json upon goal check.

    Position getPosition(){
        return position;
    }

    boolean checkIfCompleted(){
        return isCompleted;
    }

    void setCompleted(boolean completed){
        isCompleted = completed;

        if(isCompleted){
            alphaDirection = DIRECTION.UP;
        }
        else{
            alphaDirection = DIRECTION.DOWN;
        }
    }

    /**
     * updates alpha values for tweens, cannot inherit this behaviour from superclass as it is unique to this subclass
     */
    public void updateAlpha(){
        if(alphaDirection != null){
            goalAlpha = TweenManager.changeAlpha(goalAlpha, alphaDirection);
            updateGoalConnectionAlpha(alphaDirection);
        }
    }

    /**
     * This is triggered after the goals have been made, so appearsd last, and if the goal has stopped triggering, it also disappears first
     */
    public void updateGoalConnectionAlpha(DIRECTION alphaDirection){
        if(alphaDirection == DIRECTION.UP) {
            if (goalAlpha > GOAL_ALPHA_MIDWAY) {
                connectionAlpha = TweenManager.changeAlpha(connectionAlpha, alphaDirection);
            }
        }
        else if(alphaDirection == DIRECTION.DOWN){
            //instantly cut the connection rather than slowly fade out
            connectionAlpha = 0;
//            if(goalAlpha < GOAL_ALPHA_MIDWAY){
//                connectionAlpha = TweenManager.changeAlpha(connectionAlpha, alphaDirection);
//            }
        }
    }

    //for some reason these need to update, or the size is not correct, and doesn't seem to be multipled by cell_width
    public void update(){
        //rect.set(super.getX() + 5,super.getBlobY() + 5, super.getWidth() - 5,super.getBlobBottom() - 5);
        rect.set(super.getX() + 10,super.getBlobY() + 10, super.getWidth() - 10,super.getBlobBottom() - 10);
    }

    @Override
    public void draw(Canvas c) {
        //goal outline
        //rect.set(super.getX() + 5,super.getBlobY() + 5, super.getWidth() - 10,super.getBlobBottom() - 10);
        paint.setColor(this.targetColor == 0 ? ColourScheme.getColour(super.getCut()) : ColourScheme.getColour(targetColor));
        //if(!isCompleted){
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(10);
            c.drawRoundRect(rect,BLOB_RADIUS,BLOB_RADIUS, paint);
        //}
//        else {
//            paint.setStyle(Paint.Style.FILL_AND_STROKE);
//        }
        //goal inside alpha

        if(alphaDirection != null){
            paint.setStyle(Paint.Style.FILL);
            if(this.bDrawConnectedRect && this.getCut() != 0) {
                drawBlobSideRect(c, rect, paint);
            }
            paint.setAlpha(goalAlpha);
            c.drawRoundRect(rect,BLOB_RADIUS,BLOB_RADIUS, paint);
            if(!this.bDrawConnectedRect) {
                drawShadeAndShine(c);
            }
        }
        if(!Globals.isTutorialMode() && getCut() != 11){
            //only draw the text if it is not connected
            if (!bGoalConnected) {
                if(super.getCut() != 0) {
                    textPaint.setAlpha(255 - connectionAlpha);
                    c.drawText("" + super.getCut(), (super.getX() + super.getWidth()) / 2,
                            (super.getBlobY() + super.getBlobBottom() + super.getTextCenter()) / 2,
                            textPaint);
                }
            }
        }
    }

    /**
     * based on position, draws a rectangle facing the side of the Blob
     * @param c inherited drawing context
     */
    private void drawBlobSideRect(Canvas c, RectF rect, Paint paint){
        //modify rect to have no boundaries
        //this gives an interesting effect, and it's fun to look at, when its painted stokre
        //rect.set(rect.left - 5, rect.top - 5, rect.right + 10, rect.bottom + 10);
        switch(this.position){
            case TOP : {
                this.connectionRect.set(rect.left - 5, rect.top - 5 + (rect.height() /2), rect.right + 5, rect.bottom + 5 + EXTRUDE_AMT);
                break;
            }
            case BOTTOM : {
                this.connectionRect.set(rect.left - 5, rect.top - 5 - EXTRUDE_AMT, rect.right + 5, rect.bottom + 5 - (rect.height() / 2));
                break;
            }
            case LEFT : {
                this.connectionRect.set(rect.left - 5 + (rect.width() / 2), rect.top - 5, rect.right + 5 + EXTRUDE_AMT, rect.bottom + 5);
                break;
            }
            //RIGHT
            default : {
                this.connectionRect.set(rect.left - 5 - EXTRUDE_AMT, rect.top - 5, rect.right + 5 - (rect.width() / 2), rect.bottom + 5);
                break;
            }
        }
        paint.setAlpha(connectionAlpha);
        c.drawRect(connectionRect, paint);
    }

    void drawShadeAndShine(Canvas c){
        //draw shade & shine as well with the same alpha
//        this.connectionRect.set(rect.left, rect.bottom - Globals.CELL_WIDTH, rect.right,rect.bottom -10);
//        c.drawBitmap(Globals.shadow,null, connectionRect,null);
//        RectF rectTop = new RectF();
//        rectTop.set(rect.left, rect.top, rect.right,rect.top + Globals.CELL_WIDTH);
//        c.drawBitmap(Globals.shine,null,rectTop,null);
    }

    static Goal createGoal(Position pos, float startCell, float cellLength, int num){
        float[] attributes = null; //x,y,w,h values
        String errorMsg = "Check lvls.json file: ";
        if(startCell < 1)
            throw new IllegalArgumentException(errorMsg + "start cannot be less than 1");
        if(cellLength < 1 && cellLength >8)
            throw new IllegalArgumentException(errorMsg +
                    "length cannot be less than 1 or more than 8");
        if(startCell + cellLength > 9)
            throw new IllegalArgumentException(errorMsg + "goal exceeds allowable size");
        switch (pos){
            case TOP:
                attributes = new float[]{startCell,0,cellLength,1};
                break;
            case BOTTOM:
                attributes = new float[]{startCell,9,cellLength,1};
                break;
            case LEFT:
                attributes = new float[]{0,startCell,1,cellLength};
                break;
            case RIGHT:
                attributes = new float[]{9,startCell,1,cellLength};
                break;
        }
        return new Goal(pos,attributes[0],attributes[1],attributes[2],attributes[3],num);
    }

    /**
     * called when resetting a level, to make alpha instantly nothing rather than fading it away?
     */
    public void setGoalAlpha(){
        this.goalAlpha = 0;
    }

    /**
     * used when goal's target slice is 11 during tutorial
     * sets the goal's target color from Level.json upon checking for goal completion
     * @param newColor
     */
    public void setTargetColor(int newColor){ this.targetColor = newColor; }

    private Goal(float startX, float startY, float w, float h, int num) {
        super(startX, startY, w, h, num, null);
        isCompleted = false;
        alphaDirection = null;
        this.connectionRect = new RectF();
    }

    private Goal(Position p, float startX, float startY, float w, float h, int num){
        super(startX, startY, w, h, num, null);
        position = p;
        isCompleted = false;
        alphaDirection = null;
        this.connectionRect = new RectF();
    }

    enum Position {
        TOP, BOTTOM, LEFT, RIGHT
    }


    @Deprecated
    public static class AlphaInterpolator implements android.view.animation.Interpolator {

        public DIRECTION mAlphaDirection;

        public static AlphaInterpolator create(DIRECTION alphaDirection){
            return new AlphaInterpolator(alphaDirection);
        }

        public AlphaInterpolator(DIRECTION alphaDirection){
            this.mAlphaDirection = alphaDirection;
        }

        @Override
        public float getInterpolation(float time) {
            if(mAlphaDirection == DIRECTION.UP) {
                return time < 255 ? time + 1 : 255;
            }
            else {
                return time > 0 ? time - 1 : 0;
            }
        }
    }



}
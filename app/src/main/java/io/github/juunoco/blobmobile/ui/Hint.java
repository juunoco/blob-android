package io.github.juunoco.blobmobile.ui;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;

import io.github.juunoco.blobmobile.Globals;

/**
 * Created by Jun on 27/03/2018.
 * Hint object containint HintTouchLine for visual representation
 */

public class Hint {

    protected final float sliceX, sliceY;
    protected int rotation;
    protected final int sliceNo;
    protected Paint textPaint;
    protected final HintTouchLine hintTouchLine;
    protected String msg;
    //private final boolean hintUnlocked;

    /**
     * creates a hint with touchline, and text is drawn in default space beneath touchline, or rotated if touchline is vertical
     * @param hintNo
     * @param hintTouchLine
     */
    public Hint(int hintNo, HintTouchLine hintTouchLine, String msg){
        this.sliceNo = hintNo;
        this.hintTouchLine = hintTouchLine;
        this.msg = msg;
        //this.hintUnlocked = hintUnlocked;
        this.sliceX = (hintTouchLine.getX1() + hintTouchLine.getX2()) / 2f;
        this.sliceY = ((hintTouchLine.getY1() + hintTouchLine.getY2()) / 2f) + Globals.TUTORIAL_TEXT_SIZE_BIG;
        if(hintTouchLine.getY1() != hintTouchLine.getY2()){
            //is vertical
            rotation = 90;
        }
        else {
            rotation = 0;
        }
        setupPaint();
    }

    /**
     * creates a hint with no touchline
     * and x & y to draw text in a custom position below the touchline
     * @param hintNo
     */
    public Hint(int hintNo, float textX, float textY, String text){
        this.sliceNo = hintNo;
        this.hintTouchLine = null;
        this.msg = text;

        setupPaint();
        rotation = 0;
        sliceX = textX;
        sliceY = textY;
    }

    void setupPaint(){
        textPaint = new Paint();
        textPaint.setColor(Color.BLACK);
        textPaint.setTextSize(Globals.TUTORIAL_TEXT_SIZE_BIG); //64
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTypeface(Globals.getOrbitronFont());
    }

    public void update(){
        this.hintTouchLine.update();
    }

    public void draw(String extraText, Canvas c){
        this.hintTouchLine.draw(c);
        c.save();
        c.rotate(rotation, sliceX, sliceY - (Globals.CELL_WIDTH / 2));
        c.drawText(extraText + " " + this.sliceNo, sliceX, sliceY, textPaint);
        c.restore();
    }

    public final int getHintNo(){
        return this.sliceNo;
    }
    public final HintTouchLine getHintTouchLine(){
        return this.hintTouchLine;
    }
    public final String getMsg(){ return this.msg; }
    //public final boolean isUnlocked(){return this.hintUnlocked;}
}

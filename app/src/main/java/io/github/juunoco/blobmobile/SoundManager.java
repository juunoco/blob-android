package io.github.juunoco.blobmobile;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Donna on 20/11/2017.
 */

public class SoundManager {

    public static boolean bAudioMuted = false;
    public static boolean bFXMuted = false;
    public enum SOUNDSTATE {
            MENU, PLAYING
    }
    public static SOUNDSTATE soundState;

    public static boolean bIsPlayingLeveltransitionMusic = false;
    Context context;
    private static MediaPlayer blobSlice, wrongMove, gotGoal, blobRemove, blobDestroy, blobDrop, clickPlay, clickRetry, clickReturn, selectLevel, dropStop;
    private static ArrayList<MediaPlayer> randomSliceFX = new ArrayList<>();
    private static ArrayList<MediaPlayer> randomDestroyFX = new ArrayList<>();
    private static ArrayList<MediaPlayer> randomGoalCompletedFX = new ArrayList<>();
    private static ArrayList<MediaPlayer> randomDropSound = new ArrayList<>(), randomShatterFX = new ArrayList<>();

    private static MediaPlayerInstance menuMusic, transitionPage, playingBackground;
    public static ArrayList<MediaPlayerInstance> mediaPlayerInstances = new ArrayList<>();


    private static Random rand = new Random();

    /**
     *
     * @param titleFrom The music which is transitioning from
     * @param titleTo The music which is being transitioned to
     *
     *                The music transitioning from, fades into the music transitioning to.
     *                If there is no from, or to, a placeholder string "none" is provided
     *                The volume are updated with every call to SoundManager.update(), and MediaPlayerInstance.update();
     */
    static final void transitionMusic(String titleFrom, String titleTo, boolean bRestart){
        for(MediaPlayerInstance mpi : mediaPlayerInstances){
            if(mpi.getTitle().equals(titleFrom)){
                mpi.setTargetVolume(0, false);
            }
            else if(mpi.getTitle().equals(titleTo)){
                mpi.setTargetVolume(1, bRestart);
            }
        }
    }

    static MediaPlayerInstance getMediaPlayerFromTitle(String tTitle){
        for (MediaPlayerInstance mpi : mediaPlayerInstances) {
        if (mpi.getTitle().equals(tTitle)) {
                return mpi;
            }
        }
        return null;
    }


    static final void playClickPlay(){
        if(!SoundManager.bFXMuted) {
            if(clickPlay != null) {
                clickPlay.start();
            }
        }
    }

    static final void playSelectLevel(){
        if(!SoundManager.bFXMuted){
            selectLevel.start();
        }
    }

    private static final MediaPlayer getBlobSlice(){
        return blobSlice;
    }

    public static final void playBlobSlice(){
        if(!SoundManager.bFXMuted) {
            //getBlobSlice().start();
            getRandomSliceFX().start();
        }
    }

    public static final void playBlobShatter(){
        if(!SoundManager.bFXMuted){
            //getRandomShatterFX().start();
        }
    }

    static final void playWrongMove(){
        if(!SoundManager.bFXMuted) {
            wrongMove.start();
        }
    }

    static final void playClickReturn(){
        if(!SoundManager.bFXMuted){
            clickReturn.start();
        }
    }

    static final void playGotGoal(){
        if(!SoundManager.bFXMuted) {
            //gotGoal.start();
            getRandomGoalCompletionFX().start();
        }
    }


    static final void playBlobRemove(){
        if(!SoundManager.bFXMuted) {
            //blobRemove.start();
            getRandomDestroyFX().start();
        }
    }

    static final void playBlobDrop(){
        if(!SoundManager.bFXMuted) {
            getRandomDropFX();
            //blobDrop.start();
        }
    }

    static final void playClickRetry(){
        if(!SoundManager.bFXMuted) {
            clickRetry.start();
        }
    }

    //GameActivity & LevelActivity call this update method to adjust volumes
    static void update(){
        for(MediaPlayerInstance mpi : mediaPlayerInstances){
            mpi.update();
        }
    }

    static void init(Context c, SplashActivity splashActivity){
        //return new SoundManager(context);

        //FX
        //clickPlay = MediaPlayer.create(c,R.raw.click_play_b);
        clickPlay = MediaPlayer.create(c, R.raw.j_click_fx);
        clickReturn = MediaPlayer.create(c, R.raw.j_click_return_fx);//tosue
        selectLevel = MediaPlayer.create(c, R.raw.j_select_fx);
        gotGoal = MediaPlayer.create(c,R.raw.success_b);
        wrongMove = MediaPlayer.create(c,R.raw.error_b);
        blobDrop = MediaPlayer.create(c,R.raw.drop_a);//was using drop_a_2;
        blobRemove = MediaPlayer.create(c,R.raw.remove_a);
        blobDestroy = MediaPlayer.create(c, R.raw.j_sfx_blob_destroy);
        dropStop = MediaPlayer.create(c, R.raw.j_sfx_drop);

        randomDropSound.add(blobDrop);
        randomDropSound.add(MediaPlayer.create(c,R.raw.drop_a));
        randomDropSound.add(MediaPlayer.create(c,R.raw.drop_a));
        randomDropSound.add(MediaPlayer.create(c,R.raw.drop_a));
        randomDropSound.add(MediaPlayer.create(c,R.raw.drop_a));
//        randomDropSound.add(MediaPlayer.create(c, R.raw.j_snare_1));
//        randomDropSound.add(MediaPlayer.create(c, R.raw.j_snare_2));

        randomDestroyFX.add(blobRemove);
        randomDestroyFX.add(blobDestroy);

        randomShatterFX.add(MediaPlayer.create(c, R.raw.tone1));
        randomShatterFX.add(MediaPlayer.create(c, R.raw.tone2));
        randomShatterFX.add(MediaPlayer.create(c, R.raw.tone3));
        randomShatterFX.add(MediaPlayer.create(c, R.raw.tone4));
        randomShatterFX.add(MediaPlayer.create(c, R.raw.tone5));
        randomShatterFX.add(MediaPlayer.create(c, R.raw.tone6));
//        randomShatterFX.add(MediaPlayer.create(c, R.raw.tone7));
//        randomShatterFX.add(MediaPlayer.create(c, R.raw.tone8));
//        randomShatterFX.add(MediaPlayer.create(c, R.raw.tone9));
//        randomShatterFX.add(MediaPlayer.create(c, R.raw.tone10));
//        randomShatterFX.add(MediaPlayer.create(c, R.raw.tone11));
//        randomShatterFX.add(MediaPlayer.create(c, R.raw.tone12));
//        randomShatterFX.add(MediaPlayer.create(c, R.raw.tone13));
//        randomShatterFX.add(MediaPlayer.create(c, R.raw.tone14));

        randomGoalCompletedFX.add(gotGoal);
        randomGoalCompletedFX.add(MediaPlayer.create(c, R.raw.j_goal_complete));
        randomGoalCompletedFX.add(MediaPlayer.create(c, R.raw.j_goal_complete_2));
        randomGoalCompletedFX.add(MediaPlayer.create(c, R.raw.j_goal_complete_3));
        randomGoalCompletedFX.add(MediaPlayer.create(c, R.raw.j_goal_complete_4));

        clickRetry = MediaPlayer.create(c,R.raw.j_click_return_fx);

        blobSlice = MediaPlayer.create(c,R.raw.slice_d);
        randomSliceFX.add(blobSlice);
        randomSliceFX.add(MediaPlayer.create(c, R.raw.slice_d));
        randomSliceFX.add(MediaPlayer.create(c, R.raw.slice_d));
        //removing long and textured sounds for short and snappy
//        randomSliceFX.add(MediaPlayer.create(c, R.raw.j_split_2));
//        randomSliceFX.add(MediaPlayer.create(c, R.raw.j_split_3));
//        randomSliceFX.add(MediaPlayer.create(c, R.raw.j_split_4));
//        randomSliceFX.add(MediaPlayer.create(c, R.raw.j_split_5));

        //Audio
        menuMusic = MediaPlayerInstance.create(c, R.raw.menu_background_music);//(MediaPlayerInstance) MediaPlayer.create(c,R.raw.menu_background_music);
        menuMusic.setTitle("menuMusic");
        menuMusic.getMediaPlayer().setLooping(true);
        menuMusic.getMediaPlayer().setVolume(0,0);
        //menuMusic.getMediaPlayer().setOnPreparedListener(splashActivity);

        //menuMusic.start();
        transitionPage = MediaPlayerInstance.create(c, R.raw.transition_page);
        transitionPage.setTitle("transitionMusic");
        transitionPage.getMediaPlayer().setLooping(true);
        transitionPage.getMediaPlayer().setVolume(0,0);
        //transitionPage.getMediaPlayer().setOnPreparedListener(splashActivity);
        //transitionPage.start();
        playingBackground = MediaPlayerInstance.create(c, R.raw.playing_background5);
        playingBackground.setTitle("playingMusic");
        playingBackground.getMediaPlayer().setLooping(true);
        playingBackground.getMediaPlayer().setVolume(0,0);
        //playingBackground.getMediaPlayer().setOnPreparedListener(splashActivity);
        //playingBackground.start();

        mediaPlayerInstances.add(menuMusic);
        mediaPlayerInstances.add(transitionPage);
        mediaPlayerInstances.add(playingBackground);


//        for(MediaPlayerInstance mpi : mediaPlayerInstances){
//            mpi.getMediaPlayer().prepareAsync();
//        }
    }

    //recursive until non-playing sound is found
    static MediaPlayer getRandomSliceFX(){
        MediaPlayer mp = randomSliceFX.get(rand.nextInt(randomSliceFX.size()));
        if(mp.isPlaying()){
            getRandomSliceFX();
        }
        return mp;
    }

    static void playDropStop(){
        if(!SoundManager.bFXMuted) {
            dropStop.start();
        }
    }

    //removed shatter FX
    static MediaPlayer getRandomShatterFX(){
        MediaPlayer mp= randomShatterFX.get(rand.nextInt(randomShatterFX.size()));
        if(mp.isPlaying()){
            getRandomShatterFX();
        }
        return mp;
    }

    static MediaPlayer getRandomDestroyFX(){
        MediaPlayer mp = randomDestroyFX.get(rand.nextInt(randomDestroyFX.size()));
        if( mp.isPlaying()){
            getRandomDestroyFX();
        }
        return mp;
    }

    static MediaPlayer getRandomGoalCompletionFX(){
        MediaPlayer mp = randomGoalCompletedFX.get(rand.nextInt(randomGoalCompletedFX.size()));
        if(mp.isPlaying()){
            getRandomGoalCompletionFX();
        }
        return mp;
    }

    static void getRandomDropFX(){
        MediaPlayer mp = randomDropSound.get(rand.nextInt(randomDropSound.size()));
        mp.start();
//        if(mp.isPlaying()){
//            getRandomDropFX();
//        }
    }

    /**
     * stops background music when user moves game to background
     * called from onPause & onStop() of LevelActivity and GameActivity
     */
    static void stopBackgroundMusic(){
//        new Handler().postDelayed(new Runnable(){
//            public void run() {
//                for (MediaPlayerInstance mpi : mediaPlayerInstances) {
//                    mpi.getMediaPlayer().pause();
//                }
//            }
//        }, 3000);
        //check if this is a game triggered action, if so, then do nothing, is not, then mute all sounds.
        for (MediaPlayerInstance mpi : mediaPlayerInstances) {
            if(mpi.getMediaPlayer().isPlaying()) {
                mpi.getMediaPlayer().pause();
            }
        }
    }

    /**
     * resumes music when user brings app up from background
     * called from onPause & onStop() of LevelActivity and GameActivity
     */
    static void resumeBackgroundMusic(){
        for(MediaPlayerInstance mpi : mediaPlayerInstances){
            mpi.getMediaPlayer().start();
        }
    }

    /*****************************************************************/
    static void adjustAudioVolume(MediaPlayer mp, int newVolume){
        mp.setVolume(newVolume, newVolume);
    }

    static void adjustFXVolume(int newVolume){
        clickPlay.setVolume(newVolume,newVolume);
        blobSlice.setVolume(newVolume, newVolume);
        gotGoal.setVolume(newVolume, newVolume);
        wrongMove.setVolume(newVolume,newVolume);
        blobDrop.setVolume(newVolume,newVolume);
        blobRemove.setVolume(newVolume,newVolume);
        clickRetry.setVolume(newVolume,newVolume);
    }
    /**************************************************************/
}

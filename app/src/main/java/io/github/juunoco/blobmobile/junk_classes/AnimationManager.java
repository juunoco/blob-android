package io.github.juunoco.blobmobile.junk_classes;

import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.View;

/**
 * Created by Jun on 3/03/2018.
 * static helper functions for animation transitions
 */

public class AnimationManager {

    //use for fading goals in and out
    //TODO fix this
    public static void setAlphaAnimation(View v, final Runnable r) {
        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(v, "alpha",  1f, .3f);
        fadeOut.setDuration(2000);
        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(v, "alpha", .3f, 1f);
        fadeIn.setDuration(2000);

        final AnimatorSet mAnimationSet = new AnimatorSet();

        mAnimationSet.play(fadeIn).after(fadeOut);

        mAnimationSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mAnimationSet.start();
                r.run();
            }
        });
        mAnimationSet.start();
    }

}

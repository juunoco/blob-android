package io.github.juunoco.blobmobile;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.Iterator;

import io.github.juunoco.blobmobile.ui.BrowsingPageCircles;
import io.github.juunoco.blobmobile.ui.TweenManager;
import io.github.juunoco.gameui.JuunoButton;

/**
 * Created by Donna on 28/10/2017.
 * Modified by gieoon to display all levels at once.
 */

public class BrowsingManager {

    SparseArray<Level> lvlArray;
    ArrayList<BrowsingPreview> lvlPrevList;
    ArrayList<PreviewRect> previewRectList;
    private MainPageCanvas mainPageCanvas;
    private CreditsPageCanvas creditsPageCanvas;
    private SettingsPopup settingsPopup;
    //private int pageNum;
    private boolean bSettingsDisplayed = false;
    private JuunoButton nextPage, prevPage;
    public static float GRID_CELL = Globals.SCREEN_WIDTH / 12;
    private Handler handler;

    private PageLocked pageLocked;
    final Activity activity;
    public BrowsingPreview mLevelSelected;
    private int fromPage, toPage;

    //local variabels for world transition tween - scrolling page left and right
    private float worldOffset, worldTransformation, worldTransformationOriginal;
    private final float TRANSITIONDISTANCE = Globals.SCREEN_WIDTH;
    private final int TRANSITIONFRAMES = 8; //at 20 updates per second
    public boolean bWorldTransitioning = false, bWorldTransitioningToPage = false;
    private int transitionCount = 0, transitionPageCount = 0;
    private BrowsingPreview.DIRECTION transitionDirection;
    private int tweenToPageFrames = 0;
    private float tweenToPageDistance = 0;

    public static final int PREVIEW_RECT_MAX_ALPHA = 25;
    public int previewRectAlpha = PREVIEW_RECT_MAX_ALPHA;
    public int previewTextAlpha = 255;
    public boolean bTextDisappear = false;
    public boolean bTextReappear = false;
    public boolean bReappear = false;
    public boolean bDisappear = false;
    public boolean bTransitioning = false;
    public boolean bLevelSelected = false;
    private final int RECT_ALPHA_CHANGE_RATE = 5;

    private int count = 0;

    BrowsingPageCircles browsingPageCircles;

//    private void initLevels(int pageNo){
//        lvlPrevList.clear();
//        int lvlEnd = pageNo * 9;
//        if(lvlEnd > lvlArray.size()) lvlEnd = lvlArray.size();
//        int lvlStart = lvlEnd - (lvlEnd -1);
//        if(pageNo > 1) lvlStart = ((pageNo - 1) * 9 ) + 1;
//        for(int i = lvlStart; i < lvlEnd + 1; i++){
//            Level lvl = lvlArray.get(i);
//            BrowsingPreview lvlPrev = BrowsingPreview.create(this, lvl, BrowsingPreview.DIRECTION.NONE);
//            lvlPrevList.add(lvlPrev);
//        }
////        if(lvlArray.size() > lvlEnd) nextPage.setEnabled(true);
////        else nextPage.setEnabled(false);
////        if(pageNo > 1) prevPage.setEnabled(true);
////        else prevPage.setEnabled(false);
//        //Log.w("PAGE NUM: ",pageNo+"");
//    }

    private void initAllLevels(){
        lvlPrevList.clear();

        this.mainPageCanvas = new MainPageCanvas(activity.getBaseContext(), this, 0);
        this.creditsPageCanvas = new CreditsPageCanvas(this, Globals.MAX_PAGES + 1);

        for(int i = 1; i < lvlArray.size(); i++){
            Level lvl = lvlArray.get(i);
            BrowsingPreview lvlPrev = BrowsingPreview.create(this, lvl, BrowsingPreview.DIRECTION.NONE);
            lvlPrevList.add(lvlPrev);
        }

        this.browsingPageCircles = new BrowsingPageCircles(this.activity.getBaseContext(), this);
    }

    //update the levels based on which ones are visible
   // @Deprecated //due to performance issues
//    private void addAdjacentLevel(int adjacentPageNum){
//        int lvlEnd = adjacentPageNum * 9;
//        if(lvlEnd > lvlArray.size()) lvlEnd = lvlArray.size();
//        int lvlStart = lvlEnd - (lvlEnd - 1);
//        if(adjacentPageNum > 1) lvlStart = ((adjacentPageNum - 1) * 9) + 1;
//        for(int i = lvlStart; i < lvlEnd + 1; i++){
//            Level lvl = lvlArray.get(i);
//            lvlPrevList.add(BrowsingPreview.create(
//                    this,
//                    lvl,
//                    adjacentPageNum > Globals.getPageNum() ? BrowsingPreview.DIRECTION.RIGHT : BrowsingPreview.DIRECTION.LEFT
//            ));
//        }
//    }

    synchronized void update(){
        Iterator<BrowsingPreview> iter = lvlPrevList.iterator();
        while(iter.hasNext()){
            BrowsingPreview lvlPrev = iter.next();
            lvlPrev.update();
            if(lvlPrev.bRemoveFlag){
                iter.remove();
                //lvlPrevList.remove(lvlPrev);
            }
        }

        //if transitioning, set a countdown before previewRects reappear
       for(PreviewRect pr : previewRectList){
            pr.update();
       }

        previewRectDisappear();
        previewRectReappear();
        previewTextDisappear();
        previewTextReappear();

        if(this.bLevelSelected){
            if(this.mLevelSelected != null) {
                //make sure page is not locked
                if(!PurchasesManager.isPageLocked(Globals.getPageNum())) {
                    if (this.mLevelSelected.getLevel().bigBlob.bIncreaseSizeComplete) {
                        ((LevelActivity) this.activity).goToLevel();
                        reset();
                    }
                }
            }
        }

        //transition level
        if(bWorldTransitioning){
            transitionWorld();
            mainPageCanvas.update();
            creditsPageCanvas.update();
        }
        if(bWorldTransitioningToPage){
            transitionToPage();
            mainPageCanvas.update();
            creditsPageCanvas.update();
        }
        settingsPopup.update();
        pageLocked.update();

    }

    /**
     * called from Level View whenever the view resume.
     */
    void startGlowOfSelectedPreview(){
        for(final BrowsingPreview lvlPrev : lvlPrevList) {
            if (lvlPrev.getLevel().number == Globals.getCurrLvlNum()) {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        lvlPrev.startGlowing();
                    }
                }, 500); //this delay, otherwise acitivty updates without being in foreground.
            }
        }
    }

    private void reset(){
        this.mLevelSelected.getLevel().bigBlob.resetScale();
        this.mLevelSelected = null;
        this.bLevelSelected = false;
    }

//    String getDrawText(){
//        switch(Globals.getPageNum()){
//            case 1 : return "Blob Beginner";
//            case 2 : return "Blob Splitter";
//            case 3 : return ""
//        }
//    }

    void draw(Canvas c){
        c.drawBitmap(Globals.background,0,0,null);
        //Paint p = new Paint();
        //p.setTypeface(Globals.getOrbitronFont());
        //p.setTextSize(80);
        //p.setTextAlign(Paint.Align.CENTER);
        // show this text in the meantime
//        c.drawText("Page " + Globals.getPageNum(),
//                Globals.SCREEN_WIDTH/2,
//                Globals.SCREEN_HEIGHT/2 - Globals.LVL_GRID_CELL * 6,p);

        //draw settings
        settingsPopup.draw(c);

        for(PreviewRect pr : previewRectList){
            pr.draw(c);
        }

        for(int i = 0; i < lvlPrevList.size(); i++){
            BrowsingPreview lvlPrev = lvlPrevList.get(i);
            lvlPrev.draw(c);
            lvlPrev.drawText(c);
            if(lvlPrev.getLocked() || lvlPrev.getUnlocking()){
                lvlPrev.drawLocked(c);
            }
        }

        mainPageCanvas.draw(c);
        creditsPageCanvas.draw(c);

        if(!Globals.getPurchasedPremium1()){
            pageLocked.draw(c);
            //'Unlock' button that is vibrating and shaking
            //make the button and levels seem so close.
        }
        //else{
            //level pack has been purchased, remove all ads
        //}

        //if(prevPage.isEnabled()) prevPage.draw(c);
        //if(nextPage.isEnabled()) nextPage.draw(c);

        browsingPageCircles.draw(c);
    }

    //trigger the alpha decrease followed by the alpha increase.
    public void trigger(){
        bDisappear = true;
        bTextDisappear = true;
    }

    private void previewRectDisappear(){
        if(bDisappear){
            //Globals.previewRectAlpha = Globals.clamp(Globals.previewRectAlpha - 1, 0, Globals.PREVIEW_RECT_MAX_ALPHA);
            previewRectAlpha -= RECT_ALPHA_CHANGE_RATE;
            if(previewRectAlpha <= RECT_ALPHA_CHANGE_RATE){
                bReappear = true;
                bDisappear = false;
                previewRectAlpha = 0;
            }
        }
    }

    private void previewRectReappear(){
        if(bReappear) {
            //count++;
            //if(count % 5 == 0) {
            //Globals.previewRectAlpha = Globals.clamp(Globals.previewRectAlpha + 1, 0, Globals.PREVIEW_RECT_MAX_ALPHA);
                previewRectAlpha += RECT_ALPHA_CHANGE_RATE;
            //}
            //if (previewRectCount == TweenManager.BROWSING_FRAMES_TOTAL) {
            if(previewRectAlpha >= (PREVIEW_RECT_MAX_ALPHA - RECT_ALPHA_CHANGE_RATE)){
                bReappear = false;
                previewRectAlpha = PREVIEW_RECT_MAX_ALPHA;
            }
        }
    }

    private void previewTextDisappear(){
        if(bTextDisappear){
            previewTextAlpha = 0;
            if(previewTextAlpha <= 0){
                bTextDisappear = false;
                bTextReappear = true;
                previewTextAlpha = 0;
            }
        }
    }

    private void previewTextReappear(){
        if(bTextReappear){
            //previewTextAlpha += 10;
            count += 3;
            previewTextAlpha = (int)TweenManager.easeInQuart(count, 0, 255, 32);
            if(previewTextAlpha >= 250){
                bTextReappear = false;
                bTransitioning = false;
                previewTextAlpha = 255;
                count = 0;
            }
        }
    }

    private void goToPage(int pageNo){
        if(!this.isWorldTransitioning()) {
            int tPageNo = Globals.getPageNum(); //current page number

//          Intent intent = new Intent(activity , LevelActivity.class);
//          this.activity.startActivity(intent);
            if (pageNo > tPageNo) {
                //this.activity.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                slideLeft();
            } else if (pageNo < tPageNo) {
                //this.activity.overridePendingTransition(android.R.anim.slide_out_right, android.R.anim.slide_in_left);
                slideRight();
            }
            Globals.setPageNum(pageNo); //set to new page number and save in SharedPrefs
            //Globals._sharedPreferencesEditor.putInt(Globals.prefCurrentPage, pageNo); //save new page to shared prefs
            bTransitioning = true;
            trigger();
        }
    }

    /**
     * checks for touches of browsing manager
     * @param e
     * @return true if the touch is consumed, false if not
     */
    boolean onTouch(MotionEvent e) {
        Log.w("", "ON TOUCH CALLED");
        if (prevPage.isClicked(e) && prevPage.isEnabled()) {
            //setPrevPage();
            //initLevels();
        } else if (nextPage.isClicked(e) && nextPage.isEnabled()) {
            //setNextPage();
            //initLevels();
            Log.w("", "Next Button pressed");
        }

        int browsingCirclePageNo = this.browsingPageCircles.checkClicks(e);
        if(browsingCirclePageNo != -1 && !this.bWorldTransitioningToPage && !this.bWorldTransitioning){
            setFromPageToPage(Globals.getPageNum(), browsingCirclePageNo);
            slideToPage(browsingCirclePageNo);
            SoundManager.playClickPlay();
        }

        if (this.mainPageCanvas.isPlayClicked(e)){
            //auto scroll to the page last played.
            int tCurrentPage = Globals._sharedPreferences.getInt(Globals.prefCurrentPage, 1);
            slideToPage(tCurrentPage);
            //provdie the page we're going to to LevelView in order to change colour
            setFromPageToPage(0, tCurrentPage);


            //Globals.setPageNum(1);
            return true;
        }
        else if(this.mainPageCanvas.isCreditsClicked(e)){
            //chgange color from menu background colour to credits page colour
            setFromPageToPage(0, Globals.MAX_PAGES + 1);
        }
        else if(this.creditsPageCanvas.isMenuClicked(e)){
            SoundManager.playClickPlay();
            slideToPage(0);
            setFromPageToPage(Globals.MAX_PAGES + 1, 0);
        }
        else if (this.settingsPopup.isClicked(e)){
            settingsPopup.startRotating();
            bSettingsDisplayed = true;
            hideNavigationBar();
            return true;
        }
        else if (PurchasesManager.isPageLocked(Globals.getPageNum())) {
//            if (pageLocked.purchaseArea.isClicked(e) && pageLocked.purchaseArea.isEnabled()) {
//                //display the purchases
//                pageLocked.initPurchasePopup(pageNum);
//            }
            if(pageLocked.isClicked(e)){
                //pageLocked.initPurchasePopup(pageNum);
                return true;
            }
        }
        return false;
    }

    void hideNavigationBar(){
        android.support.v7.app.ActionBar actionBar = ((AppCompatActivity)activity).getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        View decorView = activity.getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        decorView.setSystemUiVisibility(uiOptions);

        ((LevelActivity)activity).getLevelView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if(hasNavBar()) Globals.SCREEN_HEIGHT = Globals.SCREEN_HEIGHT + Globals.CELL_WIDTH;
    }

    boolean hasNavBar(){
        int id = Resources.getSystem()
                .getIdentifier("config_showNavigationBar", "bool", "android");
        return id > 0 && Resources.getSystem().getBoolean(id);
    }

    /**
     * slides from current page to the targeted page.
     * Will check if page is smaller or larger, and tween accordingly.
     * Tween duration increases with the distance of the page
     * @param pageNo page to scroll to
     */
    public void slideToPage(int pageNo){
        int pageDiff = Globals.getPageNum() - pageNo;
        this.tweenToPageDistance = pageDiff * TRANSITIONDISTANCE;
        this.tweenToPageFrames = TRANSITIONFRAMES * Math.abs(pageDiff);
        startTransitionToPage();
        Globals.setPageNum(pageNo);
    }

    private void slideLeft(){
        startTransition(BrowsingPreview.DIRECTION.LEFT);
        //addAdjacentLevel(Globals.getPageNum() + 1);
//        for(BrowsingPreview lvlPrev : lvlPrevList){
//            lvlPrev.moveLeft();
//        }
    }

    private void slideRight(){
        startTransition(BrowsingPreview.DIRECTION.RIGHT);
        //addAdjacentLevel(Globals.getPageNum() - 1);
//        for(BrowsingPreview lvlPrev : lvlPrevList){
//            lvlPrev.moveRight();
//        }
    }

    /**
     * called from BrowsingPreview after it has finished unlocking, to automatically enter the next level
     * @param lvlNo to choose the level to enter
     * @return BrowsingPreview with the corresponding level
     */
    public void selectNextLevel(int lvlNo){
        for(BrowsingPreview bp : lvlPrevList){
            if(bp.getLevel().number == lvlNo){
                disableAllOtherSelects();
                Globals.setCurrLvlNum(bp.levelNum);
                SoundManager.playSelectLevel();
                bp.getLevel().bigBlob.bIncreaseSize = true;
                this.mLevelSelected = bp;
                return;
            }
        }
    }

    BrowsingPreview levelSelect(MotionEvent e){
        if(!bTransitioning) {
            if(!bLevelSelected) {
                //if(!Globals.bUnlockingAnimationTriggered){
                    if(!PurchasesManager.isPageLocked(Globals.getPageNum())) {
                        for (BrowsingPreview lvlPrev : lvlPrevList) {
                            if (lvlPrev.clicked(e)) {
                                SoundManager.playSelectLevel();
                                Globals.setCurrLvlNum(lvlPrev.levelNum);
                                lvlPrev.getLevel().bigBlob.bIncreaseSize = true;
                                disableAllOtherSelects();
                                return lvlPrev;
                            }
                        }
                    }
                //}
            }
        }
        return null;
    }

    void disableAllOtherSelects(){
        bLevelSelected = true;
    }

    //private int getPageNum(){return pageNum;}

    public void setNextPage(){
        int newPage = Globals.getPageNum() + 1;
        if(newPage <= Globals.MAX_PAGES + 1){ //+1 for credits
            goToPage(Globals.getPageNum()+1);
        }
    }

    public void setPrevPage(){
        int newPage = Globals.getPageNum() - 1;
        if(newPage >= 0) {
            goToPage(Globals.getPageNum() - 1);
        }
    }

//    private void setPageNum(int num){
//        pageNum = num;
//    }

    //private static BrowsingManager INSTANCE = new BrowsingManager();

    private void initPreviewRects(){
        for(int i = 1; i < 10; i++){
            previewRectList.add(new PreviewRect(this, i));
        }
    }

    private void updatePreviewRectDisplay(BrowsingPreview.DIRECTION direction){
        for(PreviewRect pr : previewRectList){
            pr.updateOnPageChange(direction);
        }
    }

    void setInstancePlayLocked(Activity a){
        pageLocked = PageLocked.build(a, this.activity, this);
        pageLocked.update(PurchasesManager.getProductDetails(Globals.getPageNum()));
    }

    /**
     * initializes current world offset based on location of the page
     * this is only the initial world offset
     */
    void initWorldOffset(){
        //this.worldOffset = Globals.SCREEN_WIDTH * (Globals.getPageNum() - 1);
        //this.worldOffset = 0; //start on the first MainPage
        this.worldTransformation = 0;

        //start the world at the very front always
        Globals.pageNum = 0; //start page at 0, without storing in SHaredPref

        this.worldTransformationOriginal = 0;
    }

    /**
     * for browsing preveiw to access world position and draw their data and check for selection from the correct place
     * @return transformation amount
     */
    public float getWorldTransformation(){
        return this.worldTransformation;
    }

    /**
     * for preview classes to call the BrowsingManager and check if the world is currently transitioning or not.
     * @return boolean status
     */
    public boolean isWorldTransitioning(){
        return this.bWorldTransitioning;
    }

    /**
     * initializes the transition tween with a direction
     * @param direction
     */
    private void startTransition(BrowsingPreview.DIRECTION direction){
        this.transitionDirection = direction;
        transitionCount = 0;
        bWorldTransitioning = true;
        settingsPopup.initFadeOut();
        updatePreviewRectDisplay(direction);
    }

    /**
     * when sliding to a specific page
     */
    private void startTransitionToPage(){
        transitionPageCount = 0;
        bWorldTransitioningToPage = true;
        settingsPopup.initFadeOut();
    }

    /**
     * using easing functions in TweenManager class to smoothly move and swipe the page across
     * is updated if this.bWorldTransitioning = true;
     */
    private void transitionWorld(){

        transitionCount++;
        if(transitionCount < TRANSITIONFRAMES + 1) {
            if (transitionDirection == BrowsingPreview.DIRECTION.LEFT) {
                this.worldTransformation = TweenManager.easeInOutCubic(  transitionCount, worldTransformationOriginal, -TRANSITIONDISTANCE, TRANSITIONFRAMES); //TRANSITIONFRAMES - transitionCount
            } else if (transitionDirection == BrowsingPreview.DIRECTION.RIGHT) {
                this.worldTransformation = TweenManager.easeInOutCubic(transitionCount, worldTransformationOriginal, TRANSITIONDISTANCE, TRANSITIONFRAMES);
            }
        }
        else {
            //switch off boolean trigger
            bWorldTransitioning = false;
            //reset tween frames
            transitionCount = 0;
            //save the current transformation as a constant for a relative position for the next tween .
            worldTransformationOriginal = worldTransformation;
            //fade in settings button
            settingsPopup.initFadeIn();
            updatePreviewRectDisplay(null);
        }
    }

    private void transitionToPage(){
        transitionPageCount++;
        if(transitionPageCount < this.tweenToPageFrames + 1){
            if(tweenToPageDistance < 0){
                this.worldTransformation = TweenManager.easeInOutCubic(transitionPageCount, worldTransformationOriginal, tweenToPageDistance, tweenToPageFrames);
            }
            else if(tweenToPageDistance > 0){
                this.worldTransformation = TweenManager.easeInOutCubic(transitionPageCount, worldTransformationOriginal, tweenToPageDistance, tweenToPageFrames);
            }
        }
        else {
            bWorldTransitioningToPage = false;
            transitionCount = 0;
            worldTransformationOriginal = worldTransformation;
            //action bar fade in
            settingsPopup.initFadeIn();
            updatePreviewRectDisplay(null);
        }
    }

    public BrowsingManager(Activity activity, int pageNo){
        this.activity = activity;
        this.settingsPopup = new SettingsPopup(Globals.SCREEN_WIDTH / 15 * 13, Globals.SCREEN_HEIGHT / 50, this.activity, false);
        lvlArray = LevelManager.getInstance().getLevelData();
        Log.i("Loaded levels: ",lvlArray.size()+"");
        lvlPrevList = new ArrayList<BrowsingPreview>();
        this.previewRectList = new ArrayList<PreviewRect>();
        //setPageNum(pageNo);
        prevPage = JuunoButton.create(1,14,1.5f,1.5f,"PV");
        nextPage = JuunoButton.create(8,14,1.5f,1.5f,"NX");
        prevPage.setEnabled(false);
        nextPage.setEnabled(false);
        setInstancePlayLocked(activity);
        initWorldOffset();
        //initLevels(pageNo);
        initAllLevels();
        initPreviewRects();
        handler = new Handler();
    }

    private void rest(int ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * resets the from color and tOcolor to defaults, so no transition of colours/.
     */
    public void resetFromPageToPage(){
        setFromPageToPage(0, 0);
    }

    public void setFromPageToPage(int fromPage, int toPage){
        this.fromPage = fromPage;
        this.toPage = toPage;
    }
    public final int getFromPage(){
        return this.fromPage;
    }
    public final int getToPage(){
        return this.toPage;
    }
    public final Activity getActivity(){ return this.activity; }
}

package io.github.juunoco.blobmobile;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import java.util.Collections;
import java.util.Random;

import io.github.juunoco.blobmobile.ui.ParticleEffect;
import io.github.juunoco.blobmobile.ui.ShatterIntoParticles;

/**
 * Created by Donna on 6/10/2017.
 */

public class LevelActivity extends AppCompatActivity implements Runnable {//extends MainPage

    Context context;
    LevelView levelBrowsing;
    private boolean bOnMainPage;
    BrowsingManager browsingManager;
    private BrowsingPreview lvlSelected = null;
    private Random rand = new Random();
    private ParticleEffect lvlUnlockingParticles;
    private Thread thread;
    private Handler handler;
    private Runnable r1, r2;
    long initialTime = System.nanoTime();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        bOnMainPage = false;
        handler = new Handler();
        //preload this, it takes too long to load
        browsingManager =  new BrowsingManager(this, Globals.getPageNum()); //BrowsingManager.getInstance();
        browsingManager.setInstancePlayLocked(this);
        levelBrowsing = new LevelView(browsingManager, context, this);
        this.lvlUnlockingParticles = new ParticleEffect(this, levelBrowsing, null);
        //levelBrowsing = new LevelView(BrowsingManagerViewPager._sharedInstance, context);

        setContentView(levelBrowsing);
        hideNavigationBar();
        setupClicks();

        //start first sound
        SoundManager.transitionMusic("none", "menuMusic", false);

        initRunnables();

    }

    void initRunnables(){
        r1 = new Runnable(){
            @Override
            public void run(){
                //Globals.bUnlockingAnimationTriggered = true;
                onSwipeLeft();
            }
        };

        r2 = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < browsingManager.lvlPrevList.size(); i++) {
                    BrowsingPreview bp = browsingManager.lvlPrevList.get(i);
                    if (bp.getLevel().number == Globals.iLvlToUnlock) {
                        if(bp.bLocked) {
                            bp.setWobbling(true);
                            bp.unlockingParticles = lvlUnlockingParticles;
                            bp.shatterParticles = ShatterIntoParticles.createShatterParticlesForLock(LevelActivity.this, levelBrowsing);
                            //have to move this to the end of the arrya to make it render last, but also makes it flicker...
                            //Collections.swap(browsingManager.lvlPrevList, i, browsingManager.lvlPrevList.size() - 1);
                        }
                        //break this loop since the preveiw was found, and try to avoid concurrent modification
                        break;
                    }
                }
            }
        };
    }

    void hideNavigationBar(){
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        decorView.setSystemUiVisibility(uiOptions);

        levelBrowsing.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus)
        {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_IMMERSIVE
            );
            hideNavigationBar();
        }
    }

    private void setupClicks(){
        levelBrowsing.setOnTouchListener(new View.OnTouchListener() {

            final GestureDetector gestureDetector = new GestureDetector(context, new GestureListener());

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                boolean bFlingConsumed = gestureDetector.onTouchEvent(event);

                if (!bFlingConsumed) checkTouch(event);
                return true;
            }
        });
    }

    private void checkTouch(MotionEvent e) {
        if(!browsingManager.onTouch(e)) {
            //if(Globals.iLvlToUnlock == 0) {
                lvlSelected = browsingManager.levelSelect(e);
            //}
        }
        if (e.getAction() == MotionEvent.ACTION_UP) {
            if (lvlSelected != null) {
                //if(!Globals.bUnlockingAnimationTriggered) {
                if(Globals.iLvlToUnlock == 0 && browsingManager.bTransitioning == false){
                    //start scaling here
                    this.browsingManager.mLevelSelected = lvlSelected;
                    //start glowing
                    this.browsingManager.mLevelSelected.startGlowing();
                    //goToLevel();
                }
                //if Globals.bUnlockingAnimationTriggered is true, means animation is still ongoing.
            }
        }
    }

    public void goToLevel(){
        Globals.setSolved(false);
        Globals.bInGameSequence = true;
        Intent intent = new Intent(context, GameActivity.class);
        intent.putExtra("preloading", false);
        context.startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        this.browsingManager.bLevelSelected = false;

    }

    public void onSwipeLeft(){
        browsingManager.setNextPage();
    }

    public void onSwipeRight(){
        browsingManager.setPrevPage();
    }

    public void onSwipeTop(){
        //SoundManager.playClickReturn();
        //Intent intent = new Intent(context, MainPage.class);
        //startActivity(intent);
        //overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
    public void onSwipeBottom(){
        //NO ACTION
    }

    //anonymous gesture listener tied to SurfaceView
    final class GestureListener extends GestureDetector.SimpleOnGestureListener {

        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            boolean result = false;
            //if (!Globals.bUnlockingAnimationTriggered) { //check if an animation is happening
            if (Globals.iLvlToUnlock == 0 && !browsingManager.bTransitioning && !browsingManager.bWorldTransitioning && !browsingManager.bWorldTransitioningToPage){
                try {
                    float diffY = e2.getY() - e1.getY();
                    float diffX = e2.getX() - e1.getX();
                    if (Math.abs(diffX) > Math.abs(diffY)) {
                        if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                            if (diffX > 0) {
                                onSwipeRight();
                                levelBrowsing.updateBackgroundColor(Blob.DIRECTION.RIGHT);
                            } else {
                                onSwipeLeft();
                                levelBrowsing.updateBackgroundColor(Blob.DIRECTION.LEFT);
                            }
                            result = true;
                        }
                    } else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffY > 0) {
                            onSwipeBottom();
                        } else {
                            onSwipeTop();
                        }
                        result = true;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            return result;
        }
    }

    @Override
    public void onBackPressed(){
        //if(this.viewPager.getCurrentItem() == 0)
        //go to the previous activity, MainPage Activity.
//        SoundManager.playClickReturn();
//        Intent intent = new Intent(this, MainPage.class);
//        startActivity(intent);
//        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        moveTaskToBack(true);

    }

    @Override
    protected void onResume() {
        super.onResume();
        hideNavigationBar();
        //SoundManager.resumeBackgroundMusic();
        bOnMainPage = true;
        thread = new Thread(this);
        thread.start();
        SoundManager.resumeBackgroundMusic();
        levelBrowsing.resume();
        SoundManager.soundState = SoundManager.SOUNDSTATE.MENU;
        SoundManager.transitionMusic("playingMusic", "menuMusic", false);
        SoundManager.transitionMusic("transitionMusic", "menuMusic", false);
        if(SoundManager.bIsPlayingLeveltransitionMusic) {
            SoundManager.bIsPlayingLeveltransitionMusic = false;
            handler.postDelayed(new Runnable(){
                @Override
                public void run() {
                    SoundManager.transitionMusic("transitionMusic", "menuMusic", false);
                }
            }, 1500);
        }


        //check if a lvl should be unlocked
        if(Globals.iLvlToUnlock != 0){
            //start chance to show App Rating Dialog, and fi player is past lvl 5
            if (rand.nextInt(100) > Globals.RATING_PERCENTAGE && Globals.getCurrLvlNum() > 10) {
                Dialog d = AppRater.appLaunched(LevelActivity.this, this);
                if(d != null) {
                    d.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(final DialogInterface dialog) {
                            //wait until Apprater dialog is closed before moving to the next activity.
                            //synchronously start unlock animation for this page
                            checkTriggerAnimation();
                        }
                    });
                }
            }
            else{
                checkTriggerAnimation();
            }
        }
    }

    void checkTriggerAnimation(){
        //check if the current page si the correct page.
        //<= (Globals.getPageNum() + 1 ) * 9
        if(Globals.getCurrLvlNum() % 9 == 0 ){
            //scroll to the correct page (next page) automatically, then trigger animation
            triggerAnimation(true);
        }
        else{
            //we are on the correct page, directly unlock it.
            triggerAnimation(false);
        }
    }

    void triggerAnimation(boolean bSwiping) {

        if (bSwiping){
            handler.postDelayed(r1, 1000);
        }

        handler.postDelayed(r2, bSwiping ? 2000 : 500);
    }


    @Override
    protected void onPause() {
        super.onPause();
        levelBrowsing.pause();
        bOnMainPage = false;
        SoundManager.stopBackgroundMusic();
        try{
            thread.join();
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        handler.removeCallbacks(r1, r2);
        thread = null;
    }

    @Override
    public void run() {
        final double timeU = 1000000000 / 20;
        double deltaU = 0;

        while (bOnMainPage){

            long currentTime = System.nanoTime();
            deltaU += (currentTime - initialTime) / timeU;
            initialTime = currentTime;

            if(deltaU >= 1){
                SoundManager.update();
                deltaU--;
            }
        }
    }

    public final View getLevelView(){
        return this.levelBrowsing;
    }
}

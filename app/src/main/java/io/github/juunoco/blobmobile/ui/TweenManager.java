package io.github.juunoco.blobmobile.ui;


/**
 * inspired by https://joshondesign.com/2013/03/01/improvedEasingEquations
 * and https://github.com/jesusgollonet/processing-penner-easing/blob/master/src/Elastic.java
 */

import android.graphics.RectF;

import io.github.juunoco.blobmobile.Blob;
import io.github.juunoco.blobmobile.Globals;

//static methods managing all sorts of tweens
//t = current time
//b = start value
//c = change in value
//d = duration
public class TweenManager {

    //duration of the tween
    public static final int SLICE_FRAMES_MAX = 3; //6
    //distance tween moves
    public static final int SLICE_FRAMES = 8; //3

    //disable cuts while tween is happening, this shouldnt be necessary, but there's a nasty bug
    public static boolean b_nothingIsTweening = true;

    //duration of browsing tween movement
    public static final float BROWSING_FRAMES_TOTAL = 10; //10

    //quadratic ease out, decelerating to 0 velocity.
    public static float easeOutQuad(float t, float b, float c, float d){
        t /= d;
        return -c * t * (t - 2) + b;
    }

    //quadratic ease out
    public static float easeInQuart(float t, float b, float c, float d){
        t /= d / 2;
        if (t < 1){
            return c / 2 * t * t + b;
        }
        return -c / 2 * (t * (t - 2) - 1) + b;
    }

    //simple linear tweening - no easing, no acceleration
    public static float linearTween(float t, float b, float c, float d){
        return c * t / d + b;
    }

    public static float easeOutElastic(float t, float b, float c, float d){
        if (t == 0) return b;
        if((t /= d) == 1) return b + c;
        float p = d*.3f;
        float a = c;
        float s = p / 4;
        return (a*(float)Math.pow(2, -10*t) * (float)Math.sin( (t * d - s) * ( 2 * (float)Math.PI)/p) + c + b);
    }

    public static float easeInOutElastic(float t, float b, float c, float d){
        if (t==0) return b;  if ((t/=d/2)==2) return b+c;
        float p=d*(.3f*1.5f);
        float a=c;
        float s=p/4;
        if (t < 1) return -.5f*(a*(float)Math.pow(2,10*(t-=1)) * (float)Math.sin( (t*d-s)*(2*(float)Math.PI)/p )) + b;
        return a*(float)Math.pow(2,-10*(t-=1)) * (float)Math.sin( (t*d-s)*(2*(float)Math.PI)/p )*.5f + c + b;
    }

    public static float easeInOutCubic(float t, float b, float c, float d){
        if ((t/=d/2) < 1) return c/2*t*t*t + b;
        return c/2*((t-=2)*t*t + 2) + b;
    }

    public static float easeOutCubic(float t, float b, float c, float d){
        return c*((t=t/d-1)*t*t + 1) + b;
    }

    public static float easeOutBounce(float t,float b , float c, float d) {
        if ((t/=d) < (1/2.75f)) {
            return c*(7.5625f*t*t) + b;
        } else if (t < (2/2.75f)) {
            return c*(7.5625f*(t-=(1.5f/2.75f))*t + .75f) + b;
        } else if (t < (2.5/2.75)) {
            return c*(7.5625f*(t-=(2.25f/2.75f))*t + .9375f) + b;
        } else {
            return c*(7.5625f*(t-=(2.625f/2.75f))*t + .984375f) + b;
        }
    }

    //wobble effect for when user presses on a locked level
    //takes in a count for number of step
    //returns float for degrees of wobble
    public static float wobbleDegrees(int count){
        if(count == 1){
            return 0.7f;
        }
//        else if(count == 2){
//            return 2;
//        }
//        else if(count == 3){
//            return -2;
//        }
        else{
            return -0.7f;
        }
    }

    //increments wobble animation by returning to first frame if reached max frames
    public static int wobbleIncrement(int count){
        switch(count){
            case 0 : return 1;
//            case 1 : return 2;
//            case 2 : return 3;
//            case 3 : return 4;
            default : return 0;
        }
    }

    private static final float changeAmt = Globals.SCREEN_WIDTH / 400;
    public static RectF scaleRect(RectF currentRect, int count){
        return new RectF(
                currentRect.left - (changeAmt * count),
                currentRect.top - (changeAmt * count),
                currentRect.right + (changeAmt * count),
                currentRect.bottom + (changeAmt * count)
                );
    }

    /**
     * change the alpha of the goals with a simple linear tween, can be going down or up
     * @param count current progress of animation
     * @return new alpha value
     */
    private final static int ALPHA_CHG_AMT = 10;
    public static int changeAlpha(int count, Blob.DIRECTION alphaDirection){
        switch(alphaDirection){
            case UP : return count < 255 - ALPHA_CHG_AMT ? count + ALPHA_CHG_AMT : 255;
            default : return count > 0 + ALPHA_CHG_AMT ? count - ALPHA_CHG_AMT : 0;
        }
    }

    /**
     * shattering effect tween
     * t is current frame count
     * b is starting point of tween
     * c is total change in value of tween
     * d is the total number of frames
     */
    public static class ShatteringTween {

        //if increasing, use this tween
        public static float easeIn(float t, float b, float c, float d){
            return c * ( t /= d) * t * t + b;
        }

        //if decreasing, and want to slow down, use this tween
        public static float easeOut(float t, float b, float c, float d){
            return c * (( t = t / d - 1) * t * t + 1) + b;
        }
    }
}

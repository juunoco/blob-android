package io.github.juunoco.blobmobile;

import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.util.Log;

/**
 * Created by Donna on 4/09/2017.
 */

public class ColourScheme {
    @ColorInt private static final int DEFAULT = 0xFFCCCCCC;            //0
    @ColorInt private static final int CORAL = 0xFFF26E51;          //1
    @ColorInt private static final int FLAME = 0xFFF7AD41;   //2
    @ColorInt private static final int MUSTARD = 0xFFF8D733;   //3
    @ColorInt private static final int BASIL = 0xFF8AC76E;          //4
    @ColorInt private static final int MINT = 0xFF64C29E;    //5
    @ColorInt private static final int CYAN = 0xFF54A1D8;           //6
    @ColorInt private static final int AEGEAN = 0xFF737FBD;            //7
    @ColorInt private static final int PURPLE = 0xFF8F68AB;      //8
    @ColorInt private static final int MAGENTA = 0xFFBF5FA4;            //9
    @ColorInt private static final int FUSCHIA = 0xFFEE508C;            //10
    @ColorInt private static final int BG = 0xFFffffff;
    @ColorInt private static final int BACKGROUND_1 = 0xFFf7d642;
    @ColorInt private static final int BACKGROUND_2 = 0xFF87f741;
    @ColorInt private static final int BACKGROUND_3 = 0xFF6791db;
    @ColorInt private static final int BACKGROUND_4 = 0Xffe23d3d;
    @ColorInt private static final int BACKGROUND_FINAL = 0xFF000000;

    @ColorInt private static final int PREVIEW =  0xFFD0C69B;

    @ColorInt private static final int ERROR = 0xFFFF0000;

    public static int getPreviewColour(){
        return PREVIEW;
    }

    public static int getColour(int cut){
        if(cut < 0 && cut > 10){
            Log.e("INVALID CUT","Parameter value invalid");
        }
        switch (cut){
            case 0: return DEFAULT;
            case 1: return CORAL;
            case 2: return FLAME;
            case 3: return MUSTARD;
            case 4: return BASIL;
            case 5: return MINT;
            case 6: return CYAN;
            case 7: return AEGEAN;
            case 8: return PURPLE;
            case 9: return MAGENTA;
            case 10: return FUSCHIA;
        }
        return DEFAULT;
    }

    public static int getBg(int cut){
        switch(cut){
            case 1 : return BG;
            case 2 : return BACKGROUND_1;
            case 3 : return BACKGROUND_2;
            case 4 : return BACKGROUND_3;
            case 5 : return BACKGROUND_4;
            case 6 : return BACKGROUND_FINAL;
            default: return BG;
        }
    }

    public static int getErrorColor(){
        return ERROR;
    }

    public static int getLockedColor(){
        return Color.argb(255, 180, 180, 180);
    }
}

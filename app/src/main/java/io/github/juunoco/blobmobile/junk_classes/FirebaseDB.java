package io.github.juunoco.blobmobile.junk_classes;

/**
 * Created by Jun on 28/03/2018.
 * Firebase database to store persistent data
 * Will store the unlocked hint slices.
 * Will store the current number of consumables present.
 */

public class FirebaseDB {

    public static FirebaseDB _sharedInstance = new FirebaseDB();

    private String developerPayload;

    public FirebaseDB(){
    }

    /**
     * authenticates to FirebaseRealtimeDatabase server
     * Using username as key.
     */
    public void authenticate(String developerPayload){
        this.developerPayload = developerPayload;
    }

    /**
     * writes to database that a hint slice has been unlocked
     * @param lvlNo for the lvl hints we are dealing with
     */
    public void writeHintUnlocked(int lvlNo){

    }

    /**
     * writes to database the current number of available hints
     */
    public void writeCurrentAvailableHintsAmt(){

    }

    /**
     * reads which slice the hint is unlocked to for each level
     * For example, if level is 1, and slice is 1, then slice 1 hint has been unlocked, but not slice 2.
     * If lvl = 5, slice = 10, slice 1, slice 10 has been unlocked for level 5 but slice 11 is locked.
     * If a hint slice has been unlocked, it will remain unlocked for lifetime.
     */
    public void readSliceUnlockedData(){

    }

    /**
     * reads current avaialbel hints number for this user
     */
    public void readCurrentAvailableHintsAmt(){

    }

}

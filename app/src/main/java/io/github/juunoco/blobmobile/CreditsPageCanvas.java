package io.github.juunoco.blobmobile;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.MotionEvent;

import io.github.juunoco.blobmobile.BrowsingManager;
import io.github.juunoco.blobmobile.Globals;
import io.github.juunoco.blobmobile.SoundManager;
import io.github.juunoco.blobmobile.SplashActivity;

/**
 * Created by Jun on 25/03/2018.
 * draws the credits as a canvas rather than a layout
 * This is a page in the BrowsingManager, and is the last page
 */

public class CreditsPageCanvas {

    private final float PAGEMARGIN = Globals.SCREEN_WIDTH;
    private float transformedX;
    private Bitmap creditsImg, playImg;
    private float initialCreditsX, initialPlayX, creditsY, playY;
    private float transformedCreditsX, transformedPlayX;
    private BrowsingManager browsingManager;
    private int pageNo;

    public CreditsPageCanvas(BrowsingManager bm, int pageNo){
        this.browsingManager                = bm;
        this.pageNo                         = pageNo;

        initPageOffset();
        initImages();
        initPosition();
    }

    private void initPageOffset(){
        //int pageDiff                = Globals.MAX_PAGES; //this.pageNo - Globals.getPageNum();
        this.transformedX           = (Globals.MAX_PAGES + 1) * PAGEMARGIN; //initial position of transformation
    }

    private void initImages(){
        creditsImg = Bitmap.createScaledBitmap(SplashActivity.creditsImg, (int)(Globals.SCREEN_WIDTH / 1.2f), (int)(Globals.SCREEN_HEIGHT / 2f), false);
        playImg = SplashActivity.playButton;
    }

    private void initPosition(){
        this.initialCreditsX = (Globals.SCREEN_WIDTH / 2f) - (creditsImg.getWidth() / 2f);
        this.creditsY = (Globals.SCREEN_HEIGHT / 2f) - (creditsImg.getHeight() / 2f);
        this.initialPlayX = (Globals.SCREEN_WIDTH / 2f) - (playImg.getWidth() / 2f);
        this.playY = (Globals.SCREEN_HEIGHT / 3f * 2);

        initialCreditsX = transformedCreditsX = initialCreditsX + transformedX;
        initialPlayX = transformedPlayX = initialPlayX + transformedX;
    }

    public void update(){
        this.transformedX           = this.browsingManager.getWorldTransformation();
        this.transformedPlayX       = this.initialPlayX + transformedX;
        this.transformedCreditsX    = this.initialCreditsX + transformedX;
    }

    public void draw(Canvas c){
        c.drawBitmap(creditsImg, transformedCreditsX, creditsY, null);

//        c.save();
//        c.rotate(180, (int)(playImg.getWidth() / 2f), (int)(playImg.getHeight() / 2f));
//        c.drawBitmap(playImg, transformedPlayX, playY, null);
//        c.restore();
    }

    public boolean isMenuClicked(MotionEvent e){

        if(e.getAction() == MotionEvent.ACTION_UP){
            if(e.getX() > transformedPlayX
                    && e.getX() < transformedPlayX + playImg.getWidth()
                    && e.getY() > playY
                    && e.getY() < playY + playImg.getHeight()){
               // return true;
            }
        }
        return false;
    }

}


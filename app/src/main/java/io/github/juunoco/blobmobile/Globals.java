package io.github.juunoco.blobmobile;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.util.TypedValue;

import java.util.ArrayList;

import io.github.juunoco.blobmobile.ui.HintManager;

/**
 * Created by Donna on 27/10/2017.
 */

public class Globals {

    static final int LAST_LEVEL_OF_TUTORIAL = 3;

    public static boolean bInGameSequence = false;
    static Bitmap retry, levels;
    public static Bitmap browsingCircleSmall, browsingCircleBig, browsingCircleTitleBig, browsingCircleTitleSmall, browsingCircleCreditsBig, browsingCircleCreditsSmall, background;
    private static boolean browsing, inTransition, solved, destroyMode, hintMode, dropMode;
    private static boolean bReset = false;
    private static boolean bPurchasedPremium1, bPurchasedHints1, bPurchasedHints2, bPurchasedHintsHalf, bNoAds = false, bBillingLoadingComplete = false;
    private static int currLvlNum, unlockedLvl, totalSlices;
    public static int pageNum;
    private static Typeface orbitron;
    public static int MAX_PAGES;
    public static float SCREEN_WIDTH, SCREEN_HEIGHT, CELL_WIDTH, AD_HEIGHT, LVL_GRID_CELL, EXTENDED_HEIGHT;

    //font sizes
    public static int BLOB_TEXT_SIZE, TUTORIAL_TEXT_SIZE_BIG, TUTORIAL_TEXT_SIZE_SMALL, PREVIEW_TEXT_SIZE, HINT_SLICE_SIZE;
    public static int BLOB_RADIUS, HINT_IMG_SIZE, BROWSINGCIRCLESELECTEDSIZE, BROWSINGCIRCLENORMALSIZE;

    //Shared Preference variables
    /**
     * Holds a single reference to a SharedPreference
     */
    public static SharedPreferences _sharedPreferences;
    public static SharedPreferences.Editor _sharedPreferencesEditor;

    /** Shared preferences for the current browsing level, total slices, and unlocked level
     *
     */
    public static final String prefCurrentPage = "CURRENT_LEVEL";
    public static final String prefTotalSlices= "TOTAL_SLICES";
    public static final String prefUnlockedLevel = "UNLOCKED_LEVEL";
    public static final String prefLastRewardedAdProvided = "LAST_REWARDED_AD_PROVIDED";
    public static final String prefRewardedAdAvailable = "REWARDED_AD_AVAILABLE";
    public static final String prefHintsAmt = "HINTS_AMOUNT";
    public static final String prefHintsUnlocked = "HINTS_UNLOCKED";

    /**
     * Booleans in shared preferences, updated whenever a network connection is established
     * For whether the player has purchased a level pack or not.
     */
    public static final String prefPurchasedPremium1 = "PURCHASED_PREMIUM_1";
    public static final String prefPurchasedHints1 = "PURCHASED_HINTS_1";
    public static final String prefPurchasedHints2 = "PURCHASED_HINTS_2";
    public static final String prefPurchasedHintsHalf = "PURCHASED_HINTS_HALF";

    /**
     * The audio settings are saved persistently, so if player changes volume, or mutes, this will persist into the next play
     */
    public static final String prefAudioSettings = "AUDIO_SETTINGS";
    public static final String prefFXSettings = "FX_SETTINGS";

    /**
     * When a level to unlock exists, this will have the level to unlock
     * After the level ahs been unlocked it will return to 0;
     * If 0, the player has simply entered the browsing state, otherwise they have unlocked a new level.
     */
    public static int iLvlToUnlock = 0;

    /**
     * This boolean is true when an unlocking animation is being staged after a player has unlocked a new level.
     * When true, clicking is disabled, and the BrowsingPreview which has finished unlocking will trigger it to false, and allow clicks once more.
     */
    public static boolean bUnlockingAnimationTriggered = false;

    /**
     * The 100 - percentage likelihood that interstitial ads, or a rating dialog is likely to be displayed.
     */
    public static int INTERSTITIAL_PERCENTAGE = 90;
    public static int RATING_PERCENTAGE = 90;

    //clamps a value within a range
    public static int clamp(int input, int min, int max){
        if (input < min) return min;
        if (input > max) return max;
        return input;
    }

    public static int getPageNum() {
        return pageNum;
    }

    public static void setPageNum(int pageNum) {
        Globals.pageNum = pageNum;
        if(pageNum != 0 && pageNum != Globals.MAX_PAGES + 1) {
            Globals._sharedPreferencesEditor.putInt(Globals.prefCurrentPage, Globals.getPageNum());
            Globals._sharedPreferencesEditor.commit();
        }
    }

    public static int getUnlockedLevel(){
        return Globals.unlockedLvl;
    }
    public static void setUnlockedLvl(int unlockedLevel){
        Globals.unlockedLvl = unlockedLevel;
        Globals._sharedPreferencesEditor.putInt(Globals.prefUnlockedLevel, Globals.unlockedLvl);
        Globals._sharedPreferencesEditor.commit();
    }

    public static String getHintsUnlockedForEachLevel() {
        return Globals._sharedPreferences.getString(Globals.prefHintsUnlocked, "0_0:0_0");
    }

    public static void setNoAds(boolean b){ Globals.bNoAds = b; }
    public static boolean getNoAds(){ return Globals.bNoAds; }
    public static void setBillingLoadingComplete(boolean b){ Globals.bBillingLoadingComplete = b; }
    public static boolean getBillingLoadingComplete(){ return Globals.bBillingLoadingComplete; }

    public static int getTotalSlices(){
        return Globals.totalSlices;
    }
    public static void setTotalSlices(int newSlices){
        Globals.totalSlices = newSlices;
        Globals._sharedPreferencesEditor.putInt(Globals.prefTotalSlices, Globals.totalSlices);
        Globals._sharedPreferencesEditor.commit();
    }
    public static boolean getPurchasedPremium1(){
        return Globals.bPurchasedPremium1;
    }
    public static void setPurchasedPremium1(boolean b){
        Globals.bPurchasedPremium1 = b;
    }

    public static boolean getPurchasedHints1() { return Globals.bPurchasedHints1; }
    public static boolean getPurchasedHints2() { return Globals.bPurchasedHints2; }
    public static boolean getPurchasedHintsHalf() { return Globals.bPurchasedHintsHalf; }
    public static void setPurchasedHints1(boolean b) { Globals.bPurchasedHints1 = b; }
    public static void setPurchasedHints2(boolean b) { Globals.bPurchasedHints2 = b; }
    public static void setPurchasedHintsHalf(boolean b) { Globals.bPurchasedHintsHalf = b; }

    public static void storeHintsAmt(){
        Globals._sharedPreferencesEditor.putInt(Globals.prefHintsAmt, HintManager.getInstance().getAvailableHints());
        Globals._sharedPreferencesEditor.commit();
    }

    public static void initHintsAmt(){
        int hintNo = Globals._sharedPreferences.getInt(Globals.prefHintsAmt, 0);
        HintManager.getInstance().initAvailableHints(hintNo);
    }

    public static void writeCurrentAudioSetting(boolean b){
        Globals._sharedPreferencesEditor.putBoolean(Globals.prefAudioSettings, SoundManager.bAudioMuted);
        Globals._sharedPreferencesEditor.commit();
    }
    public static void writeCurrentAudioFXSetting(boolean b){
        Globals._sharedPreferencesEditor.putBoolean(Globals.prefFXSettings, SoundManager.bFXMuted);
        Globals._sharedPreferencesEditor.commit();
    }

    public static int getCurrLvlNum() {
        return currLvlNum;
    }

    static void setCurrLvlNum(int num) {
        LevelManager.getInstance().getLevelData().get(num).reset();
        currLvlNum = num;
        Globals._sharedPreferencesEditor.putInt(Globals.prefUnlockedLevel, unlockedLvl);
        Globals._sharedPreferencesEditor.commit();
    }

    public static boolean isSolved() {
        return solved;
    }

    public static boolean isResetting() { return bReset; }

    public static void setSolved(boolean solved) {
        Globals.solved = solved;
    }

    public static void setResetting(boolean b) { bReset = b; }

    /**
     * sets the time since the last rewarded ad was provided
     * @param lastRewardedAdProvided
     */
    public static void setLastRewardedAdProvided(long lastRewardedAdProvided){
        Globals._sharedPreferencesEditor.putLong(Globals.prefLastRewardedAdProvided, lastRewardedAdProvided);
        Globals._sharedPreferencesEditor.commit();
    }

    public static long getLastRewardedAdProvided(){ return Globals._sharedPreferences.getLong(Globals.prefLastRewardedAdProvided, System.currentTimeMillis()); }

    /**
     * checks if a rewarded ad is available to the user
     * @return
     */
    public static boolean isRewardedAdAvailable(){ return Globals._sharedPreferences.getBoolean(Globals.prefRewardedAdAvailable, false); }

    static boolean isInTransition() {
        return inTransition;
    }

    static void setInTransition(boolean inTransition) { Globals.inTransition = inTransition; }

//    static boolean isBrowsing() {
//        return browsing;
//    }
//
//    static void setBrowsing(boolean browsing) {
//        Globals.browsing = browsing;
//    }

    /**
     * USed to check the draw color of the TouchLine
     * @return true if colour to draw is red
     */
    static boolean isDestroyMode() {
        return destroyMode;
    }

    /**
     * Used to check the draw color is a hint mode or not.
     * @return true if is hint mdoe
     */
    static boolean isHintMode() {
        return hintMode;
    }

    static void setHintMode(boolean hintMode){
        Globals.hintMode = hintMode;
    }

    static void setDestroyMode(boolean destroyMode) {
        Globals.destroyMode = destroyMode;
    }

    public static boolean isDropMode() {
        return dropMode;
    }

    public static void setDropMode(boolean dropMode) {
        Globals.dropMode = dropMode;
    }

    public static Typeface getOrbitronFont(){
        return orbitron;
    }

    static boolean isTutorialMode(){
        return getCurrLvlNum() <= LAST_LEVEL_OF_TUTORIAL;
    }

    private static void loadImages(Context context){
        //shadow = BitmapFactory.decodeResource(context.getResources(),R.drawable.effect_shadow);
        //shine = BitmapFactory.decodeResource(context.getResources(),R.drawable.effect_shine);
        retry = BitmapFactory.decodeResource(context.getResources(),R.drawable.actionbar_retry);
        //help = BitmapFactory.decodeResource(context.getResources(),R.drawable.bttn_help);
        levels = BitmapFactory.decodeResource(context.getResources(),R.drawable.actionbar_levels);
        background = BitmapFactory.decodeResource(context.getResources(),R.drawable.blobbs_main_bg);
        background = Bitmap.createScaledBitmap(background, (int) SCREEN_WIDTH, (int) Globals.SCREEN_HEIGHT,false);
       //background = Bitmap.createScaledBitmap(background, (int) SCREEN_WIDTH, (int) Globals.SCREEN_HEIGHT,false);
        //hints = BitmapFactory.decodeResource(context.getResources(), R.drawable.actionbar_hint);

        browsingCircleSmall = BitmapFactory.decodeResource(context.getResources(), R.drawable.circle_small);
        browsingCircleSmall = Bitmap.createScaledBitmap(browsingCircleSmall, Globals.BROWSINGCIRCLENORMALSIZE, Globals.BROWSINGCIRCLENORMALSIZE, false);
        browsingCircleBig = BitmapFactory.decodeResource(context.getResources(), R.drawable.circle_big);
        browsingCircleBig = Bitmap.createScaledBitmap(browsingCircleBig, Globals.BROWSINGCIRCLESELECTEDSIZE, Globals.BROWSINGCIRCLESELECTEDSIZE, false);
        browsingCircleTitleBig = BitmapFactory.decodeResource(context.getResources(), R.drawable.blob_big);
        browsingCircleTitleBig = Bitmap.createScaledBitmap(browsingCircleTitleBig, Globals.BROWSINGCIRCLESELECTEDSIZE, Globals.BROWSINGCIRCLESELECTEDSIZE, false);
        browsingCircleTitleSmall = BitmapFactory.decodeResource(context.getResources(), R.drawable.blob_small);
        browsingCircleTitleSmall = Bitmap.createScaledBitmap(browsingCircleTitleSmall, Globals.BROWSINGCIRCLENORMALSIZE, Globals.BROWSINGCIRCLENORMALSIZE, false);
        browsingCircleCreditsBig = BitmapFactory.decodeResource(context.getResources(), R.drawable.person_big);
        browsingCircleCreditsBig = Bitmap.createScaledBitmap(browsingCircleCreditsBig, Globals.BROWSINGCIRCLESELECTEDSIZE, Globals.BROWSINGCIRCLESELECTEDSIZE, false);
        browsingCircleCreditsSmall = BitmapFactory.decodeResource(context.getResources(), R.drawable.person_small);
        browsingCircleCreditsSmall = Bitmap.createScaledBitmap(browsingCircleCreditsSmall, Globals.BROWSINGCIRCLENORMALSIZE, Globals.BROWSINGCIRCLENORMALSIZE, false);
    }

    private static void initFonts(Context c){
        orbitron = Typeface.createFromAsset(c.getAssets(), "orbitron_reg.ttf");
    }

    public static void init(Context c){
        initFonts(c);
        pageNum = 1;
        browsing = false;
        inTransition = false;
        destroyMode = false;
        dropMode = false;
        solved = false;
        bUnlockingAnimationTriggered = false;

        Globals.BLOB_TEXT_SIZE = spToPixels(32, c);
        Globals.TUTORIAL_TEXT_SIZE_BIG = spToPixels(21, c);
        Globals.TUTORIAL_TEXT_SIZE_SMALL = spToPixels(16, c);
        Globals.PREVIEW_TEXT_SIZE = spToPixels(34, c);
        Globals.BLOB_RADIUS = spToPixels(12, c);
        Globals.HINT_SLICE_SIZE = spToPixels(4, c);
        Globals.HINT_IMG_SIZE =spToPixels(50, c);
        Globals.BROWSINGCIRCLENORMALSIZE = spToPixels(18, c);
        Globals.BROWSINGCIRCLESELECTEDSIZE = spToPixels(26, c);

        loadImages(c);
    }

    public static int spToPixels(float sp, Context context){
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_SP,
                sp,
                context.getResources().getDisplayMetrics()
        );
    }
}

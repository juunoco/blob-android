package io.github.juunoco.blobmobile.junk_classes;

/**
 * Created by Jun on 10/03/2018.
 */

public class BounceInterpolator implements android.view.animation.Interpolator {

    private double mAmplitude = 1;
    private double mFrequency = 10;

    BounceInterpolator(double amplitude, double frequency){
        mAmplitude = amplitude;
        mFrequency = frequency;
    }

    /**
     * Tween which onlly takes in time, and updates current scale
     * @param time, the frame which is currently being displayed
     * @return
     */
    public float getInterpolation(float time){
        return (float) (-1 * Math.pow(Math.E, -time / mAmplitude) * Math.cos(mFrequency * time) + 1);
    }

}

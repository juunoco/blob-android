//package io.github.juunoco.blobmobile;
//
//import android.app.Activity;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
///**
// * Created by Jun on 28/02/2018.
// * each page of the browsing view pager.
// */
//
//public class ScreenSlidePageFragment extends Fragment {
//
//    private LevelView levelView;
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
////        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.activity_browsing_page, container, false);
////        return rootView;
//
//        //retrieve args
//        Bundle args = getArguments();
//        int page = args.getInt("page");
//
//        //using own SurfaceView
//        BrowsingManager bm = new BrowsingManager(this.getActivity(), page);//BrowsingManager.getInstance();
//        bm.setInstancePlayLocked(this.getActivity());
//        this.levelView = new LevelView(bm, this.getContext());
//        //levelView.setZOrderOnTop(true);
//        //levelView.setZOrderMediaOverlay(true);
//        //levelView.surfaceHolder.setFixedSize((int)MainPage.SCREEN_WIDTH, (int)MainPage.SCREEN_HEIGHT);
//        this.levelView.run();
//        return levelView;
//    }
//
//    //have to call these, or it does not start drawing.
//    @Override
//    public void onResume() {
//        super.onResume();
//        levelView.resume();
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        levelView.pause();
//    }
//}

//package io.github.juunoco.blobmobile;
//
//import android.app.Activity;
//import android.database.CharArrayBuffer;
//import android.graphics.Bitmap;
//import android.net.Uri;
//import android.os.Parcel;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//
//import com.google.android.gms.auth.api.signin.GoogleSignIn;
//import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
//import com.google.android.gms.auth.api.signin.GoogleSignInClient;
//import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
//import com.google.android.gms.drive.Drive;
//import com.google.android.gms.games.Game;
//import com.google.android.gms.games.Games;
//import com.google.android.gms.games.Player;
//import com.google.android.gms.games.SnapshotsClient;
//import com.google.android.gms.games.snapshot.Snapshot;
//import com.google.android.gms.games.snapshot.SnapshotMetadata;
//import com.google.android.gms.games.snapshot.SnapshotMetadataChange;
//import com.google.android.gms.tasks.Continuation;
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.OnFailureListener;
//import com.google.android.gms.tasks.Task;
//
//import io.github.juunoco.blobmobile.ui.HintManager;
//
///**
// * Created by Jun on 30/03/2018.
// * Interacts with Google Play Games Service to make API calls to save the game state
// * https://developers.google.com/games/services/android/savedgames
// */
//
//public class GameService {
//
//    public static GameService _sharedInstance;
//
//    //private SnapshotsClient mSnapshotClient;
//    private GoogleSignInAccount mGoogleSignInAccount;
//    private GoogleSignInClient mGoogleSignInClient;
//    private Activity a;
//
//    public static GameService create(Activity a){
//        _sharedInstance = new GameService(a);
//        return _sharedInstance;
//    }
//
//    private GameService(Activity a){
//        this.a = a;
//        signInSilently(a);
//
//        //Games.getSnapshotsClient(a, mGoogleSignInAccount);
//    }
//
//    private void signInSilently(Activity a){
//        GoogleSignInOptions signInOptions =
//                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN)
//                    //Add the APPFOLDER scope for Snapshot support
//                    .requestScopes(Drive.SCOPE_APPFOLDER)
//                    .build();
//
//        GoogleSignInClient signInClient = GoogleSignIn.getClient(a, signInOptions);
//        signInClient.silentSignIn().addOnCompleteListener(a, new OnCompleteListener<GoogleSignInAccount>() {
//            @Override
//            public void onComplete(@NonNull Task<GoogleSignInAccount> task) {
//                if(task.isSuccessful()){
//                    onConnected(task.getResult());
//                } else {
//                    //Player will need ot sing in explicitly via UI
//                }
//            }
//        });
//    }
//
//    private void onConnected(GoogleSignInAccount signInAccount){
//        //TODO
//        mGoogleSignInAccount = signInAccount;
//    }
//
//    /**
//     * writes data to save,
//     * -current available hints
//     * -hints unlocked for each level
//     */
//    public void saveState(){
//        //create int format to transport through byte array
//        //0 means blank spot
//
//        //HintManager.getInstance().getAvailableHints(),
//
//
//    }
//
//    private byte[] intToByteArray(int a){
//        byte[] ret = new byte[4];
//        ret[3] = (byte) (a & 0xFF);
//        ret[2] = (byte) ((a >> 8) & 0xFF);
//        ret[1] = (byte) ((a >> 16) & 0xFF);
//        ret[0] = (byte) ((a >> 24) & 0xFF);
//        return ret;
//    }
//
//    private int byteArrayToInt(byte[] b){
//        return (b[3] & 0xFF) + ((b[2] & 0xFF) << 8) + ((b[1] & 0xFF) << 16) + ((b[0] & 0xFF) << 24);
//    }
//
//    private Task<SnapshotMetadata> writeSnapshot(Snapshot snapshot, byte[] data, @Nullable Bitmap coverImage, @Nullable String desc){
//        if(this.a != null && mGoogleSignInAccount != null) {
//            //set the data payload for the snapshot
//            snapshot.getSnapshotContents().writeBytes(data);
//
//            //create the change operation
//            SnapshotMetadataChange metadataChange = new SnapshotMetadataChange.Builder()
//                    //.setCoverImage(coverImage)
//                    //.setDescription(desc)
//                    .build();
//
//            SnapshotsClient snapshotsClient = Games.getSnapshotsClient(a, GoogleSignIn.getLastSignedInAccount(a));
//
//            //commit the operation
//            return snapshotsClient.commitAndClose(snapshot, metadataChange);
//        }
//
//        return null;
//    }
//
//    private Task<byte[]> loadSnapshot(){
//        //Display a progress dialog
//        // ...
//
//        //get the snapshots client from the signed in account
//        SnapshotsClient snapshotsClient = Games.getSnapshotsClient(a, GoogleSignIn.getLastSignedInAccount(a));
//
//        //in the case of conflict, the most recently modified version of this snapshot will be used
//        int conflictResolutionPolicy = SnapshotsClient.RESOLUTION_POLICY_MOST_RECENTLY_MODIFIED;
//
//        //open the saved game using its name
//            return snapshotsClient.open("default", true, conflictResolutionPolicy)
//                    .addOnFailureListener(new OnFailureListener() {
//                        @Override
//                        public void onFailure(@NonNull Exception e) {
//                            e.printStackTrace();
//                        }
//                    }).continueWith(new Continuation<SnapshotsClient.DataOrConflict<Snapshot>, byte[]>(){
//                        @Override
//                        public byte[] then(@NonNull Task<SnapshotsClient.DataOrConflict<Snapshot>> task) throws Exception {
//                            Snapshot snapshot = task.getResult().getData();
//
//                            //opening the snapshot was a success
//                            try {
//                                //extract the raw data from the snapshot
//                                return snapshot.getSnapshotContents().readFully();
//                            } catch(Exception e){
//                                //error while reading snapshot
//                                e.printStackTrace();;
//                            }
//                            return null;
//                        }
//                    }).addOnCompleteListener(new OnCompleteListener<byte[]>() {
//                        @Override
//                        public void onComplete(@NonNull Task<byte[]> task) {
//                            //Dismiss progress dialog and reflect changes in UI when complete
//                            // ...
//                            //TODO update game booleans to show if hints are available.
//                        }
//                    });
//
//
//    }
//}

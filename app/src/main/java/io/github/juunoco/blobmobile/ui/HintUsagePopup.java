package io.github.juunoco.blobmobile.ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.SparseArray;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

import io.github.juunoco.blobmobile.ActionBar;
import io.github.juunoco.blobmobile.GameActivity;
import io.github.juunoco.blobmobile.Globals;
import io.github.juunoco.blobmobile.PurchasesManager;
import io.github.juunoco.blobmobile.R;
import io.github.juunoco.gameui.CustomDialog;

/**
 * Created by Jun on 11/04/2018.
 */

public class HintUsagePopup {

    private GameActivity gameActivity;
    private HintPurchasePopup hintPurchasePopup;
    private int lvlNo;
    private ArrayList<HintSlice> hintSlices = new ArrayList<>();
    private PopupWindow popupWindow;
    private View popupView;
    private HintSlice currentHintSlice;
    private ActionBar ac;

    /**
     * created from ActionBar.java that is the bridge with the UI.
     * @param a
     * @param lvlNo
     * @return
     */
    public static HintUsagePopup create(GameActivity a, int lvlNo, ActionBar ac){
        return new HintUsagePopup(a, lvlNo, ac);
    }

    private HintUsagePopup(GameActivity a, int lvlNo, ActionBar ac){
        this.gameActivity = a;
        this.lvlNo = lvlNo;
        this.ac = ac;
        this.hintPurchasePopup = HintPurchasePopup.create(gameActivity);
    }

    public void initPopup(){
        LayoutInflater layoutInflater =
                (LayoutInflater) this.gameActivity.getBaseContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        popupView = layoutInflater.inflate(R.layout.hint_usage_popup, null);

        popupWindow = new PopupWindow(
                popupView,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                true
        );
        popupWindow.setAnimationStyle(R.style.CustomDialog_Window);

        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                ac.dimScreen(false);
            }
        });
        popupWindow.setBackgroundDrawable(new Drawable() {
            @Override
            public void draw(@NonNull Canvas canvas) {
                //NO ACTION
//                Paint p = new Paint();
//                p.setColor(Color.BLACK);
//                p.setAlpha(100);
//                canvas.drawRect(new RectF(0, 0, (int)Globals.SCREEN_WIDTH, (int)Globals.SCREEN_HEIGHT), p);
            }

            @Override
            public void setAlpha(int alpha) {

            }

            @Override
            public void setColorFilter(@Nullable ColorFilter colorFilter) {

            }

            @Override
            public int getOpacity() {
                return PixelFormat.TRANSPARENT;
            }
        });


        populateHintSlices(popupView);

        Button getMoreHintsButton = (Button) popupView.findViewById(R.id.getMoreHints);
        getMoreHintsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHintPurchasePopup();
            }
        });

        updateHintsText();

        View parent = this.gameActivity.getGameLoop();
        if(parent != null){
            //popupWindow.showAtLocation(parent, Gravity.CENTER, 0, (int)(Globals.SCREEN_HEIGHT / 6f));
            popupWindow.showAtLocation(parent, Gravity.CENTER, 0, 0);
        }
    }

    private void updateHintsText(){
        TextView tv = (TextView) popupView.findViewById(R.id.hintsRemaining);
        int currentHints = HintManager.getInstance().getAvailableHints();
        String str = gameActivity.getResources().getString(R.string.hints_remaining).concat(" ").concat(String.valueOf(currentHints));
        tv.setText(str);
    }

    private void populateHintSlices(View popupView){
        //HorizontalScrollView hsv = (HorizontalScrollView) popupView.findViewById(R.id.slicesList);
        LinearLayout ll = (LinearLayout) popupView.findViewById(R.id.slicesLayout);
        //retrieve total hints for this level
        SparseArray<Hint> hints = HintManager.getInstance().getAllHintsForLevel(this.lvlNo);

        //create list of ImageViews for these hints
        for(int i = 1; i < hints.size() + 1; i++){
            Hint tHint = hints.get(i);
            boolean b = PurchasesManager.checkNextHintAvailable(this.lvlNo, i);
            HintSlice tHintSlice = new HintSlice(tHint, b, this.lvlNo);

            RelativeLayout rl = new RelativeLayout(gameActivity);
            rl.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));

            rl.addView(tHintSlice.getImageView());
            rl.addView(tHintSlice.getImageBanner());
            rl.addView(tHintSlice.getTextView());
            ll.addView(rl);
        }
    }

    private void showHintPurchasePopup(){
        this.hintPurchasePopup.initPopup();
    }

    private void showHintUseWarning(final HintSlice hintSlice){
        final Button bYes = new Button((new ContextThemeWrapper(gameActivity, R.style.PopupButtonPositiveTheme)));
        bYes.setBackgroundColor(gameActivity.getResources().getColor(R.color.gray));
        bYes.setText(gameActivity.getResources().getString(R.string.yes));

        final Button bNo = new Button(new ContextThemeWrapper(gameActivity, R.style.PopupButtonNegativeTheme));
        bNo.setBackgroundColor(gameActivity.getResources().getColor(R.color.darkgray));
        bNo.setText(gameActivity.getResources().getString(R.string.no));

        final CustomDialog cd = new CustomDialog.Builder()
                .setTitle(gameActivity.getResources().getString(R.string.spoiler_alert))
                .setMessage(gameActivity.getResources().getString(R.string.hint_usage_warning))
                .setButtons(bYes, bNo)
                .build(gameActivity, gameActivity);

        bYes.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent e){
                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    bYes.setBackgroundColor(gameActivity.getResources().getColor(R.color.colorPrimaryDark));
                }
                else if(e.getAction() == MotionEvent.ACTION_UP){
                    bYes.setBackgroundColor(gameActivity.getResources().getColor(R.color.gray));
                    Rect bYesRect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                    if (!CustomDialog.didMoveOut(v, bYesRect, e)) {
                        //use one hint
                        currentHintSlice = hintSlice;
                        HintManager.getInstance().consumeAvailableHint();
                        HintManager.getInstance().saveHintSliceUnlockedData(hintSlice.lvlNo, hintSlice.hint.sliceNo);
                        cd.getDialog().dismiss();
                    }
                }
                return true;
            }
        });

        bNo.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent e){
                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    bNo.setBackgroundColor(gameActivity.getResources().getColor(R.color.colorPrimaryDark));
                }
                else if(e.getAction() == MotionEvent.ACTION_UP){
                    bNo.setBackgroundColor(gameActivity.getResources().getColor(R.color.darkgray));

                    Rect bNoRect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                    if (!CustomDialog.didMoveOut(v, bNoRect, e)) {
                        cd.getDialog().dismiss();
                    }
                }
                return true;
            }
        });
    }

    private void showSpoilersWarning(final HintSlice hintSlice){

        final Button bYes = new Button(new ContextThemeWrapper(gameActivity, R.style.PopupButtonPositiveTheme));
        bYes.setBackgroundColor(gameActivity.getResources().getColor(R.color.gray));
        bYes.setText(gameActivity.getResources().getString(R.string.yes));

        final Button bNo = new Button(new ContextThemeWrapper(gameActivity, R.style.PopupButtonNegativeTheme));
        bNo.setBackgroundColor(gameActivity.getResources().getColor(R.color.darkgray));
        bNo.setText(gameActivity.getResources().getString(R.string.no));

        final CustomDialog cd = new CustomDialog.Builder()
                .setTitle(gameActivity.getResources().getString(R.string.spoiler_alert))
                .setMessage(gameActivity.getResources().getString(R.string.hint_warning))
                .setButtons(bYes, bNo)
                .build(gameActivity, gameActivity);

        bYes.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent e){
                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    bYes.setBackgroundColor(gameActivity.getResources().getColor(R.color.colorPrimaryDark));
                }
                else if(e.getAction() == MotionEvent.ACTION_UP){
                    bYes.setBackgroundColor(gameActivity.getResources().getColor(R.color.gray));
                    Rect bYesRect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                    if (!CustomDialog.didMoveOut(v, bYesRect, e)) {
                        //showHints
                        currentHintSlice = hintSlice;
                        cd.getDialog().dismiss();
                    }
                }
                return true;
            }
        });

        bNo.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent e){
                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    bNo.setBackgroundColor(gameActivity.getResources().getColor(R.color.colorPrimaryDark));
                }
                else if(e.getAction() == MotionEvent.ACTION_UP){
                    bNo.setBackgroundColor(gameActivity.getResources().getColor(R.color.darkgray));

                    Rect bNoRect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                    if (!CustomDialog.didMoveOut(v, bNoRect, e)) {
                        cd.getDialog().dismiss();
                    }
                }
                return true;
            }
        });
    }

    /**
     * called from ActionBar.java to draw
     * @param c canvas to draw on, inherited from ActionBar
     */
    public void drawHintSlices(Canvas c){
        //draw all of the components of a Hint Slice is not null and hintslice is selected
        if(currentHintSlice != null) {
            String sliceText = gameActivity.getResources().getString(R.string.slice);
            currentHintSlice.getHint().draw(sliceText, c);
            for(Hint h : currentHintSlice.getPreviousHintSlices()){
                h.draw(sliceText, c);
            }
        }
    }

    public void setCurrentHintSlice(@Nullable HintSlice hintSlice){
        this.currentHintSlice = hintSlice;
    }
    public final HintSlice getCurrentHintSlice(){ return this.currentHintSlice; }

    /**
     * object representing each hint slice
     * links the ImageView to the Hint object
     */
    public class HintSlice {
        private boolean bHintAvailable;
        private Hint hint;
        private int lvlNo;
        private ImageView hintSliceImage, hintSliceBanner;
        private TextView lvlText;
        private ArrayList<Hint> previousHintsToShow = new ArrayList<>();

        public HintSlice(Hint hint, boolean bHintAvailable, int lvlNo){
            this.hint = hint;
            this.lvlNo = lvlNo;
            this.bHintAvailable = bHintAvailable;
            initHintSliceImg();
            initHintSliceBanner();
            initLvlText();
        }

        public void update(){
            this.hint.update();
            for(Hint h : previousHintsToShow){
                h.update();
            }
        }

        private void initHintSliceBanner(){
            hintSliceBanner = new ImageView(gameActivity);
            hintSliceBanner.setBackgroundResource(R.drawable.hint_banner);
            hintSliceBanner.setLayoutParams(
                    new RelativeLayout.LayoutParams(Globals.HINT_IMG_SIZE / 3, Globals.HINT_IMG_SIZE / 3)
            );
            hintSliceBanner.requestLayout();
        }

        private void initHintSliceImg(){
            hintSliceImage = new ImageView(gameActivity);
            if(bHintAvailable) {
                hintSliceImage.setBackgroundResource(R.drawable.blob_hint_on);
            }
            else {
                hintSliceImage.setBackgroundResource(R.drawable.blob_hint_off);
            }
            hintSliceImage.setLayoutParams(
                    new RelativeLayout.LayoutParams(Globals.HINT_IMG_SIZE, (int)(Globals.HINT_IMG_SIZE * 1.1f))
            );
            //hintSliceImage.getLayoutParams().width = hintSliceImage.getLayoutParams().height = Globals.HINT_SLICE_SIZE;
            hintSliceImage.setPadding(10, 2, 10, 2);
            hintSliceImage.requestLayout();


            hintSliceImage.setOnTouchListener(new View.OnTouchListener(){

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction() == MotionEvent.ACTION_DOWN){
                        //hintSliceImage.setBackgroundResource(R.drawable.hint_slice_selected);
                    }
                    if(event.getAction() == MotionEvent.ACTION_UP){
                        //hintSliceImage.setBackgroundResource(R.drawable.hint_slice);
                        //select hint
                        //check if hint is available
                        if(bHintAvailable) {
                            selectHint();
                        }
                        //if hint is not available, bring up the purchase popup
                        else{
                            //check if user has hints available
                            if(HintManager.getInstance().getAvailableHints() > 0) {
                                useHintAndSelect();
                            }
                            else {
                                //if user has no avaialble hints, show marketplace to encourage purchase
                                showHintPurchasePopup();
                            }
                        }
                        return true;
                    }
                    return true;
                }
            });
        }

        /**
         * selects the hint to display if available, otherwise, brings up the hint purchase popup
         */
        private void selectHint(){
            //if hint is available, show it, and all previous hints to this hint
            previousHintsToShow = HintManager.getInstance().getAllHintsForLevelUpTo(this.lvlNo, this.hint.sliceNo);

            //show warning dialog
            showSpoilersWarning(this);
        }

        private void useHintAndSelect(){
            previousHintsToShow = HintManager.getInstance().getAllHintsForLevelUpTo(this.lvlNo, this.hint.sliceNo);
            showHintUseWarning(this);
        }

        private void initLvlText(){
            lvlText = new TextView(gameActivity);
            lvlText.setText("  " + this.hint.sliceNo);
            lvlText.setTextColor(Color.BLACK);
            lvlText.setLayoutParams(
                    new RelativeLayout.LayoutParams(Globals.HINT_IMG_SIZE, Globals.HINT_IMG_SIZE)
            );
            lvlText.setTextSize(Globals.HINT_SLICE_SIZE);
            lvlText.setGravity(Gravity.TOP);
            if(!bHintAvailable){
               // lvlText.setAlpha(0.3f);
            }
        }

        public final Hint getHint(){ return this.hint; }
        public final ArrayList<Hint> getPreviousHintSlices(){
            return this.previousHintsToShow;
        }
        public final boolean isHintSliceAvailable(){ return this.bHintAvailable; }
        public final ImageView getImageView(){ return this.hintSliceImage; }
        public final TextView getTextView(){ return this.lvlText; }
        public final ImageView getImageBanner(){ return this.hintSliceBanner; }
    }

}

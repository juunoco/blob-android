package io.github.juunoco.blobmobile.junk_classes;

import android.graphics.Canvas;

/**
 * Created by Donna on 3/11/2017.
 */

public class Transition {

//    private static Transition INSTANCE = new Transition();

    private static int move_Left, move_Right;

    void init(){
        move_Left = 100;
        move_Right = -50;
    }

    private void transitionAnim(){
        move_Right += 15;
        move_Left -= 15;
    }

    void drawTransition(Canvas c){
//        Bitmap colours = Globals.getBorder();
//        float height = colours.getHeight() - 5;
//        float middle = Globals.SCREEN_HEIGHT / 2;
//        transitionAnim();
//        for(int i = -6; i < 7; i++){
//            if(i% 2 == 0){
//                c.drawBitmap(colours,height*(i%3) + move_Right,middle + (height * i),null);
//            }
//            else{
//                c.drawBitmap(colours,height*(i%3) + move_Left,middle + (height * i),null);
//            }
//        }
    }

    boolean hasMoved(){
        return move_Left < 100 && move_Right > -50;
    }

    static Transition create(){
        return new Transition();
    }

    private Transition(){
        init();
    }
}

package io.github.juunoco.blobmobile;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import io.github.juunoco.blobmobile.ui.HintManager;
import io.github.juunoco.blobmobile.ui.HintTouchLine;
import io.github.juunoco.blobmobile.ui.HintUsagePopup;
import io.github.juunoco.blobmobile.ui.ParticleEffect;
import io.github.juunoco.gameui.CustomDialog;
import io.github.juunoco.gameui.JuunoButton;
import io.github.juunoco.gameui.JuunoText;

/**
 * Created by Donna on 22/09/2017.
 */

public class ActionBar {

    private Level level;
    private JuunoText levelNum, message;
    private JuunoButton retry, help, levelView, hintButton, rewardedAdButton;
    private Bitmap rewardedAdBitmap;
    private boolean clickedLevelView, bRewardedAdAvailable = false;
    private GameLoop gameLoop;
    private float downX, downY;
    private SettingsPopup mSettingsPopup;
    private HintUsagePopup mHintUsagePopup;
    private GameActivity mActivity;
    public boolean bHintPressed = false, bHintsPressable = true, bDimScreen = false;

    private int hintNo;
    private final float
            SETTINGSX = 1.0f * Globals.CELL_WIDTH,//Globals.SCREEN_WIDTH / 8 * 7,
            SETTINGSY;//Globals.SCREEN_HEIGHT / 11f;

    void update(Level lvl){
        level = lvl;
        levelNum.update();
        message.update();
        levelNum.setMessage("Level " + level.number);
        message.setMessage(level.message);
        retry.update();
        levelView.update();
        mSettingsPopup.update();
        hintButton.update();
        if(mHintUsagePopup.getCurrentHintSlice() != null){
            mHintUsagePopup.getCurrentHintSlice().update();
        }
        if(bRewardedAdAvailable){
            rewardedAdButton.update();
        }
    }

    void draw(Canvas c){
        //levelNum.draw(c);
        //message.draw(c);
        retry.draw(Globals.retry,c);
//        help.draw(Globals.help,c);
        levelView.draw(Globals.levels,c);
        mSettingsPopup.draw(c);
        if(bHintsPressable) {
            hintButton.draw(SplashActivity.hintsButton, c);
        }

        if(bRewardedAdAvailable) {
            rewardedAdButton.draw(SplashActivity.rewardedAd, c);
        }

        //draw dim screen over everything
        if(bDimScreen){
            c.drawColor(mActivity.getResources().getColor(R.color.black_underlay));
        }
        if(mHintUsagePopup.getCurrentHintSlice() != null){
            mHintUsagePopup.drawHintSlices(c);
        }

    }

    /**
     * fade out action bar materials when nextLevel & shattering has triggered
     */
    void fadeOut(){
        levelNum.initFadeOut();
        message.initFadeOut();
        retry.initFadeout();
        levelView.initFadeout();
        mSettingsPopup.initFadeOut();
        hintButton.initFadeout();
    }


    void onClick(MotionEvent e, Activity activity){
        //check where coordinaets went down
        if(e.getAction() == MotionEvent.ACTION_DOWN){
            downX = e.getX();
            downY = e.getY();
        }
        //check if the down press was also on the button
        if(downX > retry.getX() && downX < retry.getWidth() && downY > retry.getY() && downY < retry.getHeight()) {
            if (retry.isClicked(e) && level != null) {
                SoundManager.playClickRetry();
                level.startFadeout(true);
//            new Handler().postDelayed(new Runnable(){
//                @Override
//                public void run(){
//                    level.reset();
//                }
//            }, 1000);
                //AnimationManager.setAlphaAnimation(activity.getCurrentFocus(), r);
                activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                ((GameActivity) activity).showInterstitial();
            }
        }
        if(downX > hintButton.getX() && downX < hintButton.getWidth() && downY > hintButton.getY() && downY < hintButton.getHeight()){
            if(hintButton.isClicked(e) && level != null && e.getAction() == MotionEvent.ACTION_UP){
                if(bHintsPressable) {
                    SoundManager.playClickPlay();
                    //check if hint has been started
                    if (mHintUsagePopup.getCurrentHintSlice() == null) {
                        startHintMode(false);
                    }
                    //continue with existing hint process
                    else {
                        //checkHint(false);
                    }
                    hideNavigationBar();
                    bHintPressed = true;
                }
            }
        }
        if(bRewardedAdAvailable) {
            if (downX > rewardedAdButton.getX() && downX < rewardedAdButton.getWidth() && downY > rewardedAdButton.getY() && downY < rewardedAdButton.getHeight()){
                SoundManager.playClickPlay();
                Globals.bInGameSequence = true;
                //initialize warning to show hint
                final Button bYes = new Button(new ContextThemeWrapper(mActivity, R.style.PopupButtonPositiveTheme));
                bYes.setText("Yes");
                bYes.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                final Button bNo = new Button(new ContextThemeWrapper(activity, R.style.PopupButtonNegativeTheme));
                bNo.setText("No Thanks");
                bNo.setBackgroundColor(activity.getResources().getColor(R.color.darkgray));
                final CustomDialog cd = new CustomDialog.Builder()
                        .setMessage("Would you like to watch an Ad for a hint?")
                        .setButtons(bYes, bNo)
                        .build(mActivity, mActivity);

                bYes.setOnTouchListener(new View.OnTouchListener(){
                    @Override
                    public boolean onTouch(View v, MotionEvent e) {
                        if (e.getAction() == MotionEvent.ACTION_DOWN) {
                            bYes.setBackgroundColor(mActivity.getResources().getColor(R.color.colorPrimaryDark));

                        } else if (e.getAction() == MotionEvent.ACTION_UP) {
                            bYes.setBackgroundColor(mActivity.getResources().getColor(R.color.gray));
                            Rect bYesRect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                            if (!CustomDialog.didMoveOut(v, bYesRect, e)) {
                                mActivity.getRewardedViewAd().show();
                                cd.getDialog().dismiss();
                            }
                        }
                        return true;
                    }
                });

                bNo.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent e) {
                        //else if confirmation is no...NO ACTION
                        //CustomDialog.scaleButton(bNo, SettingsActivity.this);
                        if (e.getAction() == MotionEvent.ACTION_DOWN) {
                            //change button colour
                            bNo.setBackgroundColor(mActivity.getResources().getColor(R.color.colorPrimaryDark));
                        } else if (e.getAction() == MotionEvent.ACTION_UP) {
                            bNo.setBackgroundColor(mActivity.getResources().getColor(R.color.darkgray));

                            Rect bNoRect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                            if (!CustomDialog.didMoveOut(v, bNoRect, e)) {
                                //SoundManager.playClickPlay();
                                SoundManager.playClickPlay();
                                bRewardedAdAvailable = false;
                                cd.getDialog().dismiss();
                            }
                        }
                        return true;
                    }
                });
            }
        }
//        if(downX > nextHintButton.getX() && downX < nextHintButton.getWidth() && downY > nextHintButton.getY() && downY < nextHintButton.getHeight()) {
//            if(nextHintButton.isClicked(e) && level != null){
//                SoundManager.playClickPlay();
//                //move to next hint
//                checkHint();
//        }
        if(downX > levelView.getX() && downX < levelView.getWidth() && downY > levelView.getY() && downY < levelView.getHeight()) {
            if (levelView.isClicked(e)) {
                SoundManager.playClickReturn();
                level.startFadeout(false);
                Globals.bInGameSequence = true;
                activity.finish();
                activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        }
        if(downX >  SETTINGSX && downX < SETTINGSX + mSettingsPopup.getSettingsImg().getWidth()
                && downY > SETTINGSY && downY < SETTINGSY + mSettingsPopup.getSettingsImg().getHeight()){
            if(mSettingsPopup.isClicked(e)){
                SoundManager.playClickPlay();
                mSettingsPopup.startRotating();
                hideNavigationBar();
            }
        }
    }

    void hideNavigationBar(){
        android.support.v7.app.ActionBar actionBar = mActivity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        View decorView = mActivity.getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        decorView.setSystemUiVisibility(uiOptions);

        this.gameLoop.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        mActivity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public void startHintMode(boolean isRewardedAd){
        Globals.setHintMode(true);
        hintNo = 1;

        //show initial Hint Popup
        mHintUsagePopup.initPopup();
        bDimScreen = true;

        checkHint(isRewardedAd);
    }

    void checkHint(boolean isRewardedAd){
//        boolean bHintAvailable = PurchasesManager.checkNextHintAvailable(hintNo);
//
//        if(isRewardedAd) {
//            hintNo = 1; //rewarded ad always provides first hint only
//            bHintAvailable = true;
//        }
//
//        if(bHintAvailable ){
//            //get touchline to draw for this level & slice
//            if(hintNo > 1){
//                sliceHintTouchline();
//            }
//            int tLvlNo = this.gameLoop.getLvl().number;
//            mCurrentHint = HintManager.getInstance().getCurrentHint(tLvlNo, this.hintNo);
//            this.hintNo++;
//        }
//        else {
//            //hint unavailable, player chose not to purchase, no further action
//
//            Globals.setHintMode(false);
//        }
    }

    /**
     * slices the blob based on this touchline.
     * Method is called after the 'slice' button is pressed.
     * A slice is sure to be found as the hint is pre-programmed to find one
     */
//    void sliceHintTouchline(){
//        HintTouchLine tl = .getHintTouchLine();
//        for(Blob blob : this.gameLoop.getLvl().blobs){
//            if(tl.getY1() == tl.getY2()
//                    && tl.getY1() < blob.getBlobBottom()
//                    && tl.getY1() > blob.getBlobY() ){
//                if(tl.getX1() < tl.getX2()
//                        && tl.getX1() >= blob.getX()
//                        && (tl.getX1() + Globals.CELL_WIDTH) <= blob.getWidth() ){
//                    cut(tl,blob,false);
//                }
//                else if(tl.getX1() > tl.getX2()
//                        && tl.getX1() <= blob.getWidth()
//                        && (tl.getX1() - Globals.CELL_WIDTH) >= blob.getX() ){
//                    cut(tl,blob,false);
//                }
//            }
//            else if(tl.getX1() == tl.getX2()
//                    && tl.getX1() > blob.getX()
//                    && tl.getX1() < blob.getWidth() ){
//                if(tl.getY1() < tl.getY2()
//                        && tl.getY1() >= blob.getBlobY()
//                        && (tl.getY1() + (Globals.CELL_WIDTH / 2)) <= blob.getBlobBottom() ){
//                    cut(tl,blob,true);
//                }
//                else if(tl.getY1() > tl.getY2()
//                        && tl.getY1() - (Globals.CELL_WIDTH / 2) <= blob.getBlobBottom()
//                        && (tl.getY1() - Globals.CELL_WIDTH) >= blob.getBlobY() ){
//                    cut(tl,blob,true);
//                }
//            }
//        }
//    }

    private void cut(HintTouchLine line, Blob blob, boolean isVert){
        Blob.DIRECTION direction1, direction2;
        SoundManager.playBlobSlice();
        float h1, w1, x2, y2, w2,h2;
        float x1 = gridValue(blob.getX());
        float y1 = gridValue(blob.getBlobY() - Globals.AD_HEIGHT);

        if(blob.getCut() == 10){
            //remove the blob
            this.gameLoop.getLvl().blobs.remove(blob);
            this.gameLoop.getLvl().getParticleEffect().showParticles(ParticleEffect.LineCoords.createLineCoords(blob.getGridX(), blob.getGridY(), blob.getGridWidth() + blob.getGridX(), blob.getGridY() + 1), blob.getCut(), false);

            gameLoop.getLvl().checks();
            //random twitch
            for(Blob b : gameLoop.getLvl().blobs){
                b.randomMove();
            }
            return;
        }

        if(isVert){
            x2 = gridValue(line.getX1());
            y2 = y1;
            w1 = x2 - x1;
            h1 = gridValue(blob.getBlobBottom())  - gridValue(blob.getBlobY());
            w2 = gridValue(blob.getWidth()) - gridValue(line.getX1());
            h2 = h1;
            direction1 = Blob.DIRECTION.LEFT;
            direction2 = Blob.DIRECTION.RIGHT;
        } else{
            x2 = x1;
            y2 = gridValue(line.getY1() - Globals.AD_HEIGHT);
            w1 = gridValue(blob.getWidth()) - x1;
            h1 = y2-y1;
            w2 = w1;
            h2 = gridValue(blob.getBlobBottom() - Globals.AD_HEIGHT) - y2;
            direction1 = Blob.DIRECTION.UP;
            direction2 = Blob.DIRECTION.DOWN;
        }
        int cut = blob.getCut()+1;
        if(Globals.isTutorialMode()) cut = 1;

        //new ParticleEffect(x2, y2, isVert? 1 : w2, isVert? h1 : 1, cut, this.view);
        gameLoop.getLvl().useParticles(
                ParticleEffect.LineCoords.createLineCoords(x2, y2, isVert? x2 : (w2 + x2), isVert? (h1 + y2) : y2),
                blob.getCut()
        );

        gameLoop.getLvl().blobs.remove(blob);
        float diff = (line.getY1() - Globals.AD_HEIGHT);
        Log.i("GRID VALUE HEIGHT: ",blob.getHeight() +" vs " + diff);
        Log.i("BLOB A: ",y1 +"," +h1);
        Log.i("BLOB B: ",y2 +"," +h2);

        Blob blobby = Blob.createBlob(x1, y1, w1, h1, cut, direction1);
        blobby.setShatterParticles(mActivity, gameLoop);
        gameLoop.getLvl().blobs.add(blobby);
        Blob blobbina = Blob.createBlob(x2, y2, w2, h2, cut, direction2);
        blobbina.setShatterParticles(mActivity, gameLoop);
        gameLoop.getLvl().blobs.add(blobbina);

        gameLoop.getLvl().checks();

        //random twitch
        for(Blob b : gameLoop.getLvl().blobs){
            b.randomMove();
        }
    }

    private float gridValue(float value){
        return value / Globals.CELL_WIDTH;
    }

    static ActionBar make(Activity a, GameLoop gameLoop){
        return new ActionBar(a, gameLoop);
    }

    private ActionBar(Activity a, GameLoop gameLoop) {
        this.mActivity = (GameActivity) a;
        final float actionBarStart = Globals.AD_HEIGHT + (Globals.CELL_WIDTH * 11);
        final float startYgrid = actionBarStart/Globals.CELL_WIDTH;
        SETTINGSY = startYgrid * Globals.CELL_WIDTH;
        this.gameLoop = gameLoop;
        levelNum = JuunoText.create(0.9f,startYgrid + 0.5f,"Level",(int) Globals.CELL_WIDTH/2);
        message = JuunoText.create(0.9f,startYgrid + 1.3f,"Message", (int) Globals.CELL_WIDTH/3);
//        retry = JuunoButton.create(4,startYgrid,1.5f,1.5f,"RT");
//        help = JuunoButton.create(5.8f,startYgrid,1.5f,1.5f,"HLP");
        levelView = JuunoButton.create(3.0f,startYgrid,1.5f,1.5f,"LVLS");
        retry = JuunoButton.create(7.0f, startYgrid, 1.5f,1.5f,"RT");
        hintButton = JuunoButton.create(5.0f, startYgrid, 1.5f, 1.5f, "HINT");
        rewardedAdButton = JuunoButton.create(4.75f, startYgrid - (Globals.CELL_WIDTH / 2), 1.2f, 1.2f, "WATCH AD");
        //nextHintButton = JuunoButton.create(2.2f, startYgrid, 1.5f, 1.5f, "NEXTHINT");
        this.mSettingsPopup = new SettingsPopup(SETTINGSX, SETTINGSY, a, true);
        this.mHintUsagePopup = HintUsagePopup.create((GameActivity)a, this.gameLoop.getLvl().number, this);
        rewardedAdBitmap = Bitmap.createScaledBitmap(SplashActivity.rewardedAd, (int)Globals.CELL_WIDTH * 2, (int)Globals.CELL_WIDTH * 2, false);
    }

    public void dimScreen(boolean b){ this.bDimScreen = b; }
    public boolean isRewardedAdAvailable(){
        return this.bRewardedAdAvailable;
    }
    public void setbRewardedAdAvailable(boolean b){ this.bRewardedAdAvailable = b; }
    public void resetHintSlice(){ this.mHintUsagePopup.setCurrentHintSlice(null); }
    public void setHintsPressable(boolean b){ this.bHintsPressable = b; }
    public JuunoButton getHintButton(){ return this.hintButton; }

}

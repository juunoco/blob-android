package io.github.juunoco.blobmobile;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v7.app.*;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.github.juunoco.gameui.CustomDialog;

/**
 * Created by Jun on 23/03/2018.
 * Popup for the settings button, when pressed, settings will come out as a popup with same functionality as the activity
 */

public class SettingsPopup {

    private Activity activity;
    private Bitmap settingsImg;
    private float x, y;
    private boolean bFadeOut;
    private boolean bFadeDown, bFadeIn, bFading, bRotating, bNothingTouched, bResetting;
    private int fadeAlpha;
    private int rotationAngle;
    private Dialog settingsPopup;
    private Paint p;
    private View settingsView;
    private final int FADEINDELAY = 40;
    private int fadeCount = FADEINDELAY;
    private final int CHANGEAMT = 50;
    private final float LEEWAY = Globals.SCREEN_HEIGHT / 20;

    public SettingsPopup(float x, float y, Activity a, boolean bPlaying){
        this.activity = a;
        this.x = x;
        this.y = y;
        this.fadeAlpha = 255;
        p = new Paint();
        this.bFading = false;
        this.bFadeDown = false;
        this.bFadeIn = false;
        this.bFadeOut = false;
        this.bRotating = false;
        rotationAngle = 0;
        this.bNothingTouched = true;
        this.bResetting = false;
        if(bPlaying){
            this.settingsImg = SplashActivity.settingsButton;
        }
        else {
            this.settingsImg = Bitmap.createScaledBitmap(
                    SplashActivity.settingsButton,
                    (int)(SplashActivity.settingsButton.getHeight() / 2f),
                    (int)(SplashActivity.settingsButton.getHeight() / 2f),
                    //(int)(Globals.CELL_WIDTH * 2),
                    false
            );
        }
        initPopup();
        initButtons();
    }

    public void update(){
        if(bFadeOut){
//           if(drawAlpha < 255 - 50) drawAlpha+=50;
//           else drawAlpha = 255;
            if(fadeAlpha > 0 + CHANGEAMT) fadeAlpha -= CHANGEAMT;
            else {
                fadeAlpha = 0;
                bFadeOut = false;
            }
        }

        if(bFadeIn){
            if(this.fadeAlpha < 255 - CHANGEAMT / 2.0f){
                fadeAlpha += CHANGEAMT / 2.0f;
            }
            else{
                fadeAlpha = 255;
                bFadeIn = false;
            }
        }


        if(bRotating){
            rotationAngle += 8; //rate of rotation
        }
    }

    public void initFadeOut(){
        bFadeOut = true;
    }

    public void initFadeIn(){
        bFadeIn = true;
    }

    public void draw(Canvas c){
        c.save();
        c.rotate(rotationAngle, x + settingsImg.getWidth() / 2, y + settingsImg.getHeight() / 2);
        p.setAlpha(fadeAlpha);
        c.drawBitmap(settingsImg, x, y, p);
//        c.drawBitmap(
//                settingsImg,
//                null,
//                new Rect((int)x, (int)y, settingsImg.getWidth(), settingsImg.getHeight()),
//                p
//                );
        c.restore();
    }

    public boolean isClicked(MotionEvent e){
        if(e.getAction() == MotionEvent.ACTION_UP) {
            if (e.getX() > x
                    && e.getX() < x + settingsImg.getWidth()
                    && e.getY() > y
                    && e.getY() < y + settingsImg.getHeight()) {
                if(bRotating){
                    //settings is shown and rotating
                    settingsPopup.dismiss();
                }
                else {
                    //settings is not shown
                    bNothingTouched = true;
                    //((Button)settingsPopup.findViewById(R.id.settingsSaveButton)).setText("Back");
                    updateResourceImages();
                    settingsPopup.show();

                }
                return true;
            }
        }
        return false;
    }

    public void startRotating(){
        this.bRotating = true;
    }

    public void stopRotating(){
        this.bRotating = false;
    }

    /**
     * shows the settings as a popup
     */
    public void initPopup(){
        //LayoutInflater inflater = LayoutInflater.from(activity);
        //settingsView = inflater.inflate(R.layout.activity_settings, null);
        this.settingsPopup = new Dialog(activity, R.style.CustomDialog);
        //settingsPopup.setContentView(settingsView);
        //settingsPopup.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsPopup.setContentView(R.layout.activity_settings);
        settingsPopup.setCanceledOnTouchOutside(true);
        WindowManager.LayoutParams lp = settingsPopup.getWindow().getAttributes();
        lp.dimAmount = 0.6f;
        settingsPopup.getWindow().setAttributes(lp);
        settingsPopup.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        settingsPopup.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                stopRotating();
            }
        });
    }

    void updateResourceImages(){
        ImageButton audioButton = (ImageButton) settingsPopup.findViewById(R.id.muteAudio);
        audioButton.setBackgroundResource(SoundManager.bAudioMuted ? R.drawable.settings_music_off : R.drawable.settings_music_on);

        ImageButton FXButton = (ImageButton) settingsPopup.findViewById(R.id.muteFX);
        FXButton.setBackgroundResource(SoundManager.bFXMuted ? R.drawable.settings_sfx_off : R.drawable.settings_sfx_on);

        TextView resetText = (TextView) settingsPopup.findViewById(R.id.resetText);
        resetText.setText("");
    }

    void initButtons(){
        final ImageButton audioButton = (ImageButton) settingsPopup.findViewById(R.id.muteAudio);
        audioButton.setBackgroundResource(SoundManager.bAudioMuted ? R.drawable.settings_music_off : R.drawable.settings_music_on);

        final ImageButton FXButton = (ImageButton) settingsPopup.findViewById(R.id.muteFX);
        FXButton.setBackgroundResource(SoundManager.bFXMuted ? R.drawable.settings_sfx_off : R.drawable.settings_sfx_on);
        final ImageButton resetButton = (ImageButton) settingsPopup.findViewById(R.id.resetButton);
        //final Button saveSettingsButton = (Button) settingsPopup.findViewById(R.id.settingsSaveButton);
        final TextView resetText = (TextView) settingsPopup.findViewById(R.id.resetText);
        resetText.setText("");

        audioButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //trigger on or off
                SoundManager.bAudioMuted = !SoundManager.bAudioMuted;
                Globals.writeCurrentAudioSetting(SoundManager.bAudioMuted);
                SoundManager.playClickPlay();
                bNothingTouched = false;
                if(SoundManager.bAudioMuted){
                    audioButton.setBackgroundResource(R.drawable.settings_music_off);
                    //SoundManager.transitionMusic("menuMusic", "none");
                    if(SoundManager.soundState == SoundManager.SOUNDSTATE.MENU) {
                        SoundManager.getMediaPlayerFromTitle("menuMusic").instantSoundChange(0);
                    }
                    else if(SoundManager.soundState == SoundManager.SOUNDSTATE.PLAYING) {
                        SoundManager.getMediaPlayerFromTitle("playingMusic").instantSoundChange(0);
                    }
                }
                else{
                    audioButton.setBackgroundResource(R.drawable.settings_music_on);
                    //SoundManager.transitionMusic("none", "menuMusic");
                    if(SoundManager.soundState == SoundManager.SOUNDSTATE.MENU) {
                        SoundManager.getMediaPlayerFromTitle("menuMusic").instantSoundChange(1);
                    }
                    else if(SoundManager.soundState == SoundManager.SOUNDSTATE.PLAYING) {
                        SoundManager.getMediaPlayerFromTitle("playingMusic").instantSoundChange(1);
                    }
                }
                if(!bNothingTouched){
                    //saveSettingsButton.setText("SAVE SETTINGS");
                }
            }
        });

        FXButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                SoundManager.bFXMuted = !SoundManager.bFXMuted;
                Globals.writeCurrentAudioFXSetting(SoundManager.bFXMuted);
                SoundManager.playClickPlay();
                bNothingTouched = false;
                if(SoundManager.bFXMuted){
                    FXButton.setBackgroundResource(R.drawable.settings_sfx_off);
                    SoundManager.adjustFXVolume(0);
                }
                else{
                    FXButton.setBackgroundResource(R.drawable.settings_sfx_on);
                    SoundManager.adjustFXVolume(1);
                    SoundManager.playClickPlay();
                }
                if(!bNothingTouched){
                    //saveSettingsButton.setText("SAVE SETTINGS");
                }
            }
        });

        resetButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                if (e.getAction() == MotionEvent.ACTION_UP) {
                    bResetting = !bResetting;
                    SoundManager.playClickPlay();
                    bNothingTouched = false;
                    if (bResetting) {
                        Rect resetRect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                        if (!CustomDialog.didMoveOut(v, resetRect, e)) {
                            if (bResetting) {
                                final Button bYes = new Button(new ContextThemeWrapper(activity, R.style.PopupButtonPositiveTheme));
                                //bYes.setBackground(getResources().getDrawable(R.drawable.custom_dialog_bttn));
                                bYes.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                                final Button bNo = new Button(new ContextThemeWrapper(activity, R.style.PopupButtonNegativeTheme));
                                //bNo.setBackground(getResources().getDrawable(R.drawable.custom_dialog_bttn));
                                bNo.setBackgroundColor(activity.getResources().getColor(R.color.darkgray));
                                //build a dialog to check for resets
                                final CustomDialog cd = new CustomDialog.Builder()
                                        .setTitle(activity.getResources().getString(R.string.reset_blob))
                                        .setMessage(
                                                activity.getResources().getString(R.string.reset_warning1),
                                                activity.getResources().getString(R.string.reset_warning2),
                                                activity.getResources().getString(R.string.reset_warning3)
                                        )
                                        .setButtons(bYes, bNo)
                                        .build(activity, activity);

                                bYes.setText(activity.getResources().getString(R.string.yes));
                                bYes.setOnTouchListener(new View.OnTouchListener() {
                                    @Override
                                    @TargetApi(21)
                                    public boolean onTouch(View v, MotionEvent e) {
                                        //if confirmation is yes,
                                        //CustomDialog.scaleButton(bYes, SettingsActivity.this);
                                        if (e.getAction() == MotionEvent.ACTION_DOWN) {
                                            //trigger button scaling animation, or button darkening
                                            bYes.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                                            //bYes.setBackgroundTintMode(PorterDuff.Mode.LIGHTEN);
                                            //check if API is larger than 21
                                            //                                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                            //                                        //bYes.getBackground().setColorFilter(getResources().getColor(R.color.colorPrimaryDark), PorterDuff.Mode.LIGHTEN);
                                            //                                        bYes.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryDark)));
                                            //                                    }
                                            //                                    else if(Build.VERSION.SDK_INT > 16){
                                            //                                        Drawable wrapDrawable = DrawableCompat.wrap(bYes.getBackground());
                                            //                                        DrawableCompat.setTint(wrapDrawable, getResources().getColor(R.color.colorPrimaryDark));
                                            //                                        bYes.setBackgroundDrawable(DrawableCompat.unwrap(wrapDrawable));
                                            //                                    }
                                        } else if (e.getAction() == MotionEvent.ACTION_UP) {
                                            //                                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                                            //                                        bYes.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.gray)));
                                            //                                    }
                                            //                                    else if(Build.VERSION.SDK_INT > 16){
                                            //                                        Drawable wrapDrawable = DrawableCompat.wrap(bYes.getBackground());
                                            //                                        DrawableCompat.setTint(wrapDrawable, getResources().getColor(R.color.gray));
                                            //                                        bYes.setBackgroundDrawable(DrawableCompat.unwrap(wrapDrawable));
                                            //                                    }
                                            bYes.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                                            //bYes.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.black_overlay)));
                                            Rect bYesRect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                                            if (!CustomDialog.didMoveOut(v, bYesRect, e)) {
                                                Globals.setUnlockedLvl(1);
                                                //Globals.setTotalSlices(0); //not resetting total slices anymore
                                                Globals.setPageNum(1);
                                                Globals.setCurrLvlNum(1);

                                                SoundManager.playClickPlay();
                                                settingsPopup.dismiss();
                                                cd.getDialog().dismiss();
                                                Globals.setResetting(true);
                                                activity.startActivity(new Intent(activity, SplashActivity.class));

                                                //activity.finish();
                                                //activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                                //will automatically load this data from shared preferences
                                            }
                                        }
                                        return true;
                                    }
                                });

                                bNo.setText(activity.getResources().getString(R.string.no));
                                bNo.setOnTouchListener(new View.OnTouchListener() {
                                    @Override
                                    public boolean onTouch(View v, MotionEvent e) {
                                        //else if confirmation is no...NO ACTION
                                        //CustomDialog.scaleButton(bNo, SettingsActivity.this);
                                        if (e.getAction() == MotionEvent.ACTION_DOWN) {
                                            //change button colour
                                            bNo.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                                        } else if (e.getAction() == MotionEvent.ACTION_UP) {
                                            bNo.setBackgroundColor(activity.getResources().getColor(R.color.darkgray));

                                            Rect bNoRect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                                            if (!CustomDialog.didMoveOut(v, bNoRect, e)) {
                                                //SoundManager.playClickPlay();
                                                SoundManager.playClickPlay();
                                                bResetting = false;
                                                cd.getDialog().dismiss();
                                            }
                                        }
                                        return true;
                                    }
                                });
                            } else {
                                //go back
                                //activity.finish();
                                //activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                SoundManager.playClickPlay();
                                settingsPopup.dismiss();
                            }
                        }
                        resetText.setText(R.string.reset_text);
                    } else {
                        resetText.setText("");
                    }
                    if (!bNothingTouched) {
                        //saveSettingsButton.setText("SAVE SETTINGS");
                    }
                    return true;
                }
                return false;
            }
        });

        //saveSettingsButton.setBackgroundColor(activity.getResources().getColor(R.color.gray));
//        saveSettingsButton.setOnTouchListener(new View.OnTouchListener(){
//            @Override
//            @SuppressLint("ClickableViewAccessibility")
//            public boolean onTouch(View v, MotionEvent e) {
//                if(e.getAction() == MotionEvent.ACTION_DOWN){
//                    saveSettingsButton.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDark));
//                }
//                if (e.getAction() == MotionEvent.ACTION_UP) {
//
//                    saveSettingsButton.setBackgroundColor(activity.getResources().getColor(R.color.gray));
//
//                }
//                return true;
//            }
//        });
    }

    public final float getX(){
        return this.x;
    }
    public final float getY(){ return this.y; }
    public final Bitmap getSettingsImg(){
        return this.settingsImg;
    }

}

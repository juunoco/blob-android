package io.github.juunoco.blobmobile;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.Random;

import io.github.juunoco.blobmobile.ui.ShatterIntoParticles;
import io.github.juunoco.blobmobile.ui.TweenManager;
import io.github.juunoco.gameui.GridObject;

/**
 * Created by Donna on 31/08/2017.
 */

public class Blob extends GridObject {

    private float blobY, blobBottom, blobX, blobWidth;
    private int cut;
    final int BLOB_RADIUS = Globals.BLOB_RADIUS; //50
    private boolean falling;
    private boolean updateMe;
    private int overlayTransparency = 0;
    private int gridHeight, gridWidth;
    private float SPEED = 12f, //10f
                GRAVITY = 0.5f;
    private Activity activity;
    private View view;

    //variables for tweening
    public enum DIRECTION {
        UP, DOWN, LEFT, RIGHT
    }
    private DIRECTION direction;
    private int count = 0;
    private float yPos, xPos;
    protected float originalHeight;
    private boolean b_going = false;
    private boolean b_tween;
    protected boolean bGoalConnected;
    protected boolean bDrawConnectedRect = true;
    protected boolean bShattering = false;
    protected ShatterIntoParticles shatterParticles;
    RectF rect;
    private float shineVariance, cellIndent;
    private final int MIN_SHINE, MAX_SHINE;
    private int shineAlpha;
    private boolean bShineIncreasing = true, bTwinkling = false;
    private Paint shinePaint;

    private int shatterDelayCount;
    private final int MAX_SHATTER_DELAY_COUNT = 50;
    public boolean bFlagShatterRemove = false;

    private int variance, varianceNo;
    private boolean bLeftVariance = false, bRightVariance = false, bTopVariance = false, bBottomVariance = false;
    private final int VARIANCE_MAX;

    //scaling
    public boolean bIncreaseSize, bIncreaseSizeComplete;
    public int iIncreaseSizeFrames = 0;
    public final int INCREASE_FRAMES = 5;

    //scaling for unlock
    public boolean bIncreaseForUnlock, isbIncreaseForUnlockComplete;
    public int iIncreaseForUnlockFrames = 0;
    public final int INCREASE_FOR_UNLOCK_FRAMES = 50;

    private float previewWorldPosition, originalLeft, originalRight;

    private final float RANDOM_MOVE_AMT;
    private Random rand;

    /**
     * Creating the Main Blob for each level
     * @return Blob
     */
    static Blob bigBlob(Activity activity, View view){
        //base blob starts from column 2, row 2, and is 8x8 in size
        return new Blob(1,1,8,8,0, null);
    }

    public static Blob createBlob(float x, float y, float w, float h, int cut, DIRECTION direction){
        return new Blob(x,y,w,h,cut,direction);
    }

    Blob(float startX, float startY, float w, float h, int cut, DIRECTION direction) {
        super(startX, startY, w, h);
        gridHeight = Math.round(h);//(int)h;
        gridWidth = Math.round(w);
        blobX = Globals.CELL_WIDTH * startX;
        blobWidth = blobX + Globals.CELL_WIDTH * w;
        blobY = (Globals.CELL_WIDTH * startY) + Globals.AD_HEIGHT;
        blobBottom = blobY + Globals.CELL_WIDTH * h;
        this.originalHeight = blobBottom;
        this.originalLeft = blobX;
        this.originalRight = blobWidth;
        this.cut = cut;
        this.rect = new RectF();
        setFalling(false);
        updateMe = false;
        this.direction = direction;
        this.yPos = (Globals.CELL_WIDTH * startY)  + Globals.AD_HEIGHT;//this.getY();
        this.xPos = Globals.CELL_WIDTH * startX;
        if(direction != null){
            this.b_tween = true;
            TweenManager.b_nothingIsTweening = false;
        }
        else{
            this.b_tween = false;
        }
        bIncreaseSize = false;
        RANDOM_MOVE_AMT = Globals.CELL_WIDTH / 12; // / 10 //where a number determines a person's experience
        this.rand = new Random();
        shatterDelayCount = rand.nextInt(MAX_SHATTER_DELAY_COUNT) + 5;
        //random variance that each Blob has in one direction.
        VARIANCE_MAX = (int)Globals.CELL_WIDTH / 10;
        variance = 0; //VARIANCE_MAX;//rand.nextInt(VARIANCE_MAX) + 1;
        varianceNo = rand.nextInt(4);
        //setVariance(); //do not draw variance anymore
        float cellSize = Globals.CELL_WIDTH;
        cellIndent = cellSize/ 4;
        //shineVariance = blobWidth / rand.nextInt(gridWidth) + 1; //randomize dbased on grid size
        shineVariance = cellSize / 4; //always constant from right side
        //shineVariance = rand.nextInt(gridWidth);
        MAX_SHINE = 255;
        MIN_SHINE = rand.nextInt(250) + 1;
        shineAlpha = MAX_SHINE;
        shinePaint = new Paint();
        shinePaint.setColor(0x40ffffff);
    }

    /**
     * sets a random variance based on a random direction. One step closer to wobbling.
     */
    void setVariance(){
        switch(varianceNo){
            case 0 : bLeftVariance = true; break;
            case 1 : bTopVariance = true; break;
            case 2 : bRightVariance = true; break;
            case 3 : bBottomVariance = true; break;
        }
    }

    void dropUntilCollision(ArrayList<Blob> blobbies){
        RectF tDropRect = pretendDrop();
        boolean tCanDrop = checkCanDropBlob(blobbies, tDropRect);
        if(tCanDrop){
            drop();
        }
    }

    boolean checkCanDropBlob(ArrayList<Blob> blobbies, RectF pretendDropRect){
        //for(Blob b: blobbies){
        for(int i = 0; i < blobbies.size(); i++){
            Blob b = blobbies.get(i);
            if(b != this){
                if(pretendDropRect.intersect(b.getBounds())){
                    //only stop fit he blob below isn't also falling
//                    if(!b.falling) {
//                        stopAt(b.getBlobY());
//                    }
                    if(falling){
                        SoundManager.playDropStop();
                    }
                    falling = false;
                    return false;
                }
            }
            if(pretendDropRect.intersect(FloorObject.getFloorObject().getBounds())){
                //stopAt(FloorObject.getFloorObject().getTop());
                if(falling){
                    SoundManager.playDropStop();
                }
                falling = false;
                return false;
            }
        }
        return true;
    }

    /**
     * simulates a drop and returns rectangle of drop area
     * @return
     */
    private synchronized RectF pretendDrop(){
        return new RectF(blobX + (int)(Globals.CELL_WIDTH / 3), blobY + Globals.CELL_WIDTH / 3f, blobWidth - (int)(Globals.CELL_WIDTH / 3), blobBottom + Globals.CELL_WIDTH / 5f);
    }

    /**
     * called to only drop if the space below is clear.
     */
    private void drop(){
        falling = true;
        blobY += SPEED;
        blobBottom += SPEED;
        //if(falling){
            SPEED += GRAVITY;
            bGoalConnected = false;
        //}
        SoundManager.playBlobDrop();
    }

//    private void stopAt(float topOfObj){
//        SPEED = 0;
//        setFalling(false);
//        blobBottom = topOfObj;
//        //VERY SMART :)
//        blobY = Math.round((blobY) / Globals.CELL_WIDTH) * Globals.CELL_WIDTH;
//        //call all goals to check positions again
//
//    }

    /**
     * random twitch / move that occurs with every slice, or every random interval of no activity
     * fix this to only be drawing the difference, and no difference should be in update
     */
    public void randomMove(){
        //ideally shoudl use the int of DIRECTION enum values
        switch(rand.nextInt(4)){
            case 0 : {
                blobY -= RANDOM_MOVE_AMT;
                break;
            }
            case 1 : {
                blobY += RANDOM_MOVE_AMT;
                break;
            }
            case 2 : {
                blobX -= RANDOM_MOVE_AMT;
                break;
            }
            case 3 : {
                blobX += RANDOM_MOVE_AMT;
                break;
            }
        }
    }

    /**
     * resolves the randomt witch by snapping back to original grid position immediately on the next update call
     */
    private void snapToNormal(){
        if(!falling) {
            blobX = Math.round((blobX) / Globals.CELL_WIDTH) * Globals.CELL_WIDTH;
            blobY = Math.round((blobY) / Globals.CELL_WIDTH) * Globals.CELL_WIDTH;
            //blobY = (gridY + 1) * Globals.CELL_WIDTH;
            //blobBottom = (gridY + 1) * Globals.CELL_WIDTH;
            blobBottom = Math.round((blobBottom) / Globals.CELL_WIDTH) * Globals.CELL_WIDTH; //working REALLY well :)
            gridY = gridToWorldPosition();
        }
        //gridY = Math.round((blobY) / Globals.CELL_WIDTH);
        //reset varaince after snapping to position.

        //snapBlobHeight();
    }

    /**
     * snap the world blobY position to the closest grid position
     */
    private int gridToWorldPosition(){
        float insuranceCheckAmount = 0;//Globals.CELL_WIDTH / 10;
        for(int i = 9; i >= 1; i--){
            if((this.blobY - Globals.AD_HEIGHT) / Globals.CELL_WIDTH + insuranceCheckAmount == i){
                return i;
            }
        }
        return 1;
    }

    /**
     * snap the blob height to a position on the grid.
     * snap based on gridY
     */
    private void snapBlobHeight(){
        if(!falling) {
//            for (int i = 9; i >= 1; i--) {
//                if(blobBottom > (i * Globals.CELL_WIDTH) + Globals.AD_HEIGHT ){
//                    blobBottom = (i * Globals.CELL_WIDTH) + Globals.AD_HEIGHT;
//                    break;
//                }
//            }
            //blobBottom = (gridY * Globals.CELL_WIDTH) + Globals.AD_HEIGHT;
        }
    }

    /**
     * Draw an overlay when 1x1 blob is on hold / long press
     */
    private void drawOverlay(Canvas c, RectF r){
//        paint of red with transparency (overlayTransparency)
        Paint p = new Paint();
        p.setColor(Color.argb(overlayTransparency,255,0,0));
        c.drawRoundRect(r,BLOB_RADIUS,BLOB_RADIUS,p);
    }

    @Override
    public void draw(Canvas c){

        if(!bGoalConnected) {
            rect.set(blobX + 5 + (bLeftVariance ? variance : 0), blobY + 5 + (bTopVariance ? variance : 0),
                    blobWidth - 5 - (bRightVariance ? variance : 0), blobBottom - 5 - (bBottomVariance ? variance : 0));
        }
        else if(bGoalConnected){
            rect.set(blobX + 5, blobY + 5, blobWidth - 5, blobBottom - 5);
        }
        paint.setColor(ColourScheme.getColour(cut));
        c.drawRoundRect(rect,BLOB_RADIUS,BLOB_RADIUS, paint);
        //if(!Globals.isTutorialMode()){
            if(!this.bGoalConnected) {
                if(this.bDrawConnectedRect) {
                    if(cut != 0) {
                        //textPaint.setAlpha(255 - connectionAlpha);
                        c.drawText("" + cut, (blobX + blobWidth) / 2,
                                (blobY + blobBottom + super.getTextCenter()) / 2, textPaint);
                    }
                }
            }
        //}
        if(this.bDrawConnectedRect && !this.bGoalConnected) {
            RectF shine = new RectF();
            shine.set(blobX+ cellIndent, blobY + (cellIndent),
                    //blobWidth - shineVariance,
                    blobWidth - shineVariance,
                    blobY + Globals.CELL_WIDTH / 2);
            //shinePaint.setAlpha(shineAlpha);
            c.drawRoundRect(shine, BLOB_RADIUS, BLOB_RADIUS, shinePaint);
        }
    }

    void updateWorldPosition(float worldTransformation){
        this.blobX = originalLeft + worldTransformation;
        this.blobWidth = originalRight + worldTransformation;
    }

    void drawPreview(Canvas c, @Nullable Paint glowPaint){
        Paint p;
        if(glowPaint != null){
            p = glowPaint;
        }
        else{
            //no glow exists
            p = new Paint();
        }
        p.setColor(ColourScheme.getColour(this.cut));
        //p.setAlpha(1); //interesting effect
        c.drawRoundRect(this.rect,BLOB_RADIUS,BLOB_RADIUS, p);
    }

    public int getCut(){
        return cut;
    }

    public float getBlobY(){
        return blobY;
    }

    public float getBlobBottom(){
        return blobBottom;
    }

    public RectF getBounds(){
        RectF bounds = new RectF();
        bounds.set(super.getX()+20,blobY, super.getWidth()+20, blobBottom +20);
        return bounds;
    }

    boolean intersects(RectF r){
        if(getBounds().intersect(r)) return true;
        else return false;
    }

    protected void checkDrawConnectionRect(){
        //stop drawing goal connection and number after a tidid of size has shattered
        if(this.getBlobBottom() - this.getBlobY() < (this.originalHeight - this.getBlobY()) / 1.5f){
            bDrawConnectedRect = false;
        }
    }

    protected void resetDrawConnection(){
        this.bDrawConnectedRect = true;
    }

    boolean isFalling() {
        return falling;
    }

    void setFalling(boolean falling) {
        this.falling = falling;
    }

    public synchronized void update(ArrayList<Blob> blobs, boolean bTouchesDisabled){
        //if(this.falling){
        //do not drop if GameLoop touches are disabled and level is being cleared
            if (!bTouchesDisabled) dropUntilCollision(blobs);
        //}
        snapToNormal();

        if(!bTwinkling) {
            if (rand.nextInt() % 500 == 0) {
                bTwinkling = true;
            }
        }

        //update shine
        if(bTwinkling) {
            if (bShineIncreasing) {
                if (shineAlpha < MAX_SHINE) {
                    shineAlpha += 10;
                    if (shineAlpha >= MAX_SHINE - 10) {
                        bShineIncreasing = false;
                    }
                }
            } else if (!bShineIncreasing) {
                if (shineAlpha > MIN_SHINE) {
                    shineAlpha -= 5;
                    if (shineAlpha <= MIN_SHINE + 5) {
                        bShineIncreasing = true;
                        bTwinkling = false;
                    }
                }
            }
        }
    }

    public void updateOffsets(float offsetX, float offsetY){
        //this.rect = new RectF();
        this.rect.set( offsetX + ((super.getX())/4),
                offsetY + ((super.getY())/4),
                offsetX + ((super.getWidth())/4),
                offsetY + ((super.getHeight())/4));
        if(bIncreaseSize){
            this.iIncreaseSizeFrames++;
            //stop the growth
            if(iIncreaseSizeFrames == INCREASE_FRAMES){
                bIncreaseSize = false;
                bIncreaseSizeComplete = true;
                this.iIncreaseSizeFrames = 0;
            }
            rect = TweenManager.scaleRect(rect, this.iIncreaseSizeFrames);
//            //go to next scene, but do not stop icnreasing size
//            if(iIncreaseSizeFrames >= INCREASE_FRAMES / 4){
//            }
        }
    }

    public void updateShatter(){
        //shrinking and emitting particles
//        if(this.bShattering){
//            this.blobBottom = this.shatterParticles.getNewBlobHeight();
//            this.shatterParticles.update();
//            checkDrawConnectionRect();
//        }
        //explode and emit particles based on size
        if(this.bShattering){
            this.shatterDelayCount--;
            if(shatterDelayCount == 0) {
                if(!this.bGoalConnected) {
                    SoundManager.playBlobShatter();
                    this.shatterParticles.explode(this);
                    this.bDrawConnectedRect = false; //stop drawing the number
                    this.shatterDelayCount = MAX_SHATTER_DELAY_COUNT;
                    //recursively create a new blank blob to explode?
                    bShattering = false;
                }
            }
        }
    }

    /**
     * scales up in size, scales down, scales up, scales down, then disappears
     */
    private void explodeBlob(){
        //this.bExploding = true;
    }

    public void resetScale(){
        this.bIncreaseSizeComplete = false;
        this.bIncreaseSize = false;
        this.iIncreaseSizeFrames = 0;
    }

    public void updateTween(){
        //set spawning tween direction

        if(direction != null) {
            switch (direction) {
                case UP: {
                    //this.blobY = TweenManager.easeOutQuad(count, yPos, 50, 15);
                    //Log.w("BLOBY: ", "" + blobY);
                    this.blobY = TweenManager.linearTween( -1 * count, yPos, TweenManager.SLICE_FRAMES, TweenManager.SLICE_FRAMES_MAX);
                    break;
                }
                case DOWN: {
                    this.blobY = TweenManager.linearTween( count, yPos, TweenManager.SLICE_FRAMES, TweenManager.SLICE_FRAMES_MAX);
                    break;
                }
                case LEFT: {
                    blobX = TweenManager.linearTween( -1 * count, xPos, TweenManager.SLICE_FRAMES, TweenManager.SLICE_FRAMES_MAX);
                    break;
                }
                case RIGHT: {
                    blobX = TweenManager.linearTween( count, xPos, TweenManager.SLICE_FRAMES, TweenManager.SLICE_FRAMES_MAX);
                    break;
                }
                default: {
                    //maybe, just simply vibrate here???
                    break;
                }
            }
        }
        blobWidth = blobX + Globals.CELL_WIDTH * gridWidth;
        this.blobBottom = blobY + Globals.CELL_WIDTH * gridHeight;
        count = b_going ? count-1 : count+1;

        if(count > TweenManager.SLICE_FRAMES_MAX){
            b_going = !b_going;
            //this.direction = switchTweenDirection();
        }
        if(count == 0) {
            direction = null;
            this.b_tween = false;
            blobX = xPos;
            blobWidth = blobX + Globals.CELL_WIDTH * gridWidth;
            //this.blobY = this.yPos;
            this.blobBottom = blobY + Globals.CELL_WIDTH * gridHeight;
            Log.w("Tween UPDATE; ", "stopped!!!");
        }
    }

    String dimensions(){
        return  "x:" + getX() + " " + "y:" + getY() + " " +
                "w:" + getWidth() + " " + "h:" + getHeight();
    }

    public final boolean getTweenStatus(){
        return this.b_tween;
    }
    public void setTweenStatus(boolean b){
        this.b_tween = b;
    }
    public boolean getGoalConnected(){
        return this.bGoalConnected;
    }
    public void setGoalConnected(boolean b){ this.bGoalConnected = b; }
    public Activity getBlobActivity(){
        return this.activity;
    }
    public View getBlobView(){
        return this.view;
    }

    /**
     * return to original height for goals after shattering and disappearing.
     */
    public void resetToOriginalHeight(){
        this.blobBottom = this.originalHeight;
    }

    public void setShatterParticles(Activity a, View v){
        this.shatterParticles = ShatterIntoParticles.createShatterParticles(a, v, this);
    }
    public void cancelShatterParticles(){
        this.bShattering = false;
        this.shatterParticles = null;
    }
    public final ShatterIntoParticles getShatterParticles(){
        return this.shatterParticles;
    }

    /**
     * Show the position of the blob in the grid
     * eg. blob with a "2" cut, 1x1 size, and at the top most right would be:
     *   [1] (0,0) (1,1)
     * @return coordinates
     */
    String getCoordinates(){
        return "["+ getCut() +"] (" + getGridPos(getX()) + ", " + getGridPos(getY()) + ")"
                + " (" + getGridPos(getWidth()) + ", " + getGridPos(getHeight()) + ")";
    }

    float getGridPos(float value){
        return value/Globals.CELL_WIDTH;
    }
}

package io.github.juunoco.blobmobile;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Jun on 22/02/2018.
 * https://stackoverflow.com/questions/14514579/how-to-implement-rate-it-feature-in-android-app
 */

public class AppRater {

    private final static String APP_TITLE = "Blob";
    private final static String APP_NAME = "io.github.juunoco.blobmobile";

    //number of days & launches until prompt turns up.
    private final static int DAYS_UNTIL_PROMPT = 3;
    private final static int LAUNCHES_UNTIL_PROMPT = 3;

    public static Dialog appLaunched(Context mContext, Activity activity) {
        //SharedPreferences prefs = mContext.getSharedPreferences("apprater", 0);
        if (Globals._sharedPreferences.getBoolean("dontshowagain", false)){
            //NO ACTION
            return null;
        }

        //Increment launch counter
        long launch_count = Globals._sharedPreferences.getLong("launch_count", 0) + 1;
        Globals._sharedPreferencesEditor.putLong("launch_count", launch_count);

        //Get date of first launch
        Long date_firstLaunch = Globals._sharedPreferences.getLong("date_firstLaunch", 0);
        if (date_firstLaunch == 0){
            date_firstLaunch = System.currentTimeMillis();
            Globals._sharedPreferencesEditor.putLong("date_firstLaunch", date_firstLaunch);
        }

        Globals._sharedPreferencesEditor.commit();

        //Wait at least n days before opening
        if(launch_count >= LAUNCHES_UNTIL_PROMPT){
            if (System.currentTimeMillis() >= date_firstLaunch + (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000)) {
                return showRateDialog(mContext, activity);
            }
        }

        return null;
    }

    public static Dialog showRateDialog(final Context mContext, final Activity activity){
        final Dialog dialog = new Dialog(mContext, R.style.CustomDialog);
        new Thread(){
            public void run(){
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.setTitle("Rate " + APP_TITLE);

                        LinearLayout ll = new LinearLayout(mContext);
                        ll.setBackgroundResource(R.drawable.settings_bg_drawable);
                        ll.setOrientation(LinearLayout.VERTICAL);

                        TextView tv = new TextView(mContext);
                        tv.setText(activity.getResources().getString(R.string.appRating1).concat(APP_TITLE).concat(activity.getResources().getString(R.string.appRating2)));
                        //"Every rating means a lot, the success of this game depends entirely on your reviews, I really appreciate your effort!"
                        tv.setWidth((int)(Globals.SCREEN_WIDTH / 1.5f));
                        tv.setPadding(16, 12, 16, 10);
                        ll.addView(tv);

                        Button b1 = new Button(mContext);
                        b1.setText(activity.getResources().getString(R.string.ratingTitle).concat(APP_TITLE));
                        b1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v){
                                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + APP_NAME)));
                                dialog.dismiss();
                            }
                        });
                        ll.addView(b1);

                        Button b2 = new Button(mContext);
                        b2.setText(activity.getResources().getString(R.string.notNow));
                        b2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        ll.addView(b2);

                        Button b3 = new Button(activity.getApplicationContext());
                        b3.setText(activity.getResources().getString(R.string.noThanks));
                        b3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Globals._sharedPreferencesEditor != null){
                                    Globals._sharedPreferencesEditor.putBoolean("dontshowagain", true);
                                    Globals._sharedPreferencesEditor.commit();
                                }
                            }
                        });
                        ll.addView(b3);

                        dialog.setContentView(ll);
                        dialog.show();
                    }
                });
            }
        }.run();

        return dialog;
    }
}

package io.github.juunoco.blobmobile;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;

import io.github.juunoco.blobmobile.ui.TweenManager;

/**
 * Created by Jun on 3/03/2018.
 * //draw orange border rect
 //fade out orange border if moving

 */

public class PreviewRect {
    public float x, y, w, h, offsetX, offsetY;
    private int lvlNo;
    private final BrowsingManager bm;
    private boolean bDisplay = true;

    PreviewRect(BrowsingManager bm, int lvlNo){
        setOffsets(lvlNo);
        this.lvlNo = lvlNo;
        this.bm = bm;
        float heightOffset = (Globals.SCREEN_HEIGHT/2) - (Globals.LVL_GRID_CELL *6);
        this.x = this.offsetX * Globals.LVL_GRID_CELL;
        this.y = (this.offsetY * Globals.LVL_GRID_CELL) + heightOffset;
        w = (Globals.LVL_GRID_CELL * 3) + x;
        h = (Globals.LVL_GRID_CELL * 3) + y;
        initView();
    }

    public void update(){
    }

    /**
     * initializes the visibility fo the preview rect
     */
    //TODO, fix out of bounds rects still being displayed on last page
    private void initView(){
        this.bDisplay = true;
        int currentLvl = ((Globals.getPageNum()) * 9) + this.lvlNo;
        if(currentLvl >= LevelManager.getInstance().getLevelData().size()){
            this.bDisplay = false;
        }
        if(Globals.getPageNum() == 0){
            this.bDisplay = false;
        }
    }

    /**
     * every update and swipe to the last page will recalculate whether the preview rect shoudl be displayed or not.
     */
    public void updateOnPageChange(@Nullable BrowsingPreview.DIRECTION direction){
        this.bDisplay = true;
        //calculate whether to display this preview rect or not
        int currentLvl = 0;
        if(direction != null) {
            if (direction == BrowsingPreview.DIRECTION.LEFT) {
                currentLvl = ((Globals.getPageNum()) * 9) + this.lvlNo;
            } else {
                currentLvl = ((Globals.getPageNum() - 1) * 9) + this.lvlNo;
                if (Globals.getPageNum() - 1 == 0) {
                    this.bDisplay = false;
                }
            }
        }

        if(currentLvl >= LevelManager.getInstance().getLevelData().size()){
            this.bDisplay = false;
        }
        if(Globals.getPageNum() == 0 || Globals.getPageNum() == Globals.MAX_PAGES + 1){
            this.bDisplay = false;
        }
    }

    //draw 9 preview rects in their set positions.
    public void draw(Canvas c){
        if(bDisplay) {
            Paint paint = new Paint();
            paint.setColor(ColourScheme.getColour(4));
            paint.setStyle(Paint.Style.FILL_AND_STROKE);
            paint.setAlpha(bm.previewRectAlpha);
            c.drawRect(x, y, w, h, paint);
        }
    }

    private void setOffsets(int lvl){
        switch(lvl % 9){
            case 1:
                offsetX = 1;
                offsetY = 1;
                break;
            case 2:
                offsetX = 4.5f;
                offsetY = 1;
                break;
            case 3:
                offsetX = 8;
                offsetY = 1;
                break;
            case 4:
                offsetX = 1;
                offsetY = 4.5f;
                break;
            case 5:
                offsetX = 4.5f;
                offsetY = 4.5f;
                break;
            case 6:
                offsetX = 8;
                offsetY = 4.5f;
                break;
            case 7:
                offsetX = 1;
                offsetY = 8;
                break;
            case 8:
                offsetX = 4.5f;
                offsetY = 8;
                break;
            case 0:
                offsetX = 8;
                offsetY = 8;
                break;
        }
    }
}

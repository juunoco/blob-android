package io.github.juunoco.blobmobile.ui;

import android.app.Activity;

import java.util.ArrayList;

import io.github.juunoco.blobmobile.Globals;
import io.github.juunoco.blobmobile.R;

/**
 * Created by Jun on 5/04/2018.
 */

public class TutorialManager {


    /**
     * get the hint touchlines for each tutorial
     * @param lvlNo
     * @return
     */
    public static ArrayList<Tutorial> getTutorial(Activity activity, int lvlNo){
        ArrayList<Tutorial> tutorials = new ArrayList<>();

        switch(lvlNo){
            case 1 : {
                tutorials.add(
                        Tutorial.createTextWithLine(
                                lvlNo,
                                HintTouchLine.create(1, 2, 9, 2),
                                activity.getResources().getString(R.string.lvl_1)));
                //"Slice the blob to match the width of the blank rectangle on the left"; //"Slice this blob by swiping it with your finger, make it the same height as the rectangle on the left";
                tutorials.add(
                        Tutorial.createTextWithNoLine(
                                lvlNo,
                                Globals.SCREEN_WIDTH / 2,
                                Globals.SCREEN_HEIGHT / 2.5f,
                                activity.getResources().getString(R.string.lvl_1_2)));

                tutorials.add(
                        Tutorial.createTextWithNoLine(
                                lvlNo,
                                Globals.SCREEN_WIDTH / 2,
                                Globals.SCREEN_HEIGHT / 2.5f + Globals.TUTORIAL_TEXT_SIZE_SMALL ,
                                activity.getResources().getString(R.string.lvl_1_3)));

                return tutorials;
            }
            case 2 : {
                tutorials.add(
                        Tutorial.createTextWithLine(
                                lvlNo,
                                HintTouchLine.create(8,1, 8, 9),
                                activity.getResources().getString(R.string.lvl_2)));

                tutorials.add(
                        Tutorial.createTextWithLine(
                                lvlNo,
                                HintTouchLine.create(5, 1, 5, 9),
                                activity.getResources().getString(R.string.lvl_2_2)));
                return tutorials;
            }
            case 3 : {
                tutorials.add(
                        Tutorial.createTextWithLine(
                                lvlNo,
                                HintTouchLine.create(7, 1, 7, 9),
                                activity.getResources().getString(R.string.lvl_2)));

                tutorials.add(
                        Tutorial.createTextWithLine(
                                lvlNo,
                                HintTouchLine.create(1, 6, 9, 6),
                                activity.getResources().getString(R.string.lvl_2_2)));

                tutorials.add(
                        Tutorial.createTextWithLine(
                                lvlNo,
                                HintTouchLine.create(1, 8, 9, 8),
                                activity.getResources().getString(R.string.lvl_2_2)));

                return tutorials;
            }
            case 4 : {
                tutorials.add(
                        Tutorial.createTextWithNoLine(
                                lvlNo,
                                Globals.SCREEN_WIDTH / 2,
                                Globals.SCREEN_HEIGHT / 2.5f,
                                activity.getResources().getString(R.string.lvl_4)));
                tutorials.add(
                        Tutorial.createTextWithNoLine(
                                lvlNo,
                                Globals.SCREEN_WIDTH / 2,
                                Globals.SCREEN_HEIGHT / 2.5f + Globals.TUTORIAL_TEXT_SIZE_SMALL,
                                activity.getResources().getString(R.string.lvl_4_2)));
                return tutorials;
            }
            case 24 : {
                tutorials.add(
                        Tutorial.createTextWithNoLine(
                                lvlNo,
                                Globals.SCREEN_WIDTH / 2,
                                Globals.SCREEN_HEIGHT / 2.5f,
                                activity.getResources().getString(R.string.lvl_23)));

                tutorials.add(
                        Tutorial.createLineWithNoText(
                                lvlNo,
                                HintTouchLine.create(1, 2, 2, 2)));

                tutorials.add(
                        Tutorial.createLineWithNoText(
                                lvlNo,
                                HintTouchLine.create(2, 1, 2, 2)));

                return tutorials;
            }
            case 25 : {
                tutorials.add(
                        Tutorial.createTextWithNoLine(
                                lvlNo,
                                Globals.SCREEN_WIDTH / 2,
                                Globals.SCREEN_HEIGHT / 2.5f,
                                activity.getResources().getString(R.string.lvl_24)));

                tutorials.add(
                        Tutorial.createLineWithNoText(
                                lvlNo,
                                HintTouchLine.create(4, 1, 4, 2)));

                tutorials.add(
                        Tutorial.createLineWithNoText(
                                lvlNo,
                                HintTouchLine.create(8, 1, 8, 2)));

                tutorials.add(
                        Tutorial.createLineWithNoText(
                                lvlNo,
                                HintTouchLine.create(4, 2, 8, 2)));

                return tutorials;
            }
        }
        return null;
    }

//    /**
//     * a package class to transport Tutorial with multiple touchlines and strings
//     */
//    public static class TutorialPackage {
//
//        final ArrayList<HintTouchLine> hintTouchLines;
//        final ArrayList<String> texts;
//
//        public TutorialPackage(ArrayList<HintTouchLine> hintTouchLines, ArrayList<String> texts){
//            this.hintTouchLines = hintTouchLines;
//            this.texts = texts;
//        }
//
//        public ArrayList<HintTouchLine> getHintTouchLines(){ return this.hintTouchLines; }
//        public ArrayList<String> getTexts(){ return this.texts; }
//
//    }
}

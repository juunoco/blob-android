package io.github.juunoco.blobmobile;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;

import io.github.juunoco.blobmobile.ui.HintManager;

/**
 * Created by Jun on 28/02/2018.
 * Loading game resources
 */

public class SplashActivity extends AppCompatActivity {//} implements MediaPlayer.OnPreparedListener {
    //load all resources here
    //private boolean bPrepared = false;
    //private int audioPreparedCount = 0;

    public static Bitmap titleBanner, gameBorderTop, gameBorderBottom, playButton, settingsButton, creditsButton, creditsImg, premiumPageText,
            hintsButton, rewardedAd, hintSlice, hints3, hints10, hints20, headstart, marketplace;

    //once loaded, forwarded user along to main activity
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        //bPrepared = false;
//        SoundManager.getMenuMusic().start();

        if(!Globals.isResetting()) {
            BrowsingPreview.lockImg = BitmapFactory.decodeResource(getResources(), R.drawable.lock_image);

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            Globals.SCREEN_WIDTH = displayMetrics.widthPixels;
            Globals.SCREEN_HEIGHT = displayMetrics.heightPixels;
            Globals.EXTENDED_HEIGHT = displayMetrics.heightPixels + getNavBarSize();
            if (hasNavBar()){
                Globals.SCREEN_HEIGHT = Globals.EXTENDED_HEIGHT;
            }
            Globals.CELL_WIDTH = Globals.SCREEN_WIDTH / 10;
            Globals.LVL_GRID_CELL = Globals.SCREEN_WIDTH / 12;
            Globals.AD_HEIGHT = Globals.CELL_WIDTH * 3;
//        AD_HEIGHT = SCREEN_HEIGHT / 2 - (CELL_WIDTH * 7);
            initImages();
            Globals.init(this);
            SoundManager.init(this, this);

            setupUI();

            LevelManager.getInstance().readLevelData(this.getBaseContext(), "lvls.json");
            HintManager.getInstance().readHintData(this.getBaseContext(), "hints.json");
            //HintManager.getInstance().readAvailableHintsData(this.getBaseContext(), "save.json");
            Globals.MAX_PAGES = LevelManager.getInstance().getLevelData().size() / 9 + 1;

            //load SharedPreferences & variables
            Globals._sharedPreferences = this.getPreferences(Context.MODE_PRIVATE);
            Globals._sharedPreferencesEditor = Globals._sharedPreferences.edit();

            Globals.initHintsAmt();
            HintManager.getInstance().loadHintSliceUnlockedData();
            Globals.setPageNum(Globals._sharedPreferences.getInt(Globals.prefCurrentPage, 1));
            Globals.setUnlockedLvl(Globals._sharedPreferences.getInt(Globals.prefUnlockedLevel, 1));
            //TESTING WITH ALL UNLOCKED
//            Globals.setUnlockedLvl(LevelManager.getInstance().getLevelData().size());
            Globals.setTotalSlices(Globals._sharedPreferences.getInt(Globals.prefTotalSlices, 1));

            //purchased SP
            Globals.setPurchasedPremium1(Globals._sharedPreferences.getBoolean(Globals.prefPurchasedPremium1, false));
            Globals.setPurchasedHints1(Globals._sharedPreferences.getBoolean(Globals.prefPurchasedHints1, false));
            Globals.setPurchasedHints2(Globals._sharedPreferences.getBoolean(Globals.prefPurchasedHints2, false));
            Globals.setPurchasedHintsHalf(Globals._sharedPreferences.getBoolean(Globals.prefPurchasedHintsHalf, false));
            Globals.setLastRewardedAdProvided(0);

            //check if ads shoudl be displayed, if anything ahs been purchased, then ads are removed
            Globals.setNoAds(
                Globals._sharedPreferences.getBoolean(Globals.prefPurchasedHints1, false)
                || Globals._sharedPreferences.getBoolean(Globals.prefPurchasedHints2, false)
                || Globals._sharedPreferences.getBoolean(Globals.prefPurchasedHintsHalf, false)
                || Globals._sharedPreferences.getBoolean(Globals.prefPurchasedPremium1, false)
            );

            //Audio Shared Preferences
            SoundManager.bAudioMuted = Globals._sharedPreferences.getBoolean(Globals.prefAudioSettings, false);
            SoundManager.bFXMuted = Globals._sharedPreferences.getBoolean(Globals.prefFXSettings, false);

            SoundManager.soundState = SoundManager.SOUNDSTATE.MENU;

            Log.w("", "Loading Splash Activity");

            //init IAB singletons
            MainViewController._sharedMvc = MainViewController.buildMVC(this);
            IAB._sharedIAB = IAB.build(this, MainViewController._sharedMvc.getUpdateListener());

//        bPrepared = true;
//        while(!bPrepared || audioPreparedCount < SoundManager.mediaPlayerInstances.size()){
//            //Wait here
//            try {
//                Thread.sleep(20);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }

            //Intent intent = new Intent(this, MainPage.class);
            Intent intent = new Intent(this, GameActivity.class);
            intent.putExtra("preloading", true);
            startActivity(intent);
            overridePendingTransition(android.R.anim.fade_out, android.R.anim.fade_in);
            finish();
        }
        else{
            //game is resetting
            manualDelay();
        }
    }

    private void manualDelay(){
        new Handler().postDelayed(new Runnable(){
                @Override
                public void run(){
                    Intent intent = new Intent(SplashActivity.this, LevelActivity.class);
                    startActivity(intent);
                    //overridePendingTransition(android.R.anim.fade_out, android.R.anim.fade_in);
                    finish();
                }
        }, 1500);

    }

    private void setupUI(){

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        if(SplashActivity.hasNavBar()) Globals.SCREEN_HEIGHT = Globals.EXTENDED_HEIGHT;

        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        decorView.setSystemUiVisibility(uiOptions);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }


    //called when Billing Manager has finished setting up
    public void onBillingManagerSetupFinished(){

    }

    void initImages(){
        titleBanner = BitmapFactory.decodeResource(getResources(), R.drawable.blob_banner);
        //titleBanner = BitmapFactory.decodeResource(getResources(), R.drawable.blobbs_title);
        //ratio of width to height is  height = width * 0.278;
        //int tWidth = (int)(Globals.SCREEN_WIDTH * 1.2f); //title covers 80% of screen width;
        //int tHeight = (int)(tWidth * 0.278f);
        //titleBanner = Bitmap.createScaledBitmap(titleBanner, tWidth, tHeight, false);
        //gameBorderTop = gameBorderBottom = BitmapFactory.decodeResource(getResources(), R.drawable.game_border);
        playButton = BitmapFactory.decodeResource(getResources(), R.drawable.bttn_play); //go right swipe left
        settingsButton = BitmapFactory.decodeResource(getResources(), R.drawable.actionbar_settings); //go left swipe right
        //settingsButton = Bitmap.createScaledBitmap(settingsButton, (int)Globals.CELL_WIDTH * 2, (int)Globals.CELL_WIDTH * 2, false);
        settingsButton = Bitmap.createScaledBitmap(settingsButton, (int)(Globals.CELL_WIDTH * 1.5f), (int)(Globals.CELL_WIDTH * 1.5f), false);
        creditsButton = BitmapFactory.decodeResource(getResources(), R.drawable.bttn_help);
        creditsImg = BitmapFactory.decodeResource(getResources(), R.drawable.credits_512);
        premiumPageText = BitmapFactory.decodeResource(getResources(), R.drawable.blob_premium_purchase_text);
        hintsButton = BitmapFactory.decodeResource(getResources(), R.drawable.actionbar_hint);
        hintsButton = Bitmap.createScaledBitmap(hintsButton, (int)Globals.CELL_WIDTH * 2, (int)Globals.CELL_WIDTH * 2, false);
        //rewardedAd = BitmapFactory.decodeResource(getResources(), R.drawable.bttn_rewarded_ad);
        rewardedAd = BitmapFactory.decodeResource(getResources(), R.drawable.rewarded_ad_available);
        hintSlice = BitmapFactory.decodeResource(getResources(), R.drawable.hint_slice);
        hintSlice = Bitmap.createScaledBitmap(hintSlice, (int)Globals.CELL_WIDTH, (int)Globals.CELL_WIDTH, false);

        hints3 = BitmapFactory.decodeResource(getResources(), R.drawable.hints3);
        hints10 = BitmapFactory.decodeResource(getResources(), R.drawable.hints10);
        hints20 = BitmapFactory.decodeResource(getResources(), R.drawable.hints20);
        headstart = BitmapFactory.decodeResource(getResources(), R.drawable.headstart_hints);
        marketplace = BitmapFactory.decodeResource(getResources(), R.drawable.title_marketplace);

    }

//    @Override
//    public void onPrepared(MediaPlayer mp) {
//        mp.start();
//        audioPreparedCount++;
//    }

    //Got this via stackOverflow...
    private static boolean hasNavBar() {
        int id = Resources.getSystem()
                .getIdentifier("config_showNavigationBar",
                        "bool", "android");
        return id > 0 && Resources.getSystem().getBoolean(id);
    }

    private int getNavBarSize(){
        Resources resources = getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }
}

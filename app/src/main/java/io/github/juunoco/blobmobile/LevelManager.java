package io.github.juunoco.blobmobile;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Donna on 21/09/2017.
 */

public final class LevelManager {

    private final static LevelManager INSTANCE = new LevelManager();

//    private Context c;
    private static SparseArray<Level> levelData;
    private int levelSaved;

    Level currentLevel;

    private String loadJsonFile(Context c, String filename){
        String json = "";
        try {
            InputStream is = c.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }

    void readLevelData(Context c,String filename){
        try {
            JSONObject obj = new JSONObject(loadJsonFile(c,filename));
            JSONArray levels = obj.getJSONArray("levels");

            for(int i = 0; i < levels.length(); i++){
                JSONObject levelObj = levels.getJSONObject(i);
                int num = levelObj.getInt("lvlNum");
                Log.i("LEVEL: ",num+"");
                String msg = levelObj.getString("msg");
                ArrayList<Goal> goalList = new ArrayList<>();
                JSONArray goals = levelObj.getJSONArray("goals");
                for(int j = 0; j < goals.length(); j++){
                    JSONObject goalObj = goals.getJSONObject(j);
                    String position = goalObj.getString("pos");
                    Goal.Position pos = null;
                    if(position.equals("top")) pos = Goal.Position.TOP;
                    if(position.equals("bot")) pos = Goal.Position.BOTTOM;
                    if(position.equals("left")) pos = Goal.Position.LEFT;
                    if(position.equals("right")) pos = Goal.Position.RIGHT;
                    int start = goalObj.getInt("start");
                    int length = goalObj.getInt("length");
                    int cut = goalObj.getInt("cut");
                    Log.i("GOAL: ",position + "," + start + "," + length + "," + cut);
                    goalList.add(Goal.createGoal(pos,start,length,cut));
                }
                Log.i("Level: ","num " + num);
                Level lvl = Level.createWmsg(num, msg, goalList);
                getLevelData().put(num,lvl);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void readLevelSaved(Context c,String filename){
        try {
            JSONObject obj = new JSONObject(loadJsonFile(c,filename));
            saveLevel(obj.getInt("highestLevelAttained"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

//    ArrayList<Level> generateLevels(){
//        for(int i = 0; i< getLevelSaved(); i++){
//            if(getLevelSaved() > 0) getLevelFromNum(i).setSolved();
//        }
//
//        ArrayList<Level> levelList = new ArrayList<>();
//        for(int j = 0; j<getLevelData().size(); j++){
//            levelList.add(getLevelData().get(j));
//        }
//        return levelList;
//    }

    public SparseArray<Level> getLevelData(){
        return levelData;
    }

    private int getLevelSaved(){
        return levelSaved;
    }

    private void saveLevel(int num){
        if(num > levelSaved) levelSaved = num;
    }

    Level getLevelFromNum(int num){
        return levelData.get(num);
    }

//    Level getCurrentLevel(){
////        return levelData.get(Globals.getCurrLvlNum());
//        return Level.createFromLevel(levelData.get(Globals.getCurrLvlNum()));
//    }

    //////// FACTORY METHOD(s) and CONSTRUCTOR(s) /////////

//    private final LevelManager INSTANCE = new LevelManager();

//    static LevelManager init(){
//        return new LevelManager();
//    }

    public static LevelManager getInstance(){
        return INSTANCE;
    }

    private LevelManager(){
        levelData = new SparseArray<>();
    }
}
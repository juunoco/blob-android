package io.github.juunoco.blobmobile;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Log;
import android.view.MotionEvent;

import javax.microedition.khronos.opengles.GL;

import io.github.juunoco.blobmobile.ui.TweenManager;

/**
 * Created by Donna on 16/09/2017.
 */

public class TouchLine {

    private Paint linePaint;
    protected float x1,y1,x2,y2;
    boolean released, diagonal;
    protected boolean drawable;
    private final float cellWidth = Globals.CELL_WIDTH;

    void draw(Canvas c){
        if(Globals.isDestroyMode()) linePaint.setColor(Color.RED);
        else if (Globals.isHintMode()) linePaint.setColor(Color.GREEN);
        else linePaint.setColor(Color.DKGRAY);
        if(drawable) c.drawLine(x1,y1,x2,y2,linePaint);
        if(released) drawable = false;
    }

    void reset(){
        x1 = y1 = x2 = y2 = 0;
    }

    private void startAt(float x, float y){
        drawable = true;
        if(x < cellWidth) x1 = cellWidth;
        else if(x > cellWidth * 9) {
            drawable = false;
            x1 = -1;
        }
        else x1 = x;
        if(y < (cellWidth * 4)) y1 = cellWidth * 4;
        else if(y > cellWidth * 12) {
            drawable = false;
            y1 = -1;
        }
        else y1 = y;
    }

    private void endAt(float x, float y){
        if(x > cellWidth * 9) x2 = cellWidth * 9;
        else x2 = x;
        if(y > cellWidth * 12) y2 = cellWidth * 12;
        else if(y < cellWidth *4) y2 = cellWidth *4;
        else y2 = y;
        diagonal = Math.abs(x2 - x1) == cellWidth && Math.abs(y2 - y1) == cellWidth;
                //|| (x2 - x1 == 0 && y2 - y1 <= 0); //both == means that it's 1 X 1
        if(diagonal){
            Log.w("","heyhey");
        }
    }

    void onTouch(MotionEvent e){
        //if(TweenManager.b_nothingIsTweening) {
            switch (e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    released = false;
                    startAt(snapToGrid(e.getX()), snapToGrid(e.getY()));
                    break;
                case MotionEvent.ACTION_MOVE:
                    released = false;
                    endAt(snapToGrid(e.getX()), snapToGrid(e.getY()));
                    break;
                case MotionEvent.ACTION_UP:
                    snapStraightLine(e);
                    released = true;
                    break;
            }
        //}
    }

    private float snapToGrid(float value){
        return (Math.round(value / Globals.CELL_WIDTH) * Globals.CELL_WIDTH);}

    private void snapStraightLine(MotionEvent event){
        if(!diagonal){
            float tempX = Math.abs(x1 - x2);
            float tempY = Math.abs(y1 - y2);
            if(tempX < tempY){ endAt(x1,snapToGrid(event.getY())); }
            if(tempX > tempY){ endAt(snapToGrid(event.getX()),y1); }
            if(tempX == tempY){ endAt(x1,y1); }
        }
        else endAt(snapToGrid(event.getX()), snapToGrid(event.getY()));
    }

    RectF getBounds(){
        return new RectF(x1 - 50,y1- 50,x2- 50,y2- 50);
    }

    static TouchLine makeLine(){
        return new TouchLine();
    }

    protected TouchLine (){
        linePaint = new Paint();
//        linePaint.setColor(Color.DKGRAY);
        linePaint.setStrokeWidth(10);
        linePaint.setPathEffect(new DashPathEffect(new float[] {20,20}, 0));
        x1 = y1 = x2 = y2 = 0;
    }

    float getStartX(){ return x1;}

    float getStartY(){ return y1;}

    float getEndX(){ return x2;}

    float getEndY(){ return y2;}
}

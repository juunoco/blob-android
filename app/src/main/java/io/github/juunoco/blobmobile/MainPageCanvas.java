package io.github.juunoco.blobmobile;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.MotionEvent;

import io.github.juunoco.gameui.JuunoButton;

/**
 * Created by Jun on 22/03/2018.
 * A new way of displaying main title page on canvas
 */

public class MainPageCanvas {
    private int pageNo;
    private Context context;
    private BrowsingManager browsingManager;
    private float initialTitleX, initialBorderX, initialPlayX, initialSettingsX, initialCreditsX;
    private float transformedX, transformedTitleX, transformedBorderX, transformedPlayX, transformedSettingsX, transformedCreditsX;
    private Bitmap titleBanner, gameBorderTop, gameBorderBottom, playButton, creditsButton;
    private final float TITLEY = (Globals.SCREEN_HEIGHT / 2) -  (SplashActivity.titleBanner.getHeight() / 2.0f);//(SplashActivity.titleBanner.getHeight() / 1.5f);
    private final float BUTTONY = TITLEY + (SplashActivity.titleBanner.getHeight()); //(Globals.SCREEN_HEIGHT / 3 * 2);
    private final float PAGEMARGIN = Globals.SCREEN_WIDTH;

    public MainPageCanvas(Context context, BrowsingManager browsingManager, int pageNo){
        //super(browsingManager);
        this.context                = context;
        this.browsingManager        = browsingManager;
        this.pageNo                 = pageNo;

        initPageOffset();
        initImages();
        initPosition();

    }

    void initImages(){
        //titleBanner to center of page
        titleBanner                 = Bitmap.createScaledBitmap(SplashActivity.titleBanner, (int)(SplashActivity.titleBanner.getWidth() / 1.2f), (int)(SplashActivity.titleBanner.getHeight() / 1.2f), false);
        //gameBorderTop               = SplashActivity.gameBorderTop;
        //gameBorderBottom            = SplashActivity.gameBorderBottom;
        //playButton                  = SplashActivity.playButton;
        //creditsButton               = SplashActivity.creditsButton;
    }

    void initPosition(){
        this.initialTitleX          = (Globals.SCREEN_WIDTH/ 2) - (titleBanner.getWidth() / 2);
        this.initialBorderX         = 0;
        //this.initialPlayX           = (Globals.SCREEN_WIDTH / 2) - (playButton.getWidth() / 2.0f);
        //this.initialCreditsX        = initialPlayX + (playButton.getWidth() * 1.5f);
        //this.initialSettingsX     = (Globals.SCREEN_WIDTH / 2) - (settingsButton.getWidth() / 1.5f);
        this.transformedTitleX      = initialTitleX + transformedX;
        //this.transformedPlayX       = initialPlayX + transformedX;
        //this.transformedCreditsX    = initialCreditsX + transformedX;
    }

    private void initPageOffset(){
        int pageDiff                = this.pageNo - Globals.getPageNum();
        this.transformedX           += pageDiff * PAGEMARGIN;
    }


    /**
     * applies world transformations
     */
    public void update(){
        this.transformedX           = this.browsingManager.getWorldTransformation();

        this.transformedTitleX      = initialTitleX + transformedX;
        this.transformedBorderX     = initialBorderX + transformedX;
        //this.transformedPlayX       = initialPlayX + transformedX;
        this.transformedSettingsX   = initialSettingsX + transformedX;
        //this.transformedCreditsX    = initialCreditsX + transformedX;

    }

    public void draw(Canvas c){
        //draw title in center
        c.drawBitmap(titleBanner, transformedTitleX, TITLEY, null);

        //c.drawBitmap(playButton, transformedPlayX, BUTTONY, null);
        //c.drawBitmap(creditsButton, transformedCreditsX, BUTTONY, null);
        //c.drawBitmap(gameBorderTop, transformedBorderX, 0, null);
        //c.drawBitmap(gameBorderBottom, transformedBorderX, Globals.SCREEN_HEIGHT - gameBorderBottom.getHeight(), null);

        //c.drawBitmap(playButton, transformedPlayX, BUTTONY, null);
        //c.drawBitmap(settingsButton, transformedSettingsX, BUTTONY, null);
    }

    /**
     * provides position of button to browsing manager
     */
    public boolean isPlayClicked(MotionEvent e){

        if(e.getAction() == MotionEvent.ACTION_UP){
            if(e.getX() > transformedTitleX
                && e.getX() < transformedTitleX + titleBanner.getWidth()
                && e.getY() > TITLEY
                && e.getY() < TITLEY + titleBanner.getHeight()){
                SoundManager.playClickPlay();
                //update background color
                return true;
            }
        }
        return false;
    }

    public boolean isCreditsClicked(MotionEvent e){
//        if(e.getAction() == MotionEvent.ACTION_UP){
//            if(e.getX() > transformedCreditsX
//                    && e.getX() < transformedCreditsX + creditsButton.getWidth()
//                    && e.getY() > BUTTONY
//                    && e.getY() < BUTTONY + creditsButton.getHeight()){
//                //pressing credits button will go to end of game, the last page.
//                SoundManager.playClickPlay();
//                this.browsingManager.slideToPage(Globals.MAX_PAGES + 1);
//                return true;
//            }
//        }
        return false;
    }
}

package io.github.juunoco.blobmobile;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Random;

import io.github.juunoco.blobmobile.ui.ParticleEffect;
import io.github.juunoco.blobmobile.ui.Tutorial;
import io.github.juunoco.blobmobile.ui.TweenManager;

/**
 * Created by Donna on 7/09/2017.
 */

public class Level {

    int number;
    String message;
    private View view;
    private Activity activity;
    private ParticleEffect particles;
    public ArrayList<Blob> blobs = new ArrayList<>();
    ArrayList<Goal> goals = new ArrayList<>();
    Blob bigBlob;
    private boolean bListeningForTutorial;
    private Random rand;
    private ArrayList<Tutorial> tutorials;
    private BrowsingManager browsingManager;
    //array for blobs that are tweening and updating

    void update(boolean bTouchesDisabled){
        checks();
        for (Goal g : goals) {
            g.update();
        }
        for(int i = blobs.size() - 1; i >= 0; i--){
            if(blobs.get(i) != null) {
                blobs.get(i).update(blobs, bTouchesDisabled);
                if (blobs.get(i).bFlagShatterRemove) {
                    this.blobs.remove(blobs.get(i));
                }
                //create a possibility of wind blowing
                else {
                    if(rand.nextInt() % 500 == 0) {
                        if(particles != null) {
                            Log.w("", "Emitting Wind Particles");
                            Blob b = blobs.get(i);
                            particles.showParticles(getRandomEdge(b), b.getCut(), true);
                        }
                    }
                }
            }
        }
        updateShatters(goals, blobs);
        //only drop the blobs if the game loop has disabled touches, and level is not being cleared.
        //dropTheBlobs(1);
    }

    /**
     * gets the middle fo a blobs edge and emits particles from that location
     * @param b blob to find the edge of
     * @return the coordinates for an edge
     */
    ParticleEffect.LineCoords getRandomEdge(Blob b){
        //if height and width are small, no need to calculate
        if(b.getGridWidth() == 1 && b.getGridHeight() == 1){
            return ParticleEffect.LineCoords.createLineCoords(b.getGridX(), b.getGridY(), b.getGridWidth() + b.getGridX(), b.getGridY() + 1);
        }
        switch(rand.nextInt(4)){
            case 0 : {
                //left edge center
                return ParticleEffect.LineCoords.createLineCoords(b.getGridX(), b.getGridY() + (b.getGridHeight() / 4), b.getGridX(), b.getGridY() + (b.getGridHeight() / 4 * 3));
            }
            case 1 : {
                //right edge center
                return ParticleEffect.LineCoords.createLineCoords(b.getGridX() + b.getGridWidth(), b.getGridY() + (b.getGridHeight() / 4), b.getGridWidth() + b.getGridX(), b.getGridY() + (b.getGridHeight() / 4 * 3));
            }
            case 2 : {
                //top edge center
                return ParticleEffect.LineCoords.createLineCoords(b.getGridX() + (b.getGridWidth() / 4), b.getGridY(), b.getGridX() + (b.getGridWidth() / 4 * 3), b.getGridY());
            }
            case 3 : {
                //bottom edge center
                return ParticleEffect.LineCoords.createLineCoords(b.getGridX() + (b.getGridWidth() / 4), b.getGridY() + b.getGridHeight(), b.getGridX() + (b.getGridWidth() / 4 * 3), b.getGridY() + b.getGridHeight());
            }
            default : return null;
        }
    }

    void updateShatters(ArrayList<? extends Blob>... arrayLists){
        for(ArrayList<? extends Blob> blobArray : arrayLists){
            for(Blob tBlob : blobArray){
                tBlob.updateShatter();
            }
        }
    }

    void draw(Canvas c){
        try{
            for (Blob b : blobs) {b.draw(c);}
        } catch (ConcurrentModificationException e){ }

        for (Goal g : goals) {g.draw(c);}

        if(tutorials != null) {
            for(Tutorial t : tutorials) {
                t.draw("", c);
            }
        }
    }

    void drawPreview(Canvas c, @Nullable Paint glowPaint){
        bigBlob.drawPreview(c, glowPaint);
        for (Goal g : goals) { g.drawPreview(c, glowPaint); }
    }

    void updateOffsets(float offsetX, float offsetY){
        bigBlob.updateOffsets(offsetX, offsetY);
        for(Goal g : goals) { g.updateOffsets(offsetX, offsetY); }
    }

    public void updateWorldPosition(float worldTransformation){
        bigBlob.updateWorldPosition(worldTransformation);
        for(Goal g : goals) { g.updateWorldPosition(worldTransformation); }
    }

    //updates all of the blobs that are going through tweens
    void updateTweens(){
        int count = 0;
        for(int i = 0; i < blobs.size(); i++){
            Blob b = blobs.get(i);
            if(b.getTweenStatus()){
                b.updateTween();
                count++;
            }
        }
        //no tweens occurring
        if(count == 0){
            TweenManager.b_nothingIsTweening = true;
        }

        for(Goal g : goals){
            g.updateAlpha();
        }
    }

    /**
     * removing a blob by using a diagonal line.
     * The diagonal line is defined in TouchLine.java endAt()
     * @param line
     * @return whether the line can be removed
     */
    boolean isValidRemove(TouchLine line){
        for(Blob b: blobs){
            if(line.diagonal
                    //&& b.getBlobBottom() == line.getEndY()
                    //&& b.getWidth() == line.getEndX()
                    && b.getGridWidth() == 1
                    && b.getGridHeight() == 1
                    ){
                if(
                        //check all diagonal 1x1's
                        //bottom right to top left.
                        (b.getWidth() == line.getStartX() && Math.abs(b.getBlobBottom() - line.getStartY()) < Globals.CELL_WIDTH / 3) && b.getX()==line.getEndX() && b.getBlobY() == line.getEndY()
                               ||
                        //top left to bottom right
                        (b.getWidth() == line.getEndX() && Math.abs(b.getBlobBottom() - line.getEndY()) < Globals.CELL_WIDTH / 3) && b.getX() == line.getStartX() && b.getBlobY() == line.getStartY() //some degree of freedom when checking
                        //(b.getWidth() == line.getEndX() && b.getBlobBottom() == line.getEndY()));
                                ||
                        //bottom left to top right
                        (b.getX() == line.getStartX() &&  Math.abs(b.getBlobBottom() - line.getStartY()) < Globals.CELL_WIDTH / 3) && b.getWidth() == line.getEndX() && b.getBlobY() == line.getEndY()
                                ||
                        //top right to bottom left
                        (b.getWidth() == line.getStartX() && b.getBlobY() == line.getStartY() && b.getX() == line.getEndX() &&  Math.abs(b.getBlobBottom() - line.getEndY()) < Globals.CELL_WIDTH / 3))
                {
                    Globals.setDestroyMode(true); //changes colour of line to red

                    if (line.released) {
                        SoundManager.playBlobRemove();
                        Globals.setDestroyMode(false);
                        line.reset();
                        Log.i("Blob removed: ", b.getCoordinates());
                        particles.showParticles(ParticleEffect.LineCoords.createLineCoords(b.getGridX(), b.getGridY(), b.getGridWidth() + b.getGridX(), b.getGridY() + 1), b.getCut(), false);
                        blobs.remove(b);
                        dropTheBlobs(b.getGridPos(b.getBlobY()));
                        checks();
                        return true;
                    }
                }
            }
            if(!line.diagonal) Globals.setDestroyMode(false);
        }
        return false;
    }

    /**
     * check if a touch event was inside bounds of a blob
     * @param e
     */
    void longPressRemoveBlob(MotionEvent e){
        for(Blob b : blobs){
            if((b.getGridWidth() == 1 && b.getGridHeight() == 1) || b.getCut() == 10) {
                if (e.getX() > b.getX()
                    && e.getX() < b.getWidth()
                    && e.getY() > b.getBlobY()
                    && e.getY() < b.getBlobBottom()) {
                    SoundManager.playBlobRemove();
                    //particles.showParticles(ParticleEffect.LineCoords.createLineCoordsFromOriginalPosition(b.getX(), b.getBlobY(), b.getWidth(), (b.getBlobBottom() - b.getBlobY()) / 3), b.getCut());
                    particles.showParticles(ParticleEffect.LineCoords.createLineCoords(b.getGridX(), b.getGridY(), b.getGridWidth() + b.getGridX(), b.getGridY() + 1), b.getCut(), false);
                    blobs.remove(b);
                    dropTheBlobs(b.getGridPos(b.getBlobY()));
                    checks();
                    break;
                }
            }
        }
    }

    /**
     * Assumes that blobs are dropping, unless they are colliding with something
     * Drops everything that can be dropped
     * @param repeat the height by grid cell to drop the blob by
     */
    private void dropTheBlobs(float repeat){
        for(int i = 1; i<repeat; i++) {
            for (Blob b : blobs) {
                //b.setFalling(true); //shoudl be a check for dropping
//                while (b.isFalling()) {
//                    b.dropUntilCollision(blobs);
//                }
            }
        }
    }


    boolean isValidCut(TouchLine tl){
        if(tl.released && tl.getEndY() >= Globals.CELL_WIDTH*4){
            for (Blob blob : blobs) {
                //if(blob.getCut() < 10){
                if(!blob.isFalling()) {
                    if (tl.getStartY() == tl.getEndY()
                            && tl.getStartY() < blob.getBlobBottom()
                            && tl.getStartY() > blob.getBlobY()) {
                        if (tl.getStartX() < tl.getEndX()
                                && tl.getStartX() >= blob.getX()
                                && (tl.getStartX() + Globals.CELL_WIDTH) <= blob.getWidth()) {
                            cut(tl, blob, false);
                            return true;
                        } else if (tl.getStartX() > tl.getEndX()
                                && tl.getStartX() <= blob.getWidth()
                                && (tl.getStartX() - Globals.CELL_WIDTH) >= blob.getX()) {
                            cut(tl, blob, false);
                            return true;
                        }
                    } else if (tl.getStartX() == tl.getEndX()
                            && tl.getStartX() > blob.getX()
                            && tl.getStartX() < blob.getWidth()) {
                        if (tl.getStartY() < tl.getEndY()
                                && tl.getStartY() >= blob.getBlobY()
                                && (tl.getStartY() + (Globals.CELL_WIDTH / 2)) <= blob.getBlobBottom()) {
                            cut(tl, blob, true);
                            return true;
                        } else if (tl.getStartY() > tl.getEndY()
                                && tl.getStartY() - (Globals.CELL_WIDTH / 2) <= blob.getBlobBottom()
                                && (tl.getStartY() - Globals.CELL_WIDTH) >= blob.getBlobY()) {
                            cut(tl, blob, true);
                            return true;
                        }
                    }
                    //}
                }
            }
        }
        return false;
    }

    /**
     * actually cuts the blob
     * @param line to cut blob at
     * @param blob to remove
     * @param isVert to see fi the slice is vertical or horizontal
     */
    private void cut(TouchLine line, Blob blob, boolean isVert){
        Blob.DIRECTION direction1, direction2;
        SoundManager.playBlobSlice();
        float h1, w1, x2, y2, w2,h2;
        float x1 = gridValue(blob.getX());
        float y1 = gridValue(blob.getBlobY() - Globals.AD_HEIGHT);

        if(blob.getCut() == 10){
            //remove the blob
            blobs.remove(blob);
            line.reset();
            particles.showParticles(ParticleEffect.LineCoords.createLineCoords(blob.getGridX(), blob.getGridY(), blob.getGridWidth() + blob.getGridX(), blob.getGridY() + 1), blob.getCut(), false);
            checks();
            //random twitch
            for(Blob b : blobs){
                b.randomMove();
            }
            return;
        }

        if(isVert){
            x2 = gridValue(line.getStartX());
            y2 = y1;
            w1 = x2 - x1;
            h1 = gridValue(blob.getBlobBottom())  - gridValue(blob.getBlobY());
            w2 = gridValue(blob.getWidth()) - gridValue(line.getStartX());
            h2 = h1;
            direction1 = Blob.DIRECTION.LEFT;
            direction2 = Blob.DIRECTION.RIGHT;
        } else{
            x2 = x1;
            y2 = gridValue(line.getStartY() - Globals.AD_HEIGHT);
            w1 = gridValue(blob.getWidth()) - x1;
            h1 = y2-y1;
            w2 = w1;
            h2 = gridValue(blob.getBlobBottom() - Globals.AD_HEIGHT) - y2;
            direction1 = Blob.DIRECTION.UP;
            direction2 = Blob.DIRECTION.DOWN;
        }
        int cut = blob.getCut()+1;
        //if(Globals.isTutorialMode()) cut = 1;

        //new ParticleEffect(x2, y2, isVert? 1 : w2, isVert? h1 : 1, cut, this.view);
        useParticles(
                ParticleEffect.LineCoords.createLineCoords(x2, y2, isVert? x2 : (w2 + x2), isVert? (h1 + y2) : y2),
                blob.getCut()
        );

        blobs.remove(blob);
        float diff = (line.getStartY() - Globals.AD_HEIGHT);
        Log.i("GRID VALUE HEIGHT: ",blob.getHeight() +" vs " + diff);
        Log.i("BLOB A: ",y1 +"," +h1);
        Log.i("BLOB B: ",y2 +"," +h2);

        Blob blobby = Blob.createBlob(x1, y1, w1, h1, cut, direction1);
        blobby.setShatterParticles(activity, view);
        blobs.add(blobby);
        Blob blobbina = Blob.createBlob(x2, y2, w2, h2, cut, direction2);
        blobbina.setShatterParticles(activity, view);
        blobs.add(blobbina);

        if(bListeningForTutorial){
            //progress the tutorial to the next step
//            if(!this.tutorial.isFinished()) {
//                this.tutorial.nextStep();
//            }
            //stop drawing tutorial
            for(Tutorial t : tutorials) {
                t.setDrawing(false);
            }
        }

        line.reset();
        dropTheBlobs(Math.max(h1,h2)+2);
        checks();

        //random twitch
        for(Blob b : blobs){
            b.randomMove();
        }

        //disable any hints shown
        ((GameActivity)activity).actionBar.resetHintSlice();
        ((GameActivity)activity).actionBar.setHintsPressable(false);
        ((GameActivity)activity).actionBar.getHintButton().setFaded(true);
    }

//    @Deprecated
//    private void cut(TouchLine line, Blob blob){
//        blobs.remove(blob);
//        blobs.add(Blob.createBlob(gridValue(blob.getX()),
//                gridValue(blob.getY()),
//                gridValue(line.getEndX()) - gridValue(blob.getX()),
//                gridValue(line.getEndY() - MainPage.AD_HEIGHT) - gridValue(blob.getY()),
//                blob.getCut() + 1));
//        blobs.add(Blob.createBlob(gridValue(line.getStartX()),
//                gridValue(line.getStartY() - MainPage.AD_HEIGHT),
//                gridValue(blob.getWidth()) - gridValue(line.getStartX()),
//                gridValue(blob.getBlobBottom())  - gridValue(line.getStartY()),
//                blob.getCut() + 1));
//        checks();
//    }

    public void checks(){
        checkIfCompleted(true);
        checkIfSolved();
        //Log.i("SOLVED?", checkIfSolved() +"");
    }

    private float gridValue(float value){
        return value / Globals.CELL_WIDTH;
    }

    public void checkIfCompleted(boolean bPlaySounds){
        int initial = getInitialStatus();

        for (Goal g : goals) {
            if(g.getCut() == 0){
                g.setCompleted(checkGoalEmpty(g));
            }
            else if(g.getCut() != 0) {
                for (Blob b : blobs) {
                    if (g.getPosition() == Goal.Position.TOP
                            //&& g.getX() == b.getX() && g.getHeight() == b.getY() //g.getBlobY() - Globals.AD_HEIGHT
                            //&& g.getWidth() == b.getWidth() && g.getCut() == b.getCut()) {
                            && g.getX() == b.getX() && g.getGridY() + 1 == b.getGridY() && !b.isFalling()
                            && g.getWidth() == b.getWidth()
                            && (g.getCut() == b.getCut() || g.getCut() == 11)) {
                        g.setCompleted(true);
                        if(g.getCut() == 11) { g.setTargetColor(b.getCut()); }
                        g.setGoalConnected(true);
                        b.setGoalConnected(true);
                        break;
                    } else if (g.getPosition() == Goal.Position.BOTTOM
                            //&& g.getX() == b.getX() && g.getY() == b.getHeight()
                            //&& g.getWidth() == b.getWidth() && g.getCut() == b.getCut()) {
                            && g.getX() == b.getX() && g.getGridY() == b.getGridY() + b.getGridHeight()
                            && g.getWidth() == b.getWidth()
                            && (g.getCut() == b.getCut() || g.getCut() == 11)) {
                        g.setCompleted(true);
                        if(g.getCut() == 11) { g.setTargetColor(b.getCut()); }
                        g.setGoalConnected(true);
                        b.setGoalConnected(true);
                        break;
                    } else if (g.getPosition() == Goal.Position.LEFT
                            && g.getWidth() == b.getX() && g.getGridY() == b.getGridY()
                            && g.getGridHeight() == b.getGridHeight()
                            && (g.getCut() == b.getCut() || g.getCut() == 11)) {
                        g.setCompleted(true);
                        if(g.getCut() == 11) { g.setTargetColor(b.getCut()); }
                        g.setGoalConnected(true);
                        b.setGoalConnected(true);
                        break;
                    } else if (g.getPosition() == Goal.Position.RIGHT
                            && g.getX() == b.getWidth() && g.getGridY() == b.getGridY()
                            && g.getGridHeight() == b.getGridHeight()
                            && (g.getCut() == b.getCut() || g.getCut() == 11)){
                            //&& g.getHeight() == b.getHeight() && g.getCut() == b.getCut()) {
                        g.setCompleted(true);
                        if(g.getCut() == 11) { g.setTargetColor(b.getCut()); }
                        g.setGoalConnected(true);
                        b.setGoalConnected(true);
                        break;
                    } else {
                        g.setCompleted(false);
                        if(g.getCut() == 11) { g.setTargetColor(0); }
                        g.setGoalConnected(false);
                        //b.setGoalConnected(false);
                    }
                }
            }
        }
        if(bPlaySounds) {
            changeStatus(initial);
        }
    }

    /**
     * loops through all blobs and sees if there is a collision with this goal's defined boundaries for targeted area
     * @param g goal to check
     * @return false if a blob is colliding and not empty, true if the space to check is empty
     */
    boolean checkGoalEmpty(Goal g){
        switch(g.getPosition()){
            case TOP : {
                //create a rectangle at the bottom of the goal
                RectF goalRect = new RectF((int)(g.getX() + (Globals.CELL_WIDTH / 2)), (int)g.getBlobBottom(), (int)g.getWidth() - (Globals.CELL_WIDTH / 2), (int)(g.getBlobBottom() + Globals.CELL_WIDTH));
                return checkNotIntersect(goalRect);
            }
            case BOTTOM: {
                RectF goalRect = new RectF((int)(g.getX() + (Globals.CELL_WIDTH / 2)), (int)(g.getBlobY() - (Globals.CELL_WIDTH / 2 )), (int)(g.getWidth() - (Globals.CELL_WIDTH / 2)), g.getBlobY());
                return checkNotIntersect(goalRect);
            }
            case LEFT: {
                RectF goalRect = new RectF((int)g.getWidth(), (int)(g.getBlobY() + (Globals.CELL_WIDTH / 2)), (int)(g.getWidth() + Globals.CELL_WIDTH), (int)g.getBlobBottom());
                return checkNotIntersect(goalRect);
            }
            case RIGHT : {
                RectF goalRect = new RectF((int)(g.getX() - (Globals.CELL_WIDTH / 2)), (int)(g.getBlobY()  + (Globals.CELL_WIDTH / 2)), (int)g.getX(), (int)g.getBlobBottom());
                return checkNotIntersect(goalRect);
            }
        }
        return true;
    }

    boolean checkNotIntersect(RectF goalRect){
        for(Blob b : blobs){
            if(b.getBounds().intersect(goalRect)){
                //instantly return false, this goal is not empty
                return false;
            }
        }
        return true;
    }

    private int getInitialStatus(){
        int count = 0;
        for(Goal g: goals){
            if(g.isCompleted) count++;
        }
        return count;
    }

    private void changeStatus(int initial){
        int count = 0;
        for(Goal g: goals){
            if(g.isCompleted) {
                count++;
            }
        }
        if(initial > count) {
            SoundManager.playWrongMove();
        }
        else if(count > initial) {
            SoundManager.playGotGoal();
        }
    }

    public void startFadeout(boolean bFadeInAsWell){
        //start fading out
        if(activity != null) {
            ((GameActivity) activity).game.startReset(bFadeInAsWell);
        }
    }

    void reset(){

        blobs.clear();
        for (Goal g : goals){
            g.setCompleted(false);
            g.setGoalAlpha();
            g.setGoalConnected(false);
            g.resetToOriginalHeight();//TODO test to see if it fixes, otherwise, remove
            g.cancelShatterParticles();//TODO test to see if it fixes, otherwise, remove
            g.resetDrawConnection();
        }
        initGoalShatterParticles();
//        blobs.clear();
//        rest(100);
        Blob bigBlob = Blob.bigBlob(this.activity, this.view);
        bigBlob.setShatterParticles(activity, this.view);
        blobs.add(bigBlob);
    }

    void checkIfSolved(){
        int size = goals.size();
        for (Goal g : goals){
            if(g.isCompleted) size --;
        }
        if(size == 0) {
            Globals.setSolved(true);
        }
        else{
            Globals.setSolved(false);
        }
    }

    void rest(int time){
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /// ACCESSORS and MUTATORS ///

//    boolean isSolved(){
//        return solved;
//    }
//
//    void setSolved(){
//        solved = true;
//        //write to file here?
//    }

//    private ArrayList<Blob> getBlobs(){
//        return blobs;
//    }
//
//    private ArrayList<Goal> getGoals(){
//        return goals;
//    }

    int getLevelNum(){
        return number;
    }
    public void setParticles(ParticleEffect p){
        this.particles = p;
    }
    public ArrayList<Tutorial> getTutorials(){ return this.tutorials; }

    public void useParticles(ParticleEffect.LineCoords lineCoords, int cut){
        if(particles==null){
            throw new NullPointerException("Particles do not exist!");
        }
        //particles.showParticles(x, y, w, h, cut);
        particles.showParticles(lineCoords, cut, false);
    }

    public void setShatterParticlesActivityAndView(Activity a, View v){
        this.activity = a;
        this.view = v;
        for(Blob b : goals){
            b.setShatterParticles(a, v);
        }
    }

    public final Activity getActivity(){
        return this.activity;
    }
    public final View getView(){
        return this.view;
    }

    public final ParticleEffect getParticleEffect(){
        return this.particles;
    }
    //// FACTORY METHODS and PRIVATE CONSTRUCTORS ////
    
    static Level create(int num, ArrayList<Goal> goalList){
        return new Level(num, goalList);
    }

    static Level createWmsg(int num, String msg, ArrayList<Goal> goalList){
        return new Level(num, msg, goalList);
    }

    void initGoalShatterParticles(){
        //inititialize shatterParticles on Goal
        for(Goal g : goals){
            g.setShatterParticles(activity, this.view);
        }
    }

    public void setTutorial(ArrayList<Tutorial> tp){
        this.tutorials = tp;
        for(Tutorial t : tp) {
            t.setDrawing(true);
        }
        this.bListeningForTutorial = true;
    }

    static Level createFromLevel(Level lvl){
//        lvl.blobs.add(Blob.bigBlob());
//        Globals.blobList.add(Blob.bigBlob());
        return Level.createWmsg(lvl.getLevelNum(),lvl.message,lvl.goals);
    }

    public void setBrowsingManager(BrowsingManager bm){
        this.browsingManager = bm;
    }

    private Level (int num, ArrayList<Goal> goalList){
        this.bigBlob = Blob.bigBlob(getActivity(), this.getView());
        bigBlob.setShatterParticles(activity, this.view);
        blobs.add(bigBlob);
        number = num;
        goals = goalList;
        initGoalShatterParticles();
        //bigBlob = Blob.bigBlob();
        this.bListeningForTutorial = false;
        rand = new Random();
    }

    private Level (int num, String msg, ArrayList<Goal> goalList){
        this.bigBlob = Blob.bigBlob(this.getActivity(), this.getView());
        bigBlob.setShatterParticles(activity, this.view);
        blobs.add(bigBlob);
        number = num;
        message = msg;
        goals = goalList;
        initGoalShatterParticles();
        //bigBlob = Blob.bigBlob();
        this.bListeningForTutorial = false;
        rand = new Random();
    }
}
package io.github.juunoco.blobmobile.ui;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;

import com.github.jinatonic.confetti.ConfettiManager;
import com.github.jinatonic.confetti.ConfettiSource;
import com.github.jinatonic.confetti.ConfettoGenerator;
import com.github.jinatonic.confetti.confetto.BitmapConfetto;
import com.github.jinatonic.confetti.confetto.Confetto;

import java.util.Random;

import io.github.juunoco.blobmobile.Blob;
import io.github.juunoco.blobmobile.BrowsingPreview;
import io.github.juunoco.blobmobile.ColourScheme;
import io.github.juunoco.blobmobile.Globals;

/**
 * Created by Jun on 13/03/2018.
 *
 * Create the illusion of a blob shattering into particles.
 * The blob will grow smaller vertically and particles will spawn from where it disappears
 * One instance exists in each class, with regular updates of location as the spawn place changes.
 */

public class ShatterIntoParticles {

    ConfettoGenerator particleGenerator,particleGeneratorWind;
    ConfettiSource confettiSource, confettiSourceWind;
    ConfettiManager cm, cmExplodeLock;

    private final float PARTICLESIZE = 31; //41
    private final float PARTICLEVARIANCE = 30; //40
    private final float RADIUS = 6;
    private final int SHATTER_FRAMES = 3 * 15; //shatter for 3 seconds at twenty frames per second //but leave some time for the particles to fall
    private float shatterStartY;
    private int delayFrames;

    //private final float LINE_ADJUST_AMT; //make this relative to screens ize, and within a certain duration
    private int shatterFrameCount;

    private Activity activity;
    private View view;
    private Blob blob;
    private Line currentLine;
    private final Random rand = new Random();
    private float shatterDistance;

    private ShatterIntoParticles(Activity a, View v, Blob b){
        this.activity = a;
        this.view = v;
        this.blob = b;
        this.currentLine = new Line(
                this.blob.getX(),
                this.blob.getWidth(),
                this.blob.getBlobBottom()
        );
        this.shatterStartY = this.blob.getY() + Globals.AD_HEIGHT;
        this.shatterFrameCount = SHATTER_FRAMES;
        this.shatterDistance = b.getBlobBottom() - shatterStartY; //distance to shatter is the bottom of the blob minus the very top of the blob.
        //LINE_ADJUST_AMT =  shatterDistance / SHATTER_FRAMES; //speed = distance / time;
        this.delayFrames = rand.nextInt(10) + 1;
    }

    /**
     * for usage in browsing preview unlocking
     * @param a
     * @param v
     */
    private ShatterIntoParticles(Activity a, View v){
        this.activity = a;
        this.view = v;
        this.shatterFrameCount = 0;
    }

    public static ShatterIntoParticles createShatterParticles(Activity a, View v, Blob b){
        return new ShatterIntoParticles(a,v,b);
    }

    public static ShatterIntoParticles createShatterParticlesForLock(Activity a, View v){
        return new ShatterIntoParticles(a, v);
    }

    private Bitmap drawableToBitmap(Drawable drawable){

        if(drawable instanceof BitmapDrawable){
            return ((BitmapDrawable)drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    private float randomSize(){
        float max = PARTICLESIZE + PARTICLEVARIANCE;
        float min = PARTICLESIZE - PARTICLEVARIANCE;
        return rand.nextInt((int)(max - min) + 1) + min;
    }

    private Bitmap getCustomParticle(int backgroundColor){
        //RectF insetRect = new RectF(randomSize(), randomSize(), randomSize(), randomSize());
        RoundRectShape shape = new RoundRectShape(new float[] { RADIUS, RADIUS, RADIUS, RADIUS, RADIUS, RADIUS, RADIUS, RADIUS }, null, null);
        ShapeDrawable shapeDrawable = new ShapeDrawable(shape);
        shapeDrawable.setIntrinsicWidth((int)randomSize());
        shapeDrawable.setIntrinsicHeight((int)randomSize());
        shapeDrawable.getPaint().setColor(backgroundColor);
        shapeDrawable.getPaint().setStyle(Paint.Style.FILL);
        shapeDrawable.getPaint().setAntiAlias(true);
        shapeDrawable.getPaint().setFlags(Paint.ANTI_ALIAS_FLAG);
        return drawableToBitmap(shapeDrawable);
    }

    private Bitmap getParticle(int cut){
        return getCustomParticle(ColourScheme.getColour(cut));
    }

    /**
     * Called continuously on each update
     * show particles and emits for a short duration
     */
    private void showParticles(){
        final int tCut = this.blob.getCut();
        this.particleGenerator = new ConfettoGenerator() {
            @Override
            public Confetto generateConfetto(Random random) {
                return new BitmapConfetto(getParticle(tCut));
            }
        };

        this.confettiSource = new ConfettiSource(
                (int)this.currentLine.x1,
                (int)this.currentLine.y,
                (int)this.currentLine.x2,
                (int)this.currentLine.y
        );

        this.activity.runOnUiThread(new Runnable() {
            @Override
            public void run(){
                cm = new ConfettiManager(activity.getBaseContext(), particleGenerator, confettiSource, (ViewGroup) view.getParent())
                        .setEmissionDuration(300) //100 milliseconds for single explosion
                        .setEmissionRate(65)
                        .setInitialRotation(-90, 180)
                        .setAccelerationY(800.8f, 350.0f)
                        .setVelocityX(0, 260)
                        .setVelocityY(0)
                        .setRotationalVelocity(20, 180)
                        .animate();
            }
        });

        this.activity.runOnUiThread(new Runnable(){
            @Override
            public void run(){
                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run(){
                        cm.terminate();
                    }
                }, 350);
            }
        });

    }

    /**
     * called from Blob to update the shatter particles progression
     * shrink and emit new particles
     */
    public void update(){
        //this.currentLine.y1 = this.currentLine.y2 = this.currentLine.y1 - LINE_ADJUST_AMT;
        //the decrease needs to be eased into.
        //if(this.currentLine.y > blob.getY()){
        if(this.delayFrames == 0) {
            if (shatterFrameCount > 0) {
                shatterFrameCount--;
                currentLine.y = TweenManager.ShatteringTween.easeOut(
                        shatterFrameCount,
                        shatterStartY,
                        shatterDistance,
                        SHATTER_FRAMES
                );
                showParticles();
            }
        }
        else{
            delayFrames--;
        }
    }

    /**
     *  A single explosion emitting particles.
     * @param blob emit different number and location of particles based on blob size & position
     */
    public synchronized void explode(Blob blob){
        //split the Blob into 1 X 1 grid blobs all of slightly varying colours.
//        for(int yy = blob.getGridY(); yy < blob.getGridHeight() + blob.getGridY(); yy++){
//            for(int xx = blob.getGridX(); xx < blob.getGridWidth() + blob.getGridX(); xx++){
//                Blob.createBlob(xx, yy, 1, 1, rand.nextInt(10) + 1, null);
//            }
//        }
        explodeParticles(blob);
        blob.bFlagShatterRemove = true;
        //((GameLoop)view).getLvl().blobs.remove(blob);
    }

    /**
     * method for exploding particles
     * @param blob to set the emission rate and location based on size and position of the blob
     */
    private void explodeParticles(Blob blob){
        final int tCut = blob.getCut();
        this.particleGenerator = new ConfettoGenerator() {
            @Override
            public Confetto generateConfetto(Random random) {
                return new BitmapConfetto(getParticle(tCut));
            }
        };

        this.confettiSource = new ConfettiSource(
                (int)blob.getX(),
                (int)blob.getBlobY(),
                (int)blob.getWidth(),
                (int)blob.getBlobBottom()
        );

        final float sizeOfBlob = (blob.getBlobBottom() - blob.getBlobY()) * (blob.getWidth() - blob.getX()) / 450; //width * height;
        final ConfettiManager tCm = new ConfettiManager(activity.getBaseContext(), particleGenerator, confettiSource, (ViewGroup) view.getParent())
                .setEmissionDuration(300) //100 milliseconds for single explosion
                .setEmissionRate(sizeOfBlob)
                .setInitialRotation(-90, 360) //from 180
                .setAccelerationY(800.8f, 450.0f)
                .setVelocityX(0, 400)
                .setVelocityY(-200, 300)
                .setRotationalVelocity(20, 240); //from 180
        this.activity.runOnUiThread(new Runnable() {
            @Override
            public void run(){
                tCm.animate();
            }
        });

        this.activity.runOnUiThread(new Runnable(){
            @Override
            public void run(){
                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run(){
                        tCm.terminate();
                    }
                }, 3000);
            }
        });
    }

    public void explodeLock(BrowsingPreview bp){
        this.particleGenerator = new ConfettoGenerator() {
            @Override
            public Confetto generateConfetto(Random random) {
                return new BitmapConfetto(getParticle(ColourScheme.getLockedColor()));
            }
        };

        this.confettiSource = new ConfettiSource(
                (int)bp.getX(),
                (int)bp.getY(),
                (int)bp.getWidth(),
                (int)bp.getHeight()
        );

        this.activity.runOnUiThread(new Runnable(){
            @Override
            public void run(){
                cmExplodeLock = new ConfettiManager(activity.getBaseContext(), particleGenerator, confettiSource, (ViewGroup) view.getParent())
                        .setEmissionDuration(300)
                        .setEmissionRate(230)
                        .setInitialRotation(-90, 360) //from 180
                        .setAccelerationY(800.8f, 450.0f)
                        .setVelocityX(0, 400)
                        .setVelocityY(-200, 300)
                        .setRotationalVelocity(20, 240) //from 180
                        .animate();
            }
        });

        this.activity.runOnUiThread(new Runnable(){
            @Override
            public void run(){
                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run(){
                        cmExplodeLock.terminate();
                    }
                }, 3000);
            }
        });
    }

    public Activity getActivity(){ return this.activity; }

    /**
     * Using original shattering concept to shorten the Blob.
     * @return new Blob height after adjusting for shortening at line
     * blob height is the bottom of the blob
     */
    public float getNewBlobHeight(){
        return currentLine.y;
    }

//    public final Line getLine(){
//        return this.currentLine;
//    }

    /**
     * inner class holding Line coordinates as an object
     */
    public class Line {
        private float x1, x2, y;

        public Line(float x1, float x2, float y){
            this.x1 = x1;
            this.x2 = x2;
            this.y = y;
        }
    }
}

package io.github.juunoco.blobmobile.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.MotionEvent;

import java.util.ArrayList;

import io.github.juunoco.blobmobile.BrowsingManager;
import io.github.juunoco.blobmobile.ColourScheme;
import io.github.juunoco.blobmobile.Globals;
import io.github.juunoco.blobmobile.MainPageCanvas;
import io.github.juunoco.blobmobile.R;
import io.github.juunoco.blobmobile.SoundManager;

/**
 * Created by Jun on 20/03/2018.
 *
 * The circles that indicate the current page in LevelActivity
 */

public class BrowsingPageCircles {

//    private final float CIRCLERADIUS = Globals.SCREEN_WIDTH / 20;
//    private final float CIRCLERADIUSSELECTED = Globals.SCREEN_WIDTH / 15;
//    private final float CIRCLEMARGIN = Globals.SCREEN_WIDTH / 30;
    private final float CIRCLERADIUS = Globals.SCREEN_WIDTH / 70;
    private final float CIRCLERADIUSSELECTED = Globals.SCREEN_WIDTH / 27;
    private final float CIRCLEMARGIN = Globals.SCREEN_WIDTH / 17;
    private final int NORMALCOLOR = Color.argb(255, 170, 170, 170);
    private final int SELECTEDCOLOR = Color.argb(255, 128, 128, 128);
    private final float TOTALOFFSETX = (Globals.SCREEN_WIDTH / 2 ) - ((CIRCLERADIUS + CIRCLEMARGIN ) * ((Globals.MAX_PAGES + 2) / 2)) + (CIRCLERADIUS / 2) + (CIRCLEMARGIN / 2 );
    private final float YPOSITION = (Globals.SCREEN_HEIGHT / 15) * 14;

    private ArrayList<BrowsingCircle> browsingCircles;
    private BrowsingManager bm;

    public BrowsingPageCircles(Context context, BrowsingManager bm){
        browsingCircles = new ArrayList<>();
        this.bm = bm;
        initCircles(context, bm);
    }

    private void initCircles(Context context, BrowsingManager bm){
        //init two circles at front for settings and title screen
        //browsingCircles.add(new BrowsingCircle(1, BitmapFactory.decodeResource(context.getResources(), R.drawable.bttn_settings), ((CIRCLERADIUS + CIRCLEMARGIN) * 1) + TOTALOFFSETX, YPOSITION));
        //Bitmap b = BitmapFactory.decodeResource(context.getResources(), R.drawable.bttn_help);
        //scale the bitmap here
//        b = Bitmap.createScaledBitmap(b, (int)CIRCLERADIUS / 3, (int)CIRCLERADIUS / 3, false);
//        RoundedBitmapDrawable circularB = RoundedBitmapDrawableFactory.create(context.getResources(), b);
//        circularB.setCircular(true);
        int mainPageNo = 0;
        browsingCircles.add(new BrowsingCircle(bm, mainPageNo, Color.WHITE, ((CIRCLERADIUS + CIRCLEMARGIN) * mainPageNo) + TOTALOFFSETX, YPOSITION));

        for(int i = 1; i < Globals.MAX_PAGES + 1; i++){
            browsingCircles.add(new BrowsingCircle(bm, i, ColourScheme.getColour(i),((CIRCLERADIUS + CIRCLEMARGIN) * i) + TOTALOFFSETX, YPOSITION));
        }

        int creditsPageNo = Globals.MAX_PAGES + 1;
        browsingCircles.add(new BrowsingCircle(bm, creditsPageNo, Color.WHITE, ((CIRCLERADIUS + CIRCLEMARGIN) * creditsPageNo) + TOTALOFFSETX, YPOSITION));

    }

    /**
     * updates to check clicks
     * @param e motion event to detect
     * @return the int of the page to go to
     */
    public int checkClicks(MotionEvent e){
        if(e.getAction() == MotionEvent.ACTION_UP) {
            for (BrowsingCircle bc : browsingCircles) {
                if (bc.isClicked(e)) {
                    return bc.getPageNo();
                }
            }
        }
        return -1;
    }

    /**
     * draws each circle with an offset from the center based on the total number of circles
     * @param c the canvas to draw on
     */
    public void draw(Canvas c){

        for(int i = 0; i < browsingCircles.size(); i++){
            browsingCircles.get(i).draw(c);
        }

    }

    private class BrowsingCircle{

        private BrowsingManager bm;
        private int pageNo;
        private Bitmap bitmap;
        private int color;
        private float x, y;

        public BrowsingCircle(BrowsingManager bm, int pageNo, int color, float x, float y){
            this.bm = bm;
            this.pageNo = pageNo;
            //this.color = color;
            //this.color = Color.argb(255, 170, 170, 170);
            this.x = x;
            this.y = y;
        }

        /**
         * overloaded constructor taking in a bitmap object
         */
        public BrowsingCircle(BrowsingManager bm, int pageNo, Bitmap bitmap, float x, float y){ //RoundedBitmapDrawable roundedBitmap
            this.bm = bm;
            this.pageNo = pageNo;
            this.bitmap = Bitmap.createScaledBitmap(bitmap, (int)CIRCLERADIUS * 2, (int)CIRCLERADIUS * 2, false );
            this.x = x;
            this.y = y;
        }

        /**
         * checks if browsing circle is clicked or not
         * @param e for motion event to check
         * @return true if clicked, false if not
         */
        public boolean isClicked(MotionEvent e){
//            if(bitmap != null) {
//                if (e.getX() > x - CIRCLERADIUSSELECTED && e.getX() < x + CIRCLERADIUSSELECTED
//                        && e.getY() > y- CIRCLERADIUSSELECTED && e.getY() < y + CIRCLERADIUSSELECTED) {
//                    //go to this page
//                    if (Globals.getPageNum() != this.pageNo) {
//                        return true;
//                    }
//                }
//            }
            //else{
                if (e.getX() > x - CIRCLERADIUSSELECTED && e.getX() < x + CIRCLERADIUSSELECTED
                        && e.getY() > y - CIRCLERADIUSSELECTED && e.getY() < y + CIRCLERADIUSSELECTED) {
                    //go to this page
                    if (Globals.getPageNum() != this.pageNo) {
                        return true;
                    }
                }
            //}
            return false;
        }

        /**
         * draws each circle
         * @param c canvas to draw on
         */
        public void draw(Canvas c){
            //overlay an extra bitmap over the circle
            if(this.bitmap != null){
                c.drawBitmap(
                        bitmap,
                        this.pageNo == Globals.getPageNum() ? x + (CIRCLERADIUSSELECTED) : x + (CIRCLERADIUS),
                        this.pageNo == Globals.getPageNum() ? y + (CIRCLERADIUSSELECTED) : y + (CIRCLERADIUS),
                        null);
//                roundedBitmap.setBounds(
////                        (int)x,
////                        (int)y,
////                        (int)(this.pageNo == Globals.getPageNum() ? CIRCLERADIUSSELECTED : CIRCLERADIUS),
////                        (int)(this.pageNo == Globals.getPageNum() ? CIRCLERADIUSSELECTED : CIRCLERADIUS)
//                        204, 204, 204, 204
//                );
//                roundedBitmap.draw(c);
            }
            else{
                Paint p = new Paint();
                //p.setColor(color);
                //c.drawCircle(x, y, this.pageNo == Globals.getPageNum() ? CIRCLERADIUSSELECTED : CIRCLERADIUS, p);
                //p.setColor(this.pageNo == Globals.getPageNum() ? SELECTEDCOLOR : NORMALCOLOR);
                //c.drawCircle(x, y, this.pageNo == Globals.getPageNum() ? CIRCLERADIUSSELECTED : CIRCLERADIUS, p);

                if(this.pageNo != 0 && this.pageNo != Globals.MAX_PAGES + 1) {
                    c.drawBitmap(
                            this.pageNo == Globals.getPageNum() ? Globals.browsingCircleBig : Globals.browsingCircleSmall,
                            this.pageNo == Globals.getPageNum() ? x - (CIRCLERADIUSSELECTED / 2) : x - (CIRCLERADIUS / 2),
                            this.pageNo == Globals.getPageNum() ? y - (CIRCLERADIUSSELECTED / 2) : y - (CIRCLERADIUS / 2),
                            null
                    );
                }

                p.setColor(Color.WHITE);
                p.setTextSize(this.pageNo == Globals.getPageNum() ?  Globals.TUTORIAL_TEXT_SIZE_BIG : Globals.TUTORIAL_TEXT_SIZE_SMALL);
                //p.setTextAlign(Paint.Align.CENTER);
                p.setTypeface(Globals.getOrbitronFont());
                if(this.pageNo == 0){
                    //c.drawText(this.bm.getActivity().getResources().getString(R.string.title), x - (CIRCLERADIUSSELECTED * 2), y + CIRCLERADIUSSELECTED, p);
                    c.drawBitmap(this.pageNo == Globals.getPageNum() ? Globals.browsingCircleTitleBig : Globals.browsingCircleTitleSmall,
                            this.pageNo == Globals.getPageNum() ? x - (CIRCLERADIUSSELECTED / 2) : x - (CIRCLERADIUS / 2),
                            this.pageNo == Globals.getPageNum() ? y - (CIRCLERADIUSSELECTED / 2) : y - (CIRCLERADIUS / 2),
                            null);
                }
                else if(this.pageNo == Globals.MAX_PAGES + 1){
                    //c.drawText(this.bm.getActivity().getResources().getString(R.string.credits), x - (CIRCLERADIUSSELECTED / 2), y + CIRCLERADIUSSELECTED, p);
                    c.drawBitmap(this.pageNo == Globals.getPageNum() ? Globals.browsingCircleCreditsBig : Globals.browsingCircleCreditsSmall,
                            this.pageNo == Globals.getPageNum() ? x - (CIRCLERADIUSSELECTED / 2) : x - (CIRCLERADIUS / 2),
                            this.pageNo == Globals.getPageNum() ? y - (CIRCLERADIUSSELECTED / 2) : y - (CIRCLERADIUS / 2),
                            null);
                }
            }
        }

        public final int getPageNo(){
            return this.pageNo;
        }
    }

}

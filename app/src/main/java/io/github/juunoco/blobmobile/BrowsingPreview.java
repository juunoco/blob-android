package io.github.juunoco.blobmobile;

import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import android.view.MotionEvent;

import java.util.Random;

import io.github.juunoco.blobmobile.ui.ParticleEffect;
import io.github.juunoco.blobmobile.ui.ShatterIntoParticles;
import io.github.juunoco.blobmobile.ui.TweenManager;

/**
 * Created by Donna on 8/10/2017.
 */

/***
 * 26/10/2017 : No actual preview yet
 */
public class BrowsingPreview {

    private float x,y,w,h,offsetX, offsetY, heightOffset;
    private float xPos;
    private Level level;
    int levelNum;
    Bitmap lock, completed;
    boolean bSolved, bLocked, bMovingLeft, bMovingRight, bRemoveFlag;
    private int count = 1;
    private float endX;

    final Random rand;
    private final BrowsingManager bm;
    private final int SCROLL_DELAY_MAX_AMT = 3;
    private float scroll_delay;
    private int wobbleFrames = 0, wobbleCount = 0;
    private boolean bWobbling = false;
    private final int WOBBLE_DURATION = 9;

    private int unlockingFrames = 0, unlockingCount = 0, unlockY = 0;
    public boolean bUnlocking = false, bDrawUnlocking = true;
    private final int UNLOCK_FRAMES = 25;//30;
    private float UNLOCK_GRAVITY;
    private int unlockAlpha = 255;
    private int randomLockDrift;
    public ParticleEffect unlockingParticles;
    public ShatterIntoParticles shatterParticles;

    private int MAX_GLOW_RADIUS = 10;
    private Paint glowPaint;
    private final int GLOW_BUFFER = 15; //durationt he glow is held at max
    private int glowRadius = 1, glowMaxBuffer = GLOW_BUFFER;
    //private int glowFramesCount = 0;
    //private final float GLOW_FRAMES_TO_CHANGE = 5;
    private boolean bGlowing = false, bGlowIncreasing = false;
    private float textOffset;
    public static Bitmap lockImg;
    private RectF rect;
    private Paint paint, fill, textPaint;

    static BrowsingPreview create(BrowsingManager bm, Level l, DIRECTION d){
        return new BrowsingPreview(bm, l, d);
    }

    /**
     * overloaded constructor used in instantiation from MainPageCanvas.java
     * @param bm
     */
    BrowsingPreview(BrowsingManager bm){
        this.bm = bm;
        this.rand = new Random();
    }

    BrowsingPreview(BrowsingManager bm, Level lvl, DIRECTION direction){
        this.bm = bm;
        level = lvl;
        this.rand = new Random();
        this.scroll_delay = rand.nextInt(SCROLL_DELAY_MAX_AMT);
        this.bRemoveFlag = false;
        levelNum = lvl.getLevelNum();
        this.glowPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG);
        paint = new Paint();
        fill = new Paint();
        textPaint = new Paint();
        checkLocked();
        setOffsets(levelNum);
        float cellGrid = BrowsingManager.GRID_CELL;
        heightOffset = (Globals.SCREEN_HEIGHT/2) - (cellGrid *6);
        x = cellGrid * getOffsetX();
        y = cellGrid* getOffsetY() + heightOffset;
        w = (cellGrid * 3) + x;
        h = (cellGrid * 3) + y;
        textOffset = (w - x) / 2;
        //setPageOffsets(direction);
        setPageOffsets();
        this.xPos = x; //original point for tween to start from.
        this.randomLockDrift = (rand.nextInt(2) == 0 ? 1 : -1);
        this.UNLOCK_GRAVITY = rand.nextInt(15) + 30;
        updateGlowPaint();
        this.lock = Bitmap.createScaledBitmap(
                lockImg,
                (lockImg.getWidth() / 3),
                (lockImg.getHeight() / 3),
                false
        );


    }

    void update(){
        if(scroll_delay == 0) {
//            if (bMovingRight || bMovingLeft) {
//                count++;
//            }
//            if (bMovingLeft) {
//                this.x = TweenManager.linearTween(-1 * count, xPos, Globals.SCREEN_WIDTH, TweenManager.BROWSING_FRAMES_TOTAL);
//            }
//            if (bMovingRight) {
//                this.x = TweenManager.linearTween(count, xPos, Globals.SCREEN_WIDTH, TweenManager.BROWSING_FRAMES_TOTAL);
//
//            }
//            //end the tween
//            //if (count > TweenManager.BROWSING_FRAMES_TOTAL) {
//            if (this.x == endX) {
//                this.bMovingLeft = false;
//                this.bMovingRight = false;
//                count = 0;
//                //this.direction = switchTweenDirection();
//            }
        }
        else {
            scroll_delay--;
        }
            //remove if offscreen to save memory

        //if(this.bMovingRight = false && this.bMovingLeft == false) { //if not moving
            if(!bm.bTransitioning){
                if (this.x < -Globals.LVL_GRID_CELL || this.x > Globals.SCREEN_WIDTH) {
                    //this.bRemoveFlag = true; //no longer removing the preview if it is out of bounds
                }
            }
        //}


        updateGlow();

        if(this.bWobbling){
                this.wobbleCount = TweenManager.wobbleIncrement(wobbleCount);
                wobbleFrames++;
                if(this.unlockingParticles == null ? wobbleFrames == WOBBLE_DURATION : wobbleFrames == WOBBLE_DURATION * 2){
                    wobbleFrames = 0;
                    wobbleCount =0;
                    bWobbling = false;
                    //check if the wobble is a precursor to the drop
                    if(this.unlockingParticles != null){
                        this.bUnlocking = true;
//                        this.unlockingParticles.showUnlockingParticles(
//                                ParticleEffect.LineCoords.createLineCoords(
//                                        this.x,
//                                        this.y + this.unlockY,
//                                        this.x + this.w,
//                                        this.y + this.h + this.unlockY
//                                ),
//                                ColourScheme.getLockedColor()
//                        );
                        this.bLocked = false;
                        //reset global value to 0 so no more levels need to be loaded

                        //Globals.bUnlockingAnimationTriggered = false; //restart listening for user touches.
                        Globals.setUnlockedLvl(Globals.getCurrLvlNum() + 1); //finally, set the unlocked level into SharedPreferences

                        this.shatterParticles.explodeLock(this);
                        bDrawUnlocking = false;
//                        this.unlockingParticles.showParticles(
//                                ParticleEffect.LineCoords.createLineCoordsFromOriginalPosition(
//                                        this.x,
//                                        this.y,
//                                        this.x + this.w,
//                                        this.y + this.h
//                                ),
//                                ColourScheme.getLockedColor(),
//                                false
//                        );
                    }
                }
        }

        if(this.bUnlocking){
            //increasing size, increasing wobbles, followed by burst and break down into particles.
            this.unlockingCount = TweenManager.wobbleIncrement(wobbleCount); //wobbles more as unlocking count increases
            unlockingFrames++;

            //particles emitted a second time for beauty
//            if(unlockingFrames == UNLOCK_FRAMES / 4) {
//                this.unlockingParticles.showUnlockingParticles(
//                        ParticleEffect.LineCoords.createLineCoords(
//                                this.x,
//                                this.y + this.unlockY,
//                                this.x + this.w,
//                                this.y + this.h + this.unlockY
//                        ),
//                        ColourScheme.getLockedColor()
//                );
//            }

            if(unlockAlpha > 4) unlockAlpha-=4;
            //this.unlockY += UNLOCK_GRAVITY;
            if(unlockingFrames == UNLOCK_FRAMES){
                unlockingFrames = unlockingCount = 0;
                bUnlocking = false;
                Globals.iLvlToUnlock = 0; //enables clicks
                //emit particles from rectangular area here
                //this might be null because particle effect is still ongoing, if so, will need to create a Handler().postDelayed();
                this.unlockingParticles = null;
                this.unlockY = 0;
                Globals.bUnlockingAnimationTriggered = false;
                //if less than max levels, automatically go to next level
                if(this.levelNum < LevelManager.getInstance().getLevelData().size()) {
                    if(!PurchasesManager.isPageLocked(Globals.getPageNum())) {
                        if(bm.mLevelSelected == null && Globals.iLvlToUnlock == 0) {
                            this.bm.selectNextLevel(this.levelNum);
                        }
                    }
                }
            }
        }

        this.x = this.xPos + this.bm.getWorldTransformation();
        this.w = (Globals.LVL_GRID_CELL * 3) + x;

        level.updateOffsets(x, y); //moving this below the code above, suddenly made bboth locks and x positions to be in sync....arghhh one line...
        level.updateWorldPosition(this.bm.getWorldTransformation());
    }

    private void initWobble(){
        bWobbling = true;
        Globals.bUnlockingAnimationTriggered = true;
    }

    private void updateGlow(){
        if(this.bGlowing){
            if(bGlowIncreasing){
                glowRadius++;
                updateGlowPaint();
                if(glowRadius == MAX_GLOW_RADIUS) {
                    bGlowIncreasing = false;
                }
            }
            else if(!bGlowIncreasing){
                glowMaxBuffer--;
                if(glowMaxBuffer <= 0) { //buffer which adds delay for glow to be max, while buffer is counting.
                    glowRadius--;
                    updateGlowPaint();
                    if (glowRadius == 1) {
                        bGlowing = false;
                        //glowRadius = MAX_GLOW_RADIUS;
                        glowMaxBuffer = GLOW_BUFFER;
                        glowRadius = 1;
                    }
                }
            }
        }
    }

    private void updateGlowPaint(){
        //can also set blur amount based on the width of the canvas
        glowPaint.setColor(0xFFFFFF00);
        glowPaint.setMaskFilter(new BlurMaskFilter(glowRadius, BlurMaskFilter.Blur.NORMAL)); //.INNER is not bad, very pale
    }

    void draw(Canvas c){

        paint.setColor(ColourScheme.getColour(3));
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(4);

        rect = new RectF(getX()-5,getY()-5,getWidth()+5,getHeight()+5);

        fill.setColor(Color.WHITE);

        c.drawRoundRect(rect,20,20, fill);
        c.drawRoundRect(rect,20,20,paint);

        if (bLocked) {
            //if locked, do not draw during transition, as it doesn't look good.
            if(!bm.isWorldTransitioning()) {
                level.drawPreview(c, this.bGlowing ? this.glowPaint : null);
            }
        }
        else if(!bLocked){
            level.drawPreview(c, this.bGlowing ? this.glowPaint : null);
        }
    }

    public void drawLocked(Canvas c){
        //level is locked
        //if(!bm.bTransitioning) {
        //if(!bm.isWorldTransitioning()){
            Paint paint = this.bGlowing ? this.glowPaint : this.paint;
            paint.setColor(ColourScheme.getLockedColor());
            paint.setAlpha(unlockAlpha);
            paint.setStyle(Paint.Style.FILL_AND_STROKE);
            c.save();
            if (this.bWobbling) {
                //set degrees of wobble
                c.rotate(
                        TweenManager.wobbleDegrees(wobbleCount),
                        this.x + (this.w / 2),
                        this.y + (this.h / 2)
                );
            }

            if(this.bDrawUnlocking){
                c.rotate((TweenManager.wobbleDegrees(wobbleCount) * this.unlockingFrames * randomLockDrift),
                this.x + (this.w / 2),
                this.y + (this.h / 2));

                c.drawRect(this.x + 2, this.y + this.unlockY + 2, this.w - 2, this.h + this.unlockY - 2, paint);
            }
            Paint p = new Paint();
            p.setAlpha(150);
            if(!bUnlocking) {
                c.drawBitmap(this.lock, x + Globals.LVL_GRID_CELL * 1f, y + Globals.LVL_GRID_CELL * 1f, p); //(x + (w/ 2 )) - (this.lock.getWidth() / 2), (y + (h / 2)) - this.lock.getHeight() / 2
            }
                //rotate back to normal
            c.restore();
        //}
    }

    public void drawText(Canvas c){
        //if(!bm.isWorldTransitioning()) {
            Paint textPaint = this.bGlowing ? this.glowPaint : this.textPaint;
            textPaint.setTypeface(Globals.getOrbitronFont());
            textPaint.setColor(Color.DKGRAY);
            textPaint.setTextSize(Globals.PREVIEW_TEXT_SIZE); //72
            textPaint.setTextAlign(Paint.Align.CENTER);
            textPaint.setAlpha(bm.previewTextAlpha);
            c.drawText("" + this.level.number, x + textOffset, (y + h) / 2  + (Globals.PREVIEW_TEXT_SIZE / 3), textPaint); //(x + w) / 2
            // + (9 * (Globals.getPageNum() - 1)
        //}
    }

    private boolean checkLocked(){
        if(this.levelNum > Globals.getUnlockedLevel()){
            this.bLocked = true;
            return true;
        }
        return false;
    }

    boolean clicked(MotionEvent e){
        if(    e.getAction() == MotionEvent.ACTION_UP
            && e.getX() >= getX() && e.getX() <= getWidth()
            && e.getY() >= getY() && e.getY() <= getHeight()){
            if(!this.bLocked){
                //increase in size
                if(Globals.iLvlToUnlock == 0) {
                    this.getLevel().bigBlob.bIncreaseSize = true;
                    return true;
                }
            }
            else if(this.bLocked){
                initWobble();
                return false;
            }
        }
        else{
            Log.w("", "LOCKED BROWSING PREVIEW PRESSED");
            return false;
        }
        return false;
    }

    public float getX() {
        return x;
    }

    public void setX(float x){ this.x = x;}

    public float getY() {
        return y;
    }

    public float getWidth() {
        return w;
    }

    public float getHeight() {
        return h;
    }

    float getOffsetX(){
        return offsetX;
    }

    float getOffsetY(){
        return offsetY;
    }

    public boolean getLocked() { return bLocked; }

    public boolean getUnlocking() { return bUnlocking; }

    public void setLocked(boolean b) { this.bLocked = b; }

    public void startGlowing(){
        this.bGlowing = true;
        this.bGlowIncreasing = true;
        this.glowRadius = 1;
    }

    @Deprecated //for single loading of level and immediate loading of adjacent levels
    private void setPageOffsets(DIRECTION direction){
        switch(direction){
            case LEFT : {
                this.x -= Globals.SCREEN_WIDTH;
                this.bMovingRight = true;
                endX = x + Globals.SCREEN_WIDTH;
                break;
            }
            case RIGHT : {
                this.x += Globals.SCREEN_WIDTH;
                this.bMovingLeft = true;
                endX = x - Globals.SCREEN_WIDTH;
                break;
            }
            //no direction, initial page that is seen after fade in
            default : {
                this.bMovingRight = this.bMovingLeft = false;
            }
        }
    }

    /**
     * sets offset based on the current page we are on.
     * If lower than current page, moves everything to the left,
     * if higher than current page, moves everything to the right
     * moves by difference of pages.
     */
    private final float PAGEMARGIN = Globals.SCREEN_WIDTH;
    private void setPageOffsets(){
        int pageOfLevel = getPageOfLevel();
        int pageDiff =  pageOfLevel - Globals.getPageNum();

        this.x += pageDiff * PAGEMARGIN;

    }

    /**
     * gets the page that the level is on
     */
    private int getPageOfLevel(){
        return (int)Math.round((this.levelNum / 9.0f) + 0.49); //this is awesome logic!!!
    }

    public enum DIRECTION {
        LEFT, RIGHT, NONE
    }

    private void setOffsets(int lvl){
        switch(lvl%9){
            case 1:
                offsetX = 1;
                offsetY = 1;
                break;
            case 2:
                offsetX = 4.5f;
                offsetY = 1;
                break;
            case 3:
                offsetX = 8;
                offsetY = 1;
                break;
            case 4:
                offsetX = 1;
                offsetY = 4.5f;
                break;
            case 5:
                offsetX = 4.5f;
                offsetY = 4.5f;
                break;
            case 6:
                offsetX = 8;
                offsetY = 4.5f;
                break;
            case 7:
                offsetX = 1;
                offsetY = 8;
                break;
            case 8:
                offsetX = 4.5f;
                offsetY = 8;
                break;
            case 0:
                offsetX = 8;
                offsetY = 8;
                break;
        }
    }

    public void moveLeft(){
        //bMovingLeft = true;
        this.scroll_delay = rand.nextInt(SCROLL_DELAY_MAX_AMT);
    }
    public void moveRight(){
        //bMovingRight = true;
        this.scroll_delay = rand.nextInt(SCROLL_DELAY_MAX_AMT);
    }
    public final Level getLevel(){
        return this.level;
    }

    public void setWobbling(boolean b){
        this.bWobbling = b;
    }
}

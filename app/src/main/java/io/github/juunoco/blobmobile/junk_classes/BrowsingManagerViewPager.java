//package io.github.juunoco.blobmobile.junk_classes;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v4.app.FragmentActivity;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentStatePagerAdapter;
//import android.support.v4.view.PagerAdapter;
//import android.support.v4.view.ViewPager;
//import android.util.SparseArray;
//
//import io.github.juunoco.blobmobile.Level;
//import io.github.juunoco.blobmobile.LevelManager;
//import io.github.juunoco.blobmobile.LevelView;
//import io.github.juunoco.blobmobile.PageLocked;
//import io.github.juunoco.blobmobile.R;
//import io.github.juunoco.blobmobile.junk_classes.MainPage;
//
///**
// * Created by Jun on 28/02/2018.
// * Trying out a browsing manager using a ViewPager with all levels loaded at once
// */
//
//public class BrowsingManagerViewPager extends FragmentActivity {
//
//    SparseArray<Level> lvlArray;
//    private int currentPage;
//    private PageLocked pageLocked;
//
//    private ViewPager viewPager;
//    private LevelView levelView;
//    private PagerAdapter pagerAdapter;
//
//    private static final int NUM_PAGES = LevelManager.getInstance().getLevelData().size() / 9;
//    //public static BrowsingManagerViewPager _sharedInstance = new BrowsingManagerViewPager();
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState){
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.browsing_view_pager);
//
//        this.viewPager = (ViewPager) findViewById(R.id.viewpager);
//        this.pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
//        this.viewPager.setAdapter(this.pagerAdapter);
//        this.viewPager.setOffscreenPageLimit(NUM_PAGES - 1);
//        //this.viewPager.setCurrentItem(0);
//        //this.viewPager.getCurrentItem();
//    }
//
//    //TODO pressing back will go to the previous page.
//    @Override
//    public void onBackPressed(){
//        //if(this.viewPager.getCurrentItem() == 0)
//        //go to the previous activity, MainPage Activity.
//
//        Intent intent = new Intent(this, MainPage.class);
//        startActivity(intent);
//        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//    }
//
//
//
//    public static class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
//
//        public ScreenSlidePagerAdapter(FragmentManager fm){
//            super(fm);
//    }
//
//        @Override
//        public Fragment getItem(int position) {
//            //ScreenSlidePageFragment sspf = new ScreenSlidePageFragment();
//            Bundle sspfArgs = new Bundle();
//            sspfArgs.putInt("page", position + 1);
//            //sspf.setArguments(sspfArgs);
//            //return sspf;
//            return null;
//        }
//
//        @Override
//        public int getItemPosition(Object object){
//            return POSITION_NONE;
//        }
//
//        @Override
//        public int getCount() {
//            //return total number of pages
//            return NUM_PAGES;
//        }
//    }
//
//
//
//}

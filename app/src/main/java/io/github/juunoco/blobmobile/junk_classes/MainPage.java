//package io.github.juunoco.blobmobile.junk_classes;
//
//import android.annotation.SuppressLint;
//import android.content.Context;
//import android.content.Intent;
//import android.support.v7.app.ActionBar;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.os.Handler;
//import android.view.View;
//import android.widget.ImageButton;
//
//import io.github.juunoco.blobmobile.LevelActivity;
//import io.github.juunoco.blobmobile.R;
//import io.github.juunoco.blobmobile.SoundManager;
//
///**
// * An example full-screen activity that shows and hides the system UI (i.e.
// * status bar and navigation/system bar) with user interaction.
// */
//public class MainPage extends AppCompatActivity implements Runnable {
//
//    boolean bOnMainPage;
//    Thread thread = null;
//    long initialTime = System.nanoTime();
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main_page);
//        mainContext = this;
//
//
//        //start first sound
//        SoundManager.transitionMusic("none", "menuMusic", false);
//
//        mContentView = findViewById(R.id.fullscreen_content);
//        playButton = (ImageButton) findViewById(R.id.play_button);
//        helpButton = (ImageButton) findViewById(R.id.help_button);
//        settingsButton = (ImageButton) findViewById(R.id.settings_button);
//        hide();
//
//        initButtons();
//
//        //init IAB singletons
//        //MainViewController._sharedMvc = MainViewController.buildMVC(this);
//        //IAB._sharedIAB = IAB.build(this, MainViewController._sharedMvc.getUpdateListener());
//
//    }
//
//    //called when Billing Manager has finished setting up
//    public void onBillingManagerSetupFinished(){
//
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        SoundManager.resumeBackgroundMusic();
//        this.bOnMainPage = true;
//        thread = new Thread(this);
//        thread.start();
//        hide();
//    }
//
//    @Override
//    protected void onPause(){
//        super.onPause();
//        SoundManager.stopBackgroundMusic();
//        this.bOnMainPage = false;
//        try{
//            thread.join();
//        }catch(InterruptedException e){
//            e.printStackTrace();
//        }
//        thread = null;
//    }
//
//    public void hide() {
//        // Hide UI first
//        ActionBar actionBar = getSupportActionBar();
//        if (actionBar != null) {
//            actionBar.hide();
//        }
//
//        // Schedule a runnable to remove the status and navigation bar after a delay
//        //mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
//        mHidePart2Runnable.run();
//    }
//
//    private void initButtons(){
//
//        playButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                SoundManager.getMenuMusic().stop();
//                SoundManager.playClickPlay();
//                //Toast.makeText(mainContext,"Clicked on Play Button",Toast.LENGTH_LONG).show();
////                Intent intent = new Intent(mainContext, GameActivity.class);
//
//                //Using one surfcaeView and manually programmed scrolling
//                Intent intent = new Intent(mainContext, LevelActivity.class);
//
//                //Using a View Pager rendering different Fragments containing SurfaceViews.
//                //Intent intent = new Intent(mainContext, BrowsingManagerViewPager.class);
//
//                mainContext.startActivity(intent);
//                //overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
//                overridePendingTransition(R.anim.fade_in, R.anim.fade_out); //android.R.anim.fade_in - from default android library
//            }
//        });
//
//        helpButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //go to credits scene, and simple explanation
//                startActivity(new Intent(MainPage.this, CreditsActivity.class));
//                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//            }
//        });
//
//        settingsButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //go to settings
//                startActivity(new Intent(MainPage.this, SettingsActivity.class));
//                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//            }
//        });
//    }
//
//    ///// PRIVATE FIELDS /////
//
//    /**
//     * Some older devices needs a small delay between UI widget updates
//     * and a change of the status and navigation bar.
//     */
//    private static final int UI_ANIMATION_DELAY = 50;
//    private final Handler mHideHandler = new Handler();
//    private View mContentView;
//    private Context mainContext;
////    private Button playButton;
//    private ImageButton playButton, helpButton, settingsButton;
//    private final Runnable mHidePart2Runnable = new Runnable() {
//        @SuppressLint("InlinedApi")
//        @Override
//        public void run() {
//            // Delayed removal of status and navigation bar
//
//            // Note that some of these constants are new as of API 16 (Jelly Bean)
//            // and API 19 (KitKat). It is safe to use them, as they are inlined
//            // at compile-time and do nothing on earlier devices.
//            mContentView.setSystemUiVisibility(
//                      View.SYSTEM_UI_FLAG_LOW_PROFILE
//                    | View.SYSTEM_UI_FLAG_FULLSCREEN
//                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
//                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
//        }
//    };
//
//    @Override
//    public void run() {
//        final double timeU = 1000000000 / 20;
//        double deltaU = 0;
//
//        while (bOnMainPage){
//
//            long currentTime = System.nanoTime();
//            deltaU += (currentTime - initialTime) / timeU;
//            initialTime = currentTime;
//
//            if(deltaU >= 1){
//                SoundManager.update();
//                deltaU--;
//            }
//        }
//    }
//}

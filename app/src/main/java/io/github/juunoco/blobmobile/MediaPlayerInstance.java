package io.github.juunoco.blobmobile;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.annotation.Nullable;

import java.io.IOException;

/**
 * Created by Jun on 10/03/2018.
 * An extension of android.media.MediaPlayer,
 * This extension updates regularly and volume is adjusted according to fadeouts as prompted by activities.
 */
//as an anonymous class, can have a static declaration, but not a constructor...
//public static class MediaPlayerInstance extends MediaPlayer {
    //had trouble extending MediapLayer, woudln't play the sound...
public class MediaPlayerInstance {
    private MediaPlayer mp;
    private final static int NO_OF_FADE_STEPS =  60; //at 20 updates per second
    private final static float deltaVolume = 1 / (float) NO_OF_FADE_STEPS; //the change with each step

    private String title;
    private float targetVolume = 0;
    private float currentVolume = 0;

    public static MediaPlayerInstance create(Context c, int layoutId){
        //return (MediaPlayerInstance) MediaPlayer.create(c, layoutId);
        return new MediaPlayerInstance(c, layoutId);
    }

    private MediaPlayerInstance(Context c, int resLayoutId){
        //super.create(c, resLayoutId);
        this.mp = MediaPlayer.create(c, resLayoutId);

    }

    public final void setTargetVolume(float newTargetVolume, @Nullable boolean bRestart){
        if(!SoundManager.bAudioMuted) {
            this.targetVolume = newTargetVolume;
            if (targetVolume == 1) {
                if (!mp.isPlaying()) {
                    //this.prepareAsync(); //requires a service to prepare async. But, shoudl eb unecessary because this is in loading
                    mp.start();
                    if(bRestart){
                        //mp.seekTo(0);
                    }
                }
            }
        }
        //if sound is muted
        else {
            this.mp.setVolume(0,0);
            //this.currentVolume = 0;
            this.targetVolume = 0;
            if(mp.isPlaying()) {
                this.mp.pause();
            }
        }
    }

    public final void setTitle(String title){
        this.title = title;
    }
    public final String getTitle(){
        return this.title;
    }
    public MediaPlayer getMediaPlayer(){
        return this.mp;
    }

    public void update(){
        if(this.currentVolume != targetVolume){
            if(targetVolume > currentVolume){
                currentVolume += deltaVolume;
                if(currentVolume > targetVolume - deltaVolume){
                    currentVolume = 1;
                }
            }
            else if(targetVolume < currentVolume){
                currentVolume -= deltaVolume;
                if(currentVolume < deltaVolume + targetVolume){
                    this.currentVolume = 0;
                    if(this.getMediaPlayer().isPlaying()) {
                        this.getMediaPlayer().pause();
                    }
                    //this.getMediaPlayer().seekTo(0);
                }
            }

            this.getMediaPlayer().setVolume(currentVolume, currentVolume);
        }
//            if(this.isPlaying()){
//                if(this.currentVolume <= deltaVolume / 6){
//                    this.stop();
//                }
//            }
    }

    /**
     * instantly changes the volume, called from SettingsPopup.java
     */
    public void instantSoundChange(int volume){
        if(volume == 0){
            this.mp.setVolume(0,0);
            this.setTargetVolume(0, false );
            if(this.mp.isPlaying()) {
                this.mp.pause();
            }
            //this.mp.seekTo(0);
        }
        else if(volume == 1){
            this.mp.setVolume(1, 1);
            this.setTargetVolume(1, false);
            //this.mp.seekTo(0);
            this.mp.start();
        }
    }

    //it's possible to do using natural steps, it steps up and down, and we can notate it to step manually...
    //float log1=(float)(Math.log(maxVolume-currVolume)/Math.log(maxVolume));
    //yourMediaPlayer.setVolume(1-log1);
}

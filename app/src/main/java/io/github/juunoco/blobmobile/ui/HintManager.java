package io.github.juunoco.blobmobile.ui;

import android.content.Context;
import android.util.SparseArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import io.github.juunoco.blobmobile.GameLoop;
import io.github.juunoco.blobmobile.Globals;
import io.github.juunoco.blobmobile.LevelManager;

/**
 * Created by Jun on 27/03/2018.
 * Reads hint.json file and creates a list of hints to be used
 * Hints are only avaialbel to the player at the start of a level, so hints are an answer assuming that the player has not touched anything.
 * Every play gets a free hint, but this is not refreshed after game is reset.
 * Player can watch an ad once a day to get a free hint as a reward.
 * this class handles all interactions with GameLoop, GameLoop neevr touches Hint or HintTouchLine.
 */

public final class HintManager {

    private final static HintManager _sharedInstance = new HintManager();

    /**
     * stores data locally in shared preferences for the slice value of hint unlocked for each level
     */
    private final HashMap<Integer, Integer> mUnlockedHintsForEachLevel = new HashMap<>();

    /**
     * int storing the current hint after player has used hints.
     * 1 for lowest value
     */
    private static int currentSlice = 1;

    /**
     * an array of all of the hints for a level
     */
    private SparseArray<LevelHint> mLevelHints = new SparseArray<>();


    /**
     * an int representing the number of hints available to be used by the system
     */
    private static int availableHintsNo = 0;//TODO setting this to 3 temporarily, should be 0 on production release

    private String loadJsonFile(Context c, String fileName){
        String json = "";
        try {
            InputStream is = c.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch(IOException e){
            e.printStackTrace();
        }
        return json;
    }

    public void readHintData(Context c, String fileName){
        try {
            JSONObject obj = new JSONObject(loadJsonFile(c, fileName));
            JSONArray hints = obj.getJSONArray("hints");

            //for each level
            for(int i = 0; i < hints.length(); i++){
                JSONObject hintObj = hints.getJSONObject(i);
                int num = hintObj.getInt("lvlNum");

                //get the hints for each level
                LevelHint levelHint = new LevelHint(num);
                JSONArray tLevelHints = hintObj.getJSONArray("levelHints");
                for(int j = 0; j < tLevelHints.length(); j++) {
                    JSONObject tHintObj = tLevelHints.getJSONObject(j);
                    int sliceNo = tHintObj.getInt("sliceNo");
                    int x1 = tHintObj.getInt("x1");
                    int y1 = tHintObj.getInt("y1");
                    int x2 = tHintObj.getInt("x2");
                    int y2 = tHintObj.getInt("y2");
                    //boolean hintUnlocked = tHintObj.getBoolean("unlocked");
                    Hint tHint = new Hint(sliceNo, HintTouchLine.create(x1, y1, x2, y2), "");
                    levelHint.add(sliceNo, tHint);
                }
                //adll hints loaded, save to array
                mLevelHints.put(num, levelHint);
            }
        }catch(JSONException ex){
            ex.printStackTrace();
        }
    }

    /**
     * reads the hint slices unlocked from Shared Preferences and saves in local HashMap
     */
    public void loadHintSliceUnlockedData(){
        //initialize hashmap to 0's for every level
        for(int i = 1; i < LevelManager.getInstance().getLevelData().size(); i++){
            mUnlockedHintsForEachLevel.put(i, 0);
        }

        String hintsUnlocked = Globals.getHintsUnlockedForEachLevel();
        String[] levelData = hintsUnlocked.split(":");
        for(String levelCombined : levelData){
            String[] level = levelCombined.split("_");
            if(level.length < 1){
                level[1] = "0";
            }
            this.mUnlockedHintsForEachLevel.put(Integer.parseInt(level[0]), Integer.parseInt(level[1]));
        }
    }

    /**
     * reads the hint slice unlocked data for a given level
     * @param levelNo to read from
     */
    public int readHintSliceUnlockedData(int levelNo){
        return this.mUnlockedHintsForEachLevel.get(levelNo);
    }

    /**
     * store the new data into memory
     */
    public void saveHintSliceUnlockedData(int newLvl, int newSlice){
        this.mUnlockedHintsForEachLevel.put(newLvl, newSlice);
        String newSaveData = "";
        for(int i = 1; i < mUnlockedHintsForEachLevel.size(); i++){
            newSaveData += i + "_" + mUnlockedHintsForEachLevel.get(i) + ":";
        }
        Globals._sharedPreferencesEditor.putString(Globals.prefHintsUnlocked, newSaveData);
        Globals._sharedPreferencesEditor.commit();
    }

    /**
     * query save.json to find currently available hints
     */
//    public void readAvailableHintsData(Context c, String fileName){
//        try {
//            JSONObject saveObj = new JSONObject(loadJsonFile(c, fileName));
//            availableHintsNo = saveObj.getInt("usableHints");
//        } catch(JSONException ex){
//            ex.printStackTrace();
//        }
//    }

    public static final HintManager getInstance(){
        return _sharedInstance;
    }

    public final Hint getCurrentHint(int lvlNo, int currentSlice){
        return mLevelHints.get(lvlNo).getHint(currentSlice);
    }

    public final SparseArray<Hint> getAllHintsForLevel(int lvlNo){ return this.mLevelHints.get(lvlNo).getHintList(); }

    /**
     * get all of the hints up to a certain slice number
     * @param lvlNo to check
     * @param sliceNo to check up to
     * @return new array with slices up to a certain point
     */
    public final ArrayList<Hint> getAllHintsForLevelUpTo(int lvlNo, int sliceNo){
        ArrayList<Hint> hints = new ArrayList<>();
        for(int i = 1; i < sliceNo; i++){
            hints.add(mLevelHints.get(lvlNo).getHint(i));
        }
        return hints;
    }

    public final int getAvailableHints(){
        return availableHintsNo;
    }

    public void initAvailableHints(int newHintNo){
        availableHintsNo = newHintNo;
    }

    public void consumeAvailableHint(){
        availableHintsNo--;
        Globals.storeHintsAmt();

        //TODO update secure backend web server
    }

    public void purchaseHint(int purchaseNo){
        availableHintsNo += purchaseNo;
        //save to sharedPreferences
        Globals.storeHintsAmt();
        //TODO store in a secure backend server

    }

    /**
     * shows which hint step the player is currently on
     * @return the current hint
     */
    public int getCurrentSliceNo(){
        return currentSlice;
    }

    /**
     * each level has an array of hints witht he level number;
     */
    private class LevelHint {

        private int lvlNo;
        private SparseArray<Hint> hints = new SparseArray<>();

        public LevelHint(int lvlNo){
            this.lvlNo = lvlNo;
        }

        public void add(int sliceNo, Hint hint){
            hints.put(sliceNo, hint);
        }

        /**
         * for accessing the totla number of hints
         * @return total number of hints
         */
        public int getTotalNoHints(){
            return hints.size();
        }

        /**
         * for accesing hint at the currentSliceNo
         * @param sliceNo
         * @return hint
         */
        public Hint getHint(int sliceNo){
            return hints.get(sliceNo);
        }
        public final SparseArray<Hint> getHintList(){ return this.hints; }
    }
}

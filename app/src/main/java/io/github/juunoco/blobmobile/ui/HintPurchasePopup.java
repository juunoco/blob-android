package io.github.juunoco.blobmobile.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.android.billingclient.api.BillingClient;

import java.util.ArrayList;

import io.github.juunoco.blobmobile.GameActivity;
import io.github.juunoco.blobmobile.Globals;
import io.github.juunoco.blobmobile.IAB;
import io.github.juunoco.blobmobile.PurchasesManager;
import io.github.juunoco.blobmobile.R;

/**
 * Created by Jun on 27/03/2018.
 * Object representing the hint popup that will be made.
 * White popup bordered by yellow gold, slightly rounded as well.
 */

public class HintPurchasePopup {

    private GameActivity activity;
    View popupView;
    PopupWindow popupWindow;
    private Dialog availableHintsDialog;
    private ArrayList<Runnable> autoCancelRunnables = new ArrayList<>();
    private final int CANCEL_DURATION = 500;

    public static HintPurchasePopup create(GameActivity a){
        return new HintPurchasePopup(a);
    }

    private HintPurchasePopup(GameActivity a){
        this.activity = a;
    }

    /**
     * creates a popup which shows the basic lightbulb and choice to use a hint
     */
    public void showUsagePopup(){
        //this.availableHintsDialog = new Dialog(activity, R.style.CustomDialog);
       // availableHintsDialog.setContentView(R.layout.hint_purchase_popup);
    }

    /**
     * initializes a popup
     */
    public void initPopup(){
        LayoutInflater layoutInflater =
                (LayoutInflater) this.activity.getBaseContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        popupView = layoutInflater.inflate(R.layout.hint_purchase_popup, null);
        //this.productGallery = (LinearLayout) this.popupView.findViewById(R.id.);

        popupWindow = new PopupWindow(
                popupView,
                (int)(Globals.SCREEN_WIDTH * 0.9f),
                (int)(Globals.SCREEN_WIDTH * 1.1f),
                true
        );
        popupWindow.setAnimationStyle(R.style.CustomDialog_Window);

        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new Drawable() {
            @Override
            public void draw(@NonNull Canvas canvas) {
                //NO ACTION
            }

            @Override
            public void setAlpha(int alpha) {

            }

            @Override
            public void setColorFilter(@Nullable ColorFilter colorFilter) {

            }

            @Override
            public int getOpacity() {
                return PixelFormat.TRANSPARENT;
            }
        });

        //can set background here
        //popupWindow.setBackgroundDrawable();
        //TextView tv = (TextView) popupView.findViewById(R.id.hintsRemaining);

        //tv.setText(getPopupText());


        TextView tv2 = (TextView) popupView.findViewById(R.id.salesText);
        tv2.setText(getText2());

//        if(!Globals.getNoAds()){
//            TextView tv3 = (TextView) popupView.findViewById(R.id.ads_remove);
//            tv3.setText(activity.getResources().getString(R.string.remove_ads));
//        }

        //add images to LinearLayout
        setupImageClicks();

        View parent = this.activity.getGameLoop();
        if(parent != null) {
            popupWindow.showAtLocation(parent, Gravity.CENTER, 0, 0);
        }

        initAutoCancelRunnables();
    }

    private String getPopupText(){
        if(Globals.getPurchasedHintsHalf()){
            return this.activity.getResources().getString(R.string.purchased_half_hints);
        }
        else if(Globals.getPurchasedHints2()){
            return this.activity.getResources().getString(R.string.purchased_two_hints);
        }
        else if(Globals.getPurchasedHints1()){
            return this.activity.getResources().getString(R.string.purchased_one_hint);
        }
        else {
            return this.activity.getResources().getString(R.string.need_a_hint);
        }
    }

    private String getText2(){
        int currentHints = HintManager.getInstance().getAvailableHints();
        if(currentHints > 0) {
            return this.activity.getResources().getString(R.string.you_have) + " " + HintManager.getInstance().getAvailableHints() + " " + this.activity.getResources().getString(R.string.you_have2);
        }
        else{
            return this.activity.getResources().getString(R.string.no_hints_remaining);
        }
    }

    void initAutoCancelRunnables(){
        for(int i = 0; i < 3; i++){
            final int tI = i;
            autoCancelRunnables.add(new Runnable(){
                @Override
                public void run(){
                    ImageView tButton;
                    switch(tI){
                        case 0 : tButton = (ImageView) popupView.findViewById(R.id.hint3); break;
                        case 1 : tButton = (ImageView) popupView.findViewById(R.id.hint5); break;
                        default : tButton = (ImageView) popupView.findViewById(R.id.hint10); break;
                    }

                    tButton.setSelected(false);
                }
            });
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    void setupImageClicks(){

        final ImageView hint3 = (ImageView) this.popupView.findViewById(R.id.hint3);
        hint3.setSelected(false);
//        if(Globals.getPurchasedHints1()){
//            hint3.setEnabled(false);
//        }
        hint3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    //scale Up
                    hint3.setSelected(true);
                    //set timer in case finger moves off, set selected back
                    new Handler().postDelayed(autoCancelRunnables.get(0), CANCEL_DURATION);
                    return true;
                }
                else if(event.getAction() == MotionEvent.ACTION_UP){
                    //purchase hint 3
                    hint3.setSelected(false);
                    PurchasesManager.showHintLocalOnlyWarning(activity, PurchasesManager.SKU_HINT3);
                    return true;
                }
                return false;
            }
        });

        final ImageView hint5 = (ImageView) this.popupView.findViewById(R.id.hint5);
        hint5.setSelected(false);
//        if(Globals.getPurchasedHints2()){
//            hint5.setEnabled(false);
//        }
        hint5.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    //scale up
                    hint5.setSelected(true);
                    new Handler().postDelayed(autoCancelRunnables.get(1), CANCEL_DURATION);
                    return true;
                }
                else if(event.getAction() == MotionEvent.ACTION_UP){
                    hint5.setSelected(false);
                    //purchase hint 5
                    PurchasesManager.showHintLocalOnlyWarning(activity, PurchasesManager.SKU_HINT5);
                    return true;
                }
                return false;
            }
        });


        final ImageView hint10 = (ImageView) this.popupView.findViewById(R.id.hint10);
        hint10.setSelected(false);
//        if(Globals.getPurchasedHintsHalf()){
//            hint10.setEnabled(false);
//        }
        hint10.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    hint10.setSelected(true);
                    new Handler().postDelayed(autoCancelRunnables.get(2), CANCEL_DURATION);
                    return true;
                }
                else if(event.getAction() == MotionEvent.ACTION_UP){
                    hint10.setSelected(false);
                    //purchase hint10
                    PurchasesManager.showHintLocalOnlyWarning(activity, PurchasesManager.SKU_HINT10);
                    return true;
                }
                return false;
            }
        });

        final ImageView hint_all_lvl_1 = (ImageView) this.popupView.findViewById(R.id.hintAllLvl1);
        hint_all_lvl_1.setSelected(false);
        if(Globals.getPurchasedHints1()){
            hint_all_lvl_1.setEnabled(false);
        }
        hint_all_lvl_1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    hint_all_lvl_1.setSelected(true);
                    new Handler().postDelayed(autoCancelRunnables.get(2), CANCEL_DURATION);
                    return true;
                }
                else if(event.getAction() == MotionEvent.ACTION_UP){
                    hint_all_lvl_1.setSelected(false);
                    //purchase hint10
                    IAB._sharedIAB.initiatePurchaseFlow(PurchasesManager.SKU_AMATEUR, BillingClient.SkuType.INAPP);
                    return true;
                }
                return false;
            }
        });
    }



}

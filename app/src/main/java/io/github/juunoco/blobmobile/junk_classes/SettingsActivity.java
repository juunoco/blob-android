//package io.github.juunoco.blobmobile.junk_classes;
//
//import android.annotation.SuppressLint;
//import android.annotation.TargetApi;
//import android.graphics.Rect;
//import android.os.Bundle;
//import android.support.v7.app.*;
//import android.view.ContextThemeWrapper;
//import android.view.MotionEvent;
//import android.view.View;
//import android.widget.Button;
//import android.widget.ImageButton;
//import android.widget.TextView;
//
//import io.github.juunoco.blobmobile.Globals;
//import io.github.juunoco.blobmobile.R;
//import io.github.juunoco.blobmobile.SoundManager;
//import io.github.juunoco.gameui.CustomDialog;
//
///**
// * Created by Jun on 8/03/2018.
// */
//
//public class SettingsActivity extends AppCompatActivity implements Runnable {
//
//    private ImageButton audioButton, FXButton, resetButton;
//    private Button saveSettingsButton;
//    private TextView resetText;
//    private boolean bResetting = false;
//    private Thread thread;
//    private boolean bRunning = false;
//
//    private boolean bNothingTouched = true;
//    long initialTime = System.nanoTime();
//
//
//    @Override
//    public void onCreate(Bundle savedInstanceState){
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_settings);
//        hideNavigationBar();
//        initButtons();
//        bRunning = true;
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        SoundManager.resumeBackgroundMusic();
//        this.bRunning = true;
//        thread = new Thread(this);
//        thread.start();
//    }
//
//    @Override
//    protected void onPause(){
//        super.onPause();
//        SoundManager.stopBackgroundMusic();
//        this.bRunning = false;
//        try{
//            thread.join();
//        }catch(InterruptedException e){
//            e.printStackTrace();
//        }
//        thread = null;
//    }
//
//    void hideNavigationBar(){
//        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
//        if (actionBar != null) {
//            actionBar.hide();
//        }
//
//        View decorView = getWindow().getDecorView();
//        int uiOptions = View.SYSTEM_UI_FLAG_LOW_PROFILE
//                | View.SYSTEM_UI_FLAG_FULLSCREEN
//                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
//                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
//        decorView.setSystemUiVisibility(uiOptions);
//    }
//
//    void initButtons(){
//
//        audioButton = (ImageButton) findViewById(R.id.muteAudio);
//        audioButton.setBackgroundResource(SoundManager.bAudioMuted ? R.drawable.bttn_unmute : R.drawable.bttn_mute);
//
//        FXButton = (ImageButton) findViewById(R.id.muteFX);
//        FXButton.setBackgroundResource(SoundManager.bFXMuted ? R.drawable.bttn_unmute : R.drawable.bttn_mute);
//        resetButton = (ImageButton) findViewById(R.id.resetButton);
//        saveSettingsButton = (Button) findViewById(R.id.settingsSaveButton);
//        resetText = (TextView) findViewById(R.id.resetText);
//
//        audioButton.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                //trigger on or off
//                SoundManager.bAudioMuted = !SoundManager.bAudioMuted;
//                Globals.writeCurrentAudioSetting(SoundManager.bAudioMuted);
//                bNothingTouched = false;
//                if(SoundManager.bAudioMuted){
//                    audioButton.setBackgroundResource(R.drawable.bttn_unmute);
//                    //SoundManager.transitionMusic("menuMusic", "none");
//                    SoundManager.getMediaPlayerFromTitle("menuMusic").instantSoundChange(0);
//
//                }
//                else{
//                    audioButton.setBackgroundResource(R.drawable.bttn_mute);
//                    //SoundManager.transitionMusic("none", "menuMusic");
//                    SoundManager.getMediaPlayerFromTitle("menuMusic").instantSoundChange(1);
//
//                }
//                if(!bNothingTouched){
//                    saveSettingsButton.setText("SAVE SETTINGS");
//                }
//            }
//        });
//
//        FXButton.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v){
//                SoundManager.bFXMuted = !SoundManager.bFXMuted;
//                Globals.writeCurrentAudioFXSetting(SoundManager.bFXMuted);
//                bNothingTouched = false;
//                if(SoundManager.bFXMuted){
//                    FXButton.setBackgroundResource(R.drawable.bttn_unmute);
//                    SoundManager.adjustFXVolume(0);
//                }
//                else{
//                    FXButton.setBackgroundResource(R.drawable.bttn_mute);
//                    SoundManager.adjustFXVolume(1);
//                    SoundManager.playClickPlay();
//                }
//                if(!bNothingTouched){
//                    saveSettingsButton.setText("SAVE SETTINGS");
//                }
//            }
//        });
//
//        resetButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                bResetting = !bResetting;
//                bNothingTouched = false;
//                if(bResetting){
//                    resetText.setText("Resetting game, level progression will revert to level 1, total slices will be reset, your level pack purchases will remain");
//                }
//                else{
//                    resetText.setText("");
//                }
//                if(!bNothingTouched){
//                    saveSettingsButton.setText("SAVE SETTINGS");
//                }
//            }
//        });
//
//        saveSettingsButton.setBackgroundColor(getResources().getColor(R.color.gray));
//        saveSettingsButton.setOnTouchListener(new View.OnTouchListener(){
//            @Override
//            @SuppressLint("ClickableViewAccessibility")
//            public boolean onTouch(View v, MotionEvent e) {
//                if(e.getAction() == MotionEvent.ACTION_DOWN){
//                    saveSettingsButton.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
//                }
//                if (e.getAction() == MotionEvent.ACTION_UP) {
//
//                    saveSettingsButton.setBackgroundColor(getResources().getColor(R.color.gray));
//                    Rect saveSettingsRect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
//                    if (!CustomDialog.didMoveOut(v, saveSettingsRect, e)) {
//                        if (bResetting) {
//                            final Button bYes = new Button(new ContextThemeWrapper(SettingsActivity.this, R.style.PopupButtonPositiveTheme));
//                            //bYes.setBackground(getResources().getDrawable(R.drawable.custom_dialog_bttn));
//                            bYes.setBackgroundColor(getResources().getColor(R.color.gray));
//                            final Button bNo = new Button(new ContextThemeWrapper(SettingsActivity.this, R.style.PopupButtonNegativeTheme));
//                            //bNo.setBackground(getResources().getDrawable(R.drawable.custom_dialog_bttn));
//                            bNo.setBackgroundColor(getResources().getColor(R.color.darkgray));
//                            //build a dialog to check for resets
//                            final CustomDialog cd = new CustomDialog.Builder()
//                                    .setTitle("Resetting Blob")
//                                    .setMessage("Are you sure you would like to reset Blob?", "This will reset your progress and the total number of blobs you have sliced", "However all Level Pack purchases will remain")
//                                    .setButtons(bYes, bNo)
//                                    .build(SettingsActivity.this, SettingsActivity.this);
//
//                            bYes.setText("Yes");
//                            bYes.setOnTouchListener(new View.OnTouchListener() {
//                                @Override
//                                @TargetApi(21)
//                                public boolean onTouch(View v, MotionEvent e) {
//                                    //if confirmation is yes,
//                                    //CustomDialog.scaleButton(bYes, SettingsActivity.this);
//                                    if (e.getAction() == MotionEvent.ACTION_DOWN) {
//                                        //trigger button scaling animation, or button darkening
//                                        bYes.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
//
//                                        //bYes.setBackgroundTintMode(PorterDuff.Mode.LIGHTEN);
//                                        //check if API is larger than 21
//                                        //                                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                                        //                                        //bYes.getBackground().setColorFilter(getResources().getColor(R.color.colorPrimaryDark), PorterDuff.Mode.LIGHTEN);
//                                        //                                        bYes.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryDark)));
//                                        //                                    }
//                                        //                                    else if(Build.VERSION.SDK_INT > 16){
//                                        //                                        Drawable wrapDrawable = DrawableCompat.wrap(bYes.getBackground());
//                                        //                                        DrawableCompat.setTint(wrapDrawable, getResources().getColor(R.color.colorPrimaryDark));
//                                        //                                        bYes.setBackgroundDrawable(DrawableCompat.unwrap(wrapDrawable));
//                                        //                                    }
//                                    } else if (e.getAction() == MotionEvent.ACTION_UP) {
//                                        //                                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
//                                        //                                        bYes.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.gray)));
//                                        //                                    }
//                                        //                                    else if(Build.VERSION.SDK_INT > 16){
//                                        //                                        Drawable wrapDrawable = DrawableCompat.wrap(bYes.getBackground());
//                                        //                                        DrawableCompat.setTint(wrapDrawable, getResources().getColor(R.color.gray));
//                                        //                                        bYes.setBackgroundDrawable(DrawableCompat.unwrap(wrapDrawable));
//                                        //                                    }
//                                        bYes.setBackgroundColor(getResources().getColor(R.color.gray));
//                                        //bYes.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.black_overlay)));
//                                        Rect bYesRect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
//                                        if (!CustomDialog.didMoveOut(v, bYesRect, e)) {
//                                            Globals.setUnlockedLvl(1);
//                                            Globals.setTotalSlices(0);
//                                            Globals.setPageNum(1);
//                                            Globals.setCurrLvlNum(1);
//                                            finish();
//                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                                            //will automatically load this data from shared preferences
//                                        }
//                                    }
//                                    return true;
//                                }
//                            });
//
//                            bNo.setText("No thank you");
//                            bNo.setOnTouchListener(new View.OnTouchListener() {
//                                @Override
//                                public boolean onTouch(View v, MotionEvent e) {
//                                    //else if confirmation is no...NO ACTION
//                                    //CustomDialog.scaleButton(bNo, SettingsActivity.this);
//                                    if (e.getAction() == MotionEvent.ACTION_DOWN) {
//                                        //change button colour
//                                        bNo.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
//                                    } else if (e.getAction() == MotionEvent.ACTION_UP) {
//                                        bNo.setBackgroundColor(getResources().getColor(R.color.darkgray));
//
//                                        Rect bNoRect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
//                                        if (!CustomDialog.didMoveOut(v, bNoRect, e)) {
//                                            cd.getDialog().dismiss();
//                                        }
//                                    }
//                                    return true;
//                                }
//                            });
//                        } else {
//                            //go back
//                            finish();
//                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                        }
//                    }
//                }
//                return true;
//            }
//        });
//    }
//
//
//    @Override
//    public void onBackPressed(){
//        super.onBackPressed();
//        SoundManager.playClickReturn();
//        bRunning = false;
//        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//    }
//
//    @Override
//    public void run() {
//        final double timeU = 1000000000 / 20;
//        double deltaU = 0;
//
//        while (bRunning){
//
//            long currentTime = System.nanoTime();
//            deltaU += (currentTime - initialTime) / timeU;
//            initialTime = currentTime;
//
//            if(deltaU >= 1){
//                SoundManager.update();
//                deltaU--;
//            }
//        }
//    }
//}

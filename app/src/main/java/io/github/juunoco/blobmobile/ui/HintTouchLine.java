package io.github.juunoco.blobmobile.ui;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.util.Log;

import io.github.juunoco.blobmobile.Blob;
import io.github.juunoco.blobmobile.Globals;
import io.github.juunoco.blobmobile.SoundManager;
import io.github.juunoco.blobmobile.TouchLine;

/**
 * Created by Jun on 27/03/2018.
 * object representing the hint touchline that appears when user asks for a hint
 * each hint creates a new hinttouchline to draw.
 */

public class HintTouchLine {

    private float x1, y1, x2, y2;
    private Paint linePaint;
    private int alternationCount = 0, moduloCount = 0;
    private final int ALTERNATIONFRAMES = 5;
    private DashPathEffect dash1, dash2;

    public static HintTouchLine create(float x1, float y1, float x2, float y2){
        return new HintTouchLine(x1, y1, x2, y2);
    }

    private HintTouchLine(float x1, float y1, float x2, float y2){
        super();
        this.x1                         = x1 * Globals.CELL_WIDTH;
        this.y1                         = y1 * Globals.CELL_WIDTH + Globals.AD_HEIGHT;
        this.x2                         = x2 * Globals.CELL_WIDTH;
        this.y2                         = y2 * Globals.CELL_WIDTH + Globals.AD_HEIGHT;
        this.dash1                      = new DashPathEffect(new float[] {20,20}, 0);
        this.dash2                      = new DashPathEffect(new float[] {20,20}, 1);

        linePaint = new Paint();
        linePaint.setStrokeWidth(10);
        linePaint.setPathEffect(dash1);
        linePaint.setColor(Color.GREEN);

    }

    public void update(){
        alternationCount++;
        if(alternationCount == ALTERNATIONFRAMES){
            alternationCount = 0;
            moduloCount++;
            if(moduloCount % 2 == 0){
                linePaint.setPathEffect(dash1);
            }
            else {
                linePaint.setPathEffect(dash2);
            }
        }
    }

    public void draw(Canvas c){
        c.drawLine(x1,y1,x2,y2,linePaint);
    }

    public final int getX1(){return (int)this.x1;}
    public final int getY1(){ return (int)this.y1; }
    public final int getX2(){ return (int)this.x2;}
    public final int getY2(){ return (int)this.y2;}

}

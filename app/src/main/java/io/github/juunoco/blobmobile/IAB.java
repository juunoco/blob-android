package io.github.juunoco.blobmobile;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;

import java.util.ArrayList;
import java.util.List;

import io.github.juunoco.blobmobile.ui.HintManager;
import io.github.juunoco.blobmobile.util.Security;
import io.github.juunoco.gameui.CustomDialog;

/**
 * Created by Jun on 23/02/2018.
 *
 * Apps using in-app products must declare the com.android.vending.BILLING permission in their APK manifest file. Without the com.android.vending.BILLING permission, developers can only manage existing in-app products using the Play Console and any attempts by the app to make In-app Billing purchases will fail.
 You may publish apps that use the com.android.vending.BILLING permission in countries where users can't make in-app purchases on Google Play. To prevent your users from receiving errors, make sure your app doesn't start the purchase flow for users in non-billable countries. To check whether a country is billable, you can use the isBillingSupported request.
 *
 * https://developer.android.com/google/play/billing/billing_best_practices.html#validating-purchase
 * -keep class com.android.vending.billing.**
 */

public class IAB implements PurchasesUpdatedListener {

    //Singleton Instance
    public static IAB _sharedIAB;

    public static IAB build(Activity activity, BillingUpdatesListener ul){
        return new IAB(activity, ul);
    }

    private BillingClient mBillingClient;
    private Activity mActivity;
    private BillingUpdatesListener mBillingUpdatesListener;
    private boolean mIsServiceConnected;
    public final List<Purchase> mPurchases = new ArrayList<>();
    ArrayList<String> skuList = new ArrayList<>();
    //list storing all of the products in inventory - queryPurchasesAsync
    static List<SkuDetails> skuInventoryList = new ArrayList<>();

    private IAB(Activity activity, final BillingUpdatesListener updatesListener){
        this.mActivity = activity;
        this.mBillingUpdatesListener = updatesListener;
        mBillingClient = BillingClient.newBuilder(mActivity).setListener(this).build();

        skuList.add(PurchasesManager.ITEM_SKU); //for testing
        skuList.add(PurchasesManager.SKU_PREMIUM_PACK_1);
        skuList.add(PurchasesManager.SKU_HINT3);
        skuList.add(PurchasesManager.SKU_HINT5);
        skuList.add(PurchasesManager.SKU_HINT10);
        skuList.add(PurchasesManager.SKU_AMATEUR);
        //skuList.add(PurchasesManager.SKU_LAZY);
        //skuList.add(PurchasesManager.SKU_CHEATER);

        startServiceConnection(new Runnable() {
            @Override
            public void run(){
                Log.w("", "IAP BILLING CLIENT SETUP SUCCESSFUL");
                queryPurchases();
            }
        });

    }

    public void startServiceConnection(final Runnable executeOnSuccess){
        mBillingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@BillingClient.BillingResponse int billingResponseCode) {
                if (billingResponseCode == BillingClient.BillingResponse.OK) {
                    //The billing client is ready, can query purchases here
                    mIsServiceConnected = true;
                    if (executeOnSuccess != null){
                        executeOnSuccess.run();
                    }
                    //query SKU details upon connection
                    querySkuDetailsAsync();
                    Globals.setBillingLoadingComplete(true);
                    //testConsumeItem();
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                mIsServiceConnected = false;
            }
        });
    }

    private void executeServiceRequest(Runnable runnable){
        if (mIsServiceConnected){
            runnable.run();
        }
        else {
            //if billing service was disconnected, try to reconnect 1 time
            startServiceConnection(runnable);
        }
    }

    @Override
    public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {
        if (responseCode == BillingClient.BillingResponse.OK){
            for (Purchase purchase : purchases){
                handlePurchase(purchase);
            }
            mBillingUpdatesListener.onPurchasesUpdates(mPurchases);
        }
        else if (responseCode == BillingClient.BillingResponse.USER_CANCELED){
            //skip this purchase as the user cancelled the operation
            //handle an error caused by a user cancelling the purchase flow
        }
        else {
            //Handle any other result codes.
        }
    }

    private void handlePurchase(Purchase purchase){
        if (!verifyValidSignature(purchase.getOriginalJson(), purchase.getSignature())){
            Log.w("", "Got a purchase: " + purchase + "; but signature is bad. Skipping...");
            return;
        }

        Log.w("", "Received verified purchase: " + purchase);

        mPurchases.add(purchase);

        //if purchase is a hint, consume it and increment value here.
        //manageConsumable(purchase);

        //modify boolean values based on purchases
        //if purchase is not updated, it is loaded from shared preferences first
        if(purchase.getSku().equals(PurchasesManager.SKU_PREMIUM_PACK_1)){
            Globals.setPurchasedPremium1(true);
            Globals._sharedPreferencesEditor.putBoolean(Globals.prefPurchasedPremium1, true);
        }
        else if(purchase.getSku().equals(PurchasesManager.SKU_AMATEUR)){
            Globals.setPurchasedHints1(true);
            Globals._sharedPreferencesEditor.putBoolean(Globals.prefPurchasedHints1, true);
        }
//        else if(purchase.getSku().equals(PurchasesManager.SKU_LAZY)){
//            Globals.setPurchasedHints2(true);
//            Globals._sharedPreferencesEditor.putBoolean(Globals.prefPurchasedHints2, true);
//        }
//        else if(purchase.getSku().equals(PurchasesManager.SKU_CHEATER)){
//            Globals.setPurchasedHintsHalf(true);
//            Globals._sharedPreferencesEditor.putBoolean(Globals.prefPurchasedHintsHalf, true);
//        }
        else if(purchase.getSku().equals(PurchasesManager.SKU_HINT3)){
            consumeItem(purchase);
        }
        else if(purchase.getSku().equals(PurchasesManager.SKU_HINT5)){
            consumeItem(purchase);
        }
        else if(purchase.getSku().equals(PurchasesManager.SKU_HINT10)){
            consumeItem(purchase);
        }

        Globals.setNoAds(
                Globals._sharedPreferences.getBoolean(Globals.prefPurchasedHints1, false)
                        || Globals._sharedPreferences.getBoolean(Globals.prefPurchasedHints2, false)
                        || Globals._sharedPreferences.getBoolean(Globals.prefPurchasedHintsHalf, false)
                        || Globals._sharedPreferences.getBoolean(Globals.prefPurchasedPremium1, false)
        );
    }

    /**
     * manage a consumable hint to be used, if it's not a hint, then does nothing
     */
//    private void manageConsumable(Purchase purchase){
//
//        if(!purchase.getSku().equals(PurchasesManager.SKU_PREMIUM_PACK_1)) {
//            //consume the item
//            consumeItem(purchase);
//
//        }
//    }

    //method using consumption and hint currency
    private void consumeItem(final Purchase purchase){
        ConsumeResponseListener consumeListener = new ConsumeResponseListener() {
            @Override
            public void onConsumeResponse(@BillingClient.BillingResponse int responseCode, String outToken){
                if(responseCode == BillingClient.BillingResponse.OK){
                    //Handle the success of the consume operation
                    if (purchase.getSku().equals(PurchasesManager.SKU_HINT3)) {
                        HintManager.getInstance().purchaseHint(3);
                        showHintConfirmation(3);
                    } else if (purchase.getSku().equals(PurchasesManager.SKU_HINT5)) {
                        HintManager.getInstance().purchaseHint(10);
                        showHintConfirmation(3);
                    } else if (purchase.getSku().equals(PurchasesManager.SKU_HINT10)) {
                        HintManager.getInstance().purchaseHint(20);
                        showHintConfirmation(3);
                    }
                }
                else {
                    //log error
//                    new CustomDialog.Builder()
//                            .setTitle(mActivity.getResources().getString(R.string.error_on_purchase))
//                            .setMessage(mActivity.getResources().getString(R.string.error_on_purchase_message))
//                            .build(mActivity, mActivity);
                   // Toast.makeText(mActivity, "Error consuming item, ERROR CODE: " + responseCode, Toast.LENGTH_LONG).show();
                }
                //TODO initiate a popup saying its an error?
                //Toast.makeText(mActivity, "Error consuming item, ERROR CODE: " + responseCode, Toast.LENGTH_LONG).show();

            }
        };
        mBillingClient.consumeAsync(purchase.getPurchaseToken(), consumeListener);
        //mBillingClient.consumeAsync(purchase.getSku(), consumeListener);
    }

    void showHintConfirmation(int hintsGained){
        LayoutInflater inflater =
                (LayoutInflater) mActivity.getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View popupView = inflater.inflate(R.layout.hint_confirmation_popup, null);

        PopupWindow pWindow = new PopupWindow(
                popupView,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                true
        );
        pWindow.setAnimationStyle(R.style.CustomDialog_Window);
        pWindow.setOutsideTouchable(true);
        pWindow.setFocusable(true);
        pWindow.setBackgroundDrawable(new Drawable() {
            @Override
            public void draw(@NonNull Canvas canvas) {}

            @Override
            public void setAlpha(int alpha) {}

            @Override
            public void setColorFilter(@Nullable ColorFilter colorFilter) {}

            @Override
            public int getOpacity() {
                return PixelFormat.TRANSPARENT;
            }
        });

        TextView tv = (TextView) popupView.findViewById(R.id.hintConfirmation);
        tv.setText(
                mActivity.getResources().getString(R.string.hint_confirmation1).concat("" + hintsGained).concat(" " + mActivity.getResources().getString(R.string.hint_confirmation2))
        );

        View parent = ((LevelActivity)mActivity).getLevelView();
        if(parent != null) {
            pWindow.showAtLocation(parent, Gravity.CENTER, 0, 0);
        }
    }

//    private void testConsumeItem(){
//        final String str = PurchasesManager.SKU_PREMIUM_PACK_1;
//        ConsumeResponseListener consumeListener = new ConsumeResponseListener(){
//            @Override
//            public void onConsumeResponse(@BillingClient.BillingResponse int responseCode, String outToken){
//                if(responseCode == BillingClient.BillingResponse.OK){
//                    if(str.equals(PurchasesManager.SKU_PREMIUM_PACK_1)){
//                        Globals.setPurchasedPremium1(true);
//                    }
//                }
//            }
//        };
//        mBillingClient.consumeAsync(str, consumeListener);
//    }

    private boolean verifyValidSignature(String signedData, String signature) {
        //scrambled
        String b64_1 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAh01OrAed7TVWFqxfUxkn2qL/2xMARWN27/IVrsdHP";
        String b64_2 ="fiCzhh3ZSZw1mnEnPqMdu3AFL/jWnyEca1a+7+9v1nYcQpSsMdhLllu6YRaVerT23Bn1ZEmeNZbAIoG";
        String b64_3 ="QuVcIBmuQ7eLKKNgVO3l8CcGL7NV0n2FxffgwSLVE6QUpS5B5SzXonDPMHXGhgbzUgM3379yG+N2wj7hvuTb3F";
        String b64_4 = "HmVuh9ZtHUPnjV/dCeFvz/yQ407b/Pg9DMTQx73Js4w1StddPoUwhI3cItjeLlFMjRBD";
        String base64EncodedPublicKey5 = "HRhfW9Y56rYxvanT4+faDRK8bGMUOmR1jWasXZSDXaOLbOkW0Tu473lzzqoX8fijFg3wIDAQAB";
        return Security.verifyPurchase(b64_1.concat(b64_2).concat(b64_3).concat(b64_4).concat(base64EncodedPublicKey5), signedData, signature);
    }

    /**
     * Query all available products
     */
    public void querySkuDetailsAsync(){

        //create a runnable from the request to use inside the connection retry policy
        Runnable queryRequest = new Runnable(){
            @Override
            public void run() {
                //create the SkyDetailsParams object
                SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
                params.setSkusList(skuList).setType("inapp");
                mBillingClient.querySkuDetailsAsync(params.build(), new SkuDetailsResponseListener() {
                    @Override
                    public void onSkuDetailsResponse(int responseCode, List<SkuDetails> skuDetailsList) {
                        //listener.onSkuDetailsResponse(responseCode, skuDetailsList);
                        //respond to this event!!
                        if(responseCode == 0) {
                            Log.w("", "Product Details Queried!!!");
                            skuInventoryList = skuDetailsList;

                        }
                    }
                });
            }
        };
        executeServiceRequest(queryRequest);
    }

    public static SkuDetails getProduct(String inputSku) {
        if (skuInventoryList != null && skuInventoryList.size() > 0) {
            for (SkuDetails skuDetail : skuInventoryList){
                if (skuDetail.getSku().equals(inputSku)){
                    return skuDetail;
                }
            }
        }
        return null;
    }

    /**
     * Query purchases across various use cases and deliver the result in a formalized way through a listener
     * Checks for what has been purchased by the user
     */
    public void queryPurchases() {
        Runnable queryToExecute = new Runnable() {
            @Override
            public void run(){
                long time = System.currentTimeMillis();
                Purchase.PurchasesResult purchasesResult = mBillingClient.queryPurchases(BillingClient.SkuType.INAPP);
                Log.w("", "Querying purchases elapsed time: " + (System.currentTimeMillis() - time) + "ms");
                onQueryPurchasesFinished(purchasesResult);
            }
        };
        executeServiceRequest(queryToExecute);
    }


    public void initiatePurchaseFlow(final String skuId, final @BillingClient.SkuType String billingType){

        //if (mBillingClient.isFeatureSupported(BillingClient.FeatureType.))

        //check client is ready
        if(mBillingClient.isReady()) {
            if (validatePurchase(skuId)) {

                //TODO check this productID/orderID on a proper secure server.

                final Runnable purchaseFlowRequest = new Runnable() {
                    @Override
                    public void run() {
                        BillingFlowParams.Builder mParams = BillingFlowParams.newBuilder()
                                .setSku(skuId)
                                .setType(billingType);
                        //.setOldSkus(oldSkus);
                        mBillingClient.launchBillingFlow(mActivity, mParams.build());
                    }
                };
                this.mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        executeServiceRequest(purchaseFlowRequest);
                    }
                });
            }
        }
    }

    private boolean validatePurchase(final String skuId){
        //check that this skuId is available for purchasing
        //check if this skuid has been purfchased already
        for (Purchase purchase : mPurchases){
            if (purchase.getSku() == skuId){
                return false;
            }
        }
        return true;
    }


    private void onQueryPurchasesFinished(Purchase.PurchasesResult result){
        //Have we been disposed of in the meantime? If so, or bad result code, then quit
        if (mBillingClient == null || result.getResponseCode() != BillingClient.BillingResponse.OK){
            Log.w("", "Billing client was null or result code (" + result.getResponseCode() + ") was  bad - quitting");
            return;
        }

        Log.w("", "Query inventory was successful");

        //Update the UI and purchases inventory with new list of purchases
        mPurchases.clear();
        onPurchasesUpdated(BillingClient.BillingResponse.OK, result.getPurchasesList());
    }

    public interface BillingUpdatesListener {
        void onBillingClientSetupFinished();
        void onConsumeFinished(String token, @BillingClient.BillingResponse int result);
        void onPurchasesUpdates(List<Purchase> purchases);
    }
}






//    //create a sideways scrollable popup here.
//    private void addSkuRows(final List<String> inList, List<String> skusList, final @BillingClient.SkuType String billingType, final Runnable executeWhenFinished) {
//        //IAB._sharedIAB.querySkuDetailsAsync(billingType, skusList,
//
//    }

//considering each item is a one-time purchase, available items are those which have not been bought.
//    public void queryAvailableItems(){
//        skuList.clear();
//        skuList.add("level_pack_premium_1 ");
//        skuList.add("android.test.purchased");
//        Bundle querySkus = new Bundle();
//        querySkus.putStringArrayList("ITEM_ID_LIST", skuList);
//
//        Bundle skuDetails = IAB._sharedIAB.mBillingClient.
//    }


//was implenmenting using own way, but now am using library

    /*
@Deprecated
public class IAB {

    IabHelper mHelper;

    //public static final String LVL_PACK_1 = ;
    //public static final String

    public static IAB _sharedIAB;
    public static final String ITEM_SKU = "android.test.purchased";

    public static IAB build(Context c){
        return new IAB(c);
    }

    public IAB(Context c){
        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAh01OrAed7TVWFqxfUxkn2qL/2xMARWN27/IVrsdHPfiCzhh3ZSZw1mnEnPqMdu3AFL/jWnyEca1a+7+9v1nYcQpSsMdhLllu6YRaVerT23Bn1ZEmeNZbAIoGQuVcIBmuQ7eLKKNgVO3l8CcGL7NV0n2FxffgwSLVE6QUpS5B5SzXonDPMHXGhgbzUgM3379yG+N2wj7hvuTb3FHmVuh9ZtHUPnjV/dCeFvz/yQ407b/Pg9DMTQx73Js4w1StddPoUwhI3cItjeLlFMjRBDHRhfW9Y56rYxvanT4+faDRK8bGMUOmR1jWasXZSDXaOLbOkW0Tu473lzzqoX8fijFg3wIDAQAB";

        String strEncrypted = XOREncryption.encryptDecrypt(base64EncodedPublicKey);

        String strEncrypted = "\u0006\n\u0018\t\n;\n\n" +
                "\u0013,( #(8\fz&{\u0001\u0010\u001A\u0006\u0017\n" +
                "\u0002\u001E\b\u0002��s\u0002\u001C\u0002\n" +
                "\u0013\b$\u001A\b\u0002��\u000E\u00029{r\u001E9\u00024/t\u0005\u001D\u0014\u0017:;7\u001E;:%q \u0007lc3\u000E\u0010\u0019\u0014\u001Fyt~\u0002\u0015#8'\u0019\u001B%8\b99#p\u000B\u0018\u0019&z.?\u000E-\u0001:\u000E5>p\u0010\n" +
                "\u000F~!\u0014?2\u00062*r0`tzr5`%\u001A2\u001A3\u00028\u000E5#\u000F='6g\u0012\u00110\u001D&#\u001Fqb\t-`\u0011\u0006<.\n" +
                "\u000B)\u0002\u0018$\u0004��>\u00152\u0002\u0001<>\u0012f.\u000F\u001A��\n" +
                "6\u001D\fb'{\u0012(\u0004\u001D|\n" +
                "\u0007{-c\n" +
                ";7-$&\u0018\u000F\u0007\u000Eu��\u001E3\u0002~\u0001d\u00189\t$-\u0015\u001B\u000E\u0019\u0013\u00049,!+\u001E$\u001Cxpfr:\u0016`\n" +
                "c<)f#5$\u001F!b\n" +
                "\u000B<\u001D69r\u0019%\u0003\u0016\u0001%)\u0007d'\u0012.\u0005'1l(\u001Awa|!~\u001B$h\u000F\u000E\u0005\u001A;fx\t\"\u007F4`\u001875/\u0013>\u001E49\u0002p2\u00027;.\u000F=\n" +
                "\u000E;\u0019\u0001\u0015\u0003\u00119-\u0014h\u0012vg9\u001A)=\"?\u001Fwz-\"\u0015\u0019\bi)\u0004\u001C\u001E\f<\u0019r;\u001C\"\"\u0013\u0019\u0002\u000F\u001B0\u0004\u000F3\u0004(\u0006{\u0017$\u007Ftb'9+:,\ts%8!\u00056x4\u0018\u000F\u0002��\n" +
                "\u0001";

        Log.w("", strEncrypted);
        String strDecrypted = XOREncryption.encryptDecrypt(strEncrypted);
        Log.w("", strDecrypted);

        boolean b = base64EncodedPublicKey.equals(strDecrypted);
        mHelper = new IabHelper(c, strDecrypted);

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()){
                    //Oh no, there was a problem
                    Log.w("", "Problem setting up In-app Billing: " + result);
                }

                //Hooray, IAB is fully set up.
                _sharedIAB.queryPurchasedItems();
                _sharedIAB.queryPurchaseableItems();
            }
        });
    }

    public void closeIAP() throws IabHelper.IabAsyncInProgressException {
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }

    public void queryPurchasedItems() {
        IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
            @Override
            public void onQueryInventoryFinished(IabResult result, Inventory inv) {
                if (result.isFailure()) {
                    //handle error here
                } else {
                    //does the user have the premium upgrade?

                    //update UI accordingly

                }
            }
        };

        try {
            mHelper.queryInventoryAsync(mGotInventoryListener);
        } catch (IabHelper.IabAsyncInProgressException ex) {
            Log.w("ERROR: ", ex.getMessage());
        }
    }

    public void queryPurchaseableItems() {
        boolean bRetrieveProductDetails = true;
        final List skuList = new ArrayList();
        skuList.add("level_pack_premium_1 ");
        skuList.add(ITEM_SKU);

        IabHelper.QueryInventoryFinishedListener mQueryFinishedListener = new IabHelper.QueryInventoryFinishedListener() {
            @Override
            public void onQueryInventoryFinished(IabResult result, Inventory inv) {
                if (result.isFailure()){
                    //handle error
                    return;
                }
                else {
                    String levelPack1Price = inv.getSkuDetails((String) skuList.get(0)).getPrice();
                    String testPurchasePrice = inv.getSkuDetails((String) skuList.get(1)).getPrice();
                    Purchase testPurchase = inv.getPurchase((String)skuList.get(1));
                    //update the UI
                }
            }
        };
        try {
            mHelper.queryInventoryAsync(bRetrieveProductDetails, skuList, skuList, mQueryFinishedListener);
        }catch(IabHelper.IabAsyncInProgressException ex){
            Log.w("ERROR: ", ex.getMessage());
        }
    }
}
*/
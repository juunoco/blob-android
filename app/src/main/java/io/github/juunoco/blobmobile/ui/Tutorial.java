package io.github.juunoco.blobmobile.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Stack;

import io.github.juunoco.blobmobile.Globals;
import io.github.juunoco.gameui.CustomDialog;

/**
 * Created by Jun on 14/03/2018.
 *
 * A tutorial dialog that uses CustomDialog.java to display steps to the user guiding them on how to play
 * Each popup has no buttons, and progresses as a cut is made and notified from Level.java
 */

public class Tutorial extends Hint {

    //the level for which this popup is designated
    private int associatedLvl;
    private int currentStep;
    //private final Activity activity;
    //private final Context context;
    //private ArrayList<PopupDetail> popupDetails = new ArrayList<>();
    //private Stack<CustomDialog> dialogStack = new Stack<>();
    private boolean bFinished;
    private String text;
    private boolean bDrawing = false;

//    public Tutorial(Activity activity, Context context, int associatedLvl){
//        this.associatedLvl = associatedLvl; //not really in use at the moment, but architecture-wise, should be.
//        this.currentStep = 0;
//        this.activity = activity;
//        this.context = context;
//        this.bFinished = false;
//    }

    private Tutorial(int lvlNo, HintTouchLine hintTouchLine, String text){
        super(lvlNo, hintTouchLine, text);
        this.text = text;
        //this.rotation = rotationDegrees;
    }

    private Tutorial(int lvlNo, float stringX, float stringY, String text){
        super(lvlNo, stringX, stringY, text);
        this.text = text;
        this.textPaint.setTextSize(Globals.TUTORIAL_TEXT_SIZE_SMALL);
    }

    private Tutorial(int lvlNo, HintTouchLine hintTouchLine){
        super(lvlNo, hintTouchLine, "");
        this.text = "";
    }

    /**
     * creates a new hint touchline assuming the hint touchline is horizontal
     * @param lvlNo to create hinttouchline and string from
     * @return
     */
    public static Tutorial createTextWithLine(int lvlNo, HintTouchLine hintTouchLine, String text){
        return new Tutorial(lvlNo, hintTouchLine, text);
    }

    public static Tutorial createTextWithNoLine(int lvlNo, float stringX, float stringY, String text){
        return new Tutorial(lvlNo, stringX, stringY, text);
    }

    public static Tutorial createLineWithNoText(int lvlNo, HintTouchLine hintTouchLine){
        return new Tutorial(lvlNo, hintTouchLine);

    }

    @Override
    public void draw(String extraText, Canvas c){
        if(bDrawing){
            if(hintTouchLine != null) {
                hintTouchLine.draw(c);
                c.save();
                c.rotate(rotation,
                        (((hintTouchLine.getX1() + hintTouchLine.getX2()) / 2f) - Globals.CELL_WIDTH),
                        hintTouchLine.getY1() + Globals.CELL_WIDTH);
                c.drawText(
                        text,
                        (hintTouchLine.getX1() + hintTouchLine.getX2()) / 2f,
                        (hintTouchLine.getY1() + Globals.CELL_WIDTH / 1.5f),
                        textPaint
                );
                c.restore();

            }
            else if(hintTouchLine == null){
                //draw text in middle fo blob

                c.drawText(text, sliceX, sliceY, textPaint);

            }
        }
    }

    public void setDrawing(boolean b){
        this.bDrawing = b;
    }

    /**
     * popupdetails are inserted into local array in the order in which they should be displayed for this level
     * @param text to be displayed
     * @param x position of display
     * @param y position of display
     */
//    public void insertPopupDetail(String text, float x, float y){
//        this.popupDetails.add(new PopupDetail(text, x, y));
//    }

//    public void init(){
//        dialogStack.push(createDialog());
//    }

//    public void nextStep(){
//        if(this.currentStep < this.popupDetails.size() - 1) {
//            this.currentStep++;
//            //close current popup and open new one after a delay
//            dialogStack.pop();
//
//            activity.runOnUiThread(new Runnable(){
//                @Override
//                public void run() {
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            dialogStack.push(createDialog());
//                        }
//                    }, 500);
//                }
//            });
//
//        }
//        else {
//            //end the tutorial
//            this.bFinished = true;
//        }
//    }

    /**
     * creates the custom dialog itself
     */
//    private CustomDialog createDialog(){
//
////        final Button bOkay = new Button(new ContextThemeWrapper(this.activity, R.style.PopupButtonPositiveTheme));
////        bOkay.setText("Ok");
////        bOkay.setBackgroundColor(activity.getResources().getColor(R.color.gray));
//
//        Window tWindow = null;
//
//        final CustomDialog cd = new CustomDialog.Builder()
//                .setTitle(null)
//                .setMessage(getCurrentText())
//                //.setButtons(bOkay)
//                .setWindow(tWindow)
//                .build(activity, context);
//
//        tWindow = cd.getDialog().getWindow();
//        //tWindow.requestFeature(Window.FEATURE_NO_TITLE);
//        //tWindow.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        //int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
//        //tWindow.getDecorView().setSystemUiVisibility(uiOptions);
//        //tWindow.setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
//        WindowManager.LayoutParams wlp = tWindow.getAttributes();
//        wlp.gravity = Gravity.TOP; //sets coroidnates to the top
//        wlp.x = (int)getCurrentX();
//        wlp.y = (int)getCurrentY();
//        //wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND; //prevents dimming behind the dialog
//        tWindow.setAttributes(wlp);
//        tWindow.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);//remove shadow behind dialog
//
////        bOkay.setOnTouchListener(new View.OnTouchListener() {
////            @Override
////            public boolean onTouch(View v, MotionEvent event) {
////                if(event.getAction() == MotionEvent.ACTION_DOWN){
////                    bOkay.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDark));
////                }
////                else if(event.getAction() == MotionEvent.ACTION_UP){
////                    bOkay.setBackgroundColor(activity.getResources().getColor(R.color.gray));
////                    cd.getDialog().dismiss();
////                }
////                return false;
////            }
////        });
//        return cd;
//    }

    public boolean isFinished(){
        return this.bFinished;
    }

//    private String getCurrentText(){
//        return this.popupDetails.get(currentStep).text;
//    }
//    private float getCurrentX(){
//        return this.popupDetails.get(currentStep).x;
//    }
//    private float getCurrentY(){
//        return this.popupDetails.get(currentStep).y;
//    }

    /**
     * sets up tutorial based on current level number
     * 0, 0 are the coordinates for the middle of the screen.
     */
    //public static Tutorial initTutorialPopup(Activity a, Context c, int lvlNo){
//        switch(lvlNo){
//            case 1 : {
//                Tutorial tp = new Tutorial(a, c, 1);
////                tp.insertPopupDetail(
////                        "Slice the blob by sliding your finger across it",
////                        0,
////                        Globals.SCREEN_HEIGHT / 2);
//                tp.insertPopupDetail(
//                        "Slice the blob to match the width of the blank rectangle on the left",
//                        0,
//                        Globals.SCREEN_HEIGHT / 2.5f
//                );
//                tp.init();
//                return tp;
//            }
//            case 2 : {
//                Tutorial tp = new Tutorial(a, c, 2);
//                tp.insertPopupDetail(
//                        "Slice the blob to match the size of both of the blank rectangles",
//                        0,
//                        Globals.SCREEN_HEIGHT / 2.5f
//                );
//                tp.init();
//                return tp;
//            }
//            case 3 : {
//                Tutorial tp = new Tutorial(a, c, 3);
//                tp.insertPopupDetail(
//                        "Now there is a number, try and match the number as well",
//                        0,
//                        Globals.SCREEN_HEIGHT / 2.5f
//                );
//                tp.init();
//                return tp;
//            }
//            //eventually, when the level exists need one more tutorial for the empty levels
//            case 24 : {
//                Tutorial tp = new Tutorial(a, c, 24);
//                tp.insertPopupDetail(
//                        "this level has an empty, you need to clear the area in front of it of any blobs",
//                        0,
//                        Globals.SCREEN_HEIGHT / 2.5f
//                );
//                tp.init();
//                return tp;
//            }
//        }
//        return null;
//    }


    /**
     * class representing each step in the tutorial dialog
     * class holding a string of the text to display
     * and position x, y, for where to display it
     */
    private class PopupDetail {
        public String text;
        public float x, y;

        public PopupDetail(String text, float x, float y){
            this.text = text;
            this.x = x;
            this.y = y;
        }
    }

}

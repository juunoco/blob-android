package io.github.juunoco.blobmobile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.WindowManager.LayoutParams;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import java.util.ArrayList;
import java.util.Random;

import io.github.juunoco.blobmobile.junk_classes.OpenGlView;
import io.github.juunoco.blobmobile.ui.HintTouchLine;
import io.github.juunoco.blobmobile.ui.ParticleEffect;
import io.github.juunoco.blobmobile.ui.Tutorial;
import io.github.juunoco.blobmobile.ui.TutorialManager;

public class GameActivity extends AppCompatActivity {

    GameLoop game;
    TouchLine line;
    Level lvl;
    ActionBar actionBar;
    Activity activity;
    AdView adView;
    Random rand;
    Handler handler;
    private InterstitialAd mInterstitialAd;
    private RewardedVideoAd mRewardedViewAd;
    private boolean bStarted = true, bStopLoadingAd;  //cancels loading ad if the acitivty has been exited
    //final public long AD_DELAY_TIME = 3000;
    private Runnable closeDelayRunnable;
    private WindowManager wm;
    private ArrayList<Tutorial> tutorials = new ArrayList<>();
    private ShimmerView shimmerView;
    private OpenGlView mGLView;
    private boolean bIsPreloading;
    private TextView textView; //for displaying when ad fails to load
    //ad text is stored locally here
    //can also move it to Strings.xml...
    int[] alternateDisplayText = {
            R.string.default1,
            R.string.default2,
            R.string.default3,
            R.string.default4
    };
    //onClick = open LevelPack popup.
    //onClick = open hint popup

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Globals.bInGameSequence = false;
        Globals.iLvlToUnlock = 0;
        this.bIsPreloading = getIntent().getExtras().getBoolean("preloading");

        if(!bIsPreloading) {
            context = this;
            //mGLView = new OpenGlView(this);

            SoundManager.soundState = SoundManager.SOUNDSTATE.PLAYING;

            //setContentView(mGLView);
            SoundManager.transitionMusic("menuMusic", "playingMusic", true);
            game = new GameLoop(context, this);
            setContentView(game);

            line = game.getTouchLine();
            actionBar = game.getActionBar();
            lvl = game.getLvl();
            rand = new Random();
            handler = new Handler();

            activity = this;
            wm = activity.getWindowManager();
            bStopLoadingAd = false;
            //Log.w("","ACTIVITY RESOURCES ARE: " + activity.getResources());
            setupUI();
            setupClicks();

            //check if a tutorial should be loaded here
            setupTutorial();

            lvl.setParticles(new ParticleEffect(activity, game, lvl));
            lvl.setShatterParticlesActivityAndView(activity, game);
        }
    }

    @Override
    protected void onDestroy(){
        if(!bIsPreloading) dismissViews();
        super.onDestroy();

        if(!bIsPreloading) {
            if (SoundManager.bIsPlayingLeveltransitionMusic) {
                SoundManager.transitionMusic("transitionMusic", "menuMusic", false);
            } else {
                SoundManager.transitionMusic("playingMusic", "menuMusic", false);
            }
        }
    }

    public void setupTutorial(){
        runOnUiThread(new Runnable(){
            @Override
            public void run(){
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //tutorial = Tutorial.initTutorialPopup(GameActivity.this, GameActivity.this, lvl.number);
                        ArrayList<Tutorial> tutorials = TutorialManager.getTutorial(GameActivity.this, lvl.number);

                        if (tutorials != null) {
                            //listen for a slice in Level.java, this will progress the tutorial to the next stage
                            lvl.setTutorial(tutorials);
//                        setupUI();
//                        onWindowFocusChanged(true);
                        }
                    }
                }, 500);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(bIsPreloading){
            //Google AdMob banner
            MobileAds.initialize(this, "ca-app-pub-9859357986849249~74772115256");

            //Google AdMob Interstitial
            MobileAds.initialize(this, "ca-app-pub-9859357986849249/2134698586");


            startActivity(new Intent(this, LevelActivity.class));
            overridePendingTransition(android.R.anim.fade_out, android.R.anim.fade_in);
            finish();
        }
        if(!bIsPreloading) {
            game.resume();

            //mGLView.onResume();
            SoundManager.resumeBackgroundMusic();

            //TODO Temporarily commented out
            //if (!Globals.getNoAds()) {
                initBannerAd();
                initInterstitialAd();
                if(!actionBar.bHintPressed) {
                    showInterstitial();
                }
            //}
            checkProvideRewardedAd();
            setupUI();

            if(actionBar.bHintPressed){
                //cancel hintPressed boolean to retrigger interstitials
                actionBar.bHintPressed = false;
            }
            actionBar.dimScreen(false);


        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(!bIsPreloading) {
            game.pause();
            setupUI();
            //mGLView.onPause();
            if(!Globals.bInGameSequence) {
                SoundManager.stopBackgroundMusic();
            }
        }
    }

    @Override
    public void onStop(){
        super.onStop();
        if (!bIsPreloading) {


            setupUI();
            if (closeDelayRunnable != null) {
                handler.removeCallbacks(closeDelayRunnable);
            }
            //start of level activity will trigger it.
            if(!Globals.bInGameSequence) {
                SoundManager.stopBackgroundMusic();
            }
        }
    }


    public void initShimmerEffect(){
        //this adds shimmer to whole view, so not necessary currently
        //add a shimmer to the whole page
//        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View layout = inflater.inflate(R.layout.shimmer_overlay, null);//(ViewGroup) findViewById(R.id.shimmer_view_container)
//        ShimmerFrameLayout container = (ShimmerFrameLayout)layout.findViewById(R.id.shimmer_view_container);
//        wm.addView(layout, this.initShimmerLayoutParams());
//        container.startShimmerAnimation();

        //customized ShimmerView from programmatic class instead of dependency
        this.shimmerView = new ShimmerView(this);
        addContentView(shimmerView, initShimmerLayoutParams());
        //wm.addView(shimmerView, this.initShimmerLayoutParams());
        shimmerView.setY(shimmerView.getY() + Globals.AD_HEIGHT);
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Globals.bInGameSequence = true;
        SoundManager.playClickReturn();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    private void dismissViews(){
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        //textView dismissal works, but have not tested adView yet.
        if(adView != null && adView.isShown()) {
            //adView.destroy();
            //adView.setAdListener(null);
            wm.removeView(adView);
            //adView = null;
        }
        if(textView != null && textView.isShown()){
            wm.removeView(textView);
        }
//        if(shimmerView != null){
//           //(shimmerView);
//        }
        bStopLoadingAd = true;
    }


    //// PRIVATE STUFF ////

    private View blobArea;
    private Context context;

    //RELATIVE & LINEAR LAYOUT DO NOT WORK WELL WITH SURFACE VIEW when adding an additional view as an overlay, so using WIndowManager LayoutParams
    private WindowManager.LayoutParams initBannerLayoutParams(){
        WindowManager.LayoutParams windowParams = new WindowManager.LayoutParams();
        windowParams.gravity = Gravity.TOP;
        //windowParams.x = 0;
        windowParams.y = 0; //135;
        windowParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        windowParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        windowParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                | LayoutParams.FLAG_FULLSCREEN
                | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;

        windowParams.format = PixelFormat.TRANSLUCENT;
        windowParams.windowAnimations = 0;
        return windowParams;
    }

    /**
     * init shimmer layout parameters as a view overlaying the play grid area
     * @return window parameter with boundary settings
     */
    private WindowManager.LayoutParams initShimmerLayoutParams(){
        WindowManager.LayoutParams windowParams = new WindowManager.LayoutParams();
        windowParams.gravity = Gravity.TOP;
        //windowParams.y = (int)Globals.AD_HEIGHT; //doesn't work
        windowParams.height = (int)Globals.LVL_GRID_CELL * 12;
        windowParams.width = LayoutParams.MATCH_PARENT;
        windowParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                | WindowManager.LayoutParams.FLAG_FULLSCREEN
                | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;
        windowParams.format = PixelFormat.TRANSLUCENT; //vs opaque & transparent??
        //windowParams.format = PixelFormat.OPAQUE;
        return windowParams;
    }

    private void initBannerAd(){
        this.adView = new AdView(this);
        adView.setAdSize(AdSize.BANNER); //BANNER = 320 X 100
        //official test adUnitID: ca-app-pub-9859357986849249/2134698586
        adView.setAdUnitId("ca-app-pub-3940256099942544/6300978111"); //test adUnitId
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        this.adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded(){
                //when an ad has finished loading


                //LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT); //this si always in center
                //activity.addContentView(adView, params);
                //LinearLayout bannerLayout = (LinearLayout) adView;
                //RelativeLayout.LayoutParams bannerParams = new RelativeLayout.LayoutParams((int)MainPage.SCREEN_WIDTH, (int)MainPage.SCREEN_HEIGHT / 8);
                //bannerParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//                bannerParams.setMargins(0, 200, 0, 0);
//                if (Build.VERSION.SDK_INT >= 19) {
//                    // LinearLayout.LayoutParams requires API 19
//                    LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(
//                            LayoutParams.MATCH_PARENT,
//                            LayoutParams.WRAP_CONTENT
//                    );
//                    linearParams.setMargins(0, 160, 0, 0);
//                    activity.addContentView(adView, linearParams);
//                }
//
                if(!adView.isShown() && !bStopLoadingAd) {
                    setupUI();
                    wm.addView(adView, initBannerLayoutParams());
                }

                Log.w("ADVIEW IS SHOWN? ", "" + adView.isShown());

            }


            @Override
            public void onAdFailedToLoad(int errorCode){
                //when an ad request fails
                //due to no network?
                Log.w("GAMEACTIVITY 171: ", "ADVIEW FAILED TO LOAD");
                //using strings that show the passing of time, and are informative, but a gentle push in the direction of showing commitment to the app

                //create banner template and place randomized string of text into it.
                WindowManager wm = activity.getWindowManager();

                //LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                //TextView view = (View) inflater.inflate(R.layout.activity_main, null);
                textView = new TextView(activity);
                textView.setText(getDefaultText());
                //textView.setMinWidth(450);

                textView.setGravity(Gravity.CENTER);
                textView.setWidth((int)Globals.CELL_WIDTH * 8);
                textView.setMinHeight((int)(Globals.CELL_WIDTH * 1.5));
                textView.setTextSize(14);
                textView.setTextColor(Color.WHITE);
                textView.setBackgroundColor(Color.BLACK);
                try {
                    wm.addView(textView, initBannerLayoutParams());
                    //sometimes promoting purchase, sometimes asking if they need a hint, etc.
                    //add a click listener to detect presses and lead to hint screen, or levelpack screen.
                }catch(WindowManager.BadTokenException ex){
                    ex.getStackTrace();
                }
            }

            @Override
            public void onAdOpened(){
                //when an ad opens an overlay that covers the screen
            }

            @Override
            public void onAdClosed(){
                //when the user is about to return to the ad after tapping on an ad
            }
        });
    }

    private String getDefaultText(){
        return getResources().getString(alternateDisplayText[rand.nextInt(alternateDisplayText.length - 1)]);
    }

    private void initInterstitialAd(){
        this.mInterstitialAd = new InterstitialAd(this);
        //official adUnitId;: ca-app-pub-9859357986849249/1277321605
        this.mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712"); //using test adUnitId;
        this.mInterstitialAd.loadAd(new AdRequest.Builder().build());

        this.mInterstitialAd.setAdListener(new AdListener(){
            @Override
            public void onAdLoaded(){
                //make sure the loaded ad is only the starting one
                if (bStarted) {
                    showInterstitial();
                    Log.w("", "INTERSTITIAL AD LOADED!!!");
                    bStarted = false;
                }
            }

            @Override
            public void onAdFailedToLoad(int errorCode){
                Log.w("GAMEACTIVITY 171: ", "INTERSTITIAL FAILED TO LOAD");

                //WindowManager wm = activity.getWindowManager();

                //LinearLayout layout = (LinearLayout) activity.findViewById(R.id.interstitialLayout);
                //LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
                //activity.setContentView(R.layout.interstitial);
//                final View interstitialLayout = game.findViewById(R.id.interstitialLayout);
//                interstitialLayout.setBackgroundColor(Color.BLACK);
//                final TextView textView = (TextView) game.findViewById(R.id.interstitialText);
//                textView.setTextSize(24);
//                textView.setText(getDefaultText());
//                textView.setTextColor(Color.WHITE);
//                textView.setVisibility(View.VISIBLE);
//                interstitialLayout.setVisibility(View.VISIBLE);

//                activity.addContentView(findViewById(R.id.interstitialLayout), params);
//                activity.addContentView(textView, params);
//
                //closeDelayRunnable = new CloseDelayRunnable(textView, interstitialLayout);
                //closeDelayRunnable = new CloseDelayRunnable();
                //create a timer that will show a close image.
                //if this image is pressed, the interstitial will close.
                //handler.postDelayed(closeDelayRunnable, AD_DELAY_TIME);

                //will this show a blank ad?
                //mInterstitialAd.show();

                //can create a blank canvas here and display a simple black screen...it's easy, but is it necessary?
            }

            @Override
            public void onAdOpened(){
                //when the ad is displayed
            }

            @Override
            public void onAdLeftApplication(){
                //when the user has left the app

            }

            @Override
            public void onAdClosed(){
                //when ad closed, load the next interstitial
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });
    }

    public void showInterstitial(){
        int r = rand.nextInt(100);
        if(r > Globals.INTERSTITIAL_PERCENTAGE){
            if(this.mInterstitialAd.isLoaded()){
                this.mInterstitialAd.show();
            }
            else {
                Log.w("", "WAS UNABLE TO LOAD INTERSTITIAL AD");
            }
        }
    }

    /**
     * checks if a rewarded ad shoudl be provided to the user or not.
     * If the time gap is one day, then provide the rewarded ad.
     * @return true if ad shoudl be provided, false if not
     */
    void checkProvideRewardedAd(){
        //check if the last ad was provided more than a day ago
        if(Globals.getBillingLoadingComplete()) {
            if (Globals.getLastRewardedAdProvided() + (24 * 60 * 60 * 1000) < System.currentTimeMillis()) {
                //if yes, start loading rewarded ad
                //if no ads are purchased
                if (!Globals.getNoAds()) {
                    initRewardedAd();
                }
            }
        }
        //if billing has not been loaded, then wait and check again
        else {
            //check if internet is connected
//            new Handler().postDelayed(new Runnable(){
//                @Override
//                public void run(){
//                    checkProvideRewardedAd();
//                }
//            }, 3000);
        }
    }

    void initRewardedAd(){
        //check if rewarded ad is available to user'
        //official adUnit id is: ca-app-pub-9859357986849249/8378698769
        mRewardedViewAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedViewAd.loadAd("ca-app-pub-3940256099942544/5224354917", new AdRequest.Builder().build());
        mRewardedViewAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
            @Override
            public void onRewardedVideoAdLoaded() {
                //display rewarded ad button to user
                actionBar.setbRewardedAdAvailable(true);
            }

            @Override
            public void onRewardedVideoAdOpened() {}

            @Override
            public void onRewardedVideoStarted() {}

            @Override
            public void onRewardedVideoAdClosed() {}

            @Override
            public void onRewarded(RewardItem rewardItem) {
                Globals.setLastRewardedAdProvided(System.currentTimeMillis());
                actionBar.startHintMode(true);
                setupUI();
            }

            @Override
            public void onRewardedVideoAdLeftApplication() {}

            @Override
            public void onRewardedVideoAdFailedToLoad(int i) {
                //display error Message to user
//                new CustomDialog.Builder()
//                        .setMessage("Oops, there was an error displaying the ad")
//                        .build(GameActivity.this, GameActivity.this);
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus)
        {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_IMMERSIVE
            );
            setupUI();
        }
    }



    private void setupUI(){
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        //if(SplashActivity.hasNavBar()) Globals.SCREEN_HEIGHT = Globals.EXTENDED_HEIGHT;

        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        decorView.setSystemUiVisibility(uiOptions);

        game.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        getWindow().setFlags(LayoutParams.FLAG_FULLSCREEN, LayoutParams.FLAG_FULLSCREEN);

    }

    private void setupClicks(){
        final GestureDetector gestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener(){
            @Override
            public void onLongPress(MotionEvent e){
                lvl.longPressRemoveBlob(e);
            }
        });

        game.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //rest(50);
                if(!game.isTouchesDisabled()) {
                    line.onTouch(event);
                    //if(line.released) rest(100); //maybe this is causing particles to stop dropping normally;
                    if (!line.released) {
                        gestureDetector.onTouchEvent(event);
                    }
                    actionBar.onClick(event, activity);
                }
                return true;
            }
        });
    }

    void rest(int time){
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public GameLoop getGameLoop(){
        return this.game;
    }
    public RewardedVideoAd getRewardedViewAd() { return this.mRewardedViewAd; }

//    public class CloseDelayRunnable implements Runnable {
//
//       // TextView textView;
//        //View interstitialLayout;
//
//        public CloseDelayRunnable(){
//           // this.textView = textView;
//            //this.interstitialLayout = view;
//            activity.setContentView(game);
//        }
//
//
//        @Override
//        public void run(){
//            //create the close image.
//            textView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    textView.setVisibility(View.GONE);
//                    interstitialLayout.setVisibility(View.GONE);
//                }
//            });
//        }
//
//        public void setContents(){
//
//        }
//    }
}



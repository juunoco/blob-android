package io.github.juunoco.blobmobile;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;

import io.github.juunoco.blobmobile.junk_classes.Transition;
import io.github.juunoco.blobmobile.ui.ParticleEffect;
import io.github.juunoco.blobmobile.ui.Tutorial;

import static io.github.juunoco.blobmobile.Globals.CELL_WIDTH;

/**
 * Created by Jun on 1/08/2017.
 * Updated by Donna on 31/08/2017
 * game loop, updating, and drawing machine
 */

public class GameLoop extends SurfaceView implements Runnable {

    //boolean variable to track if the game is playing or not
    //volatile = always changing and never cached thread locally. Different according to each thread.
    volatile boolean playing = false;
    private boolean bTouchesDisabled;

    Thread thread = null;
    SurfaceHolder surfaceHolder;
    TouchLine line;
    Level lvl;
    ActionBar ac;
    Transition transition;
    Activity parentActivity;
    GameLoop gameLoop;
    private boolean bCheckingSecondTime = false;
    private boolean bResetPressed = false;
    private boolean bDrawReset = false;
    private boolean bFadeInAsWell = false;
    private int resetAlpha;
    private static final int RESET_ALPHA_CHG_AMT = 13;
    Paint resetPaint;
    Blob.DIRECTION resetFadeDirection;

    final int UPS = 20;
    final int FPS = 60;
    long initialTime = System.nanoTime();

    public void init(Activity a){
        if(a != null) {
            surfaceHolder = getHolder();
            line = TouchLine.makeLine();
            lvl = LevelManager.getInstance().getLevelFromNum(Globals.getCurrLvlNum());
            ac = ActionBar.make(a, this);
            gameLoop = this;
            //transition = Transition.create();

            Globals.iLvlToUnlock = 0;
//        if(Globals.bUnlockingAnimationTriggered){
//            Globals.bUnlockingAnimationTriggered = false; //restart listening for user touches.
//        }
            resetAlpha = 0;
            bTouchesDisabled = false;
        }
    }

    /**
     * Call this view's OnClickListener, if it is defined.  Performs all normal
     * actions associated with clicking: reporting accessibility event, playing
     * a sound, etc.
     *
     * @return True there was an assigned OnClickListener that was called, false
     * otherwise is returned.
     */
    @Override
    public boolean performClick() {
        return super.performClick();
    }

    @Override
    public void run(){
//        while(playing){
//            update();
//            rest(20);
//            draw();
//        }
        final double timeU = 1000000000 / UPS;
        final double timeF = 1000000000 / FPS;
        double deltaU = 0, deltaF = 0;
        int frames = 0, ticks = 0;
        long timer = System.currentTimeMillis();

        while (playing){

            long currentTime = System.nanoTime();
            deltaU += (currentTime - initialTime) / timeU;
            deltaF += (currentTime - initialTime) / timeF;
            initialTime = currentTime;

            if(deltaU >= 1){
                update();
                ticks++;
                deltaU--;
            }

            if(deltaF >= 1){
                draw();
                frames++;
                deltaF--;
            }

            if(System.currentTimeMillis() - timer > 1000){
                System.out.println(String.format("UPS: %s, FPS: %s", ticks, frames));
                frames = 0;
                ticks = 0;
                timer += 1000;
            }
        }
    }

    private void update(){
//        if(!Globals.isBrowsing()){
            //moved this from inside update() to init() to be able to initialize particles in level class
            //lvl = LevelManager.getInstance().getLevelFromNum(Globals.getCurrLvlNum());
//            lvl.setView(this);
//        }
        if(lvl != null) {
            lvl.update(bTouchesDisabled);

            if(bResetPressed) {
                if(resetFadeDirection == Blob.DIRECTION.UP) {
                    resetAlpha+=RESET_ALPHA_CHG_AMT;
                    if (resetAlpha > 255 - RESET_ALPHA_CHG_AMT) {
                        resetAlpha = 255;
                        if(bFadeInAsWell) {
                            resetFadeDirection = Blob.DIRECTION.DOWN;
                            this.ac.setHintsPressable(true);
                            this.ac.getHintButton().setFaded(false);
                            ((GameActivity)parentActivity).setupTutorial();
                        }
                        else{
                            //stop fade in, and leave the level
                            bResetPressed = false;
                            resetFadeDirection = null;
                            bTouchesDisabled = false;
                            _goToBrowsing();
                        }
                        this.lvl.reset();
                    }
                }
                else if(resetFadeDirection == Blob.DIRECTION.DOWN){
                    resetAlpha-= RESET_ALPHA_CHG_AMT;
                    if(resetAlpha < 0 + RESET_ALPHA_CHG_AMT){
                        resetAlpha = 0;
                        bResetPressed = false;
                        resetFadeDirection = null;
                        bFadeInAsWell = false;
                    }
                }
            }

            if (Globals.isSolved()) {
//                Globals.setInTransition(true);
                //Globals.setCurrLvlNum(lvl.getLevelNum()+1);
                //Experimental particles, but maybe unnecessary for first version.
                //ExplosionParticles.initExplosionParticles(this, parentActivity);

                if(bCheckingSecondTime == false){ //only do one extra check
                    nextCheck();
                    bCheckingSecondTime = true;
                }
//                rest(3000);
//                Globals.setInTransition(false);
            }
            ac.update(lvl);
            lvl.isValidCut(line);
            lvl.isValidRemove(line);
            lvl.updateTweens();

            SoundManager.update();
        }
    }

    public void startReset(boolean bFadeInAsWell){
        if(bFadeInAsWell) {
            this.bFadeInAsWell = true;
        }
        this.bResetPressed = true;
        this.resetFadeDirection = Blob.DIRECTION.UP;
    }

    private void _goToBrowsing(){
        //go back to browsing
        bDrawReset = true;
        lvl.setParticles(new ParticleEffect(parentActivity, gameLoop, lvl));
        parentActivity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        parentActivity.finish();

        //lvl.reset();//TODO commenting this might cause problems, but let's see.
        //line.reset();
    }

    private void nextCheck(){
        //check if all goals are still fulfilled after any dropping or extra swipes
        try {
            parentActivity.runOnUiThread(new Runnable(){
                @Override
                public void run(){
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            lvl.checkIfCompleted(false);
                            lvl.checkIfSolved();
                            //bCheckingSecondTime = false;
                            if(Globals.isSolved()) {
                                //line.reset();
                                //lvl.reset();
                                ac.fadeOut();
                                nextLevel();
                            }
                        }
                    }, 1000);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            nextCheck();
        }
    }

    private void setAllShattering(ArrayList<? extends Blob>... arrayLists){
        for(ArrayList<?> array : arrayLists) {
            for (Object temp : array) {
                ((Blob)temp).bShattering = true;
            }
        }
    }

    //next level process
    //shimmering => Shattering => alpha fade out
    private void nextLevel(){
        SoundManager.transitionMusic("playingMusic", "transitionMusic", false);
        SoundManager.bIsPlayingLeveltransitionMusic = true;
        bTouchesDisabled = true;

        //stop drawing tutorials
        if(this.getLvl().getTutorials() != null) {
            for(Tutorial t : this.getLvl().getTutorials()){
                t.setDrawing(false);
            }
        }
        ((GameActivity)parentActivity).initShimmerEffect();

        parentActivity.runOnUiThread(new Runnable(){
            @Override
            public void run(){
                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run(){
                        //start the shattering animation
                        setAllShattering(lvl.blobs, lvl.goals);
                        //next delay for transition and fade out and level clear-up blobs shattering into particles effect
                    }
                }, 300);
            }
        });

        parentActivity.runOnUiThread(new Runnable(){
            @Override
            public void run(){
                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run(){
                        ((GameActivity)parentActivity).initShimmerEffect();
                    }
                }, 3000);
            }
        });

        parentActivity.runOnUiThread(new Runnable(){
            @Override
            public void run(){
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(Globals.getUnlockedLevel() == Globals.getCurrLvlNum()) {
                            Globals.iLvlToUnlock = Globals.getCurrLvlNum() + 1;
                            //disables touches in LevelActivity, should only do this if the next level is locked
                            //Globals.bUnlockingAnimationTriggered = true;
                        }
                        startReset(false);
                    }
                }, 4500);
            }
        });
        //DO NOT set the unlocked level here, set it after the animation ahs finished, otherwise animation does not run.
        Globals.bInGameSequence = true;
    }

    private void draw(){
        //checking if surface is valid
        if(surfaceHolder.getSurface().isValid()){
            Canvas canvas = surfaceHolder.lockCanvas();
            setupCanvas(canvas);
            drawState(canvas);
            //draw fadeout overlay
            if(bResetPressed || bDrawReset){
                resetPaint = new Paint();
                resetPaint.setColor(ColourScheme.getBg(0));
                resetPaint.setAlpha(resetAlpha);
                canvas.drawRect(new RectF(0, 0, Globals.SCREEN_WIDTH, Globals.SCREEN_HEIGHT), resetPaint);
            }

            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    private void drawState(Canvas c){
            drawGrid(c);
            if(lvl != null) lvl.draw(c);
            line.draw(c);
            ac.draw(c);
//        }
        if(Globals.isInTransition()){
            //transition.drawTransition(c);
        }
    }

    private void resetTransition(){
        //transition.init();
    }

    private void setupCanvas(Canvas c){
        c.drawColor(ColourScheme.getBg(1));
//        c.drawColor(Color.WHITE);
//        c.drawBitmap(Globals.getBorder(),0,-5,null);
//        c.drawBitmap(Globals.getBorder(),0,(int) SCREEN_HEIGHT - CELL_WIDTH,null);
    }

    private void drawGrid(Canvas c){
        Paint p = new Paint();
        p.setColor(Color.LTGRAY);

        for(float x = 0.5f; x < 10; x += 0.5){
            for(float y = 0.5f; y < CELL_WIDTH - 1; y += 0.5){
                c.drawCircle(CELL_WIDTH * x, CELL_WIDTH * y, 3,p);
            }
        }
    }

    TouchLine getTouchLine(){ return line;}

    ActionBar getActionBar() { return ac;}

    public Level getLvl(){ return lvl; }

//    LevelManager getLvlMgr() { return lvlMgr;}

//    BrowsingManager getBrowsingMgr(){
//        return browsingMgr;
//    }

    public void pause(){
        playing = false;
        try{
            thread.join();
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        thread = null;
    }

    public void resume(){
        playing = true;
        thread = new Thread(this);
        thread.start();
    }

    void rest(int time){
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean isTouchesDisabled(){
        return this.bTouchesDisabled;
    }

    //using the 2-3 constructors of View in GameLoop so I can use XML layout

    public GameLoop(Context context, Activity a) {
        super(context);
        this.parentActivity = a;
        init(a);
    }

    public GameLoop(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(null);
    }

    public GameLoop(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(null);
    }
}
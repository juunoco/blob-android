package io.github.juunoco.blobmobile;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.SkuDetails;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

import io.github.juunoco.blobmobile.ui.HintManager;
import io.github.juunoco.gameui.CustomDialog;

/**
 * Created by Jun on 26/02/2018.
 * manages the purchases, through IAB client
 */

public class PurchasesManager {

    public static final String ITEM_SKU = "android.test.purchased";
    public static final String SKU_PREMIUM_PACK_1 = "level_pack_premium_1";

    public static final int PURCHASE_PAGE_1 = 5;
    public static final int PURCHASE_PAGE_2 = 6;

    //removed 'level_pack_premium_2' purchase
    public static final String SKU_HINT3 = "blob_hint_1"; //3 hints
    public static final String SKU_HINT5 = "blob_hint_5"; //10 hints
    public static final String SKU_HINT10 = "blob_hint_10"; //20 hints
    //1 - 18 = FREE
    //19 - 36 = PREMIUM_1;
    //37 - 54 = PREMIUM_2;

    public static final String SKU_AMATEUR = "blob_step_1";
    //public static final String SKU_LAZY = "blob_step_2";
    //public static final String SKU_CHEATER = "blob_step_half";

    //checks to see if a page is locked based on whether level pack products have been purchased or not
    //returns true if page is locked
    //returns false if page is not locked
    static boolean isPageLocked(int pageNo){

        switch(pageNo){
            case 5 :
            case 6 :
                //check if premium 1 is purchased
                //if(MainViewController._sharedMvc.getPurchases().) {
                if(Globals.getPurchasedPremium1()){
                    return false;
                }
                return true;

//            case 7 :
//            case 8 :
//                //check if premium 2 is purchased
//                if(MainViewController._sharedMvc.getPurchases().size() == 2) {
//                    return false;
//                }
//                return true;

            default : return false;
        }
    }

    public static ProductDetails getProductDetails(int pageNo){
        switch(pageNo){
            case 5 :case 6 :
                return new ProductDetails(
                        "Premium Pack 1",
                        IAB.skuInventoryList.size() > 0 ? IAB.skuInventoryList.get(0).getPrice() : "",// 1.39f,
                        "BLOB PREMIUM PACK",
                        //"Difficulty: Medium"
                        "Slice up 18 unique levels!",
                        "levels 28 - 45",
                        ""

                );
            case 7 : case 8 :
                return new ProductDetails(
                        "Premium Pack 2",
                        IAB.skuInventoryList.size() > 0 ? IAB.skuInventoryList.get(1).getPrice() : "",//1.79f,
                        "BLOB ULTIMATE PACK",
                        //"Difficulty: Headache"
                        "Slice through 18 challenging levels",
                        "Become the Blob Master!",
                        "levels 46 - 63"
                );
            default : return null;
        }
    }


    /**
     * checks if next hint is available
     * slice number starts at 1.
     * @param lvlNo to check for
     * @param sliceNo that has been requested
     * @return true if next hint is available, false if next hint is unavailable
     */
    public static boolean checkNextHintAvailable(int lvlNo, int sliceNo){
        int unlockedSlice = HintManager.getInstance().readHintSliceUnlockedData(lvlNo);
        if(unlockedSlice >= sliceNo){
            return true;
        }
        else {
//            if(Globals.getPurchasedHintsHalf()){
//                return true;
//            }
//            else if(Globals.getPurchasedHints2()){
//                if(sliceNo <= 2){
//                    return true;
//                }
//                else {
//                    return false;
//                }
//            }
            if(Globals.getPurchasedHints1()){
                if(sliceNo == 1){
                    return true;
                }
                else {
                    return false;
                }
            }
            return false;
        }
    }
//    public static boolean checkNextHintAvailable(int lvlNo){
//        //check if next hint has already been purchased by running through JSON file.
//        boolean bHintExist = checkHintUnlocked(lvlNo);
//        if(!bHintExist){
//            //check if hints have been purchased and are available for use
//            boolean bHintPurchased = checkHintPurchaseAvailable();
//            if(bHintPurchased){
//                //if hint now exists, consumeHint();
//                consumeHint();
//                return true;
//            }
//            //TODO purchase hint if it is not available
//            else if(!bHintPurchased){
//                //display hint purchase popup
//
//                //TODO if hint is not purchased, and popup is cancelled, return to gameplay
//                return false;
//            }
//        }
//        else if(bHintExist) {
//            return true;
//        }
//        return false;
//    }


    /**
     * displays a CustomDialog warning the user that hints are only available locally on device, and will not be transferred to a separate device.
     */
    public static void showHintLocalOnlyWarning(final Activity activity, final String skuDetail){

        final Button bYes = new Button(new ContextThemeWrapper(activity, R.style.PopupButtonPositiveTheme));
        bYes.setBackgroundColor(activity.getResources().getColor(R.color.gray));
        bYes.setText(activity.getResources().getString(R.string.ok));

        final Button bNo = new Button(new ContextThemeWrapper(activity, R.style.PopupButtonNegativeTheme));
        bNo.setBackgroundColor(activity.getResources().getColor(R.color.darkgray));
        bNo.setText(activity.getResources().getString(R.string.no));

        final CustomDialog cd = new CustomDialog.Builder()
                .setTitle(activity.getResources().getString(R.string.hint_local_title))
                .setMessage(activity.getResources().getString(R.string.hint_warning_msg))
                .setButtons(bYes, bNo)
                .build(activity, activity);

        bYes.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent e){
                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    bYes.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                }
                else if(e.getAction() == MotionEvent.ACTION_UP){
                    bYes.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                    Rect bYesRect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                    if (!CustomDialog.didMoveOut(v, bYesRect, e)) {
                        //purchase consumable Hints
                        IAB._sharedIAB.initiatePurchaseFlow(skuDetail, BillingClient.SkuType.INAPP);
                        cd.getDialog().dismiss();
                    }
                }
                return true;
            }
        });

        bNo.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent e){
                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    bNo.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                }
                else if(e.getAction() == MotionEvent.ACTION_UP){
                    bNo.setBackgroundColor(activity.getResources().getColor(R.color.darkgray));

                    Rect bNoRect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                    if (!CustomDialog.didMoveOut(v, bNoRect, e)) {
                        cd.getDialog().dismiss();
                    }
                }
                return true;
            }
        });

    }

    private static boolean checkHintUnlocked(int lvlNo){
        //return HintManager.getInstance().getCurrentHint(lvlNo).isUnlocked();
        return true;
    }

    private static boolean checkHintPurchaseAvailable(){
        return HintManager.getInstance().getAvailableHints() > 0;
    }





    /**
     * consumes the hint and saves it into persistent JSON storage
     */
    private static void consumeHint(){
        HintManager.getInstance().consumeAvailableHint();
    }

    public static class ProductDetails {
        String title;
        String price;
        String description;
        String difficulty, text2, text3;

        ProductDetails(String title, String price, String description, String difficulty, String text2, String text3){
            this.title = title;
            this.price = price;
            this.description = description;
            this.difficulty = difficulty;
            this.text2 = text2;
            this.text3 = text3;
        }
    }


}

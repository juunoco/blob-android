package io.github.juunoco.blobmobile;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;


import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.SkuDetails;

import io.github.juunoco.gameui.CustomDialog;
import io.github.juunoco.gameui.GridObject;
import io.github.juunoco.gameui.JuunoButton;

/**
 * Created by Jun on 26/02/2018.
 */

public class PageLocked { //extends GridObject {

    String title;
    String price; //converted from int
    String description;
    String difficulty, text2, text3;

    JuunoButton purchaseArea;
    Activity activity;

    PurchasePopup purchasePopup;
    private BrowsingManager browsingManager;
    private Context context;
    private Paint textPaint;
    private int pageNo1, pageNo2;
    private String removeAdsText;
    private float initialAdsText1, initialAdsText2;
    private float initialPremiumX1, initialPremiumX2, scaledPremiumX1, scaledPremiumX2, transformedPremiumX1, transformedPremiumX2, transformedX1, transformedX2;
    private final float YPOSITION = (Globals.SCREEN_HEIGHT / 2) - (BrowsingManager.GRID_CELL * 5.2f),
                        SCALEDYPOSITION = (Globals.SCREEN_HEIGHT / 2) - (BrowsingManager.GRID_CELL * 5.2f) - (BrowsingManager.GRID_CELL * 0.25f),
                        TEXTYPOSITION = (Globals.SCREEN_HEIGHT / 6 * 5);
    private final float PAGE_MARGIN = Globals.SCREEN_WIDTH;
    private Bitmap premiumPageText1, premiumPageText2, scaledPremiumPageText1, scaledPremiumPageText2;
    private boolean bIsTouched; //for scaling animation when button is pressed

    //playing around just for fun :)
    private RadialGradient radialGradient;

    //hwo to create Java builders().build()?
    public static PageLocked build(Activity a, Context c, BrowsingManager bm){
        return new PageLocked(a, c, bm);
    }

    private PageLocked(Activity a, Context context, BrowsingManager browsingManager){
        this.activity = a;
        this.context                = context;
        this.browsingManager        = browsingManager;
        this.pageNo1                = PurchasesManager.PURCHASE_PAGE_1;
        this.pageNo2                = PurchasesManager.PURCHASE_PAGE_2;
        this.bIsTouched             = false;
        this.removeAdsText          = this.activity.getResources().getString(R.string.remove_ads);
        this.textPaint              = new Paint();

        this.textPaint.setTextSize(Globals.TUTORIAL_TEXT_SIZE_SMALL);
        this.textPaint.setColor(Color.BLACK);
        this.textPaint.setTextAlign(Paint.Align.CENTER);
        initPageOffset();
        initImages();
        initPosition();
   }
    private final float textX = Globals.SCREEN_WIDTH / 2;
    private final float textY = Globals.SCREEN_HEIGHT / 2;
    private final RectF purchaseRect = new RectF(
            Globals.SCREEN_WIDTH / 6,
            Globals.SCREEN_HEIGHT / 4,
            Globals.SCREEN_WIDTH / 6 * 5,
            Globals.SCREEN_HEIGHT / 4 * 3
    );

    private void initPageOffset(){
        int pageDiff1               = this.pageNo1 - Globals.getPageNum();
        this.transformedX1          += pageDiff1 * PAGE_MARGIN;
        int pageDiff2               =this.pageNo2 - Globals.getPageNum();
        this.transformedX2          += pageDiff2 * PAGE_MARGIN;
    }

    private void initImages(){
        this.premiumPageText1 = this.premiumPageText2 = Bitmap.createScaledBitmap(
                SplashActivity.premiumPageText,
                (int)(Globals.SCREEN_WIDTH - (BrowsingManager.GRID_CELL * 1.6f)),
                (int)(((Globals.SCREEN_HEIGHT / 2) + (BrowsingManager.GRID_CELL * 5.2f)) - ((Globals.SCREEN_HEIGHT / 2) - (BrowsingManager.GRID_CELL * 5.2f))),
                false
        );

        this.scaledPremiumPageText1 = this.scaledPremiumPageText2 = Bitmap.createScaledBitmap(
                SplashActivity.premiumPageText,
                premiumPageText1.getWidth() + (int)((BrowsingManager.GRID_CELL * 0.5f)),
                premiumPageText2.getHeight() + (int)((BrowsingManager.GRID_CELL * 0.5f)),
                false
                );
    }

    private void initPosition(){
        this.initialPremiumX1       = this.browsingManager.getWorldTransformation() + transformedX1 + (BrowsingManager.GRID_CELL * 0.8f);
        this.initialPremiumX2       = this.browsingManager.getWorldTransformation() + transformedX2 + (BrowsingManager.GRID_CELL * 0.8f);
        this.scaledPremiumX1        = initialPremiumX1 - (BrowsingManager.GRID_CELL * 0.17f);
        this.scaledPremiumX2        = initialPremiumX2 - (BrowsingManager.GRID_CELL * 0.17f);
        this.initialAdsText1        = this.browsingManager.getWorldTransformation() + transformedX1 + Globals.SCREEN_WIDTH / 2;
        this.initialAdsText2        = this.browsingManager.getWorldTransformation() + transformedX2 + Globals.SCREEN_WIDTH / 2;
    }

    public void update(PurchasesManager.ProductDetails productDetails){
        if(productDetails != null) {
            this.title = productDetails.title;
            //this.price = Float.toString(productDetails.price);
            this.description = productDetails.description;
            this.difficulty = productDetails.difficulty;
            this.text2 = productDetails.text2;
            this.text3 = productDetails.text3;
            //this.purchaseArea = JuunoButton.create(MainPage.SCREEN_WIDTH / 2, MainPage.SCREEN_HEIGHT / 2, MainPage.SCREEN_WIDTH / 2, MainPage.SCREEN_HEIGHT / 3, this.title);
            //this.purchaseArea.setEnabled(true);
            this.price = IAB.getProduct(PurchasesManager.ITEM_SKU) == null ? "" : IAB.getProduct(PurchasesManager.ITEM_SKU).getPrice();
        }
    }

    public void update(){

        this.transformedX1 = this.browsingManager.getWorldTransformation();
        this.transformedX2 = this.browsingManager.getWorldTransformation();

        if(bIsTouched) {
            this.transformedPremiumX1 = scaledPremiumX1 + transformedX1;
            this.transformedPremiumX2 = scaledPremiumX2 + transformedX2;
        }
        else{
            this.transformedPremiumX1 = initialPremiumX1 + transformedX1;
            this.transformedPremiumX2 = initialPremiumX2 + transformedX2;
        }
    }

    //scale bitmap when touched
    private synchronized void createScaledImg(boolean bIsTouched){
        Bitmap tBitmap = Globals.getPageNum() == pageNo1 ? premiumPageText1 : premiumPageText2;

        if(bIsTouched) {
            tBitmap = Bitmap.createScaledBitmap(
                    tBitmap,
                    (int)(tBitmap.getWidth() + (BrowsingManager.GRID_CELL * 0.5f)),
                    (int)(tBitmap.getHeight() + (BrowsingManager.GRID_CELL * 0.5f)),
                    false
            );
            initialPremiumX1 -= BrowsingManager.GRID_CELL * 0.25f;
            initialPremiumX2 -= BrowsingManager.GRID_CELL * 0.25f;
        }
        else {
            tBitmap = Bitmap.createScaledBitmap(
                    tBitmap,
                    (int)(Globals.SCREEN_WIDTH - (BrowsingManager.GRID_CELL * 1.6f)),
                    (int)(((Globals.SCREEN_HEIGHT / 2) + (BrowsingManager.GRID_CELL * 5.2f)) - ((Globals.SCREEN_HEIGHT / 2) - (BrowsingManager.GRID_CELL * 5.2f))),
                    false
            );
            initialPremiumX1 += BrowsingManager.GRID_CELL * 0.25f;
            initialPremiumX2 += BrowsingManager.GRID_CELL * 0.25f;
        }

        if(Globals.getPageNum() == pageNo1){
            this.premiumPageText1 = tBitmap;
        }
        else {
            this.premiumPageText2 = tBitmap;
        }
        update(); //ensure an update of coordinates before these changes are drawn to screen
    }

    //shoudl extend and use JuunoButton...argh but whatever
    public boolean isClicked(MotionEvent e){
        if(e.getX() > purchaseRect.left && e.getX() < purchaseRect.right &&
                e.getY() > purchaseRect.top && e.getY() < purchaseRect.bottom) {

            if(e.getAction() == MotionEvent.ACTION_DOWN){
                bIsTouched = true;
                update();
                //scaleImg(bIsTouched);
                return true;
            }
            if (e.getAction() == MotionEvent.ACTION_UP) {
                bIsTouched = false;
                //scaleImg(bIsTouched);
                update();
                SoundManager.playClickPlay();
                initConfirmationPopup();
                return true;
            }
        }

        //if action up was touched outside of area and scale is up, scale back down
        else {
        //if(e.getAction() == MotionEvent.ACTION_UP || e.getAction() == MotionEvent.ACTION_CANCEL){
            if(bIsTouched){
                bIsTouched = false;
                //scaleImg(bIsTouched);
            }
        }

        return false;
    }

    public void draw(Canvas c){
//        //grey out eveyrthing
//        Paint p = new Paint();
//        p.setColor(Color.argb(225, 32, 32, 32));
//        p.setStyle(Paint.Style.FILL);
//        c.drawRect(0.0f, 0.0f, Globals.SCREEN_WIDTH, Globals.SCREEN_HEIGHT, p);
//
//        //draw white/yellow purchase area
//        Paint p2 = new Paint();
//        p2.setColor(Color.LTGRAY);
//        p2.setAlpha(225);
//        p2.setStyle(Paint.Style.FILL);
//        this.radialGradient = new RadialGradient(
//                Globals.SCREEN_WIDTH / 2,
//                Globals.SCREEN_HEIGHT / 2,
//                Globals.SCREEN_WIDTH / 3,
//                //new int[] { Color.WHITE, Color.argb(190, 129, 166, 226) },
//                new int[] { Color.WHITE, Color.argb(190, 242, 239, 215) },
//                null,
//                Shader.TileMode.CLAMP
//        );
//        p2.setShader(this.radialGradient);
//        c.drawRoundRect(purchaseRect,24, 24, p2);
//
//        if(this.title != null){
//            //text in purchase area
//            Paint p3 = new Paint();
//
//            //purchase area pretty blue rounded rect
//            p3.setColor(Color.argb(145, 48, 113, 219));
//            c.drawRoundRect(new RectF(
//                    purchaseRect.left + Globals.SCREEN_WIDTH / 35,
//                    purchaseRect.bottom - Globals.SCREEN_HEIGHT / 9,
//                    purchaseRect.right - Globals.SCREEN_WIDTH / 35,
//                    purchaseRect.bottom - Globals.SCREEN_HEIGHT / 60
//            ), 32, 32, p3);
//
//            //purchase area blakc underscore
//            c.drawRoundRect(new RectF(
//                    purchaseRect.left + Globals.SCREEN_WIDTH / 25,
//                    purchaseRect.bottom - Globals.SCREEN_HEIGHT / 20,
//                    purchaseRect.right - Globals.SCREEN_WIDTH / 25,
//                    purchaseRect.bottom - Globals.SCREEN_HEIGHT / 30
//            ), 64, 64, p3);
//
//            p3.setTypeface(Globals.getOrbitronFont());
//            p3.setColor(Color.BLACK);
//            p3.setTextSize(55);
//            p3.setTextAlign(Paint.Align.CENTER);
//            c.drawText(this.description, textX, purchaseRect.top + p3.getTextSize() * 3, p3);
//            p3.setTextSize(45);
//            c.drawText(this.difficulty, textX, purchaseRect.top + p3.getTextSize() * 6.5f, p3);
//            c.drawText(this.text2, textX, purchaseRect.top + p3.getTextSize() * 7.8f, p3);
//            c.drawText(this.text3, textX, purchaseRect.top + p3.getTextSize() * 9.5f, p3);
//
//            p3.setTextSize(100);
//            c.drawText( this.price, textX, textY + p3.getTextSize() * 4, p3);
//        }

        if(this.bIsTouched){
            c.drawBitmap(this.scaledPremiumPageText1, transformedPremiumX1, SCALEDYPOSITION, null);
            c.drawBitmap(this.scaledPremiumPageText2, transformedPremiumX2, SCALEDYPOSITION, null);
        }
        else {
            c.drawBitmap(this.premiumPageText1, transformedPremiumX1, YPOSITION, null);
            c.drawBitmap(this.premiumPageText2, transformedPremiumX2, YPOSITION, null);
        }

        if(!Globals.getNoAds()) {
            c.drawText(removeAdsText, transformedX1 + initialAdsText1, TEXTYPOSITION, textPaint);
            c.drawText(removeAdsText, transformedX2 + initialAdsText2, TEXTYPOSITION, textPaint);
        }

        //draw purchase area
//        if(this.purchaseArea != null) {
//            purchaseArea.draw(c);
//        }

        if (this.purchasePopup != null){
            purchasePopup.draw(c);
        }
    }

    public void initConfirmationPopup() {
        SkuDetails skuDetail = IAB.getProduct(PurchasesManager.SKU_PREMIUM_PACK_1);
        if (skuDetail != null) {

            final Button bYes = new Button(new ContextThemeWrapper(activity, R.style.PopupButtonPositiveTheme));
            final Button bNo = new Button(new ContextThemeWrapper(activity, R.style.PopupButtonNegativeTheme));

            bYes.setBackgroundColor(activity.getResources().getColor(R.color.gray));
            bNo.setBackgroundColor(activity.getResources().getColor(R.color.darkgray));

            final CustomDialog cd = new CustomDialog.Builder()
            //new AlertDialog.Builder(this.activity, R.style.ConfirmationPurchaseTheme)
                    .setTitle("Confirm Purchase")
                    //list dynamic price
                    .setMessage("Are you sure you would like to purchase Blob Premium Pack for " + skuDetail.getPrice())
                    //.setIcon(android.R.drawable.ic_dialog_alert)
                    .setButtons(bYes, bNo)
                    .build(activity, activity);
                    //.setNegativeButton(android.R.string.no, null)
                    //.show();

            bYes.setText("Yes");
            bNo.setText("No thanks");

            bYes.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent e) {
                    if (e.getAction() == MotionEvent.ACTION_DOWN) {
                        bYes.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                    } else if (e.getAction() == MotionEvent.ACTION_UP) {
                        bYes.setBackgroundColor(activity.getResources().getColor(R.color.gray));
                        //bYes.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.black_overlay)));
                        Rect bYesRect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                        if (!CustomDialog.didMoveOut(v, bYesRect, e)) {
                            IAB._sharedIAB.initiatePurchaseFlow(PurchasesManager.SKU_PREMIUM_PACK_1, BillingClient.SkuType.INAPP);
                            cd.getDialog().dismiss();
                        }
                    }
                    return true;
                }
            });

            bNo.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent e) {
                    if (e.getAction() == MotionEvent.ACTION_DOWN) {
                        bNo.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                    } else if (e.getAction() == MotionEvent.ACTION_UP) {
                        bNo.setBackgroundColor(activity.getResources().getColor(R.color.darkgray));
                        Rect bNoRect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                        if (!CustomDialog.didMoveOut(v, bNoRect, e)) {
                            cd.getDialog().dismiss();
                        }
                    }
                    return true;
                }
            });
        }
        else if(skuDetail == null){
            //request connection to internet
            LayoutInflater inflater =
                    (LayoutInflater) activity.getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View popupView = inflater.inflate(R.layout.internet_request_popup, null);

            PopupWindow pWindow = new PopupWindow(
                    popupView,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    true
            );
            pWindow.setAnimationStyle(R.style.CustomDialog_Window);
            pWindow.setOutsideTouchable(true);
            pWindow.setFocusable(true);
            pWindow.setBackgroundDrawable(new Drawable() {
                @Override
                public void draw(@NonNull Canvas canvas) {

                }

                @Override
                public void setAlpha(int alpha) {

                }

                @Override
                public void setColorFilter(@Nullable ColorFilter colorFilter) {

                }

                @Override
                public int getOpacity() {
                    return PixelFormat.TRANSPARENT;
                }
            });

            TextView tv = (TextView) popupView.findViewById(R.id.requestInternet);
            tv.setText(activity.getResources().getString(R.string.request_internet_premium_pack));

            View parent = ((LevelActivity)activity).getLevelView();
            if(parent != null) {
                pWindow.showAtLocation(parent, Gravity.CENTER, 0, 0);
            }
        }
    }
    public void initPurchasePopup(int pageNo){
        this.purchasePopup = new PurchasePopup(activity, pageNo, false);
    }
}

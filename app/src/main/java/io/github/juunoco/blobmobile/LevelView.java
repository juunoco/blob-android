package io.github.juunoco.blobmobile;

import android.animation.ArgbEvaluator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Random;

import io.github.juunoco.blobmobile.ui.BrowsingPageCircles;
import io.github.juunoco.blobmobile.ui.ParticleEffect;

/**
 * Created by Donna on 6/10/2017.
 */

public class LevelView extends SurfaceView implements Runnable  {

    boolean browsing;
    Thread thread = null;
    SurfaceHolder surfaceHolder;
//    Context context;
    BrowsingManager browsingMgr;
    private float backgroundCount;
    private boolean bBackgroundChanging;
    private int startColor, endColor;
    private ArgbEvaluator argbEvaluator;
    private Random rand;
    private ParticleEffect browsingParticles;

    long initialTime = System.nanoTime();
    final int UPS = 20;
    final int FPS = 60;

    void init(BrowsingManager bm){
        surfaceHolder = getHolder();
        //surfaceHolder.setFixedSize((int)MainPage.SCREEN_WIDTH * 2, (int)MainPage.SCREEN_HEIGHT * 2);
        //LevelManager.getInstance().readHintData(getContext(),"lvls.json");
        //Globals.setCurrLvlNum(1);
        //browsingMgr = BrowsingManager.getInstance();
        //browsingMgr.setInstancePlayLocked(a);
        this.browsingMgr = bm;
        this.backgroundCount = 0.0f;
        this.bBackgroundChanging = false;
        argbEvaluator = new ArgbEvaluator();
        startColor = endColor = ColourScheme.getColour(Globals.getPageNum());
        rand = new Random();

    }

    @Override
    public void run() {
        final double timeU = 1000000000 / UPS;
        final double timeF = 1000000000 / FPS;
        double deltaU = 0, deltaF = 0;
        int frames = 0, ticks = 0;
        long timer = System.currentTimeMillis();

        while (browsing){

            long currentTime = System.nanoTime();
            deltaU += (currentTime - initialTime) / timeU;
            deltaF += (currentTime - initialTime) / timeF;
            initialTime = currentTime;

            if(deltaU >= 1){
                update();
                ticks++;
                deltaU--;
            }

            if(deltaF >= 1){
                draw();
                frames++;
                deltaF--;
            }

            if(System.currentTimeMillis() - timer > 1000){
                System.out.println(String.format("UPS: %s, FPS: %s", ticks, frames));
                frames = 0;
                ticks = 0;
                timer += 1000;
            }
        }
    }

    private void update(){
        browsingMgr.update();
        SoundManager.update();
        if(this.bBackgroundChanging){
            backgroundCount += 0.04;
            if(backgroundCount >= 1){
                bBackgroundChanging = false;
                browsingMgr.resetFromPageToPage();
            }
        }

        if(browsingMgr.getFromPage() != 0 || browsingMgr.getToPage() != 0){
            updateBackgroundColorTo(browsingMgr.getFromPage(), browsingMgr.getToPage());
        }

        spawnWindParticles();
    }

    /**
     * spawn wind particles randomly
     */
    private void spawnWindParticles(){
        if(browsingParticles != null){
            //if(rand.nextInt() % 100 == 0) {
                //get list of goals for this level
               // browsingParticles.showParticles(createRandomLine(), getRandomCut(), false);
            //}
        }
    }

    /**
     * gets top or bottom edge of the canvas
     * creates a random line between two points on the canvas
     * @return dual oordinates for an edge to create a line to spawn along
     */
    ParticleEffect.LineCoords createRandomLine(){
        //if height and width are small, no need to calculate
        int x1 = rand.nextInt((int)Globals.SCREEN_WIDTH);
        int y1 = rand.nextInt((int)Globals.SCREEN_HEIGHT);
        int x2 = rand.nextInt((int)Globals.SCREEN_WIDTH);
        int y2 = rand.nextInt((int)Globals.SCREEN_HEIGHT);

        return ParticleEffect.LineCoords.createLineCoords(
            x1, y1, x2, y2
        );
    }

    /**
     * @return a random cut to spawn wind particles for the background
     */
    private int getRandomCut(){
        return rand.nextInt(11);
    }

    private void draw(){
        //checking if surface is valid
        if(surfaceHolder.getSurface().isValid()){
            Canvas canvas = surfaceHolder.lockCanvas();
            setupBaseGraphics(canvas);
            browsingMgr.draw(canvas);
            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    private void setupBaseGraphics(Canvas c){
        //c.drawColor(ColourScheme.getBg(Globals.getPageNum()));
        Object bgColor = argbEvaluator.evaluate(
                backgroundCount,
                startColor,
                endColor);
        c.drawColor((Integer)bgColor);
        //c.drawColor(Color.WHITE);
    }

    /**
     * starts updating background color
     * @param fromPage color to change from
     * @param toPage color to change to
     */
    public void updateBackgroundColorTo(int fromPage, int toPage){
        startColor = ColourScheme.getColour(fromPage);
        endColor = ColourScheme.getColour(toPage);
        bBackgroundChanging = true;
        if(backgroundCount >= 1) backgroundCount = 0;

    }

    public void updateBackgroundColor(Blob.DIRECTION swipeDirection){
//        ValueAnimator colorAnimator = ObjectAnimator.ofInt(this, "backgroundColor", Globals.getPageNum() - 1, Globals.getPageNum());
//        colorAnimator.setDuration(3000);
//        colorAnimator.setEvaluator(new ArgbEvaluator());
//        colorAnimator.start();

        if(swipeDirection == Blob.DIRECTION.LEFT){
//            if(Globals.getPageNum() ==  LevelManager.getInstance().getLevelData().size() / 9){
//                //boundary UI response if on the side
//                startColor = ColourScheme.getErrorColor();
//                endColor = ColourScheme.getBg(Globals.getPageNum());
//            }
            //else {
                //startColor = ColourScheme.getBg(Globals.getPageNum() - 1);
                //endColor = ColourScheme.getBg(Globals.getPageNum());
            //now using  the same colour scheme
            startColor = ColourScheme.getColour(Globals.getPageNum() - 1);
            endColor = ColourScheme.getColour(Globals.getPageNum());
            //}
        }
        else if(swipeDirection == Blob.DIRECTION.RIGHT){
//            if(Globals.getPageNum() ==1){
//                startColor = ColourScheme.getErrorColor();
//                endColor = ColourScheme.getBg(Globals.getPageNum());
//            }
            //else {
//                startColor = ColourScheme.getBg(Globals.getPageNum() + 1);
//                endColor = ColourScheme.getBg(Globals.getPageNum());
            startColor = ColourScheme.getColour(Globals.getPageNum() + 1);
            endColor = ColourScheme.getColour(Globals.getPageNum());
            //}
        }
        bBackgroundChanging = true;
        backgroundCount = 0;
    }

    final BrowsingManager getBrowsingMgr(){
        return browsingMgr;
    }

    /**
     * Call this view's OnClickListener, if it is defined.  Performs all normal
     * actions associated with clicking: reporting accessibility event, playing
     * a sound, etc.
     *
     * @return True there was an assigned OnClickListener that was called, false
     * otherwise is returned.
     */
    @Override
    public boolean performClick() {
        return super.performClick();
    }

    public void pause(){
        browsing = false;
        try{
            thread.join();
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        thread = null;
    }

    public void resume(){
        browsing = true;
        browsingMgr.startGlowOfSelectedPreview();
        thread = new Thread(this);
        thread.start();
    }

    public LevelView(BrowsingManager bm, Context context, Activity a) {
        super(context);
        init(bm);
        browsingParticles = new ParticleEffect(a, this, null);
    }

    public LevelView(BrowsingManager bm, Context context, AttributeSet attrs) {
        super(context, attrs);
        init(bm);
    }

    public LevelView(BrowsingManager bm, Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(bm);
    }
}

package io.github.juunoco.blobmobile;

import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.Build;
import android.renderscript.Sampler;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.LinearInterpolator;

/**
 * Created by Jun on 13/03/2018.
 * Following tutorials from =>
 * https://medium.com/@madmuc/android-shimmer-effect-on-custom-view-4ce18b7e7a92
 * https://gist.github.com/f3401pal/25c46a138447e778d0f05c8f3fe5fad4
 *
 * Creates a layer with transparent area behind it.
 *
 */

public class ShimmerView extends View implements ValueAnimator.AnimatorUpdateListener {

    private float lineHeight, hSpacing, vSpacing, imageSize, cornerRadius;

    private static final int H_SPACING_DP = 16;
    private static final int V_SPACING_DP = 16;
    private static final int IMAGE_SIZE_DP = 40;
    private static final int LIST_ITEM_LINES = 3;
    private static final int CORNER_RADIUS_DP = 2;
    private static final int ITEM_PATTERN_BG_COLOR = Color.WHITE;
    private static float transparentBitmapW, transparentBitmapH;

    private static final int EDGE_ALPHA = 0, CENTER_ALPHA = 170; //12 : 50
    private static final int SHADER_COLOR_R = 255, SHADER_COLOR_G = 255, SHADER_COLOR_B = 255; //170,170,170
    private static final int CENTER_COLOR = Color.argb(CENTER_ALPHA, SHADER_COLOR_R, SHADER_COLOR_G, SHADER_COLOR_B);
    private static final int EDGE_COLOR = Color.argb(EDGE_ALPHA, SHADER_COLOR_R, SHADER_COLOR_G, SHADER_COLOR_B);

    private static final int ANIMATION_DURATION = 1500;

    private Paint paint, shaderPaint;
    private int[] shaderColors;
    private Bitmap listItemPattern;
    private ValueAnimator animator;
    private float shaderWidth, shaderEdgeWidth, shaderAlpha;
    private static final float SHADER_WIDTH_CHG_AMT = .05f, SHADER_EDGE_WIDTH_CHG_AMT = 0.5f;
    private static final float SHADER_CENTER_WIDTH_LIMIT = 0.3f, SHADER_EDGE_WIDTH_LIMIT = 0.5f;

    public ShimmerView(Context context){
        super(context);
        init(context);
    }

    private void init(Context context){
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        hSpacing = dpToPixels(dm, H_SPACING_DP);
        vSpacing = dpToPixels(dm, V_SPACING_DP);
        imageSize = dpToPixels(dm, IMAGE_SIZE_DP);
        cornerRadius = dpToPixels(dm, CORNER_RADIUS_DP);
        transparentBitmapW = Globals.SCREEN_WIDTH;
        transparentBitmapH = Globals.SCREEN_HEIGHT;

        paint = new Paint();
        shaderPaint = new Paint();
        shaderPaint.setAntiAlias(true);
        shaderColors = new int[]{EDGE_COLOR, CENTER_COLOR, EDGE_COLOR};

        animator = ValueAnimator.ofFloat(-1f, 2f);
        animator.setDuration(ANIMATION_DURATION);
        animator.setInterpolator(new LinearInterpolator());
        //animator.setRepeatCount(1); //ValueAnimator.INFINITE
        animator.addUpdateListener(this);

        shaderWidth = .1f;
        shaderEdgeWidth = .25f;
    }


    private void initBitmaps(int w, int h){
       // listItemPattern = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        //Canvas c = new Canvas(listItemPattern);

        //create the transparent bitmap that you see the shimmer on
        //Bitmap item = createBitmap(w);
        //int top = 0;
        //do {
        //    c.drawBitmap(item, 0, top, paint);
            //top = top + item.getHeight();
        //} while(top < c.getHeight());

        //fill the rectangles with the background color
        //blocks out what is seen
        //c.drawColor(ITEM_PATTERN_BG_COLOR, PorterDuff.Mode.SRC_IN);
    }

    /**
     * The transparent bitmap that vovers the whole screen
     * @param w width of bitmap
     */
    private Bitmap createBitmap(int w){
        int h = calculatePatternHeight(LIST_ITEM_LINES);

        Bitmap.Config conf = Bitmap.Config.ALPHA_8;
        Bitmap bm = Bitmap.createBitmap(w, h, conf); //(int)(Globals.LVL_GRID_CELL * 10

        Canvas c = new Canvas(bm);
        c.drawColor(Color.argb(255, 0, 0, 0));

        Paint bmPaint = new Paint();
        bmPaint.setAntiAlias(true);
        bmPaint.setColor(Color.argb(0, 0, 0, 0));
        bmPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));

        RectF rectF = new RectF(vSpacing, hSpacing, vSpacing + imageSize, hSpacing + imageSize);
        c.drawRoundRect(rectF, cornerRadius, cornerRadius, bmPaint);

        return bm;
    }

    @Override
    @TargetApi(19)
    public void onAnimationUpdate(ValueAnimator valueAnimator){
        if(Build.VERSION.SDK_INT > 19) {
            if (isAttachedToWindow()) {
                float f = (float) valueAnimator.getAnimatedValue();
                updateShader(getWidth(), f);
                invalidate();
            } else {
                animator.cancel();
            }
        }
    }

    @Override
    protected void onVisibilityChanged(@NonNull View changedView, int visibility){
        super.onVisibilityChanged(changedView, visibility);
        switch(visibility){
            case VISIBLE:
                animator.start();
                break;
            case INVISIBLE :
            case GONE :
                animator.cancel();
                break;
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh){
        super.onSizeChanged(w, h, oldw, oldh);
        if(!animator.isStarted()){
            animator.start();
        }
        updateShader(w, -1f);
        if(h > 0 && w > 0){
            //initBitmaps(w, h);
            initBitmaps((int)transparentBitmapW, (int)transparentBitmapH);
        }
        else{
            listItemPattern = null;
            animator.cancel();
        }
    }

    private void updateShader(float w, float f){
        float left = w * f;
        //modifyShader();
        LinearGradient shader = new LinearGradient(left, left, left + w, left + w, shaderColors, new float[]{0f, .05f, .25f}, Shader.TileMode.CLAMP);
        //new float[]{0f, .5f, 1f} //the width

        shaderPaint.setShader(shader);
    }

//    void modifyShader(){
//        shaderWidth += SHADER_WIDTH_CHG_AMT;
//        shaderEdgeWidth += SHADER_EDGE_WIDTH_CHG_AMT;
//        if(shaderWidth >= SHADER_CENTER_WIDTH_LIMIT){
//            shaderWidth = 0;
//        }
//        if(shaderEdgeWidth >= SHADER_EDGE_WIDTH_LIMIT){
//            shaderEdgeWidth = 0;
//        }
//    }

    @Override
    protected void onDraw(Canvas canvas){
        canvas.drawColor(EDGE_COLOR);
        canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), shaderPaint);
        if(listItemPattern != null){
            canvas.drawBitmap(listItemPattern, 0, 0, paint);
        }
    }


    private int calculatePatternHeight(int lines){
        return (int) ((lines * lineHeight) + (hSpacing * (lines + 1)));
    }

    //converts dp value to pixels on screen
    private float dpToPixels(DisplayMetrics metrics, int dp){
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
    }


}

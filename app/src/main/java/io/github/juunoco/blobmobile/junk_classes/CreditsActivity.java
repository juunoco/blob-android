//package io.github.juunoco.blobmobile.junk_classes;
//
//import android.os.Bundle;
//import android.support.v7.app.*;
//import android.view.View;
//
//import io.github.juunoco.blobmobile.R;
//import io.github.juunoco.blobmobile.SoundManager;
//
///**
// * Created by Jun on 8/03/2018.
// */
//
//public class CreditsActivity extends AppCompatActivity {
//
//    //private
//
//    @Override
//    public void onCreate(Bundle savedInstanceState){
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_credits);
//        hideNavigationBar();
//        initButtons();
//    }
//
//    @Override
//    public void onPause(){
//        super.onPause();
//        SoundManager.stopBackgroundMusic();
//    }
//
//    @Override
//    public void onResume(){
//        super.onResume();
//        SoundManager.resumeBackgroundMusic();
//    }
//
//
//    void hideNavigationBar(){
//        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
//        if (actionBar != null) {
//            actionBar.hide();
//        }
//
//        View decorView = getWindow().getDecorView();
//        int uiOptions = View.SYSTEM_UI_FLAG_LOW_PROFILE
//                | View.SYSTEM_UI_FLAG_FULLSCREEN
//                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
//                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
//        decorView.setSystemUiVisibility(uiOptions);
//    }
//
//    void initButtons(){
//
//    }
//
//    @Override
//    public void onBackPressed(){
//        super.onBackPressed();
//        SoundManager.playClickReturn();
//        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//    }
//}

package io.github.juunoco.blobmobile;

import android.app.Activity;

import com.android.billingclient.api.Purchase;

import java.util.List;

/**
 * Created by Jun on 24/02/2018.
 */

public class MainViewController {

    private SplashActivity mActivity;
    private UpdateListener updateListener;
    public static MainViewController _sharedMvc;
    private List<Purchase> mPurchases;

    public static MainViewController buildMVC(SplashActivity activity){
        return new MainViewController(activity);
    }

    public MainViewController(SplashActivity activity){
        this.mActivity = activity;
        this.updateListener = new UpdateListener();
    }

    public final UpdateListener getUpdateListener(){
        return this.updateListener;
    }
    public final List<Purchase> getPurchases(){
        return mPurchases;
    }

    private class UpdateListener implements IAB.BillingUpdatesListener {
        @Override
        public void onBillingClientSetupFinished() {
            mActivity.onBillingManagerSetupFinished();
        }

        @Override
        public void onConsumeFinished(String token, int result) {

        }

        @Override
        public void onPurchasesUpdates(List<Purchase> purchases) {
            //purchases were found
            mPurchases = purchases;
        }

    }

}

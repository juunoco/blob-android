package io.github.juunoco.blobmobile.ui;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.view.View;
import android.view.ViewGroup;

import com.github.jinatonic.confetti.ConfettiManager;
import com.github.jinatonic.confetti.ConfettiSource;
import com.github.jinatonic.confetti.ConfettoGenerator;
import com.github.jinatonic.confetti.confetto.BitmapConfetto;
import com.github.jinatonic.confetti.confetto.Confetto;

import java.util.Random;

import io.github.juunoco.blobmobile.ColourScheme;
import io.github.juunoco.blobmobile.Globals;
import io.github.juunoco.blobmobile.Level;
import io.github.juunoco.blobmobile.R;

/**
 * Created by Jun on 18/02/2018.
 */
public class ParticleEffect {
    public final float PARTICLESIZE = 15;
    public final float PARTICLEVARIANCE = 12;
    private final float WINDPARTICLESIZE = 3;
    private final float WINDPARTICLEVARIANCE = 2;
    private final int WINDVELOCITYMAX = 375; //175
    public final float RADIUS = 6;
    public static final int drawableResId = R.drawable.particle_base;

    final String TAG = this.getClass().getSimpleName().toString();
    final int noParticles = 20;

    final int timeToLive = 5000;
    final View view;
    final Activity activity;
    final Level level;
    final Random rand = new Random();
    final Rect bound = new Rect(0, 0, (int)Globals.SCREEN_WIDTH, (int)Globals.SCREEN_HEIGHT);
    private float windVelocityX, windVelocityY;

    int cut;

    //final List<Bitmap> allPossibleConfetti = constructBitmapsForConfetti();
    ConfettoGenerator particleGenerator;
    ConfettiSource confettiSource;

    //load bitmap particle textures
    Bitmap particle0 = customParticle(ColourScheme.getColour(0));
    Bitmap particle1 = customParticle(ColourScheme.getColour(1));
    Bitmap particle2 = customParticle(ColourScheme.getColour(2));
    Bitmap particle3 = customParticle(ColourScheme.getColour(3));
    Bitmap particle4 = customParticle(ColourScheme.getColour(4));
    Bitmap particle5 = customParticle(ColourScheme.getColour(5));
    Bitmap particle6 = customParticle(ColourScheme.getColour(6));
    Bitmap particle7 = customParticle(ColourScheme.getColour(7));
    Bitmap particle8 = customParticle(ColourScheme.getColour(8));
    Bitmap particle9 = customParticle(ColourScheme.getColour(9));
    Bitmap particle10 = customParticle(ColourScheme.getColour(10));
    Bitmap particleBG = customParticle(ColourScheme.getColour(11));

    public ParticleEffect(Activity a, View v, Level l){
        activity = a;
        view = v;
        level = l;
    }

    private Bitmap drawableToBitmap(Drawable drawable){

        if(drawable instanceof BitmapDrawable){
            return ((BitmapDrawable)drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    private float randomSize(){
        float max = PARTICLESIZE + PARTICLEVARIANCE;
        float min = PARTICLESIZE - PARTICLEVARIANCE;
        return rand.nextInt((int)(max - min) + 1) + min;
    }

    private Bitmap getParticle(int cut){
//        switch(cut){
//            case 0 : return particle0;
//            case 1 : return particle1;
//            case 2 : return particle2;
//            case 3 : return particle3;
//            case 4 : return particle4;
//            case 5 : return particle5;
//            case 6 : return particle6;
//            case 7 : return particle7;
//            case 8 : return particle8;
//            case 9 : return particle9;
//            case 10: return particle10;
//            default: return particleBG;
//        }
        return customParticle(ColourScheme.getColour(cut));
    }

    private Bitmap customParticle(int backgroundColor){
        //RectF insetRect = new RectF(randomSize(), randomSize(), randomSize(), randomSize());
        RoundRectShape shape = new RoundRectShape(new float[] { RADIUS, RADIUS, RADIUS, RADIUS, RADIUS, RADIUS, RADIUS, RADIUS }, null, null);
        ShapeDrawable shapeDrawable = new ShapeDrawable(shape);
        shapeDrawable.setIntrinsicWidth((int)randomSize());
        shapeDrawable.setIntrinsicHeight((int)randomSize());
        shapeDrawable.getPaint().setColor(backgroundColor);
        shapeDrawable.getPaint().setStyle(Paint.Style.FILL);
        shapeDrawable.getPaint().setAntiAlias(true);
        shapeDrawable.getPaint().setFlags(Paint.ANTI_ALIAS_FLAG);
        return drawableToBitmap(shapeDrawable);
//        shape.setShape(GradientDrawable.RECTANGLE);
//        shape.setCornerRadii(new float[] { 8, 8, 8, 8, 0, 0, 0, 0 });
//        shape.setColor(backgroundColor);
//        shape.setStroke(3, backgroundColor);

//        Bitmap bitmap = Bitmap.createBitmap(shape.getIntrinsicWidth(), shape.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
//        Canvas canvas = new Canvas(bitmap);
//        canvas.drawBitmap(bitmapmap, 0, 0, new Paint());
        //v.setBackground(shape);
//        return bitmap;
    }

    private Bitmap getWindParticle(int backgroundColor){
        RoundRectShape shape = new RoundRectShape(new float[] { RADIUS, RADIUS, RADIUS, RADIUS, RADIUS, RADIUS, RADIUS, RADIUS }, null, null);
        ShapeDrawable shapeDrawable = new ShapeDrawable(shape);
        shapeDrawable.setIntrinsicWidth((int)randomWindSize());
        shapeDrawable.setIntrinsicHeight((int)randomWindSize());
        shapeDrawable.getPaint().setColor(ColourScheme.getColour(backgroundColor));
        shapeDrawable.getPaint().setStyle(Paint.Style.FILL);
        shapeDrawable.getPaint().setAntiAlias(true);
        shapeDrawable.getPaint().setFlags(Paint.ANTI_ALIAS_FLAG);
        return drawableToBitmap(shapeDrawable);
    }

    private float randomWindSize(){

        float max = WINDPARTICLESIZE + WINDPARTICLEVARIANCE;
        float min = WINDPARTICLESIZE - WINDPARTICLEVARIANCE;
        return rand.nextInt((int) (max - min) + 1) + min;
    }



    /**
     * used to emit geenral particles with each slice, as well as wind particles.
     * @param lineCoords for location of the particles
     * @param cut for the shade of the particles
     * @param bIsWind determines if this emission is a wind emission or not
     */
    public void showParticles(LineCoords lineCoords, int cut, final boolean bIsWind){
        final int tCut = cut;

        //each wind instance has the same velocity
        if(bIsWind){
            this.windVelocityX = rand.nextInt(WINDVELOCITYMAX) - (WINDVELOCITYMAX / 2);
            this.windVelocityY = rand.nextInt(WINDVELOCITYMAX) - (WINDVELOCITYMAX / 2);
        }

        this.particleGenerator = new ConfettoGenerator() {
            @Override
            public Confetto generateConfetto(Random random) {
                //final Bitmap bitmap = allPossibleConfetti.get(random.nextInt(noParticles));
                //final Drawable d = activity.getResources().getDrawable(drawableResId);

                //final Bitmap bitmap = BitmapFactory.decodeResource(activity.getResources(), drawableToBitmap(d)); //doesn't work with xml defined shapes
                //final Bitmap bitmap = drawableToBitmap(d);
                //Log.w(TAG, "showParticles: ACTIVITY.GETRESOURCES(): " + activity.getResources() );
                //Log.w(TAG, "showParticles: BITMAP: " + bitmap);
                if(bIsWind){
                    return new BitmapConfetto(getWindParticle(tCut));
                }
                else {
                    return new BitmapConfetto(getParticle(tCut));
                }
            }
            //activity.getResources()
        };

        this.confettiSource = new ConfettiSource(
                (int) lineCoords.x1,
                (int) lineCoords.y1,
                (int) lineCoords.x2,
                (int) lineCoords.y2
        );


        this.activity.runOnUiThread(new Runnable() {
            @Override
            public void run(){
                if(bIsWind) {
                        new ConfettiManager(activity.getBaseContext(), particleGenerator, confettiSource, (ViewGroup) view.getParent())
                            .setEmissionDuration(400) //100 milliseconds for single explosion
                            .setEmissionRate(100)//sizeOfBlob)
                            .setInitialRotation(-90, 360) //from 180
                            //.setAccelerationY(100.8f, 50.0f)
                            .setVelocityX(windVelocityX, 15)
                            .setVelocityY(windVelocityY, 15)
                            .setRotationalVelocity(20, 240) //from 180
                            .setTouchEnabled(true)
                            .setBound(bound)
                            .animate();
                }
                else{
                    //not wind
                    new ConfettiManager(activity.getBaseContext(), particleGenerator, confettiSource, (ViewGroup) view.getParent())
                            .setEmissionDuration(300) //300 milliseconds
                            .setEmissionRate(100)
                            .setInitialRotation(-90, 180)
                            .setAccelerationY(800.8f, 350.0f)
                            .setVelocityX(0, 260)
                            .setVelocityY(0)
                            .setRotationalVelocity(20, 180)
                            .animate();
                }
            }
        });
    }

    public void showUnlockingParticles(LineCoords lineCoords, int cut){
        final int tCut = cut;
        this.particleGenerator = new ConfettoGenerator() {
            @Override
            public Confetto generateConfetto(Random random) {
                return new BitmapConfetto(getParticle(tCut));
            }
            //activity.getResources()
        };

        this.confettiSource = new ConfettiSource(
                (int)(lineCoords.x1),
                (int)(lineCoords.y1),
                (int)(lineCoords.x2),
                (int)(lineCoords.y2)
        );

        this.activity.runOnUiThread(new Runnable() {
            @Override
            public void run(){
                new ConfettiManager(activity.getBaseContext(), particleGenerator, confettiSource, (ViewGroup) view.getParent())
                        .setEmissionDuration(400) //300 milliseconds
                        .setEmissionRate(120)
                        .setInitialRotation(-90, 180)
                        .setAccelerationY(800.8f, 350.0f)
                        .setVelocityX(0, 260)
                        .setVelocityY(0)
                        .setRotationalVelocity(20, 180)
                        .animate();
            }
        });
    }

    /**
     * package anonymous inner class to hold line coordinates without creating a rectangle.
     * x1, y1 = point of line start
     * x2, y2= point of line end
     */
    public static class LineCoords {

        public float x1, y1, x2, y2;

        private LineCoords(float x1, float y1, float x2, float y2, boolean bTranslateToOriginalPosition){
            if(bTranslateToOriginalPosition) {
                this.x1 = x1 * Globals.CELL_WIDTH;
                this.y1 = y1 * Globals.CELL_WIDTH + Globals.AD_HEIGHT;
                this.x2 = x2 * Globals.CELL_WIDTH;
                this.y2 = y2 * Globals.CELL_WIDTH + Globals.AD_HEIGHT;
            }
            else {
                this.x1 = x1;
                this.y1 = y1;
                this.x2 = x2;
                this.y2 = y2;
            }
        }

        public static LineCoords createLineCoords(float x1, float y1, float x2, float y2){
            return new LineCoords(x1, y1, x2, y2, true);
        }

        public static LineCoords createLineCoordsFromOriginalPosition(float x1, float y1, float x2, float y2){
            return new LineCoords(x1, y1, x2, y2, false);
        }
    }

    //Using Leonid's particle system library
//    public void showParticles (float x, float y, float w, float h, int cut){
//        this.cut = cut;
//        this.activity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//            //stuff that updates ui
//                //leonids library
//                new ParticleSystem(activity, noParticles, drawableResId, timeToLive)
//                        .setSpeedRange(0.2f, 0.5f)
//                        //.setAcceleration(0.001f, 90)
//                        .setAccelerationModuleAndAndAngleRange(0.0005f, 0.001f, 45, 135)
//                        .setScaleRange(0.2f, 0.8f)
//                        .oneShot(view, noParticles);
//            }
//        });
//    }
}

## Portions

Here's a few of the features:
- Super challenging
- Requires concentration
- Incrementally harder levels
- Awesome music
- Meditative slicing

Check out the game on Play Store

![https://play.google.com/store/apps/details?id=io.github.juunoco.blobmobile&hl=en] Portions

Or, our website 
![https:juunoco.github.io] Juunoco